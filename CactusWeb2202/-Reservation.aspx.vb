﻿Imports System.Xml.Linq
Imports System.Linq
Imports System.Net.Mail
Imports System.IO

Partial Class Reservation
Inherits System.Web.UI.Page

    Protected Sub btnReserve_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReserve.Click
        If Page.IsValid Then

            ' If Not String.IsNullOrEmpty(Session("Extracode")) Then
            Dim pickupdate, dropoffdatedate As String
            Dim pickuptime, dropofftime As String
            Dim pr As New RequestMaker
            Dim cls As New RequestMaker
            Dim comments
            Dim classcode As String
            Dim ExtraCode As String

            Dim pickuplocation As String
            Dim dropofflocation As String

            ExtraCode = Session("ExtraCode")

            Dim disc = Request.QueryString("disc")
            Dim prcode
            Dim companyID = Request.QueryString("companyID")
            Dim htRentalParam As Hashtable = Nothing
            htRentalParam = TryCast(Session("_htRentalParam"), Hashtable)
            Dim val
            If htRentalParam Is Nothing Then
                classcode = Request.QueryString("classCode")

                comments = ""
                pickuptime = Trim(Request.QueryString("putime"))
                dropofftime = Trim(Request.QueryString("dotime"))

                pickuplocation = Trim(Request.QueryString("ploc"))
                dropofflocation = Trim(Request.QueryString("rloc"))

                pickupdate = Trim(Request.QueryString("pudt"))
                dropoffdatedate = Trim(Request.QueryString("dodt"))
                val = pickupdate.IndexOf("/")
                Trace.Write("val" & val)
                If val = -1 Then
                    pickupdate = Trim(Request.QueryString("pudt"))
                    dropoffdatedate = Trim(Request.QueryString("dodt"))
                Else
                    pickupdate = cls.processDateString(Request.QueryString("pudt")) + " " + pickuptime
                    dropoffdatedate = cls.processDateString(Request.QueryString("dodt")) + " " + dropofftime
                End If


                SaveReservation(pickuplocation, dropoffdatedate, pickupdate, dropoffdatedate, Request.QueryString("rateID"), "", classcode, ExtraCode, Request.QueryString("disc"), "")



            Else

                Dim name, lastname, phone, email
				'address, city, state, zip, 
                Dim cardname, cardtype, cardnumber, cardcode, cardexpiration, TotalCost
                Dim picklocation As String
                Dim droplocation As String
                Dim pudt As String
                Dim dodt As String
                Dim conf As String

                Dim classDescription As String

                pudt = htRentalParam("PickupDate")
                classcode = htRentalParam("ClassCode")
                classDescription = htRentalParam("ClassDescription")
                dodt = htRentalParam("DropoffDate")
                picklocation = htRentalParam("PickupLocation")
                droplocation = htRentalParam("DropoffLocation")
                TotalCost = htRentalParam("TotalCharge")

                name = htRentalParam("FirstName")
                lastname = htRentalParam("LastName")
                'address = htRentalParam("Address")
                'city = htRentalParam("City")
                'state = htRentalParam("State")
                'zip = htRentalParam("Zip")
                email = htRentalParam("Email")
                conf = htRentalParam("Conf")

                '  htRentalParam.Add("CardName", cardname)
                'cardtype = htRentalParam("CardType")
                'cardexpiration = htRentalParam("CardExpiration")
                'cardnumber = htRentalParam("CardNumber")
                'cardcode = htRentalParam("CardCode")


                Trace.Write("INSIDE")
                SaveReservation(picklocation, droplocation, pudt, dodt, Request.QueryString("rateID"), conf, classcode, ExtraCode, disc, "Modified Online")

                ' SendEmail(conf, cartype, "Modify Reservation")

            End If

                ' Response.Redirect("confirmation.aspx?conf=" & conf & "")
                '  Trace.Write("CONf", conf)
                ' SendEmail(conf, cartype)
                ' Else
                'Response.Redirect("sessionexpired.aspx")
                ' End If

            End If
    End Sub

    
    Public Sub SendEmail(ByVal conf As String, ByVal cartype As String)

        Dim body As String

        Dim fromaddress As String = System.Configuration.ConfigurationManager.AppSettings("fromaddress")
        Dim toaddress As String = System.Configuration.ConfigurationManager.AppSettings("fromaddress")
        Dim msg As String
        msg = ""


        Dim pickupdate, dropoffdatedate As String
        Dim pickuptime, dropofftime As String
        Dim pr As New RequestMaker
        Dim cls As New RequestMaker
        Dim pickuploc
        Dim dropoffloc
        Dim age = tbAge.Text
        Dim promo = Request.QueryString("disc")
        '  Dim del = drpdelivery.SelectedItem.Value
        Dim val
        ' age = tb

        pickuptime = Trim(Request.QueryString("putime"))
        dropofftime = Trim(Request.QueryString("dotime"))

        'dropoffdatedate = pr.processDateString(Trim(Request.QueryString("dodt"))) & " " & Me.Request.QueryString("dotime")
        pickupdate = Trim(Request.QueryString("pudt"))
        dropoffdatedate = Trim(Request.QueryString("dodt"))
        Val = pickupdate.IndexOf("/")
        'pickupdate = pr.processDateString(Trim(Request.QueryString("pudt"))) & " " & Me.Request.QueryString("putime")
        If val = -1 Then
            pickupdate = pickupdate.Insert(4, "/").Insert(2, "/")
            dropoffdatedate = dropoffdatedate.Insert(4, "/").Insert(2, "/")

        Else
            Trace.Write("I'm inside")
            pickupdate = Trim(Request.QueryString("pudt"))
            dropoffdatedate = Trim(Request.QueryString("dodt"))
            Trace.Write("Insert slash")

        End If



        pickupdate = Request.QueryString("pudt") + " " + pickuptime
        dropoffdatedate = Request.QueryString("dodt") + " " + dropofftime

        pickuploc = cls.GetLocation(Request.QueryString("ploc"))
        dropoffloc = cls.GetLocation(Request.QueryString("rloc"))

        Dim html As New StringBuilder


        html.AppendLine("<table width=600 cellspacing=0 cellpadding=4 border=0>")
        html.AppendLine("<tr>")
        html.AppendLine("<td colspan=3>**************************************************************************************************************</td>")
        html.AppendLine("</tr>")
        html.AppendLine("<tr>")
        html.AppendLine("<td colspan=3>THIS IS AN AUTOMATED RESERVATION CONFIRMATION EMAIL</td>")
        html.AppendLine("</tr>")



        'html.AppendLine("<tr>")
        'html.AppendLine("<td colspan=3><b class=red>RENTAL LOCATION CONTACT INFROMATION IS LISTED AT THE END OF THIS EMAIL</b></td>")
        'html.AppendLine("</tr>")

        html.AppendLine("<tr>")
        html.AppendLine("<td colspan=3>**************************************************************************************************************</td>")
        html.AppendLine("</tr>")

        html.AppendLine("<tr>")
        'html.AppendLine("<td colspan=3>Thank You for choosing Ochs Rent a Car for your car rental needs</td>")
        html.AppendLine("<td colspan=3>Thank You for choosing  Savannah Auto Rental</td>")
        html.AppendLine("</tr>")


        html.AppendLine("<tr>")
        html.AppendLine("<td>Confirmation No:</td>")
        html.AppendLine("<td>" & conf & "</td>")
        html.AppendLine("</tr>")

        html.AppendLine("<tr>")
        html.AppendLine("<td colspan=3 align=left><hr align=left width=93%></td>")
        ' html.AppendLine("<td>" & conf & "</td>")
        html.AppendLine("</tr>")


        html.AppendLine("<tr>")
        html.AppendLine("<td>Pickup:</td>")
        html.AppendLine("<td>" & pickupdate & "</td>")
        html.AppendLine("</tr>")


        html.AppendLine("<tr>")
        html.AppendLine("<td>Return:</td>")
        html.AppendLine("<td>" & dropoffdatedate & "</td>")
        html.AppendLine("</tr>")


        html.AppendLine("<tr>")
        html.AppendLine("<td>Vehicle Class:</td>")
        html.AppendLine("<td>" & cartype & "</td>")
        html.AppendLine("</tr>")

        html.AppendLine("<tr>")
        html.AppendLine("<td colspan=3 align=left><hr align=left width=93%></td>")
        ' html.AppendLine("<td>" & conf & "</td>")
        html.AppendLine("</tr>")

        html.AppendLine("<tr>")
        html.AppendLine("<td>Email:</td>")
        html.AppendLine("<td>" & tbEmail.Text & "</td>")
        html.AppendLine("</tr>")

        html.AppendLine("<tr>")
        html.AppendLine("<td>Phone:</td>")
        html.AppendLine("<td>" & tbCell.Text & "</td>")
        html.AppendLine("</tr>")


        html.AppendLine("<tr>")
        html.AppendLine("<td>Name:</td>")
        html.AppendLine("<td>" & tbFirstName.Text & "</td>")
        html.AppendLine("</tr>")


        html.AppendLine("<tr>")
        html.AppendLine("<td>Last Name:</td>")
        html.AppendLine("<td>" & tbLastName.Text & "</td>")
        html.AppendLine("</tr>")

        'html.AppendLine("<tr>")
        'html.AppendLine("<td>Date of Birth:</td>")
        'html.AppendLine("<td>" & age & "</td>")
        'html.AppendLine("</tr>")




        'html.AppendLine("<tr>")
       ' html.AppendLine("<td>Address:</td>")
        'html.AppendLine("<td>" & tbSA.Text & "</td>")
        'html.AppendLine("</tr>")

        'html.AppendLine("<tr>")
        'html.AppendLine("<td>City:</td>")
       ' html.AppendLine("<td>" & tbCity.Text & "</td>")
       ' html.AppendLine("</tr>")


       ' html.AppendLine("<tr>")
       ' html.AppendLine("<td>State:</td>")
       ' html.AppendLine("<td>" & tbState.Text & "</td>")
       ' html.AppendLine("</tr>")


       ' html.AppendLine("<tr>")
       ' html.AppendLine("<td>Zip:</td>")
       ' html.AppendLine("<td>" & tbZip.Text & "</td>")
       ' html.AppendLine("</tr>")

        'html.AppendLine("<tr>")
        '       html.AppendLine("<td>Airline /Flight No:</td>")
        '       html.AppendLine("<td>" & tbairlinenumber.Text & "</td>")
        '       html.AppendLine("</tr>")
	
        'html.AppendLine("<tr>")
        '       html.AppendLine("<td>Drivers License No:</td>")
        '       html.AppendLine("<td>" & tbLicensenumber.Text & "</td>")
        '       html.AppendLine("</tr>")

	'html.AppendLine("<tr>")
      '  html.AppendLine("<td>Driver's License Expiration:</td>")
       ' html.AppendLine("<td>" & tbLicenseexpiration.Text & "</td>")
      '  html.AppendLine("</tr>")

      '  html.AppendLine("<tr>")
      '  html.AppendLine("<td>Card Information:</td>")
       ' html.AppendLine("<td>" & txtCreditCard.Text.Substring(10).PadLeft(16, "*"c) & "</td>")
       ' html.AppendLine("</tr>")

	
        'html.AppendLine("<tr>")
        '       html.AppendLine("<td>Credit Card Type:</td>")
        '       html.AppendLine("<td>" & drpcard.SelectedItem.Text & "</td>")
        '       html.AppendLine("</tr>")



        'html.AppendLine("<tr>")
        '       html.AppendLine("<td>Credit Card No:</td>")
        '       html.AppendLine("<td>" & txtCreditCard.Text & "</td>")
        '       html.AppendLine("</tr>")

        'html.AppendLine("<tr>")
        '       html.AppendLine("<td>Card Code/AVS:</td>")
        '       html.AppendLine("<td>" & txtcode.Text & "</td>")
        '       html.AppendLine("</tr>")

        'html.AppendLine("<tr>")
        '       html.AppendLine("<td>Expiration Date MM / YY:</td>")
        '       html.AppendLine("<td>" & txtExpMonth.Text & "/" & txtExpYear.Text & "</td>")
        '       html.AppendLine("</tr>")

        If Len(promo) > 2 Then
            html.AppendLine("<tr>")
            html.AppendLine("<td>Promo Code:</td>")
            html.AppendLine("<td>" & promo & "</td>")
            html.AppendLine("</tr>")
        End If


        html.AppendLine("</table>")


        Dim address As New StringBuilder

        Dim locadd As String
        Dim locadd2
        Dim locname As String
        Dim locphone
        Dim locemail
        Dim loczip
        Dim locstate As String
        Dim loc As New RequestMaker
        Dim add1
        Dim city
        Dim state
        Dim zip
        Dim phone


        loc.GetAddress(Request.QueryString("ploc"), add1, city, state, zip, phone)

        locadd = add1
        locadd2 = city & "," & state & " " & zip
        locphone = phone




        'address.AppendLine("<table width=400 cellspacing=0 cellpadding=3 border=0>")



        'address.AppendLine("<tr>")
       ' address.AppendLine("<td>" & locadd & "</td>")
       ' address.AppendLine("</tr>")

        'address.AppendLine("<tr>")
       ' address.AppendLine("<td>" & locadd2 & "</td>")
       ' address.AppendLine("</tr>")

        'address.AppendLine("<tr>")
        'address.AppendLine("<td>Telephone:" & locphone & "</td>")
        'address.AppendLine("</tr>")


       ' address.AppendLine("</table>")

        Dim rt As New StringBuilder
        rt.AppendLine("<table width=350 cellspacing=0 cellpadding=0 border=0>")
        rt.AppendLine("<tr>")
        '  rt.AppendLine("<td>" & lblemail.Text & "</td>")
        rt.AppendLine("</tr>")

        'rt.AppendLine("<tr>")
        'rt.AppendLine("<td><a href=""http://datamin/rental/rental-policies.htm"">Click here to view our Policies</a></td>")
        'rt.AppendLine("</tr>")
        rt.AppendLine("</table>")

        body = html.ToString & "<br><br>" & rt.ToString & "<br><br>" & address.ToString & "<hr align=left width=93%>"


        Dim ploc = Request.QueryString("ploc")


        Trace.Write("CONF", conf)

        Try

            Dim strFrom = fromaddress
            Dim strTo = tbEmail.Text
            Dim MailMsg As New MailMessage(New MailAddress(strFrom.Trim()), New MailAddress(strTo))
            MailMsg.BodyEncoding = Encoding.Default
            MailMsg.Subject = "Savannah Auto Rental: Confirmation"
            MailMsg.CC.Add(toaddress)
            MailMsg.Body = body
            MailMsg.Priority = MailPriority.High
            MailMsg.IsBodyHtml = True
            'Smtpclient to send the mail message 

            Dim SmtpMail As New SmtpClient
            Dim basicAuthenticationInfo As New System.Net.NetworkCredential("donotreply@datamine.net", "datamine2013")

            SmtpMail.Host = "mail.datamine.net"
            SmtpMail.EnableSsl = False
            SmtpMail.Port = 25
            SmtpMail.UseDefaultCredentials = False
            SmtpMail.Credentials = basicAuthenticationInfo
            SmtpMail.Send(MailMsg)
        Catch ex As Exception
            Trace.Write(ex.ToString)
        End Try

    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not Page.IsPostBack Then
            Dim htRentalParam As Hashtable = Nothing

            htRentalParam = TryCast(Session("_htRentalParam"), Hashtable)
           
            If htRentalParam Is Nothing Then
                Dim pickupdate, dropoffdatedate As String
                Dim pickuptime, dropofftime As String
                Dim discount As String
                Dim ploc As String
                Dim dloc As String
                Dim cl As New RequestMaker
                Dim classCode = Request.QueryString("classCode")
                dropoffdatedate = Request.QueryString("dodt")
                pickupdate = Request.QueryString("pudt")

                discount = Trim(Request.QueryString("disc"))

                ploc = Trim(Request.QueryString("ploc"))

                dloc = Trim(Request.QueryString("rloc"))
                DisplayVehicleInfo(ploc, dloc, pickupdate, dropoffdatedate)
                GetClassDetail()
                lblcost.Text = "$" & Request.QueryString("TotalCost")
                lblreservationdetail.Text = Session("RentalDetail")
                lblimage.Text = "<img width=260  src=carimages/" + classCode + ".jpg>"
                '   HyperLink1.NavigateUrl = "terms.aspx?ploc=" & ploc
            Else


                Trace.Write("FALSE")
                '  pnlCard.Enabled = False

                If Request.QueryString("modify") = "true" Then
                Else
                    '  lnkStart.Visible = False
                    '  lnkChange.Visible = False
                    ' lblvchange.Visible = False
                End If

                LoadInfo(htRentalParam)
                ' title.Text = "Modifying Reservation"
             

            End If



            'DisplayVehicleInfo()
        End If

    End Sub

    Public Sub SaveReservation(ploc, dloc, ptime, dtime, rateID, conf, classcode, Extracode, disc, subject)
        Dim ccNo As String = ""
        Dim IsExtra As Boolean
        IsExtra = True
        If Request.QueryString("modify") = "personal" Or Request.QueryString("modify") = "true" Then
            ' ccNo = Session("_CCNo")
        Else
            '  ccNo = txtCreditCard.Text
        End If

        If Request.QueryString("modify") = "personal" Then
            IsExtra = False
        End If
        Dim picloc As String = Request.QueryString("ploc")
        Dim drploc As String = Request.QueryString("rloc")
        ' Dim reqXml As String = RequestMaker.AddRezRequest(ptime, dtime, rateID, classcode, "", Me.Session.SessionID, Request.UserHostAddress, tbFirstName.Text, tbLastName.Text, tbEmail.Text, tbCell.Text, tbSA.Text, "", tbCity.Text, tbState.Text, tbZip.Text, conf, ploc, dloc, "", Extracode, disc, IsExtra)

        ' Dim reqXml As String = RequestMaker.AddRezRequest(ptime, dtime, Request.QueryString("rateID"), Request.QueryString("classCode"), "", Me.Session.SessionID, Request.UserHostAddress, tbFirstName.Text, tbLastName.Text, tbEmail.Text, tbCell.Text, tbSA.Text, "", tbCity.Text, tbState.Text, tbZip.Text, conf, "PAR", "PAR", "", Extracode, disc, drpcard.SelectedItem.Text, txtCreditCard.Text, txtcode.Text, txtExpMonth.Text, tbLicensenumber.Text, tbAge.Text, tbairlinenumber.Text, tbLicenseexpiration.Text, IsExtra)

        Dim reqXml As String = RequestMaker.AddRezRequest(ptime, _
                                                   dtime, _
                                                   Request.QueryString("rateID"), Request.QueryString("classCode"), _
                                                   "", Me.Session.SessionID, _
                                                   Request.UserHostAddress, tbFirstName.Text, tbLastName.Text, tbEmail.Text, _
                                                   tbCell.Text, tbSA.Text, "", tbCity.Text, tbState.Text, _
                                                   tbZip.Text, conf, picloc, drploc, "", Extracode, disc, drpcard.SelectedItem.Value, Me.txtCreditCard.Text, Me.txtcode.Text, txtExpMonth.Text & "/" & Me.txtExpYear.Text, tbLicensenumber.Text, tbAge.Text, tbLicenseexpiration.Text, IsExtra)
        ' tbZip.Text, conf, picloc, drploc, "", Extracode, disc, drpcard.SelectedItem.Value, Me.txtCreditCard.Text, Me.txtcode.Text, txtExpMonth.Text & "/" & Me.txtExpYear.Text, tbLicensenumber.Text, tbAge.Text, tbairlinenumber.Text, tbairlinenumber.Text, tbLicenseexpiration.Text, IsExtra)

        Dim repXml As String = RequestMaker.MakeRateRequest(reqXml)
        'reserveLink.Text = repXml
        Dim xml As XDocument = XDocument.Parse(repXml)
      
        'SendEmail()
        Dim cartype, cxml
        cxml = RequestMaker.MakeRateRequest(RequestMaker.RateRequestClass(classcode))

        Dim classdoc As XDocument = XDocument.Parse(cxml)
        conf = xml...<ConfirmNum>.Value
        If Not String.IsNullOrEmpty(conf) Then

            lblconf.Text = conf
            cartype = lblcls.Text & "-" & classdoc...<ClassDescription>.Value
            SendEmail(conf, cartype)
            Session.Clear()
            pnlsummary.Visible = True
            pnlReserve.Visible = False
            ' lnkChange.Visible = False
            ' lnkStart.Visible = False
            'lblvchange.Visible = False
            ' Response.Write("CONF" & conf)
            pnlLeft.Visible = True
            Session.Abandon()
        Else

            lblMessage.Text = xml...<MessageDescription>.Value + " Please Search Again"

        End If

    End Sub

    Public Sub LoadInfo(htRentalParam)
        Dim name, lastname, address, city, state, zip, phone, email
        Dim cardname, cardtype, cardnumber, cardcode, cardexpiration, TotalCost
        Dim picklocation As String
        Dim droplocation As String
        Dim pudt As String
        Dim dodt As String
        Dim classCode As String
        Dim classDescription As String

        pudt = htRentalParam("PickupDate")
        Trace.Write("PUDT" & pudt)
        classCode = htRentalParam("ClassCode")
        classDescription = htRentalParam("ClassDescription")
        dodt = htRentalParam("DropoffDate")
        picklocation = htRentalParam("PickupLocation")
        droplocation = htRentalParam("DropoffLocation")
        TotalCost = htRentalParam("TotalCharge")

        name = htRentalParam("FirstName")
        lastname = htRentalParam("LastName")
        'address = htRentalParam("Address")
        'city = htRentalParam("City")
        'state = htRentalParam("State")
        'zip = htRentalParam("Zip")
        email = htRentalParam("Email")
        phone = htRentalParam("Phone")

        '  htRentalParam.Add("CardName", cardname)
        'cardtype = htRentalParam("CardType")
        'cardexpiration = htRentalParam("CardExpiration")
        'cardnumber = htRentalParam("CardNumber")
        'cardcode = htRentalParam("CardCode")
        tbFirstName.Text = name
        tbLastName.Text = lastname
        tbSA.Text = address
        tbCell.Text = phone
        'tbState.Text = state
        tbEmail.Text = email
        txtConfemail.Text = email
        'tbCity.Text = city
        'tbState.Text = state
        'tbZip.Text = zip
        lblcost.Text = "$" & TotalCost
        lblcls.Text = classDescription

        '  HyperLink1.NavigateUrl = "terms.aspx?ploc=" & picklocation

        ' pnlCard.Enabled = True
        'txtCreditCard.Visible = False
        ' txtCreditCard.ReadOnly = True
        '  Session("_CCNo") = cardnumber.ToString()
      '

        ' If cardexpiration.ToString.Length > 2 Then
        '   txtExpMonth.Text = cardexpiration.ToString.Substring(0, 2)
        '  txtExpYear.Text = cardexpiration.ToString.Substring(cardexpiration.ToString.Length - 2)
        ' End If

        'If cardtype = "" Then
        'Else
        '    drpcard.SelectedValue = cardtype
        'End If


        Trace.Write("CALLING DISPLAY BEFORE")
        DisplayVehicleInfo(picklocation, droplocation, pudt, dodt)

        lblmconf.Text = htRentalParam("Conf")
        If (String.IsNullOrEmpty(lblmconf.Text)) Then
            lblconftext.Visible = False
        Else
            lblconftext.Visible = True
        End If

        lblreservationdetail.Text = Session("RentalDetail")
        lblimage.Text = "<img width=260  src=carimages/" + classCode + ".jpg>"
    End Sub
    Public Sub DisplayVehicleInfo(ploc, dloc, ptime, dtime)
        Trace.Write("INSIDE DISPLAY")
        Dim pickupdate, dropoffdatedate As String
        Dim pickuptime, dropofftime As String

        Dim loc As New RequestMaker

        Trace.Write("PTIME" & ptime)
        lblploc.Text = loc.GetLocation(ploc)
        lbldloc.Text = loc.GetLocation(dloc)
        pickuptime = Request.QueryString("putime")
        dropofftime = Request.QueryString("dotime")
        If Not String.IsNullOrEmpty(ptime) Then
            lblptime.Text = ptime.Insert(4, "/").Insert(2, "/")
        Else
            Response.Redirect("sessionExpired.aspx")
        End If
        If Not String.IsNullOrEmpty(dtime) Then
            lbldtime.Text = dtime.Insert(4, "/").Insert(2, "/")
        End If

        'lblcode1.Text = RequestMaker.Classes(Request.QueryString("classCode"))
        Dim address, city, state, zip, phone
        loc.GetAddress(ploc,address, city, state, zip, phone)
		'
        'lbladdress.Text = address
        'lblcity.Text = city
        'lblstate.Text = state
        'lblzip.Text = zip
        'lblphone.Text = phone


        loc.GetAddress(dloc,address, city, state, zip, phone)
		'
        'lbldaddress.Text = address
        'lbldcity.Text = city
        'lbldstate.Text = state
        'lbldzip.Text = zip
        'lbldphone.Text = phone


    End Sub

    Public Sub GetClassDetail()
        Dim xml2 As String
        xml2 = RequestMaker.MakeRateRequest(RequestMaker.RequestClass(Request.QueryString("classCode")))
        Dim doc As XDocument = XDocument.Parse(xml2)
        Dim luggage, capacity
        ' cls = doc...<ClassDescription>.Value
        luggage = doc...<LuggageCount>.Value
        capacity = doc...<PassengerCount>.Value
        Dim make = doc...<SimilarText>.Value

        lblcls.Text = make
        lblclass.Text = make
        ' lblluggage.Text = luggage
        'lblpassenger.Text = capacity
        ' lblmake.Text = make

    End Sub
    

    Protected Sub lnkChange_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkChange.Click
        Dim vh As New RequestMaker
        vh.RetainVehicleInfo()
    End Sub
  
 

End Class
