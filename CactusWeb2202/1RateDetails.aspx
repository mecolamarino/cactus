<%@ Page Language="VB" AutoEventWireup="false" CodeFile="RateDetails.aspx.vb" Inherits="RateDetails" Trace="false" MasterPageFile="MastarList.master" Culture="auto" meta:resourcekey="PageResource1" UICulture="auto" %>

<%@ Register Src="Controls/address.ascx" TagName="address" TagPrefix="ad" %>
<asp:Content ID="Content3" ContentPlaceHolderID="head" runat="Server">
    <title>Rate Details</title>
    <link rel="stylesheet" type="text/css" href="css/jquery.qtip.min.css"/>
</asp:Content>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="ContentPlaceHolder1">

	<div class="col-sm-12" style="background:#fff; margin-bottom:2px; padding-top:5px; padding-bottom:5px;">
    	<div class="col-sm-3">
        <strong><asp:Label ID="lblRentalItineraryText" Text="Rental Itinerary" runat="server" meta:resourcekey="lblRentalItineraryTextResource1"></asp:Label></strong>
        </div>
	
    <div class="col-sm-3 text-left">
    <!-- Pick up Text -->
	<strong><asp:Label ID="lblpickuptest" Text="Pick-up" runat="server" meta:resourcekey="lblpickuptestResource1"> </asp:Label></strong>
    <br />
    <!-- Pick up Loc -->
    <strong>Location: </strong>
    <asp:Label ID="lblploc" runat="server" meta:resourcekey="lblplocResource1"></asp:Label>
    <!-- Address -->
    <asp:Label ID="lbladdress" runat="server" meta:resourcekey="lbladdressResource1"></asp:Label>
    <br />
    <asp:Label ID="lblcity" runat="server" meta:resourcekey="lblcityResource1"></asp:Label>
    <asp:Label ID="lblstate" runat="server" meta:resourcekey="lblstateResource1"></asp:Label>
    <asp:Label ID="lblzip" runat="server" meta:resourcekey="lblzipResource1"></asp:Label>
    <!-- Phone --> 
    <asp:Label ID="lblphone" runat="server" meta:resourcekey="lblphoneResource1"></asp:Label>
    <!-- Date & Time -->
    <strong>Date: </strong>
    <asp:Label ID="lblpickupdateText" runat="server"  meta:resourcekey="lblpickupdateTextResource1"></asp:Label>
    <asp:Label ID="lblptime" runat="server" meta:resourcekey="lblptimeResource1"></asp:Label>
	</div>
    <!-- End Pick Up -->
    
    <!-- Start Drop Off -->
    <div class="col-sm-3 text-left">
    <!-- Drop of Text -->
    <strong><asp:Label ID="lbldropfftext" Text="Drop off" runat="server" meta:resourcekey="lbldropfftextResource1"></asp:Label></strong>
    <br />
    <!-- Location -->
    <strong>Location: </strong>
    <asp:Label ID="lbldloc" runat="server" meta:resourcekey="lbldlocResource1"></asp:Label>
    <!-- Address -->
    <asp:Label ID="lbldaddress" runat="server" meta:resourcekey="lbldaddressResource1"></asp:Label>
    <br />
    <asp:Label ID="lbldcity" runat="server" meta:resourcekey="lbldcityResource1"></asp:Label>
    <asp:Label ID="lbldstate" runat="server" meta:resourcekey="lbldstateResource1"></asp:Label>
    <asp:Label ID="lbldzip" runat="server" meta:resourcekey="lbldzipResource1"></asp:Label>
    <!-- Phone -->
    <asp:Label ID="lbldphone" runat="server" meta:resourcekey="lbldphoneResource1"></asp:Label>
    <!-- Date & Time -->
    <strong>Date: </strong>
    <asp:Label ID="lbldropffDatetext" runat="server" meta:resourcekey="lbldropffDatetextResource1"></asp:Label>
    <asp:Label ID="lbldtime" runat="server" meta:resourcekey="lbldtimeResource1"></asp:Label>
    </div>
    
    <!-- Buttons -->
    <div class="col-sm-3">
    <asp:LinkButton ID="lnkChange" CssClass="grey-btn" Text="&laquo; Modify Dates" runat="server" meta:resourcekey="lnkChangeResource1"></asp:LinkButton>  
    <asp:LinkButton ID="lblvchange" CssClass="grey-btn" Text="&laquo; Change Vehicle" CausesValidation="False" runat="server" meta:resourcekey="lblvchangeResource1"></asp:LinkButton>
    </div>
</div>


<div class="col-xs-12" style="background:#fff;">
	<div class="col-sm-6">
    <h2><asp:Label ID="lblextrasText" Text="Choose Extras" runat="server"></asp:Label></h2>
    
    <asp:Repeater ID="rpExtras" runat="server">
    
    <ItemTemplate>
    
<div class="col-xs-12 <%# Eval("ExtraCode") %>" style="padding:10px 10px;">
    <div class="col-xs-1" style="padding:0px;">
    <asp:CheckBox Font-Size="X-Small" ID="chkExtra" ToolTip='<%# Eval("ExtraCode") %>' runat="server" AutoPostBack="True" ClientIDMode="Static" OnCheckedChanged="chkExtra_OnCheckedChanged" meta:resourcekey="chkExtraResource1" /> </asp:CheckBox>
    </div>
    
    <div class="col-xs-2" style="padding:0px;">
    <asp:TextBox Style="margin-left: 7px" Min="0" OnTextChanged="txtQuantity_TextChanged" AutoPostBack="true"  ClientIDMode="Static" runat="server" Visible='<%#(Eval("ExtraCode").ToString() <> "UA01" AND Eval("ExtraCode").ToString() <> "SLI" AND Eval("ExtraCode").ToString() <> "RLP" AND Eval("ExtraCode").ToString() <> "PAI/PEC")%>' Width="55px" TextMode="Number" ID="txtQuantity" Text="1"></asp:TextBox>
    </div>
    
    <div class="col-xs-8" style="padding:0px;">
    <asp:Label ID="Label1" Text='<%# Eval("ExtraDesc") %>' runat="server" meta:resourcekey="Label1Resource2"></asp:Label>
    &nbsp;-&nbsp;$<asp:Label ID="Label2" Text='<%# Eval("ExtraDesc2") %>' runat="server" meta:resourcekey="Label2Resource1"></asp:Label>
    
    
 	</div>
    
    <div class="col-xs-1" style="padding:0px;">
    
    <script> 
		var x = "<%# Eval("ExtraCode") %>"
    	var y = ""
		
		if (x == "PAI/PEC") {
		 	y = "PAIPEC"
		} else {
			y = "<%# Eval("ExtraCode") %>"
		}
		
		document.write('<a href="#" id=tip-' + y + ' class="tooltipp">?</a>');
			
    </script>
    
    </div>
</div>                            
      </ItemTemplate>
      
</asp:Repeater>

</div>
      <!--          <asp:CheckBox ID="chkInsurance" ClientIDMode="Static"  runat="server"/>Insurance
                 END EXTRAS TABLE -->


<div class="col-sm-6">
     <asp:Label ID="Label1" runat="server" meta:resourcekey="Label1Resource1"></asp:Label>
     
     <asp:Label Visible="False" ID="leftContent" runat="server" meta:resourcekey="leftContentResource1"></asp:Label>
     
     <asp:Label ID="lblcontent" Font-Size="X-Small" runat="server" meta:resourcekey="lblcontentResource1"></asp:Label>
     
     <asp:Label ID="lblbook" runat="server" meta:resourcekey="lblbookResource1"></asp:Label>
</div>
</div>
    
</asp:Content>