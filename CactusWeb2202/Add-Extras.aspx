﻿<%@ Page Title="" Language="VB" MasterPageFile="MastarList.master" Trace="false" AutoEventWireup="false" CodeFile="Add-Extras.aspx.vb" Inherits="Add_Extras" Culture="auto" meta:resourcekey="PageResource1" UICulture="auto" %>

<%@ Register Src="Controls/ResSteps.ascx" TagName="step" TagPrefix="st" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

<div class="row_fleet_listing">

        <h2 style="margin: 0px;">
            <asp:Label ID="lbladdon" Text="Select Vehicle Add-ons"
                runat="server" meta:resourcekey="lbladdonResource1"></asp:Label></h2>

        <div class="row_new_listing">

            <div class="new_row_select">

                <div style="float: right; width: 300px; margin-right: 10px;">


                    <st:step ID="step" runat="server" />
                    <h3 style="font-weight: normal;">
                        <asp:Label ID="lblmodeldescription" runat="server"
                            meta:resourcekey="lblmodeldescriptionResource1"></asp:Label>
                        <br>
                        <asp:Label ID="lblclassdescription" CssClass="dateFormat" runat="server"
                            meta:resourcekey="lblclassdescriptionResource1"></asp:Label></h3>
                    <p class="bluebold">
                        <strong>
                            <asp:Label ID="lblVehicleFeatures"
                                Text="Vehicle Features" runat="server"
                                meta:resourcekey="lblVehicleFeaturesResource1"></asp:Label></strong>
                    </p>
                    <div class="cell_features_icons">
                        <div class="cell_icons-1">
                            <asp:Label ID="lblpassenger" runat="server"
                                meta:resourcekey="lblpassengerResource1"></asp:Label>
                        </div>
                        <div class="cell_icons-2">
                            <asp:Label ID="lblluggage" runat="server"
                                meta:resourcekey="lblluggageResource1"></asp:Label>
                        </div>
                        <div class="cell_icons-3">
                            <asp:Label ID="lblgas" runat="server"></asp:Label></div>
                    </div>
                    <div style="height: 320px;">
                        <asp:Image ID="imgcar" Width="250" runat="server"
                            meta:resourcekey="imgcarResource1" /><asp:LinkButton runat="server"
                                CssClass="btn_gray" CausesValidation="False" Text="Change Vehicle"
                                ID="LinkButton1" meta:resourcekey="LinkButton1Resource1"></asp:LinkButton>
                    </div>



                </div>



                <div style="float: left; width: 500px; margin-right: 10px;">


                    <span style="float: right; width: 200px; margin-left: 5px;">



                        <!-- END FEATURE ICON CELL-->
                        <ul>
                            <asp:Label ID="lblclassnotes" runat="server"></asp:Label>
                        </ul>

                    </span>



                    <p class="bluebold">
                        <strong>
                            <asp:Label ID="lblratedetails" runat="server"
                                Text="Rate Details" meta:resourcekey="lblratedetailsResource1"></asp:Label></strong>
                    </p>
                    <asp:Label ID="lblratedetail" runat="server"
                        meta:resourcekey="lblratedetailResource1"></asp:Label>
                    <h3>
                        <asp:Label ID="lblextras" runat="server" Text="Extras"
                            meta:resourcekey="lblextrasResource1"></asp:Label></h3>
                    <asp:GridView ID="gvExtras" runat="server" GridLines="None" ShowHeader="False"
                        AutoGenerateColumns="False" meta:resourcekey="gvExtrasResource1">
                        <Columns>
                            <asp:TemplateField meta:resourcekey="TemplateFieldResource1">
                                <ItemTemplate>
                                    <tr>
                                        <td width="120">
                                            <asp:CheckBox runat="server" ID="chkExtra" name="chkExtra"
                                                AutoPostBack="False"
                                                ToolTip='<%# Eval("ExtraCode") %>' meta:resourcekey="chkExtraResource1" />
                                            <asp:Label ID="lbladditem" runat="server" Text="Add"
                                                meta:resourcekey="lbladditemResource1"></asp:Label></td>
                                        <td width="100">
                                            <asp:Image Width="50" runat="server" ID="imgExtra"
                                                meta:resourcekey="imgExtraResource1" /></td>
                                        <td width="250">
                                            <strong>
                                                <div id='<%# "div_" + Eval("ExtraCode") %>' class="extra_head" onclick="showDescription(this.id);">
                                                    <%# Eval("ExtraDesc") %>
                                                    <span style="color: #CCC; font-size: 16pt; margin-left: 10px;">+
                                                    </span>
                                                </div>
                                            </strong>

                                            <div class="extra_text">
                                                <div id='<%# "div_" + Eval("ExtraCode") + "_Desc" %>' style="display: none;">
                                                    <asp:Label ID="Label2" Text='<%# Eval("HelpContent") %>' runat="server"
                                                        meta:resourcekey="Label2Resource1"></asp:Label>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <h3><%# Eval("ExtraDesc2")%><asp:Label ID="lblday" Text='/day' runat="server"
                                                meta:resourcekey="lbldayResource1"></asp:Label></h3>
                                        </td>
                                        <asp:DropDownList ID="ddlExtra" runat="server" Visible='<%# Convert.ToBoolean(Eval("HasDDL")) %>'
                                            AutoPostBack="True" meta:resourcekey="ddlExtraResource1">
                                            <asp:ListItem Value="1" meta:resourcekey="ListItemResource1">1</asp:ListItem>
                                            <asp:ListItem Value="2" meta:resourcekey="ListItemResource2">2</asp:ListItem>
                                        </asp:DropDownList>
                                        <asp:HiddenField ID="hfExtraDesc" runat="server" Value='<%# Eval("ExtraDesc") %>' />


                                    </tr>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                    <div style="text-align: right; padding: 15px;">
                        <asp:LinkButton runat="server"
                            CssClass="btn_res" CausesValidation="False" Text="Continue" ID="btnContinue"
                            meta:resourcekey="btnContinueResource1"></asp:LinkButton>
                    </div>





                    <div>
                    </div>


                </div>

                <!-- END SELECT CAR CELL-->
            </div>


            <div style="clear: both;"></div>



        </div>
    </div>

    <script type="text/javascript">

        $('#step2').addClass("step_on");
        $('#step1').addClass("step_off");
        $('#step3').addClass("step_off");

        $('#hstep2').addClass("step_on_h");
        $('#hstep1').addClass("step_off_h");

        $('#hstep3').addClass("step_off_h");

        function showDescription(id) {
            $("#" + id + "_Desc").toggle();
        }

    </script>
</asp:Content>

