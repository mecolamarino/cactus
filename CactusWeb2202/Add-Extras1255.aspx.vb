﻿
Imports System.Data
Imports System.Threading
Imports System.Globalization
Partial Class Add_Extras
    Inherits System.Web.UI.Page

    Protected Overrides Sub InitializeCulture()
        Culture = Common.MyCulture

        'set culture to current thread
        Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(Common.MyCulture)
        Thread.CurrentThread.CurrentUICulture = New CultureInfo(Common.MyCulture)

        'call base class
        MyBase.InitializeCulture()
    End Sub
    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Dim rateId As Integer
            rateId = Request.QueryString("rateid")
            If rateId > 0 Then
                Dim vl = DirectCast(Session("_vehicleList"), IEnumerable(Of Rate))
                'Dim vl = TryCast(Session("_vehicleList"), Hashtable)
                Dim vd1 = vl.Where(Function(x) x.RateID = rateId)
                Session("_vehicleDetail") = vd1.FirstOrDefault()
                ' Response.Redirect("Add-Extras.aspx")
            End If
            Dim vd = DirectCast(Session("_vehicleDetail"), Rate)
            'If vd Is Nothing Then
            '    Response.Redirect("SessionExpired.aspx")
            'Else
            loadRateDetails()

            'Trace.Write("CLASS CODE" & vd.ClassCode)
            'Trace.Write("location" & vd.PickupLocation)

            'Dim p = From item In RezCentralRequests.GetExtras(RezCentralRequests.RequestExtras(vd.PickupLocation, vd.ClassCode)) _
            Dim p = (From item In RezCentralRequests.GetExtras(RezCentralRequests.RequestExtras("", 2)) _
                   Select ExtraCode = item.ExtraCode, _
                   ExtraDesc = item.ExtraDesc, _
                   ExtraDesc2 = EnPrice(item.ExtraAmount), _
                   HasDDL = (New String() {"ADDDR", "KST2", "KST1", "USA", "UNDERAGE"}).Any(Function(i) i = item.ExtraCode), _
                   Max = If(item.ExtraCode = "USA", 10, If(item.ExtraCode = "UNDERAGE", 3, 2)), _
                   HelpContent = GetLocalResourceObject(item.ExtraCode)).ToList()

            gvExtras.DataSource = p
            gvExtras.DataBind()


            'Dim p = From item In RezCentralRequests.GetExtras(RezCentralRequests.RequestExtras("TRAC", vd.ClassCode)) _
            '        Select ExtraCode = item.ExtraCode, _
            '        ExtraDesc = item.ExtraDesc, _
            '        ExtraDesc2 = EnPrice(item.ExtraAmount), _
            '        HasDDL = (New String() {"BCS6", "I6S"}).Any(Function(i) i = item.ExtraCode),
            '        Max = If(item.ExtraCode = "USA", 10, If(item.ExtraCode = "UNDERAGE", 3, 2))

            'Dim k As IEnumerable = From item In p
            '                      Join dt In helpDataTable.Rows On item.ExtraCode Equals dt("HelpCode").ToString() _
            '                       Select item.ExtraCode, _
            '                       item.ExtraDesc, _
            '                       item.ExtraDesc2, _
            '                       item.HasDDL, _
            '                       item.Max, _
            '                       HelpContent = dt("HelpContent").ToString(), _
            '                       Helpcode = dt("HelpCode").ToString()
            'gvExtras.DataSource = k
            'gvExtras.DataBind()

            'End If
        End If
    End Sub

    Private Function EnPrice(ByVal desc) As String
        If CInt(desc) = 0 Then
            desc = "FREE!"
        Else
            desc = desc
        End If
        Return desc
    End Function

    Public Sub loadRateDetails()
        Dim helpDataTable As New DataTable
        'If Session("MyCulture") = "en-US" Then
        '    CreateHelpTable(helpDataTable)
        'Else
        '    CreateHelpTableFr(helpDataTable)
        'End If
        Dim vd = DirectCast(Session("_vehicleDetail"), Rate)

        If vd IsNot Nothing Then
            Trace.Write("VD IS NOT NOTHING")
            Dim extrasCodes As New ArrayList
            Dim age As String
            extrasCodes.Clear()

            Dim htRentalParam As Hashtable = Nothing

            htRentalParam = TryCast(Session("_htRentalParam"), Hashtable)
            age = htRentalParam("Age")
            extrasCodes.Add(age)
            For Each row As GridViewRow In gvExtras.Rows
                Dim chkExtra As CheckBox = row.FindControl("chkExtra")
                Dim txtExtra As TextBox = row.FindControl("txtExtra")
                If chkExtra IsNot Nothing Then
                    Trace.Write("NOT CHECKED")
                    If chkExtra.Checked Then
                        Trace.Write("CHECKED")
                        'For i As Integer = 1 To Convert.ToInt16(txtExtra.Text)
                        Trace.Write("CHECKED and inserted")
                        extrasCodes.Add(chkExtra.ToolTip)
                        ' Next
                    End If
                End If
            Next

            Dim pickupLocID As String, dropOffLocID As String, pickupDate As String, dropOffDate As String, pickupTime As String, dropOffTime As String, promoCode As String
            pickupLocID = htRentalParam("PickupLocation")
            dropOffLocID = htRentalParam("DropoffLocation")
            pickupDate = htRentalParam("PickupDate")
            dropOffDate = htRentalParam("DropoffDate")
            pickupTime = htRentalParam("PickupTime")
            dropOffTime = htRentalParam("DropoffTime")
            promoCode = htRentalParam("PromoCode")


            Dim xml As String = RezCentralRequests.MakeRateRequest( _
                                        RezCentralRequests.RateRequestByExtras( _
                                             pickupDate.ToString(), _
                                             dropOffDate.ToString(), _
                                             vd.ClassCode, _
                                             vd.RateCode, _
                                             Me.Session.SessionID, _
                                             Me.Request.UserHostAddress, _
                                             "", _
                                             "", _
                                            vd.RateID, _
                                            extrasCodes, _
                                            1))

            xml = xml.Replace("</PRE>", "</TRNXML></PRE>")
            Dim doc As XDocument = XDocument.Parse(xml)

            Dim p = From item In doc.Descendants("DailyExtra") _
                    Select ExtraDesc = item.Element("ExtraDesc").Value, ExtraAmount = item.Element("ExtraAmount").Value, ExtraCode = item.Element("ExtraCode").Value


            Dim extrasAmount As Double = 0.0
            Dim extraRates As New List(Of ExtraRate)
            Dim promoPrice As Double = 0.0
            Dim totalprice As Double = 0.0
            totalprice = doc...<TotalCharges>.Value


            ' extrasCodes.Clear()

            For Each item In p.ToList()
                If Val(item.ExtraAmount) > 0.0 Then ' To avoid negative values e.g. promo price
                    extrasAmount += Val(item.ExtraAmount)
                Else
                    promoPrice = -(Val(item.ExtraAmount))
                End If

                'extrasCodes.Add(item.ExtraCode)

                If extraRates.Exists(Function(x) x.ExtraCode = item.ExtraCode) Then
                    extraRates.Find(Function(x) x.ExtraCode = item.ExtraCode).Qty += 1
                Else
                    extraRates.Add(New ExtraRate(item.ExtraCode, item.ExtraDesc, Val(item.ExtraAmount), 1))
                End If
            Next


            'vd.ExtrasCodes = extrasCodes
            'vd.ExtraRates = extraRates
            'vd.TotalCharges = totalprice
            Dim rentalDetail As String = ""
            Dim qtytext, ratetext, subtext

            rentalDetail = "<table  cellpadding='5' border=0 cellspacing='1'>" + _
                                "<tr  bgcolor=""#1B429A"">" + _
                                    "<td bgcolor=""#017dc7"" class=""white"" >" + _
                                        "<strong>Quantity</strong>" + _
                                    "</td>" + _
                                    "<td bgcolor=""#017dc7"" class=""white"" >" + _
                                        "Rate" + _
                                    "</td>" + _
                                    "<td bgcolor=""#017dc7"" class=""white"">" + _
                                        "Subtotal" + _
                                    "</td>" + _
                                "</tr>"

            'If CInt(vd.RentalMonths) <> 0 Then
            '    rentalDetail += "<tr bgcolor=""#e4f1fe"" >" + _
            '                            "<td bgcolor=""#e4f1fe"">" + vd.RentalMonths.ToString + " Month" + _
            '                            "</td>" + _
            '                             "<td bgcolor=""#e4f1fe"" >" + _
            '                                String.Format("{0:$#,##0.00}", vd.RatePerMonth) + _
            '                            "</td>" + _
            '                            "<td bgcolor=""#e4f1fe"" >" + _
            '                                String.Format("{0:$#,##0.00}", vd.RentalMonths * vd.RatePerMonth) + _
            '                            "</td>" + _
            '                        "</tr>"
            'End If

            'If CInt(vd.RentalWeeks) <> 0 Then
            '    Dim wktxt

            '    wktxt = "Weekly Rate (" & vd.RentalDays & " days)"


            '    rentalDetail += "<tr bgcolor=""#e4f1fe"" >" + _
            '                            "<td bgcolor=""#e4f1fe"" >" + wktxt + _
            '                            "</td>" + _
            '                             "<td bgcolor=""#e4f1fe"" >" + _
            '                                String.Format("{0:$#,##0.00}", vd.ratePerWeek) + _
            '                            "</td>" + _
            '                            "<td bgcolor=""#e4f1fe"" >" + _
            '                                String.Format("{0:$#,##0.00}", vd.RentalWeeks * vd.ratePerWeek) + _
            '                            "</td>" + _
            '                        "</tr>"
            'End If

            'If CInt(vd.rate1days) <> 0 Then
            '    rentalDetail += "<tr bgcolor=""#e4f1fe"">" + _
            '                            "<td bgcolor=""#e4f1fe"">" + vd.rate1days.ToString + " Day" + _
            '                            "</td>" + _
            '                             "<td bgcolor=""#e4f1fe"" >" + _
            '                                String.Format("{0:$#,##0.00}", vd.Rate1PerDay) + _
            '                            "</td>" + _
            '                            "<td bgcolor=""#e4f1fe"" >" + _
            '                                String.Format("{0:$#,##0.00}", vd.rate1days * vd.Rate1PerDay) + _
            '                            "</td>" + _
            '                        "</tr>"
            'End If
            'If CInt(vd.RentalHours) <> 0 Then
            '    rentalDetail += "<tr bgcolor=""#e4f1fe"" >" + _
            '                            "<td bgcolor=""#e4f1fe"">" + vd.RentalHours.ToString + " Hour" + _
            '                            "</td>" + _
            '                             "<td bgcolor=""#e4f1fe"" >" + _
            '                                String.Format("{0:$#,##0.00}", vd.RatePerHour) + _
            '                            "</td bgcolor=""#e4f1fe"">" + _
            '                            "<td bgcolor=""#e4f1fe"" >" + _
            '                                String.Format("{0:$#,##0.00}", vd.RentalHours * vd.RatePerHour) + _
            '                            "</td>" + _
            '                        "</tr>"
            'End If





            'For Each extra As ExtraRate In vd.ExtraRates
            '    rentalDetail += "<tr>" + _
            '                        "<td>" + _
            '                            extra.ExtraDescription + _
            '                        "</td>" + _
            '                        "<td></td>" + _
            '                        "<td >" + _
            '                            String.Format("{0:$#,##0.00}", extra.ExtraAmount * extra.Qty) + _
            '                        "</td>" + _
            '                    "</tr>"
            'Next

            If doc...<Surcharge>.Value <> "" Then
                Dim surcharge As Double

                surcharge = CDbl(doc...<Surcharge>.Value)
                rentalDetail += "<tr><td>Surcharge</td><td></td><td>$" + surcharge.ToString("N2") + "</td></tr>"
            End If

            If doc...<Tax1Rate>.Value <> "" Then
                rentalDetail += "<tr><td>" & doc...<Tax1Desc>.Value & "</td><td>" + PercentVal((doc...<Tax1Rate>.Value)) + "%</td><td>$" + doc...<Tax1Charge>.Value + "</td></tr>"
            End If

            If doc...<Tax2Rate>.Value <> "" Then
                rentalDetail += "<tr><td>" & doc...<Tax2Desc>.Value & "</td><td >" + PercentVal((doc...<Tax2Rate>.Value)) + "%</td><td>$" + doc...<Tax2Charge>.Value + "</td></tr>"

            End If

            If doc...<Tax3Rate>.Value <> "" Then
                rentalDetail += "<tr><td>" & doc...<Tax3Desc>.Value & "</td><td >" + PercentVal((doc...<Tax3Rate>.Value)) + "%</td><td>$" + doc...<Tax3Charge>.Value + "</td></tr>"

            End If

            If doc...<Tax4Rate>.Value <> "" Then
                rentalDetail += "<tr><td>" & doc...<Tax4Desc>.Value & "</td><td>" + PercentVal((doc...<Tax4Rate>.Value)) + "%</td><td>$" + doc...<Tax4Charge>.Value + "</td></tr>"
            End If

            If doc...<RateDiscount>.Value <> "" Then
                rentalDetail += "<tr><td>Discount</td><td></td><td>$" + doc...<RateDiscount>.Value + "</td></tr>"
            End If

            rentalDetail += "<tr>" + _
                                    "<td bgcolor=FBDDDD>Total Estimate" + _
                                    "</td>" + _
                                     "<td bgcolor=FBDDDD></td>" + _
                                    "<td  bgcolor=ff0000><span class=white>" + _
                                        String.Format("{0:$#,##0.00}", totalprice) + _
                                    "</span></td>" + _
                                "</tr>"

            rentalDetail += "</table>"

            Session("RentDetail") = rentalDetail.ToString

            lblratedetail.Text = rentalDetail.ToString
            'lblluggage.Text = vd.Luggage
            'lblpassenger.Text = vd.Passengers
            lblclassdescription.Text = Server.HtmlDecode(vd.ClassDescription)
            lblmodeldescription.Text = vd.modeldesc

            'If Not String.IsNullOrEmpty(vd.mpgCity) Then
            '    lblgas.Text = vd.mpgCity.ToString & "/" & vd.mpgHighway.ToString
            'Else
            '    lblgas.Text = "0"
            'End If
            'If Not String.IsNullOrEmpty(vd.classNotes) Then
            '    lblclassnotes.Text = Common.FormatText(vd.classNotes)
            'Else
            '    lblclassnotes.Text = ""
            'End If

            imgcar.ImageUrl = vd.Image
            ' Response.Write("rental detail" & rentalDetail)

        End If
    End Sub


    Protected Sub chkExtra_OnCheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        loadRateDetails()
    End Sub

    Public Function PercentVal(ByVal val) As String
        Dim Pval As Decimal
        Pval = val * 100
        Dim str As String

        str = Pval.ToString("N2")
        Return str

    End Function

    Protected Sub gvExtras_DataBound(sender As Object, e As System.EventArgs) Handles gvExtras.DataBound

        ' Dim chkExtra As CheckBox = e.row.FindControl("chkExtra")

        'Dim imgbtnHelp As ImageButton = e.FindControl("imgbtnHelp")
        'Dim ddlExtra As DropDownList = e.Item.FindControl("ddlExtra")

        'If imgbtnHelp IsNot Nothing And ddlExtra IsNot Nothing And imgbtnHelp.CommandArgument.ToUpper() = "USA" Then

        '    For index = 3 To 10
        '        ddlExtra.Items.Add(New ListItem(index, index))
        '    Next
        'End If

        'If imgbtnHelp IsNot Nothing And ddlExtra IsNot Nothing And imgbtnHelp.CommandArgument.ToUpper() = "UNDERAGE" Then

        '    For index = 3 To 3
        '        ddlExtra.Items.Add(New ListItem(index, index))
        '    Next
        'End If

    End Sub

    Protected Sub gvExtras_RowDataBound(sender As Object, e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvExtras.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim chkExtra As CheckBox = e.Row.FindControl("chkExtra")
            Dim img As Image = e.Row.FindControl("imgExtra")
            Dim lblday As Label = e.Row.FindControl("lblday")

            If chkExtra.ToolTip = "CHILD" Then
                img.ImageUrl = "images/options/bcs.jpg"
            End If

            If chkExtra.ToolTip = "GPS" Then
                img.ImageUrl = "images/options/gps.jpg"

            End If


            If chkExtra.ToolTip = "CHILD" Then
                img.ImageUrl = "images/options/is.jpg"
            End If

            If chkExtra.ToolTip = "GAS" Then
                img.ImageUrl = "images/options/gas.jpg"
                lblday.Text = ""
            End If

            If chkExtra.ToolTip = "SP" Then
                img.ImageUrl = "images/options/sp.jpg"
            End If

            If chkExtra.ToolTip = "BS" Then
                img.ImageUrl = "images/options/bs.jpg"
            End If

            If chkExtra.ToolTip = "CDW4" Then
                img.ImageUrl = "images/options/li.jpg"
            End If

            If chkExtra.ToolTip = "CDW5" Then
                img.ImageUrl = "images/options/li.jpg"
            End If

            If chkExtra.ToolTip = "CDW3" Then
                img.ImageUrl = "images/options/li.jpg"
            End If

            If chkExtra.ToolTip = "CDW2" Then
                img.ImageUrl = "images/options/li.jpg"
            End If

            If chkExtra.ToolTip = "CDW1" Then
                img.ImageUrl = "images/options/li.jpg"
            End If

            If chkExtra.ToolTip = "ADD-DRIVER" Then
                img.ImageUrl = "images/options/ADD-DRIVER.gif"
            End If

            If chkExtra.ToolTip = "CELL" Then
                img.ImageUrl = "images/options/cps.jpg"
            End If

            If chkExtra.ToolTip = "RSA" Then
                img.ImageUrl = "images/options/rsa.jpg"
            End If

            If chkExtra.ToolTip = "GASC" Then
                img.ImageUrl = "images/options/gas.jpg"
                lblday.Text = ""
            End If

            If chkExtra.ToolTip = "HOTSPOT" Then
                img.ImageUrl = "images/options/HOTSPOT.jpg"
            End If

        End If

        'IS GAS SP
    End Sub

    Protected Sub btnContinue_Click(sender As Object, e As System.EventArgs) Handles btnContinue.Click
        Dim qsr As String = ("?pudt=" + Request.QueryString("pudt") + "&putime=" + Request.QueryString("putime") + "&dodt=" + Request.QueryString("dodt") + "&dotime=" + Request.QueryString("dotime") + "&classCode=" + Request.QueryString("classCode") + "&rateCode=" + Request.QueryString("rateCode") + "&rateID=" + Request.QueryString("rateID") & "&ploc=" + Request.QueryString("ploc") + "&rloc=" + Request.QueryString("rloc") + "&disc=" & Request.QueryString("disc") & "&modify=" & Request.QueryString("modify"))
        Response.Redirect("reservation.aspx" + qsr)
    End Sub

    Protected Sub LinkButton1_Click(sender As Object, e As System.EventArgs) Handles LinkButton1.Click
        Response.Redirect("list.aspx")
    End Sub

End Class
