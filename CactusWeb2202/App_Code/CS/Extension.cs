using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;
using System.Text.RegularExpressions;
using System.IO;
using System.Xml.Serialization;
using System.Reflection;
using System.Web;
using System.Configuration;
using System.Security.Cryptography;
using System.Linq.Expressions;

using System.ComponentModel.DataAnnotations;
using System.Globalization;

using System.Runtime.Serialization.Formatters.Binary;

using System.Net.NetworkInformation;

public static class Extension
{
    public static string ToFirstCapital(this string value, int a)
    {
        return value.Length > 0 ? value.Length > 1 ? char.ToUpper(value[0]) + value.Substring(1) : char.ToUpper(value[0]).ToString() : string.Empty;
    }

    /// <summary>
    /// Converts string value into Integer. 
    /// </summary> 
    /// <param name="value"></param>
    /// <remarks>Extended By : Anirudh Krishan Vaishnav</remarks>
    /// <returns>Integer Equilent of string value or Zero if invalid integer string</returns>
    public static int ToInt(this string value)
    {
        int result = 0;
        if (value != null)
            int.TryParse(value, out result);
        return result;

    }

    /// <summary>
    /// Converts Integer Equailent.
    /// </summary>
    /// <param name="value">string value that need to be converted</param>
    /// <param name="default">Integer Default value that will be returned if Invalid string provided into parameter.</param>
    /// <remarks>Extended By : Anirudh Krishan Vaishnav</remarks>
    /// <returns>Integer Equilent of string value or Default if invalid integer string</returns>
    public static int? ToIntOrDefault(this string value, int? @default)
    {
        int result = 0;
        if (int.TryParse(value, out result))
            return result;
        else
            return @default;

    }

    /// <summary>
    /// Converts string value into Long. 
    /// </summary> 
    /// <param name="value"></param>
    /// <remarks>Extended By : Anirudh Krishan Vaishnav</remarks>
    /// <returns>Long Equilent of string value or Zero if invalid Long string</returns>
    public static long ToLong(this string value)
    {
        long result = 0;
        long.TryParse(value, out result);
        return result;

    }

    /// <summary>
    /// Converts string value into Float. 
    /// </summary> 
    /// <param name="value"></param>
    /// <remarks>Extended By : Anirudh Krishan Vaishnav</remarks>
    /// <returns>Float Equilent of string value or Zero if invalid float string</returns>
    public static float ToFloat(this string value)
    {
        float result = 0;
        float.TryParse(value, out result);
        return result;

    }

    /// <summary>
    /// Converts string value into Double. 
    /// </summary> 
    /// <param name="value"></param>
    /// <remarks>Extended By : Anirudh Krishan Vaishnav</remarks>
    /// <returns>Double Equilent of string value or Zero if invalid Double string</returns>
    public static double ToDouble(this string value)
    {
        double result = 0;
        double.TryParse(value, out result);
        return result;

    }
    /// <summary>
    /// Converts string value into Decimal. 
    /// </summary> 
    /// <param name="value"></param>
    /// <remarks>Extended By : Anirudh Krishan Vaishnav</remarks>
    /// <returns>Double Equilent of string value or Zero if invalid Double string</returns>
    public static decimal ToDecimal(this string value)
    {
        decimal result = 0;
        decimal.TryParse(value, out result);
        return result;

    }

    /// <summary>
    /// Converts string value into Boolean. 
    /// </summary> 
    /// <param name="value"></param>
    /// <remarks>Extended By : Anirudh Krishan Vaishnav</remarks>
    /// <returns>Boolean Equilent of string value or Zero if invalid Boolean string</returns>
    public static bool ToBool(this string value)
    {
        bool result = false;
        if (string.IsNullOrEmpty(value))
        {
            return result;
        }
        if (value.Trim() == "1")
            result = true;
        else if (value.Trim() == "0")
            result = false;
        else
            bool.TryParse(value, out result);

        return result;
    }

    /// <summary>
    /// Converts string value into Boolean. 
    /// </summary> 
    /// <param name="value"></param>
    /// <remarks>Extended By : Anirudh Krishan Vaishnav</remarks>
    /// <returns>Boolean Equilent of string value or Default if invalid Boolean string</returns>
    public static bool ToBool(this string value, bool @default)
    {
        bool result = false;
        if (value.Trim() == "1")
            result = true;
        else if (value.Trim() == "0")
            result = false;
        else
        {
            if (bool.TryParse(value, out result))
                return result;
            else
                return @default;

        }

        return result;
    }

    /// <summary>
    /// Upper case First later of any string
    /// </summary>
    /// <param name="str"></param>
    /// <remarks>Extended By : Manish sharma</remarks>
    /// <returns>Upper case First later of any string</returns>
    public static string UppercaseFirst(this string value)
    {
        // Check for empty string.
        if (string.IsNullOrEmpty(value))
        {
            return string.Empty;
        }
        // Return char and concat substring.
        if (value.Length == 1)
        {
            return char.ToUpper(value[0]).ToString();
        }
        else
        {
            return char.ToUpper(value[0]) + value.Substring(1);
        }
    }
    public static bool IsBetweenLamdaDateTime(this DateTime dt, DateTime start, DateTime end)
    {
        return dt >= start && dt <= end;

    }
    public static bool IsBetweenLamdaDate(this DateTime dt, DateTime start, DateTime end)
    {
        return dt.Date >= start.Date && dt.Date <= end.Date;

    }
    public static bool IsBetweenLamdaMonth(this DateTime dt, DateTime start, DateTime end)
    {
        return dt.Month >= start.Month && dt.Month <= end.Month;
    }
    public static bool IsBetweenLamdaTimeZoneDateTime(this DateTime dt, DateTime start, DateTime end, string timeZone)
    {
        TimeZoneInfo mountain = TimeZoneInfo.FindSystemTimeZoneById(timeZone);
        return TimeZoneInfo.ConvertTimeFromUtc(dt.ToUniversalTime(), mountain) >= TimeZoneInfo.ConvertTimeFromUtc(start.ToUniversalTime(), mountain) && TimeZoneInfo.ConvertTimeFromUtc(dt.ToUniversalTime(), mountain) <= TimeZoneInfo.ConvertTimeFromUtc(end.ToUniversalTime(), mountain);
        // return dt >= start && dt <= end;

    }
    public static DateTime TimeZoneDateTime(this DateTime dt, string timeZone, bool IsUserValid = true)
    {
        try
        {
            // return TimeZoneDateTime(dt);
            TimeZoneInfo mountain = TimeZoneInfo.FindSystemTimeZoneById(timeZone);
            return TimeZoneInfo.ConvertTimeFromUtc(dt.ToUniversalTime(), mountain);
        }
        catch (Exception ex)
        {
            return dt;
        }
    }


    public static string EclipseString(this string str, int maxlength)
    {
        string tmpstr = string.Empty;
        try
        {
            if (str.Length > maxlength)
                tmpstr = str.Substring(0, maxlength) + "...";
            else
                tmpstr = str;
        }
        catch (Exception ex)
        {
            tmpstr = str;
        }
        return tmpstr;
    }
    public static bool IsMobile(this string userAgent)
    {
        userAgent = userAgent.ToLower();

        return userAgent.Contains("iphone") |
                userAgent.Contains("ppc") |
                userAgent.Contains("windows ce") |
                userAgent.Contains("blackberry") |
                userAgent.Contains("opera mini") |
                userAgent.Contains("mobile") |
                userAgent.Contains("palm") |
                userAgent.Contains("portable") |
                userAgent.Contains("ipad") |
                userAgent.Contains("android") |
                userAgent.Contains("meego");
    }
    public static string ToTrim(this object value)
    {
        try
        {
            var str = value.ToString();
            str = str.Trim();
            return str;
        }
        catch (Exception ex)
        {
            return string.Empty;
        }
    }

    public static string GetQueryString(this string value, string queryStr)
    {
        try
        {
            string strv = string.Empty;
            string str = value;
            var strsLinks = str.Split('&');
            for (int i = 0; i < strsLinks.Length; i++)
            {
                var querystring = strsLinks[i].Split('=');
                if (querystring.Length > 0)
                {
                    if (querystring[0] == queryStr)
                    {
                        return querystring[1];
                    }
                }
            }
            return strv;
        }
        catch (Exception ex)
        {
            return string.Empty;
        }
    }


    public static string ToNiceNumber(this float num)
    {
        // first strip any formatting;
        //$n = (0+str_replace(",","",$n));

        // is this a number?
        // if(!is_numeric($n)) return false;

        // now filter it;
        if (num > 1000000000000) return Math.Round((decimal)(num / 1000000000000), 2).ToString() + " T";
        else if (num > 1000000000) return Math.Round((decimal)(num / 1000000000), 2).ToString() + " B";
        else if (num > 1000000) return Math.Round((decimal)(num / 1000000), 2).ToString() + " M";
        else if (num > 1000) return Math.Round((decimal)(num / 1000), 2).ToString() + " K";

        return string.Format("{0}", num);//number_format(num);
    }
    /// <summary>
    /// Converts an <see cref="int"/> to its textual representation
    /// </summary>
    /// <param name="num">
    /// The number to convert to text
    /// </param>
    /// <returns>
    /// A textual representation of the given number
    /// </returns>
    public static string ToText(this int num)
    {
        StringBuilder result = new StringBuilder();
        if (num <= 99999)
        {
            result.Append(num / 1000 + "K");
        }
        return result.ToString();
    }
    public static string ToText1(this int num)
    {
        StringBuilder result;

        if (num < 0)
        {
            return string.Format("Minus {0}", ToText(-num));
        }

        if (num == 0)
        {
            return "Zero";
        }

        if (num <= 19)
        {
            var oneToNineteen = new[]
            {
                "One",
                "Two",
                "Three",
                "Four",
                "Five",
                "Six",
                "Seven",
                "Eight",
                "Nine",
                "Ten",
                "Eleven",
                "Twelve",
                "Thirteen",
                "Fourteen",
                "Fifteen",
                "Sixteen",
                "Seventeen",
                "Eighteen",
                "Nineteen"
            };

            return oneToNineteen[num - 1];
        }

        if (num <= 99)
        {
            result = new StringBuilder();

            var multiplesOfTen = new[]
            {
                "Twenty",
                "Thirty",
                "Forty",
                "Fifty",
                "Sixty",
                "Seventy",
                "Eighty",
                "Ninety"
            };

            result.Append(multiplesOfTen[(num / 10) - 2]);

            if (num % 10 != 0)
            {
                result.Append(" ");
                result.Append(ToText(num % 10));
            }

            return result.ToString();
        }

        if (num == 100)
        {
            return "One Hundred";
        }

        if (num <= 199)
        {
            return string.Format("One Hundred and {0}", ToText(num % 100));
        }

        if (num <= 999)
        {
            result = new StringBuilder((num / 100).ToText());
            result.Append(" Hundred");
            if (num % 100 != 0)
            {
                result.Append(" and ");
                result.Append((num % 100).ToText());
            }

            return result.ToString();
        }

        if (num <= 999999)
        {
            result = new StringBuilder((num / 1000).ToText());
            result.Append(" Thousand");
            if (num % 1000 != 0)
            {
                switch ((num % 1000) < 100)
                {
                    case true:
                        result.Append(" and ");
                        break;
                    case false:
                        result.Append(", ");
                        break;
                }

                result.Append((num % 1000).ToText());
            }

            return result.ToString();
        }

        if (num <= 999999999)
        {
            result = new StringBuilder((num / 1000000).ToText());
            result.Append(" Million");
            if (num % 1000000 != 0)
            {
                switch ((num % 1000000) < 100)
                {
                    case true:
                        result.Append(" and ");
                        break;
                    case false:
                        result.Append(", ");
                        break;
                }

                result.Append((num % 1000000).ToText());
            }

            return result.ToString();
        }

        result = new StringBuilder((num / 1000000000).ToText());
        result.Append(" Billion");
        if (num % 1000000000 != 0)
        {
            switch ((num % 1000000000) < 100)
            {
                case true:
                    result.Append(" and ");
                    break;
                case false:
                    result.Append(", ");
                    break;
            }

            result.Append((num % 1000000000).ToText());
        }
        return result.ToString();
    }
    public static string ToRequestUserHostAddress(this string hostAddressIP)
    {
        string myLocalDevelopmentIP = "";
        try
        {
            if (!string.IsNullOrEmpty(hostAddressIP) && hostAddressIP.Equals("::1"))
            {
                return myLocalDevelopmentIP;
            }
            else
            {
                return hostAddressIP;
            }
        }
        catch (Exception ex)
        {
            return myLocalDevelopmentIP;
        }
    }

    #region IsValidGuid
    private static Regex isGuid =
            new Regex(@"^(\{){0,1}[0-9a-fA-F]{8}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{12}(\}){0,1}$", RegexOptions.Compiled);

    public static bool IsValidGuid(this string candidate, out Guid output)
    {
        bool isValid = false;
        output = Guid.Empty;

        if (candidate != null)
        {

            if (isGuid.IsMatch(candidate))
            {
                output = new Guid(candidate);
                isValid = true;
            }
        }

        return isValid;
    }
    #endregion

    #region Serialize/Deserialize Methods
    /// <summary>
    /// Returns an XML string based on the ShoppingCart object.
    /// </summary>
    /// <returns>string</returns>
    public static string Serialize(this object type)
    {
        try
        {
            var sb = new StringBuilder();
            var sw = new StringWriter(sb);
            var ser = new XmlSerializer(type.GetType());
            ser.Serialize(sw, type);
            sw.Close();
            return sb.ToString();
        }
        catch (Exception ex)
        {
            return ex.ToString();
        }
    }

    /// <summary>
    /// Deserializes the provided string into the object cast as the provided type.
    /// </summary>
    /// <typeparam name="T">The type of item to base the XML schema on.</typeparam>
    /// <param name="s">The serialized XML based on the XML schema of the provided type.</param>
    /// <returns>Type</returns>
    public static T Deserialize<T>(this string s) where T : class
    {
        try
        {
            var ser = new XmlSerializer(typeof(T));
            var sr = new StringReader(s);
            return ser.Deserialize(sr) as T;
        }
        catch (Exception ex)
        {
            return null;
        }
    }
    #endregion

    /// <summary>
    /// Truncate a String
    /// </summary>
    /// <param name="str">String to truncate</param>
    /// <param name="MaxLength">MaxLength</param>
    /// <returns></returns>
    public static string ToTruncateString(this string str, int MaxLength, bool IsShowEllipses = false)
    {
        string tmpStr = str;
        try
        {
            if (!string.IsNullOrEmpty(str) && str.Length > MaxLength)
            {
                tmpStr = str.Substring(0, MaxLength);

                if (IsShowEllipses)
                    tmpStr = tmpStr.Substring(0, (MaxLength - 3)) + "...";
            }
            else
            {
                tmpStr = str;
            }
            return tmpStr;
        }
        catch (Exception ex)
        {
            return str;
        }

    }

    public static string ToTruncateString(this string str, int MaxLength)
    {
        string tmpStr = str;
        try
        {
            if (!string.IsNullOrEmpty(str) && str.Length > MaxLength)
            {
                tmpStr = str.Substring(0, MaxLength);
            }
            else
            {
                tmpStr = str;
            }
            return tmpStr;
        }
        catch (Exception ex)
        {
            return str;
        }

    }

    public static string ToTruncateHtmlString(this string str, int MaxLength, bool IsShowEllipses)
    {
        string tmpStr = str;
        try
        {
            tmpStr = System.Text.RegularExpressions.Regex.Replace(tmpStr, @"<(.|\n)*?>", string.Empty);
            if (!string.IsNullOrEmpty(tmpStr) && tmpStr.Length > MaxLength)
            {
                if (IsShowEllipses)
                    tmpStr = tmpStr.Substring(0, (MaxLength - 3)) + "...";
                else
                    tmpStr = tmpStr.Substring(0, MaxLength);
            }
            else
            {

            }
            return tmpStr;
        }
        catch (Exception ex)
        {
            return str;
        }

    }

    public static DateTime ToDateTime(this string value)
    {
        DateTime result = DateTime.Now;
        if (value != null)
            DateTime.TryParse(value, out result);
        return result;

    }
    //public static int ToTotalMonths(this DateTime startDate, DateTime endDate)
    //{
    //    return System.Data.Linq.SqlClient.SqlMethods.DateDiffMonth(startDate, endDate);
    //}

    public static IEnumerable<DateTime> EachDay(this DateTime from, DateTime thru)
    {
        for (var day = from.Date; day.Date <= thru.Date; day = day.AddDays(1))
            yield return day;
    }

    public static IEnumerable<DateTime> EachMonth(this DateTime from, DateTime thru)
    {
        for (var month = from.Date; month.Date <= thru.Date || month.Month == thru.Month; month = month.AddMonths(1))
            yield return month;
    }

    public static IEnumerable<DateTime> EachDayTo(this DateTime dateFrom, DateTime dateTo)
    {
        return EachDay(dateFrom, dateTo);
    }

    public static IEnumerable<DateTime> EachMonthTo(this DateTime dateFrom, DateTime dateTo)
    {
        return EachMonth(dateFrom, dateTo);
    }

    public static DateTime ExtractDateAndTime(this string dateString, string timeString)
    {
        try
        {
            DateTime dt1 = DateTime.ParseExact(dateString + " " + timeString, "MM/dd/yyyy H:mm", CultureInfo.InvariantCulture);

            return dt1;
        }
        catch (Exception ex)
        {
            return DateTime.Now;
        }
    }
    //public static IEnumerable<TEnum> Values<TEnum>()
    //    where TEnum : struct,  IComparable, IFormattable, IConvertible
    //        {
    //    var enumType = typeof(TEnum);

    //    // Optional runtime check for completeness    
    //    if (!enumType.IsEnum)
    //    {
    //        throw new ArgumentException();
    //    }

    //    return Enum.GetValues(enumType).Cast<TEnum>();
    //}





    public static List<EnumBoxData1> ToEnumDropDown1<TEnum>() where TEnum : struct,  IComparable, IFormattable, IConvertible
    {
        var enumType = typeof(TEnum);

        // Optional runtime check for completeness    
        if (!enumType.IsEnum)
        {
            throw new ArgumentException();
        }
        var enumDefaultValue = Enum.GetValues(enumType).Cast<TEnum>().Select(x =>
            new EnumBoxData1()
            {
                Name = x.ToString().ToSplitOnCapitals(),
                EnumValue = x.ToString(),
                Id = (x.ToString().ToInt())
            })
         .ToList();
        enumDefaultValue.Insert(0, new EnumBoxData1() { Name = "--Select--", EnumValue = "--Select--", Id = 1 });
        return enumDefaultValue;
    }

   


    public static string TimeAgo(this DateTime dt)
    {
        if (dt > DateTime.Now)
            return "about sometime from now";
        TimeSpan span = DateTime.Now - dt;
        if (span.Days > 365)
        {
            int years = (span.Days / 365);
            if (span.Days % 365 != 0)
                years += 1;
            return String.Format("about {0} {1} ago", years, years == 1 ? "year" : "years");
        }
        if (span.Days > 30)
        {
            int months = (span.Days / 30);
            if (span.Days % 31 != 0)
                months += 1;
            return String.Format("about {0} {1} ago", months, months == 1 ? "month" : "months");
        }
        if (span.Days > 0)
            return String.Format("about {0} {1} ago", span.Days, span.Days == 1 ? "day" : "days");
        if (span.Hours > 0)
            return String.Format("about {0} {1} ago", span.Hours, span.Hours == 1 ? "hour" : "hours");
        if (span.Minutes > 0)
            return String.Format("about {0} {1} ago", span.Minutes, span.Minutes == 1 ? "minute" : "minutes");
        if (span.Seconds > 5)
            return String.Format("about {0} seconds ago", span.Seconds);
        if (span.Seconds <= 5)
            return "just now";
        return string.Empty;
    }

    public static string NullSafe(this string target)
    {
        return target ?? "";
    }

    public static TEnum? TryParseEnum<TEnum>(this string target) where TEnum : struct
    {
        if (string.IsNullOrWhiteSpace(target))
            return null;
        TEnum result;
        if (Enum.TryParse(target, out result))
            return result;
        return null;
    }

    public static TEnum ParseEnum<TEnum>(this string target) where TEnum : struct
    {
        if (string.IsNullOrWhiteSpace(target))
            return default(TEnum);
        TEnum result;
        if (Enum.TryParse<TEnum>(target, out result))
            return result;
        return default(TEnum);
    }

    #region Encryption/Decryption methods
    public static string DecryptString(this string coded, string key = "", string ivKey = "")
    {
        try
        {
            if (string.IsNullOrEmpty(key)) { key = EncryptionKeys.EncryptionKey; }
            if (string.IsNullOrEmpty(ivKey)) { ivKey = EncryptionKeys.IVKey; }
            RijndaelManaged cryptProvider = new RijndaelManaged();
            cryptProvider.KeySize = 256;
            cryptProvider.BlockSize = 256;
            cryptProvider.Mode = CipherMode.CBC;
            SHA256Managed hashSHA256 = new SHA256Managed();
            cryptProvider.Key = hashSHA256.ComputeHash(ASCIIEncoding.ASCII.GetBytes(key));
            string iv = ivKey;
            cryptProvider.IV = hashSHA256.ComputeHash(ASCIIEncoding.ASCII.GetBytes(iv));
            byte[] cipherTextByteArray = EncodeBase64(coded);
            MemoryStream ms = new MemoryStream();
            CryptoStream cs = new CryptoStream(ms, cryptProvider.CreateDecryptor(), CryptoStreamMode.Write);
            cs.Write(cipherTextByteArray, 0, cipherTextByteArray.Length);
            cs.FlushFinalBlock();
            cs.Close();
            byte[] byt = ms.ToArray();
            return Encoding.ASCII.GetString(byt);
        }
        catch (Exception ex) { return string.Empty; }
    }

    // NOTE: We don't use the Encrypt method within the backoffice, but we've supplied the encryption method here as an 
    //       example to your vendors and others of how to write a simple Rijndael enryption method in C#.
    //       
    public static string EncryptString(this string uncoded, string key = "", string ivKey = "")
    {
        try
        {
            if (string.IsNullOrEmpty(key)) { key = EncryptionKeys.EncryptionKey; }
            if (string.IsNullOrEmpty(ivKey)) { ivKey = EncryptionKeys.IVKey; }
            RijndaelManaged cryptProvider = new RijndaelManaged();
            cryptProvider.KeySize = 256;
            cryptProvider.BlockSize = 256;
            cryptProvider.Mode = CipherMode.CBC;
            SHA256Managed hashSHA256 = new SHA256Managed();
            cryptProvider.Key = hashSHA256.ComputeHash(ASCIIEncoding.ASCII.GetBytes(key));
            string iv = ivKey;
            cryptProvider.IV = hashSHA256.ComputeHash(ASCIIEncoding.ASCII.GetBytes(iv));
            byte[] plainTextByteArray = ASCIIEncoding.ASCII.GetBytes(uncoded);
            MemoryStream ms = new MemoryStream();
            CryptoStream cs = new CryptoStream(ms, cryptProvider.CreateEncryptor(), CryptoStreamMode.Write);
            cs.Write(plainTextByteArray, 0, plainTextByteArray.Length);
            cs.FlushFinalBlock();
            cs.Close();
            byte[] byt = ms.ToArray();
            return Convert.ToBase64String(byt);
        }
        catch (Exception ex) { return string.Empty; }
    }
    private static byte[] EncodeBase64(string data)
    {
        string s = data.Trim().Replace(" ", "+");
        if (s.Length % 4 > 0)
            s = s.PadRight(s.Length + 4 - s.Length % 4, '=');
        return Convert.FromBase64String(s);
    }
    #endregion


    public static string ToMimeType(this string fileName)
    {
        string mimeType = "application/unknown";
        string ext = System.IO.Path.GetExtension(fileName).ToLower();
        Microsoft.Win32.RegistryKey regKey = Microsoft.Win32.Registry.ClassesRoot.OpenSubKey(ext);
        if (regKey != null && regKey.GetValue("Content Type") != null)
            mimeType = regKey.GetValue("Content Type").ToString();
        return mimeType;
    }
    // Convert an object to a byte array
    public static byte[] ToObjectToByteArray(this object obj)
    {
        if (obj == null)
            return null;
        BinaryFormatter bf = new BinaryFormatter();
        MemoryStream ms = new MemoryStream();
        bf.Serialize(ms, obj);
        return ms.ToArray();
    }

    // Convert a byte array to an Object
    public static object ToByteArrayToObject(this byte[] arrBytes)
    {
        MemoryStream memStream = new MemoryStream();
        BinaryFormatter binForm = new BinaryFormatter();
        memStream.Write(arrBytes, 0, arrBytes.Length);
        memStream.Seek(0, SeekOrigin.Begin);
        object obj = (object)binForm.Deserialize(memStream);
        return obj;
    }

    public static string ToGetMacAddress()
    {
        var macAddress = string.Empty;
        try
        {
            macAddress = string.Join(":", (from z in GetMacAddress().GetAddressBytes() select z.ToString("X2")).ToArray());

            return macAddress;
        }
        catch (Exception ex)
        {
            return macAddress;        
        }
    }

    /// <summary>
    /// Gets the MAC address of the current PC.
    /// </summary>
    /// <returns></returns>
    private static PhysicalAddress GetMacAddress()
    {

        foreach (NetworkInterface nic in NetworkInterface.GetAllNetworkInterfaces())
        {
            // Only consider Ethernet network interfaces
            if (nic.NetworkInterfaceType == NetworkInterfaceType.Ethernet &&
                nic.OperationalStatus == OperationalStatus.Up)
            {
                return nic.GetPhysicalAddress();
            }
        }
        return new PhysicalAddress(null);
    }



    public static string ToUrlFriendlyString(this string value)
    {
        value = (value ?? "").Trim().ToLower();

        var url = new StringBuilder();

        foreach (char ch in value)
        {
            switch (ch)
            {
                case ' ':
                    url.Append('-');
                    break;
                default:
                    url.Append(Regex.Replace(ch.ToString(), @"[^A-Za-z0-9'()\*\\+_~\:\/\?\-\.,;=#\[\]@!$&]", ""));
                    break;
            }
        }

        return url.ToString();
    }
 

}

public class Varience
{
    string _prop;
    public string Property
    {
        get { return _prop; }
        set { _prop = value; }
    }
    object _valA;
    public object ValA
    {
        get { return _valA; }
        set { _valA = value; }
    }
    object _valB;
    public object ValB
    {
        get { return _valB; }
        set { _valB = value; }
    }

}

public static class ExtensionMethods
{
    public static IQueryable<T> OrderByPrperty<T>(
           this IQueryable<T> source, string propertyName, bool asc)
    {
        var type = typeof(T);
        string methodName = asc ? "OrderBy" : "OrderByDescending";
        var property = type.GetProperty(propertyName);
        var parameter = Expression.Parameter(type, "p");
        var propertyAccess = Expression.MakeMemberAccess(parameter, property);
        var orderByExp = Expression.Lambda(propertyAccess, parameter);
        MethodCallExpression resultExp = Expression.Call(typeof(Queryable), methodName,
                          new Type[] { type, property.PropertyType },
                          source.Expression, Expression.Quote(orderByExp));
        return source.Provider.CreateQuery<T>(resultExp);
    }

    //public static IQueryable<T> LikeByProperty<T>(this IQueryable<T> source,
    //               string keyword, params string[] propertyNames)
    //{

    //    foreach (var propertyName in propertyNames)
    //    {
    //        var type = typeof(T);
    //        var property = type.GetProperty(propertyName);
    //        var parameter = Expression.Parameter(type, "p");
    //        var propertyAccess = Expression.MakeMemberAccess(parameter, property);
    //        var constant = Expression.Constant("%" + keyword + "%");
    //        var like = typeof(SqlMethods).GetMethod("Like",
    //                   new Type[] { typeof(string), typeof(string) });
    //        MethodCallExpression methodExp =
    //              Expression.Call(null, like, propertyAccess, constant);
    //        Expression<Func<T, bool>> lambda =
    //              Expression.Lambda<Func<T, bool>>(methodExp, parameter);
    //        source = source.Where(lambda);
    //    }
    //    return source;
    //}

    public static TTarget Transfer<TSource, TTarget>(TSource source)
    where TTarget : class, new()
    {
        var target = new TTarget();
        Transfer(source, target);
        return target;
    }
    static void Transfer(object source, object target)
    {
        var sourceType = source.GetType();
        var targetType = target.GetType();

        var sourceParameter = Expression.Parameter(typeof(object), "source");
        var targetParameter = Expression.Parameter(typeof(object), "target");

        var sourceVariable = Expression.Variable(sourceType, "castedSource");
        var targetVariable = Expression.Variable(targetType, "castedTarget");

        var expressions = new List<Expression>();

        expressions.Add(Expression.Assign(sourceVariable, Expression.Convert(sourceParameter, sourceType)));
        expressions.Add(Expression.Assign(targetVariable, Expression.Convert(targetParameter, targetType)));

        foreach (var property in sourceType.GetProperties(BindingFlags.Public | BindingFlags.Instance))
        {
            if (!property.CanRead)
                continue;

            var targetProperty = targetType.GetProperty(property.Name, BindingFlags.Public | BindingFlags.Instance);
            if (targetProperty != null
                    && targetProperty.CanWrite
                    && targetProperty.PropertyType.IsAssignableFrom(property.PropertyType))
            {
                expressions.Add(
                    Expression.Assign(
                        Expression.Property(targetVariable, targetProperty),
                        Expression.Convert(
                            Expression.Property(sourceVariable, property), targetProperty.PropertyType)));
            }
        }

        var lambda =
            Expression.Lambda<Action<object, object>>(
                Expression.Block(new[] { sourceVariable, targetVariable }, expressions),
                new[] { sourceParameter, targetParameter });

        var del = lambda.Compile();

        del(source, target);
    }



    public static IQueryable<T> Where<T>(this IQueryable<T> query,
        string column, object value, Enums.WhereOperation  operation)
    {
        if (string.IsNullOrEmpty(column))
            return query;

        ParameterExpression parameter = Expression.Parameter(query.ElementType, "p");

        MemberExpression memberAccess = null;
        foreach (var property in column.Split('.'))
            memberAccess = MemberExpression.Property
               (memberAccess ?? (parameter as Expression), property);

        //change param value type
        //necessary to getting bool from string
        ConstantExpression filter = Expression.Constant
            (
                Convert.ChangeType(value, memberAccess.Type)
            );

        //switch operation
        Expression condition = null;
        LambdaExpression lambda = null;
        switch (operation)
        {
            //GreatorThan ==
            case Enums.WhereOperation.GreatorThan:
                condition = Expression.GreaterThan(memberAccess, filter);
                lambda = Expression.Lambda(condition, parameter);
                break;
            //StartWith ==
            case Enums.WhereOperation.StartWith:
                condition = Expression.Call(memberAccess,
                    typeof(string).GetMethods().FirstOrDefault(method => method.Name == "StartsWith"),
                    Expression.Constant(value));
                lambda = Expression.Lambda(condition, parameter);
                break;
            //StartWith ==
            case Enums.WhereOperation.EndWith:
                condition = Expression.Call(memberAccess,
                    typeof(string).GetMethods().FirstOrDefault(method => method.Name == "EndsWith"),
                    Expression.Constant(value));
                lambda = Expression.Lambda(condition, parameter);
                break;
            //equal ==
            case Enums.WhereOperation.Equal:
                condition = Expression.Equal(memberAccess, filter);
                lambda = Expression.Lambda(condition, parameter);
                break;
            //not equal !=
            case Enums.WhereOperation.NotEqual:
                condition = Expression.NotEqual(memberAccess, filter);
                lambda = Expression.Lambda(condition, parameter);
                break;

            //string.Contains()
            case Enums.WhereOperation.Contains:
                condition = Expression.Call(memberAccess,
                    typeof(string).GetMethod("Contains"),
                    Expression.Constant(value));
                lambda = Expression.Lambda(condition, parameter);
                break;
        }


        MethodCallExpression result = Expression.Call(
               typeof(Queryable), "Where",
               new[] { query.ElementType },
               query.Expression,
               lambda);

        return query.Provider.CreateQuery<T>(result);
    }



    public static IQueryable<T> WhereLike<T>(this IQueryable<T> query, string[] columns, object value, Enums.WhereOperation operation)
    {
        if (columns.Length == 0)
            return query;
        foreach (var column in columns)
        {
            ParameterExpression parameter = Expression.Parameter(query.ElementType, "p");

            MemberExpression memberAccess = null;
            foreach (var property in column.Split('.'))
                memberAccess = MemberExpression.Property
                   (memberAccess ?? (parameter as Expression), property);

            //change param value type
            //necessary to getting bool from string
            ConstantExpression filter = Expression.Constant
                (
                    Convert.ChangeType(value, memberAccess.Type)
                );

            //switch operation
            Expression condition = null;
            LambdaExpression lambda = null;
            switch (operation)
            {
                //GreatorThan ==
                case Enums.WhereOperation.GreatorThan:
                    condition = Expression.GreaterThan(memberAccess, filter);
                    lambda = Expression.Lambda(condition, parameter);
                    break;
                //StartWith ==
                case Enums.WhereOperation.StartWith:
                    condition = Expression.Call(memberAccess,
                        typeof(string).GetMethods().FirstOrDefault(method => method.Name == "StartsWith"),
                        Expression.Constant(value));
                    lambda = Expression.Lambda(condition, parameter);
                    break;
                //StartWith ==
                case Enums.WhereOperation.EndWith:
                    condition = Expression.Call(memberAccess,
                        typeof(string).GetMethods().FirstOrDefault(method => method.Name == "EndsWith"),
                        Expression.Constant(value));
                    lambda = Expression.Lambda(condition, parameter);
                    break;
                //equal ==
                case Enums.WhereOperation.Equal:
                    condition = Expression.Equal(memberAccess, filter);
                    lambda = Expression.Lambda(condition, parameter);
                    break;
                //not equal !=
                case Enums.WhereOperation.NotEqual:
                    condition = Expression.NotEqual(memberAccess, filter);
                    lambda = Expression.Lambda(condition, parameter);
                    break;

                //string.Contains()
                case Enums.WhereOperation.Contains:
                    condition = Expression.Call(memberAccess,
                        typeof(string).GetMethod("Contains"),
                        Expression.Constant(value));
                    lambda = Expression.Lambda(condition, parameter);
                    break;
            }


            MethodCallExpression result = Expression.Call(
                   typeof(Queryable), "Where",
                   new[] { query.ElementType },
                   query.Expression,
                   lambda);

            query = query.Provider.CreateQuery<T>(result);
        }

        return query;
    }

    public static IQueryable<T> ETForStartsWith<T>(this IQueryable<T> query, string propertyValue, PropertyInfo propertyInfo)
    {
        ParameterExpression e = Expression.Parameter(typeof(T), "e");
        MemberExpression m = Expression.MakeMemberAccess(e, propertyInfo);
        ConstantExpression c = Expression.Constant(propertyValue, typeof(string));
        MethodInfo mi = typeof(string).GetMethod("StartsWith", new Type[] { typeof(string) });
        Expression call = Expression.Call(m, mi, c);

        Expression<Func<T, bool>> lambda = Expression.Lambda<Func<T, bool>>(call, e);
        return query.Where(lambda);
    }
    public static IEnumerable<TSource> DistinctBy<TSource, TKey>
     (this IEnumerable<TSource> source, Func<TSource, TKey> keySelector)
    {
        HashSet<TKey> knownKeys = new HashSet<TKey>();
        foreach (TSource element in source)
        {
            if (knownKeys.Add(keySelector(element)))
            {
                yield return element;
            }
        }
    }
    public static ObservableCollection<T> ToObservableCollection<T>(this IEnumerable<T> thisCollection)
    {

        if (thisCollection == null) return null;
        var oc = new ObservableCollection<T>();

        foreach (var item in thisCollection)
        {
            oc.Add(item);
        }

        return oc;
    }
    public static string ToSplitOnCapitals(this string theString)
    {
        theString = theString.Replace(" ", "");
        if (!string.IsNullOrEmpty(theString))
        {
            StringBuilder builder = new StringBuilder();

            foreach (char c in theString)
            {
                if (Char.IsUpper(c) && builder.Length > 0)
                {
                    builder.Append(' ');
                }
                builder.Append(c);
            }
            theString = builder.ToString();
        }
        return theString;
    }
    //public static ObjectQuery<T> Include<T>(this ObjectQuery<T> query, Expression<Func<T, object>> exp)
    //{
    //    Expression body = exp.Body;
    //    MemberExpression memberExpression = (MemberExpression)exp.Body;
    //    string path = GetIncludePath(memberExpression);
    //    return query.Include(path);
    //}

    //private static string GetIncludePath(MemberExpression memberExpression)
    //{
    //    string path = "";
    //    if (memberExpression.Expression is MemberExpression)
    //    {
    //        path = GetIncludePath((MemberExpression)memberExpression.Expression) + ".";
    //    }
    //    PropertyInfo propertyInfo = (PropertyInfo)memberExpression.Member;
    //    return path + propertyInfo.Name;
    //}

}
public class Enums
{
    public enum WhereOperation
    {
        [Display(Name = "sw")]
        StartWith,
        [Display(Name = "ew")]
        EndWith,
        [Display(Name = "gt")]
        GreatorThan,
        [Display(Name = "eq")]
        Equal,
        [Display(Name = "ne")]
        NotEqual,
        [Display(Name = "cn")]
        Contains
    }
}

public class EnumBoxData1
{
    public string Name { get; set; }
    public string EnumValue { get; set; }
    public int Id { get; set; }

}


public static class EncryptionKeys
{
    public static string EncryptionKey = "SHgwuip492hD78GOh39450jk9h";
    public static string IVKey = "silentloginivkey";
}