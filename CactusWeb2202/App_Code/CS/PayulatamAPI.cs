﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using System.Net;
using System.IO;

/// <summary>
/// Summary description for PayulatamAPI
/// </summary>
public class PayulatamAPI
{
    public static WebRequest req;
    public static WebResponse rep;
    public static string cactusAccountId = System.Configuration.ConfigurationManager.AppSettings["cactusAccountId"];
    public static string cactusMerchantId = System.Configuration.ConfigurationManager.AppSettings["cactusMerchantId"];
    public static string cactusAPIKey = System.Configuration.ConfigurationManager.AppSettings["cactusAPIKey"];
    public static string cactusAPILogin = System.Configuration.ConfigurationManager.AppSettings["cactusAPILogin"];
    public static string cactusAPIUrl = System.Configuration.ConfigurationManager.AppSettings["cactusAPIUrl"];
    public static bool mode = System.Configuration.ConfigurationManager.AppSettings["isTest"].ToBool();
    public static string notifyUrl = System.Configuration.ConfigurationManager.AppSettings["notifyUrl"];
    public PayulatamAPI()
    {
        //
        // TODO: Add constructor logic here
        //   
                //           Dim fullName as string = tbFirstName.Text+""+ tbLastName.Text;               
      //  CreditCardsPayment("payment_test_00000001","6dcd21f14036ad057988089df9a28434","100","ARS","1",fullName,tbEmail.Text,tbCell.Text,"5415668464654",tbSA.Text,"",tbCity.Text,tbState.Text,"",tbZip.Text,"",tbSA.Text,"",tbCity.Text,tbState.Text,"",tbZip.Text,"","1",fullName,tbEmail.Text,tbCell.Text,"5415668464654",tbSA.Text,"",tbCity.Text,tbState.Text,"",tbZip.Text,"",Me.txtCreditCard.Text,Me.txtcode.Text,txtExpMonth.Text & "/" & Me.txtExpYear.Text,"","AUTHORIZATION_AND_CAPTURE", drpcard.SelectedItem.Value,"AR",Me.Session.SessionID,"","","")
    }

   



    public static string CreditCardsPayment(string referenceCode, string signature, decimal value,
        string currency, string merchantBuyerId, string buyerFullName,
        string buyerEmailAddress, string buyerContactPhone, string buyerDniNumber,
        string buyerStreet1, string buyerStreet2, string buyerCity, string buyerState,
        string buyerCountry, string buyerpostalCode, string buyerPhone,
         string street1, string street2, string city, string state,
        string country, string postalCode, string phone,



        string merchantPayerId, string payerFullName,
        string payerEmailAddress, string payerContactPhone, string payerDniNumber,
        string payerStreet1, string payerStreet2, string payerCity, string payerState,
        string payerCountry, string payerpostalCode, string payerPhone, string creditCardNumber,
        string cardSecurityCode, string expirationDate, string nameOnCard, 
        string paymentMethod, string paymentCountry, string deviceSessionId, string ipAddress, string cookie, string userAgent)
    {
        StringBuilder textBuilder = new StringBuilder();
        string paymentType = "AUTHORIZATION_AND_CAPTURE";
        textBuilder.AppendLine("<request>");
        textBuilder.AppendLine("<language>es</language>");
        textBuilder.AppendLine("<command>SUBMIT_TRANSACTION</command>");
        textBuilder.AppendLine("<merchant>");
        textBuilder.AppendLine("<apiKey>" + cactusAPIKey + "</apiKey>");
        textBuilder.AppendLine("<apiLogin>" + cactusAPILogin + "</apiLogin>");
        textBuilder.AppendLine("</merchant>");
        textBuilder.AppendLine("<transaction>");
        textBuilder.AppendLine("<order>");
        textBuilder.AppendLine("<accountId>" + cactusAccountId + "</accountId>");
        textBuilder.AppendLine("<referenceCode>" + referenceCode + "</referenceCode>");
        textBuilder.AppendLine("<description>payment test</description>");
        textBuilder.AppendLine("<language>es</language>");
        textBuilder.AppendLine("<signature>" + signature + "</signature>");
        textBuilder.AppendLine("<notifyUrl>" + notifyUrl + "</notifyUrl>");
        textBuilder.AppendLine("<additionalValues>");
        textBuilder.AppendLine("<entry>");
        textBuilder.AppendLine("<string>TX_VALUE</string>");
        textBuilder.AppendLine("<additionalValue>");
        textBuilder.AppendLine(" <value>" + value + "</value>");
        textBuilder.AppendLine("<currency>" + currency + "</currency>");
        textBuilder.AppendLine("</additionalValue>");
        textBuilder.AppendLine("</entry>");
        textBuilder.AppendLine("</additionalValues>");
        textBuilder.AppendLine("<buyer>");
        textBuilder.AppendLine("<merchantBuyerId>" + merchantBuyerId + "</merchantBuyerId>");
        textBuilder.AppendLine("<fullName>" + buyerFullName + "</fullName>");
        textBuilder.AppendLine("<emailAddress>" + buyerEmailAddress + "</emailAddress>");
        textBuilder.AppendLine("<contactPhone>" + buyerContactPhone + "</contactPhone>");
        textBuilder.AppendLine("<dniNumber>" + buyerDniNumber + "</dniNumber>");
        textBuilder.AppendLine("<shippingAddress>");
        textBuilder.AppendLine("<street1>" + buyerStreet1 + "</street1>");
        textBuilder.AppendLine("<street2>" + buyerStreet2 + "</street2>");
        textBuilder.AppendLine("<city>" + buyerCity + "</city>");
        textBuilder.AppendLine("<state>" + buyerState + "</state>");
        textBuilder.AppendLine("<country>" + buyerCountry + "</country>");
        textBuilder.AppendLine("<postalCode>" + buyerpostalCode + "</postalCode>");
        textBuilder.AppendLine("<phone>" + buyerPhone + "</phone>");
        textBuilder.AppendLine("</shippingAddress>");
        textBuilder.AppendLine("</buyer>");
        textBuilder.AppendLine("<shippingAddress>");
        textBuilder.AppendLine("<street1>" + street1 + "</street1>");
        textBuilder.AppendLine("<street2>" + street2 + "</street2>");
        textBuilder.AppendLine("<city>" + city + "</city>");
        textBuilder.AppendLine("<state>" + state + "</state>");
        textBuilder.AppendLine("<country>" + country + "</country>");
        textBuilder.AppendLine("<postalCode>" + postalCode + "</postalCode>");
        textBuilder.AppendLine("<phone>" + phone + "</phone>");
        textBuilder.AppendLine("</shippingAddress>");
        textBuilder.AppendLine("</order>");
        textBuilder.AppendLine("<payer>");
        textBuilder.AppendLine("<merchantPayerId>" + merchantPayerId + "</merchantPayerId>");
        textBuilder.AppendLine("<fullName>" + payerFullName + "</fullName>");
        textBuilder.AppendLine("<emailAddress>" + payerEmailAddress + "</emailAddress>");
        textBuilder.AppendLine("<contactPhone>" + payerContactPhone + "</contactPhone>");
        textBuilder.AppendLine("<dniNumber>" + payerDniNumber + "</dniNumber>");
        textBuilder.AppendLine("<billingAddress>");
        textBuilder.AppendLine("<street1>" + payerStreet1 + "</street1>");
        textBuilder.AppendLine("<street2>" + payerStreet2 + "</street2>");
        textBuilder.AppendLine("<city>" + payerCity + "</city>");
        textBuilder.AppendLine("<state>" + payerState + "</state>");
        textBuilder.AppendLine("<country>" + payerCountry + "</country>");
        textBuilder.AppendLine("<postalCode>" + payerpostalCode + "</postalCode>");
        textBuilder.AppendLine("<phone>" + payerPhone + "</phone>");
        textBuilder.AppendLine("</billingAddress>");
        textBuilder.AppendLine("</payer>");
        textBuilder.AppendLine("<creditCard>");
        textBuilder.AppendLine("<number>" + creditCardNumber + "</number>");
        textBuilder.AppendLine("<securityCode>" + cardSecurityCode + "</securityCode>");
        textBuilder.AppendLine("<expirationDate>" + expirationDate + "</expirationDate>");
        textBuilder.AppendLine("<name>" + nameOnCard + "</name>");
        textBuilder.AppendLine("</creditCard>");
        textBuilder.AppendLine("<extraParameters>");
        textBuilder.AppendLine("<entry>");
        textBuilder.AppendLine("<string>INSTALLMENTS_NUMBER</string>");
        textBuilder.AppendLine("<string>1</string>");
        textBuilder.AppendLine("</entry>");
        textBuilder.AppendLine("</extraParameters>");
        textBuilder.AppendLine("<type>" + paymentType + "</type>");
        textBuilder.AppendLine("<paymentMethod>" + paymentMethod + "</paymentMethod>");
        textBuilder.AppendLine("<paymentCountry>" + paymentCountry + "</paymentCountry>");
        textBuilder.AppendLine("<deviceSessionId>" + deviceSessionId + "</deviceSessionId>");
        textBuilder.AppendLine(" <ipAddress>" + ipAddress + "</ipAddress>");
        textBuilder.AppendLine("<cookie>" + cookie + "</cookie>");
        textBuilder.AppendLine("<userAgent>" + userAgent + "</userAgent>");
        textBuilder.AppendLine("</transaction>");
        textBuilder.AppendLine("<isTest>" + mode + "</isTest>");
        textBuilder.AppendLine("</request>");
        return textBuilder.ToString();

    }

    public static string CashOrBankPayments(string refrenceCode, string signature, decimal value, string currency, string emailAddress, string paymentType, string paymentMethod, string paymentCountry, string ipAddress)
    {

        StringBuilder textBuilder = new StringBuilder();


        textBuilder.AppendFormat(@"<request>
        <language>es</language>
        <command>SUBMIT_TRANSACTION</command>
        <merchant>
        <apiKey>{0}</apiKey>
         <apiLogin>{2}</apiLogin>
        </merchant>
         <transaction>
        <order>
         <accountId>{3}</accountId>
         <referenceCode>{4}</referenceCode>
         <description>payment test</description>
         <language>es</language>
         <signature>{5}</signature>
         <notifyUrl>{6}</notifyUrl>
         <additionalValues>
            <entry>
               <string>TX_VALUE</string>
               <additionalValue>
                  <value>{7}</value>
                  <currency>{8}</currency>
               </additionalValue>
            </entry>
         </additionalValues>
         <buyer>
            <emailAddress>{9}</emailAddress>
         </buyer>
        </order>
       <type>{10}</type>
      <paymentMethod>{11}</paymentMethod>
     
       <paymentCountry>{12}</paymentCountry>
       <ipAddress>{13}</ipAddress>
       </transaction>
      <isTest>{14}</isTest>
      </request>", cactusAPIKey, cactusAPILogin, cactusAccountId, refrenceCode, signature, notifyUrl, value, currency, emailAddress, paymentType, paymentMethod, paymentCountry, ipAddress, mode);
        return textBuilder.ToString();

    }

    public static string BankTransfer(string paymentMethod, string paymentCountry)
    {

        StringBuilder textBuilder = new StringBuilder();


        textBuilder.AppendFormat(@"<request>
        <language>en</language>
        <command>GET_BANKS_LIST</command>
        <merchant>
        <apiLogin>{0}</apiLogin>
        <apiKey>{1}</apiKey>
        </merchant>
        <isTest>{2}</isTest>
        <bankListInformation>
        <paymentMethod>{3} </paymentMethod>
        <paymentCountry>{4}</paymentCountry>
        </bankListInformation>
        </request>", cactusAPILogin, cactusAPIKey, mode, paymentMethod, paymentCountry);
        return textBuilder.ToString();
    }

  
    public static string PingPayment()
    {

        StringBuilder textBuilder = new StringBuilder();


        textBuilder.AppendFormat(@"<request>
        <language>en</language>
      <command>PING</command>
      <merchant>
      <apiLogin>{0}</apiLogin>
      <apiKey>{1}</apiKey>
        </merchant>
      <isTest>{2}</isTest>
      </request>", cactusAPILogin, cactusAPIKey, mode);
        return textBuilder.ToString();
    }
    public static string ActivePaymentMethodsQuery()
    {

        StringBuilder textBuilder = new StringBuilder();


        textBuilder.AppendFormat(@"<request>
        <language>en</language>
       <command>GET_PAYMENT_METHODS</command>
        <merchant>
        <apiLogin>{0}</apiLogin>
       <apiKey>{1}</apiKey>
       </merchant>
       <isTest>{2}</isTest>
       </request>", cactusAPILogin, cactusAPIKey, mode);
        return textBuilder.ToString();
    }
    public static string MakePaymentRequest(string para)
    {
        string result;
        req = WebRequest.Create(cactusAPIUrl);
        req.Method = "POST";
        req.ContentType = "application/xml;charset=utf-8";
        StreamWriter writer = new StreamWriter(req.GetRequestStream());
        writer.Write(para);
        writer.Close();
        rep = req.GetResponse();
        Stream receivestream = rep.GetResponseStream();


        Encoding encode = Encoding.GetEncoding("utf-8");

        StreamReader readstream = new StreamReader(receivestream, encode);
        result = readstream.ReadToEnd();

        return result;
    }
}