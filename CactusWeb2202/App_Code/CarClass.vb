﻿Imports System.Xml
Imports System.Xml.Serialization
Imports System.Linq
Imports System.Xml.Linq
Imports System.Web.Script.Serialization
Imports System.Net
Imports System.IO

Public Class CarClass  'Get car class info for display in car results

    Public Sub New(classCode As String)

        Dim req As WebRequest
        Dim rep As WebResponse
        Dim url As String = System.Configuration.ConfigurationManager.AppSettings("Url")

        Dim uri As String = url


        req = WebRequest.Create(uri)
        req.Method = "POST"
        req.ContentType = "text/xml"

        Dim writer As New StreamWriter(req.GetRequestStream())

        'Request car class info
        writer.Write(RequestMaker.RateRequestClass(classCode))

        writer.Close()

        rep = req.GetResponse()

        Dim receivestream As Stream = rep.GetResponseStream()
        Dim encode As Encoding = Encoding.GetEncoding("utf-8")
        Dim readstream As New StreamReader(receivestream, encode)

        Dim read(256) As [Char]
        Dim result As String
        'Dim IsLocal As Boolean
        Dim counter As Integer
        Dim Tcounter As Integer
        counter = 0
        Tcounter = 0
        result = readstream.ReadToEnd()

        'Write debug output
        'Debug.WriteLine(result)

        Dim xml As XDocument = XDocument.Parse(result)
        ' Trace.Write()
        _passengers = xml...<PassengerCount>.Value
        _luggage = xml...<LuggageCount>.Value
        _mpgcity = xml...<MPGCity>.Value
        _mpghighway = xml...<MPGHighway>.Value


    End Sub

    Private Property _passengers As Integer

    ReadOnly Property passengers As Integer
        Get
            Return _passengers
        End Get
    End Property

    Private Property _luggage As Integer

    ReadOnly Property luggage As Integer
        Get
            Return _luggage
        End Get
    End Property
    Private Property _mpgcity As Integer

    ReadOnly Property mpgCity As Integer
        Get
            Return _mpgcity
        End Get
    End Property

    Private Property _mpghighway As Integer

    ReadOnly Property mpgHighway As Integer
        Get
            Return _mpghighway
        End Get
    End Property


End Class
