﻿Imports Microsoft.VisualBasic
Imports System.Net.Mail
Imports System.Net
Imports System.IO
Public Class Common
    Public Const m_DefaultCulture As String = "en-US"
   Public Shared ReadOnly Property MyCulture() As String
        Get
            If HttpContext.Current.Session("MyCulture") Is Nothing OrElse String.IsNullOrEmpty(HttpContext.Current.Session("MyCulture").ToString()) Then
                Return m_DefaultCulture
            Else
                Return HttpContext.Current.Session("MyCulture").ToString()
            End If
        End Get
    End Property
    Public Shared Function SendMail(MailBody As String, EmailTo As String, MailFrom As String, Subject As String) As Boolean
        Try
            Dim msg As New MailMessage()

            EmailTo = EmailTo.Trim()

            If EmailTo.Contains(";") Or EmailTo.Contains(",") Then
                Dim deli As Char() = New Char(1) {}
                deli(0) = ";"c
                deli(1) = ","c

                Dim emails As String() = EmailTo.Split(deli)

                For Each toEmail As String In emails
                    msg.To.Add(toEmail)
                Next
            Else
                msg.[To].Add(EmailTo)
            End If

            msg.From = New MailAddress(MailFrom)
            msg.Subject = Subject
            msg.Body = MailBody
            msg.IsBodyHtml = True

            Dim smtp As New SmtpClient(ConfigurationManager.AppSettings("SMTP_HOST"), Integer.Parse(ConfigurationManager.AppSettings("SMTP_PORT")))
            smtp.EnableSsl = Boolean.Parse(ConfigurationManager.AppSettings("SMTP_EnableSsl"))
            smtp.Credentials = New NetworkCredential(ConfigurationManager.AppSettings("EMAIL_ACC"), ConfigurationManager.AppSettings("EMAIL_PWD"))
            smtp.Send(msg)

            msg.Dispose()
            Return True
        Catch ex As Exception
        End Try
        Return False
    End Function

    Public Shared Function SendMail(MailBody As String, EmailTo As String, MailFrom As String, Subject As String, attachments As Attachment()) As Boolean
        Try
            Dim msg As New MailMessage()

            If EmailTo.Contains(";") Or EmailTo.Contains(",") Then
                Dim deli As Char() = New Char(1) {}
                deli(0) = ";"c
                deli(1) = ","c

                Dim emails As String() = EmailTo.Split(deli)

                For Each toEmail As String In emails
                    msg.To.Add(toEmail)
                Next
            Else
                msg.To.Add(EmailTo)
            End If

            msg.From = New MailAddress(MailFrom)
            msg.Subject = Subject
            msg.Body = MailBody
            msg.IsBodyHtml = True

            For Each attchmnts As Attachment In attachments
                msg.Attachments.Add(attchmnts)
            Next

            Dim smtp As New SmtpClient(ConfigurationManager.AppSettings("SMTP_HOST"), Integer.Parse(ConfigurationManager.AppSettings("SMTP_PORT")))
            smtp.EnableSsl = Boolean.Parse(ConfigurationManager.AppSettings("SMTP_EnableSsl"))
            smtp.Credentials = New NetworkCredential(ConfigurationManager.AppSettings("EMAIL_ACC"), ConfigurationManager.AppSettings("EMAIL_PWD"))
            smtp.Send(msg)
            msg.[To].Clear()
            Return True

        Catch ex As Exception
        End Try
        Return False
    End Function

    Public Shared Function ProcessDateString(ByVal _date As String) As String
        Dim re() As String
        Dim c() As Char = {"/"c}
        re = _date.Split(c)
        If re(0).Length < 2 Then
            re(0) = "0" + re(0)
        End If
        If re(1).Length < 2 Then
            re(1) = "0" + re(1)
        End If
        Return re(0) + re(1) + re(2)
    End Function


    'Public Shared Function SendConfirmationEmail(ByRef r As Rate, ByVal pickupLoc As String, ByVal dropoffLoc As String) As Boolean
    '    Try

    '          Common.SendMail(GetConfirmationEmailBody(r, pickupLoc, dropoffLoc), ConfigurationManager.AppSettings("fromaddress"), "Ontrac - Confirmation# " + r.ConfirmationNumber)

    '        Return True
    '    Catch ex As Exception

    '    End Try

    '    Return False
    'End Function

    'Public Shared Function GetConfirmationEmailBody(ByRef r As Rate, ByVal pickupLoc As String, ByVal dropoffLoc As String) As String
    '    Dim sBody As String = ""

    '    If MyCulture = "es-US" Then
    '        sBody = File.ReadAllText(HttpContext.Current.Server.MapPath("~/EmailTemplate/ConfirmationEmail-ES.html"))
    '    Else
    '        sBody = File.ReadAllText(HttpContext.Current.Server.MapPath("~/EmailTemplate/ConfirmationEmail.html"))
    '    End If


    '    sBody = sBody.Replace("<%ConfirmationNumber%>", r.ConfirmationNumber)
    '    sBody = sBody.Replace("<%PickupLoc%>", pickupLoc)
    '    sBody = sBody.Replace("<%PickupDateTime%>", r.PickupDateTime.ToString("yyyy-MM-dd hh:mmtt"))
    '    sBody = sBody.Replace("<%DropoffLoc%>", dropoffLoc)
    '    sBody = sBody.Replace("<%DropoffDateTime%>", r.DropOffDateTime.ToString("yyyy-MM-dd hh:mmtt"))
    '    sBody = sBody.Replace("<%CarDesc%>", r.ModelDesc)
    '    sBody = sBody.Replace("<%RenterEmail%>", r.RenterEmail)
    '    sBody = sBody.Replace("<%RenterPhone%>", r.RenterPhone)
    '    sBody = sBody.Replace("<%RenterFirstName%>", r.RenterFirstName)
    '    sBody = sBody.Replace("<%RenterLastName%>", r.RenterLastName)


    '    Dim rentalDetail As String = ""

    '    rentalDetail = "<table style='width:100%;border-spacing:0;border-collapse:collapse;' cellpadding='0' cellspacing='0'>" + _
    '                        "<tr>" + _
    '                            "<td style='width:300px; border:1px solid #666666; background-color:#CCCCCC;'>" + _
    '                                "<strong>" + HttpContext.GetGlobalResourceObject("GlobalResource", "Quantity") + "</strong>" + _
    '                            "</td>" + _
    '                            "<td style='width:100px; border:1px solid #666666; background-color:#CCCCCC; text-align:right;'>" + _
    '                                "<strong>" + HttpContext.GetGlobalResourceObject("GlobalResource", "Rate") + "</strong>" + _
    '                            "</td>" + _
    '                            "<td style='width:150px; border:1px solid #666666; background-color:#CCCCCC; text-align:right;'>" + _
    '                                "<strong>" + HttpContext.GetGlobalResourceObject("GlobalResource", "Subtotal") + "</strong>" + _
    '                            "</td>" + _
    '                        "</tr>"

    '    If r.RentalMonths > 0 Then

    '        rentalDetail += "<tr>" + _
    '                            "<td style='width:300px; border:1px solid #666666; background-color:#CCCCCC;'>" + _
    '                                String.Format("{0:###0} {1}", r.RentalMonths, HttpContext.GetGlobalResourceObject("GlobalResource", "Months")) + _
    '                            "</td>" + _
    '                            "<td style='width:100px; border:1px solid #666666; text-align:right;'>" + _
    '                                String.Format("{0:$#,##0.00}", r.RatePerMonth) + _
    '                            "</td>" + _
    '                            "<td style='width:150px; border:1px solid #666666; text-align:right;'>" + _
    '                                String.Format("{0:$#,##0.00}", r.RentalMonths * r.RatePerMonth) + _
    '                            "</td>" + _
    '                        "</tr>"
    '    End If

    '    If r.RentalWeeks > 0 Then

    '        rentalDetail += "<tr>" + _
    '                            "<td style='width:300px; border:1px solid #666666; background-color:#CCCCCC;'>" + _
    '                                String.Format("{0:###0} {1}", r.RentalWeeks, HttpContext.GetGlobalResourceObject("GlobalResource", "Weeks")) + _
    '                            "</td>" + _
    '                            "<td style='width:100px; border:1px solid #666666; text-align:right;'>" + _
    '                                String.Format("{0:$#,##0.00}", r.RatePerWeek) + _
    '                            "</td>" + _
    '                            "<td style='width:150px; border:1px solid #666666; text-align:right;'>" + _
    '                                String.Format("{0:$#,##0.00}", r.RentalWeeks * r.RatePerWeek) + _
    '                            "</td>" + _
    '                        "</tr>"
    '    End If

    '    If r.RentalDays > 0 Then

    '        rentalDetail += "<tr>" + _
    '                            "<td style='width:300px; border:1px solid #666666; background-color:#CCCCCC;'>" + _
    '                                String.Format("{0:###0} {1}", r.RentalDays, HttpContext.GetGlobalResourceObject("GlobalResource", "Days")) + _
    '                            "</td>" + _
    '                            "<td style='width:100px; border:1px solid #666666; text-align:right;'>" + _
    '                                String.Format("{0:$#,##0.00}", r.RatePerDay) + _
    '                            "</td>" + _
    '                            "<td style='width:150px; border:1px solid #666666; text-align:right;'>" + _
    '                                String.Format("{0:$#,##0.00}", r.RentalDays * r.RatePerDay) + _
    '                            "</td>" + _
    '                        "</tr>"
    '    End If

    '    If r.RentalHours > 0 Then

    '        rentalDetail += "<tr>" + _
    '                            "<td style='width:300px; border:1px solid #666666; background-color:#CCCCCC;'>" + _
    '                                String.Format("{0:###0} {1}", r.RentalHours, HttpContext.GetGlobalResourceObject("GlobalResource", "Hours")) + _
    '                            "</td>" + _
    '                            "<td style='width:100px; border:1px solid #666666; text-align:right;'>" + _
    '                                String.Format("{0:$#,##0.00}", r.RatePerHour) + _
    '                            "</td>" + _
    '                            "<td style='width:150px; border:1px solid #666666; text-align:right;'>" + _
    '                                String.Format("{0:$#,##0.00}", r.RentalHours * r.RatePerHour) + _
    '                            "</td>" + _
    '                        "</tr>"
    '    End If

    '    rentalDetail += "    <tr>" + _
    '                            "<td style='width:300px; border:1px solid #666666; background-color:#CCCCCC;'>" + _
    '                                "<strong>" + HttpContext.GetGlobalResourceObject("GlobalResource", "RentalRate") + "</strong>" + _
    '                            "</td>" + _
    '                            "<td style='width:100px; border:1px solid #666666; text-align:right;'>" + _
    '                            "</td>" + _
    '                            "<td style='width:150px; border:1px solid #666666; text-align:right;'>" + _
    '                                String.Format("{0:$#,##0.00}", r.RatePlusLate) + _
    '                            "</td>" + _
    '                        "</tr>" + _
    '                        "<tr>" + _
    '                            "<td style='width:300px; border:1px solid #666666; background-color:#CCCCCC;'>" + _
    '                                HttpContext.GetGlobalResourceObject("GlobalResource", "IncludedKM") + _
    '                            "</td>" + _
    '                            "<td style='width:100px; border:1px solid #666666; text-align:right;'>" + _
    '                                String.Format("{0:###0.00}", r.TotalFreeMiles) + _
    '                            "</td>" + _
    '                            "<td style='width:150px; border:1px solid #666666; text-align:right;'>" + _
    '                            "</td>" + _
    '                        "</tr>"

    '    For Each extra As ExtraRate In r.ExtraRates
    '        rentalDetail += "<tr>" + _
    '                            "<td style='width:300px; border:1px solid #666666; background-color:#CCCCCC;'>" + _
    '                                extra.ExtraDescription + _
    '                            "</td>" + _
    '                            "<td style='width:100px; border:1px solid #666666; text-align:right;'>" + _
    '                            "</td>" + _
    '                            "<td style='width:150px; border:1px solid #666666; text-align:right;'>" + _
    '                                String.Format("{0:$#,##0.00}", extra.ExtraAmount * extra.Qty) + _
    '                            "</td>" + _
    '                        "</tr>"
    '    Next

    '    rentalDetail += "    <tr>" + _
    '                            "<td style='width:300px; border:1px solid #666666; background-color:#CCCCCC;'>" + _
    '                                HttpContext.GetGlobalResourceObject("GlobalResource", "GST") + _
    '                            "</td>" + _
    '                            "<td style='width:100px; border:1px solid #666666; text-align:right;'>" + _
    '                                String.Format("{0:#,##0.00%}", r.GSTRate) + _
    '                            "</td>" + _
    '                            "<td style='width:150px; border:1px solid #666666; text-align:right;'>" + _
    '                                String.Format("{0:$#,##0.00}", r.GSTAmount) + _
    '                            "</td>" + _
    '                        "</tr>" + _
    '                        "<tr>" + _
    '                            "<td style='width:300px; border:1px solid #666666; background-color:#CCCCCC;'>" + _
    '                                HttpContext.GetGlobalResourceObject("GlobalResource", "PST") + _
    '                            "</td>" + _
    '                            "<td style='width:100px; border:1px solid #666666; text-align:right;'>" + _
    '                                String.Format("{0:#,##0.00%}", r.PSTRate) + _
    '                            "</td>" + _
    '                            "<td style='width:150px; border:1px solid #666666; text-align:right;'>" + _
    '                                String.Format("{0:$#,##0.00}", r.PSTAmount) + _
    '                            "</td>" + _
    '                        "</tr>" + _
    '                        "<tr>" + _
    '                            "<td style='width:300px; border:1px solid #666666;'>" + _
    '                                "<strong>" + HttpContext.GetGlobalResourceObject("GlobalResource", "EstTotal") + "</strong>" + _
    '                            "</td>" + _
    '                            "<td style='width:100px; border:1px solid #666666; color:red; text-align:right;'>" + _
    '                                "CAN $" + _
    '                            "</td>" + _
    '                            "<td style='width:150px; border:1px solid #666666; text-align:right;'>" + _
    '                                String.Format("{0:$#,##0.00}", r.TotalCharges) + _
    '                            "</td>" + _
    '                        "</tr>" + _
    '                    "</table>"

    '    sBody = sBody.Replace("<%RentalDetail%>", rentalDetail)

    '    Return sBody
    'End Function


    Public Shared Function FormatText(txt As String) As String
        If Len(txt) > 4 Then
            txt = txt.Replace("(Li)", "<li>")
            txt = txt.Replace("(/li)", "</li>")
        Else
            txt = ""
        End If

        Return txt
    End Function
End Class
