﻿Imports Microsoft.VisualBasic
Imports System.Runtime.CompilerServices

Public Class Extentions

    Public Shared Function ChangeCurrency(Amount As Double) As String
        Dim strAmount = String.Empty
        Dim currency = 1
        If Not HttpContext.Current.Session("Currency") Is Nothing Then
            currency = HttpContext.Current.Session("Currency")
        End If
        Amount = Amount / currency
        strAmount = Amount.ToString("N2")
        strAmount = String.Format("{0}{1}", "$", strAmount)
        Return strAmount
    End Function
    Public Shared Sub GetCurrency()
        If HttpContext.Current.Session("Currency") Is Nothing Or HttpContext.Current.Session("Currency") = 1 Then

            HttpContext.Current.Response.Write("ARS")

        Else
            HttpContext.Current.Response.Write("USD")

        End If

    End Sub
    Public Shared Function CurrencyType() As String
        Dim result
        If HttpContext.Current.Session("Currency") Is Nothing Or HttpContext.Current.Session("Currency") = 1 Then

            result = "ARS"

        Else
            result = "USD"

        End If
        Return result
    End Function
End Class
