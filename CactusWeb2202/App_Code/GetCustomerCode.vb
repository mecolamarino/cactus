﻿Imports Microsoft.VisualBasic

Public Class GetCustomerCode


    Shared Function GetCode() As String

        Dim htRentalParam As Hashtable = Nothing

        htRentalParam = TryCast(System.Web.HttpContext.Current.Session("_htRentalParam"), Hashtable)
        If Not IsNothing(htRentalParam) Then
            Dim code As String
            code = htRentalParam("PickupLocation")
            If code = "MCO" Then
                Return System.Configuration.ConfigurationManager.AppSettings("OrlandoCode")
            Else

                Return System.Configuration.ConfigurationManager.AppSettings("CustomerNumber")
            End If

        End If

        Return System.Configuration.ConfigurationManager.AppSettings("CustomerNumber")

    End Function




End Class
