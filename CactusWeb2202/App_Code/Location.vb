﻿Imports Microsoft.VisualBasic
Imports System.Net
Imports System.Web.Script.Serialization


Public Class Locations
    Public Property IPAddress() As String
        Get
            Return m_IPAddress
        End Get
        Set(value As String)
            m_IPAddress = value
        End Set
    End Property
    Private m_IPAddress As String
    Public Property CountryName() As String
        Get
            Return m_CountryName
        End Get
        Set(value As String)
            m_CountryName = value
        End Set
    End Property
    Private m_CountryName As String
    Public Property CountryCode() As String
        Get
            Return m_CountryCode
        End Get
        Set(value As String)
            m_CountryCode = value
        End Set
    End Property
    Private m_CountryCode As String
    Public Property CityName() As String
        Get
            Return m_CityName
        End Get
        Set(value As String)
            m_CityName = value
        End Set
    End Property
    Private m_CityName As String
    Public Property RegionName() As String
        Get
            Return m_RegionName
        End Get
        Set(value As String)
            m_RegionName = value
        End Set
    End Property
    Private m_RegionName As String
    Public Property ZipCode() As String
        Get
            Return m_ZipCode
        End Get
        Set(value As String)
            m_ZipCode = value
        End Set
    End Property
    Private m_ZipCode As String
    Public Property Latitude() As String
        Get
            Return m_Latitude
        End Get
        Set(value As String)
            m_Latitude = value
        End Set
    End Property
    Private m_Latitude As String
    Public Property Longitude() As String
        Get
            Return m_Longitude
        End Get
        Set(value As String)
            m_Longitude = value
        End Set
    End Property
    Private m_Longitude As String
    Public Property TimeZone() As String
        Get
            Return m_TimeZone
        End Get
        Set(value As String)
            m_TimeZone = value
        End Set
    End Property
    Private m_TimeZone As String

    Public Sub New()

    End Sub
    Public Shared Function GetGioLocation() As String
        Try
            Dim ipAddress As String = HttpContext.Current.Request.ServerVariables("HTTP_X_FORWARDED_FOR")
            If String.IsNullOrEmpty(ipAddress) Then
                ipAddress = HttpContext.Current.Request.ServerVariables("REMOTE_ADDR")
            End If
            If HttpContext.Current.Request.IsLocal Then
                ipAddress = "12.215.42.19" '"127.0.0.1"
            End If

            Dim APIKey As String = "30714549e9729eaa60b3a07a88304d168fcbf11a4ac76cd7bcb214bb0232ba65"
            Dim url1 As String = String.Format("http://api.ipinfodb.com/v3/ip-city/?key={0}&ip={1}&format=json", APIKey, ipAddress)
            Using client As New WebClient()
                Dim json As String = client.DownloadString(url1)
                Dim location As Locations = New JavaScriptSerializer().Deserialize(Of Locations)(json)
                Return location.CountryCode
            End Using
        Catch ex As Exception
            Return ""
        End Try

    End Function
End Class



