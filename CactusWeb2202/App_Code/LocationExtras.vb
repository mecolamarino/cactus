﻿Imports Microsoft.VisualBasic

Public Class LocationExtras
    Public Sub New(ByVal extraCode As String, ByVal extraDesc As String, ByVal extraAmount As String)
        Me.ExtraCode = extraCode
        Me.ExtraDesc = extraDesc
        Me.ExtraAmount = extraAmount
    End Sub

    Private _extraCode As String
    Public Property ExtraCode() As String
        Get
            Return _extraCode
        End Get
        Set(ByVal value As String)
            _extraCode = value
        End Set
    End Property

    Private _extraDesc As String
    Public Property ExtraDesc() As String
        Get
            Return _extraDesc
        End Get
        Set(ByVal value As String)
            _extraDesc = value
        End Set
    End Property

    Private _extraAmount As String
    Public Property ExtraAmount() As String
        Get
            Return _extraAmount
        End Get
        Set(ByVal value As String)
            _extraAmount = value
        End Set
    End Property

End Class
