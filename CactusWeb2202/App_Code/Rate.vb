﻿Public Class Rate
    Public Sub New()

    End Sub
    Public Sub New(ByVal id As Integer, ByVal code As String, ByVal desc As String, ByVal ratePlan As String, ByVal rateCode As String, ByVal days As Integer, ByVal amount As Double, ByVal prepaidAmount As Double, ByVal prepaidBaseDiscount As Double, ByVal dayMiles As Double, ByVal totalMiles As Double, ByVal rateCharge As Double, ByVal tax As Double, ByVal totalCharge As Double, ByVal prepaidTotalCharge As Double, ByVal ch As String, ByVal image As String, ByVal rateDiscount As Double, ByVal modeldesc As String, ratepermile As String, ByVal ratperdays As Double)
        'Public Sub New(ByVal id As Integer, ByVal pickupLocID As Integer, ByVal dropOffLocID As Integer, ByVal picDate As DateTime, ByVal dropDate As DateTime, ByVal code As String, ByVal desc As String, ByVal ratePlan As String, ByVal rateCode As String, ByVal days As Integer, ByVal amount As Double, ByVal dayMiles As Double, ByVal totalMiles As Double, ByVal rateCharge As Double, ByVal tax As Double, ByVal totalCharge As Double, ByVal ch As String, ByVal image As String, ByVal rateDiscount As Double, ByVal modeldesc As String, ratepermile As String)
        Me.ClassCode = code
        Me.ClassDescription = desc
        Me.MilesIncluded = dayMiles
        Me.RateAmount = amount
        Me.PrepaidAmount = prepaidAmount
        Me.PrepaidTotalCharge = prepaidTotalCharge
        Me.RateCharge = rateCharge
        Me.RateID = id
        Me.RatePlan = ratePlan
        Me.RentalDays = days
        Me.RateCode = rateCode
        Me.TotalCharge = totalCharge
        Me.TotalFreeMiles = totalMiles
        Me.TotalTax = TotalTax
        Me.CH = ch
        Me.Image = image
        'Me.OrderNum = RequestMaker.ClassOrders.Item(code)
        Me.RateDiscount = rateDiscount
        Me.modeldesc = modeldesc
        Me.ratepermile = ratepermile
        Me.RatePerDay = ratperdays
        _prepaidbasediscount = prepaidBaseDiscount

        'Limit the prepaid total to the base discount or 0 if none is found
        AdjustDiscount()

        '  Me.messagedesc = messagedesc  
        'Me.PickupLocation = pickupLocation
    End Sub

    Private Sub AdjustDiscount()
        'Limit the prepaid total to the base discount or 0 if none is found
        If _prepaidbasediscount = 0.0 Then
            _prepaidtotalcharge = _totalCharge
            _prepaidamount = _rateAmount
        End If

        If (_totalCharge - _prepaidtotalcharge) / _totalCharge > (_prepaidbasediscount + 0.005) Then
            _prepaidtotalcharge = _totalCharge * (1 - _prepaidbasediscount)
        End If

    End Sub

    Private _ratepermile As String
    Public Property ratepermile As String
        Get
            Return _ratepermile
        End Get
        Set(ByVal value As String)
            _ratepermile = value
        End Set
    End Property

    Private _modeldesc As String
    Public Property modeldesc() As String
        Get
            Return _modeldesc
        End Get
        Set(ByVal value As String)
            _modeldesc = value
        End Set
    End Property
    Private _rateID As Integer
    Public Property RateID() As Integer
        Get
            Return _rateID
        End Get
        Set(ByVal value As Integer)
            _rateID = value
        End Set
    End Property


    Private _ClassCode As String
    Public Property ClassCode() As String
        Get
            Return _ClassCode
        End Get
        Set(ByVal value As String)
            _ClassCode = value
        End Set
    End Property


    Private _ClassDescription As String
    Public Property ClassDescription() As String
        Get
            Return _ClassDescription
        End Get
        Set(ByVal value As String)
            _ClassDescription = value
        End Set
    End Property


    Private _rateCode As String
    Public Property RateCode() As String
        Get
            Return _rateCode
        End Get
        Set(ByVal value As String)
            _rateCode = value
        End Set
    End Property


    Private _ratePlan As String
    Public Property RatePlan() As String
        Get
            Return _ratePlan
        End Get
        Set(ByVal value As String)
            _ratePlan = value
        End Set
    End Property


    Private _rentalDays As Integer
    Public Property RentalDays() As Integer
        Get
            Return _rentalDays
        End Get
        Set(ByVal value As Integer)
            _rentalDays = value
        End Set
    End Property


    Private _rateAmount As Double
    Public Property RateAmount() As Double
        Get
            Return _rateAmount
        End Get
        Set(ByVal value As Double)
            _rateAmount = value
        End Set
    End Property

    Private _prepaidamount As Double
    Public Property PrepaidAmount() As Double
        Get
            Return _prepaidamount
        End Get
        Set(ByVal value As Double)
            _prepaidamount = value
        End Set
    End Property

    Private _rateDiscount As Double
    Public Property RateDiscount() As Double
        Get
            Return _rateDiscount
        End Get
        Set(ByVal value As Double)
            _rateDiscount = value
        End Set
    End Property

    Private _milesIncluded As Double
    Public Property MilesIncluded() As Double
        Get
            Return _milesIncluded
        End Get
        Set(ByVal value As Double)
            _milesIncluded = value
        End Set
    End Property


    Private _totalFreeMiles As Double
    Public Property TotalFreeMiles() As Double
        Get
            Return _totalFreeMiles
        End Get
        Set(ByVal value As Double)
            _totalFreeMiles = value
        End Set
    End Property


    Private _rateCharge As Double
    Public Property RateCharge() As Double
        Get
            Return _rateCharge
        End Get
        Set(ByVal value As Double)
            _rateCharge = value
        End Set
    End Property


    Private _totalTax As Double
    Public Property TotalTax() As Double
        Get
            Return _totalTax
        End Get
        Set(ByVal value As Double)
            _totalTax = value
        End Set
    End Property


    Private _totalCharge As Double
    Public Property TotalCharge() As Double
        Get
            Return _totalCharge
        End Get
        Set(ByVal value As Double)
            _totalCharge = value
        End Set
    End Property

    Private _prepaidtotalcharge As Double
    Public Property PrepaidTotalCharge() As Double
        Get
            Return _prepaidtotalcharge
        End Get
        Set(ByVal value As Double)
            _prepaidtotalcharge = value
        End Set
    End Property


    Private _ch As String
    Public Property CH() As String
        Get
            Return _ch
        End Get
        Set(ByVal value As String)
            _ch = value
        End Set
    End Property


    Private _image As String
    Public Property Image() As String
        Get
            Return _image
        End Get
        Set(ByVal value As String)
            _image = value
        End Set
    End Property


    Private _orderNum As Integer
    Public Property OrderNum() As Integer
        Get
            Return _orderNum
        End Get
        Set(ByVal value As Integer)
            _orderNum = value
        End Set
    End Property

    Private _pickupLocation As String
    Public Property PickupLocation() As String
        Get
            Return _pickupLocation
        End Get
        Set(ByVal value As String)
            _pickupLocation = value
        End Set
    End Property
    Private _rateperday As Double
    Public Property RatePerDay() As Double
        Get
            Return _rateperday
        End Get
        Set(ByVal value As Double)
            _rateperday = value
        End Set
    End Property

    Private Property _prepaidbasediscount As Double

End Class

Public Class ExtraRate
    Public Sub New(ByVal extraCode As String, ByVal rate As Double, ByVal qty As Int16, ByVal autoapply As String)
        Me._extraCode = extraCode
        Me._rate = rate
        Me._qty = qty
        Me._autoapply = autoapply
    End Sub
    Private _autoapply As String
    Public Property autoapply() As String
        Get
            Return _autoapply
        End Get
        Set(ByVal value As String)
            _autoapply = value
        End Set
    End Property

    Private _extraCode As String
    Public Property ExtraCode() As String
        Get
            Return _extraCode
        End Get
        Set(ByVal value As String)
            _extraCode = value
        End Set
    End Property

    Private _rate As Integer
    Public Property Rate() As Double
        Get
            Return _rate
        End Get
        Set(ByVal value As Double)
            _rate = value
        End Set
    End Property

    Private _qty As Integer
    Public Property Qty() As Integer
        Get
            Return _qty
        End Get
        Set(ByVal value As Integer)
            _qty = value
        End Set
    End Property


End Class