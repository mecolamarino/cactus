﻿Public Class RateJEEp
    Public Sub New()

    End Sub
    Public Sub New(ByVal rateId As Integer, ByVal pickupLoc As String, ByVal dropOffLoc As String, ByVal pickupDateTime As DateTime, ByVal dropOffDateTime As DateTime, ByVal classCode As String, ByVal classDesc As String, ByVal modeldesc As String, ByVal ratePlan As String, ByVal rateCode As String, ByVal rentalMonths As Integer, ByVal ratePerMonth As Double, ByVal rentaldays As Integer, ByVal ratePerDay As Double, ByVal rentalHours As Integer, ByVal ratePerHour As Double, ByVal rentalweeks As Integer, ByVal ratePerweek As Double, ByVal dayMiles As Double, ByVal totalMiles As Double, ByVal ratePlusLate As Double, ByVal gstRate As Double, ByVal gstAmount As Double, ByVal pstRate As Double, ByVal pstAmount As Double, ByVal totalTaxes As Double, ByVal totalExtras As Double, ByVal totalCharges As Double, ByVal image As String, ByVal rateDiscount As Double, ByVal passengers As Integer, ByVal luggage As Integer, ByVal promoCode As String, classNotes As String, mpgCity As String, mpgHighway As String, ratecharge As Double, ByVal rate1days As Integer, ByVal Rate1PerDay As Double)
        Me.RateID = rateId
        Me.PickupLocation = pickupLoc
        Me.DropOffLocation = dropOffLoc
        Me.PickupDateTime = pickupDateTime
        Me.DropOffDateTime = dropOffDateTime
        Me.ClassCode = classCode
        Me.ClassDescription = classDesc
        Me.MilesIncluded = dayMiles
        Me.RatePlan = ratePlan
        Me.RentalMonths = rentalMonths
        Me.RatePerMonth = ratePerMonth
        Me.RentalDays = rentaldays
        Me.RatePerDay = ratePerDay
        Me.RentalHours = rentalHours
        Me.RatePerHour = ratePerHour
        Me.RentalWeeks = rentalweeks
        Me.ratePerWeek = ratePerweek
        Me.RateCode = rateCode
        Me.RatePlusLate = ratePlusLate 'rateCharge
        Me.GSTRate = gstRate
        Me.GSTAmount = gstAmount
        Me.PSTRate = pstRate
        Me.PSTAmount = pstAmount
        Me.TotalTaxes = totalTaxes
        Me.TotalExtras = totalExtras
        Me.TotalCharges = totalCharges
        Me.TotalFreeMiles = totalMiles
        Me.Image = image
        Me.RateDiscount = rateDiscount
        Me.ModelDesc = modeldesc
        Me.Passengers = passengers
        Me.Luggage = luggage
        Me.PromoCode = promoCode
        Me.classNotes = classNotes
        Me.mpgCity = mpgCity
        Me.mpgHighway = mpgHighway
        Me.RateCharge = ratecharge
        Me.rate1days = rate1days
        Me.Rate1PerDay = Rate1PerDay
        '   Me.OrderNum = RequestMaker.ClassOrders.Item(classCode)

    End Sub


    Private _Rate1PerDay As Double
    Public Property Rate1PerDay As Double
        Get
            Return _Rate1PerDay
        End Get
        Set(ByVal value As Double)
            _Rate1PerDay = value
        End Set
    End Property

    Private _rate1days As Double
    Public Property rate1days() As Integer
        Get
            Return _rate1days
        End Get
        Set(ByVal value As Integer)
            _rate1days = value
        End Set
    End Property

    Private _RateCharge As Double
    Public Property RateCharge As Double
        Get
            Return _RateCharge
        End Get
        Set(ByVal value As Double)
            _RateCharge = value
        End Set
    End Property

    Private _mpgCity As String
    Public Property mpgCity() As String
        Get
            Return _mpgCity
        End Get
        Set(ByVal value As String)
            _mpgCity = value
        End Set
    End Property

    Private _mpgHighway As String
    Public Property mpgHighway() As String
        Get
            Return _mpgHighway
        End Get
        Set(ByVal value As String)
            _mpgHighway = value
        End Set
    End Property


    Private _classNotes As String
    Public Property classNotes() As String
        Get
            Return _classNotes
        End Get
        Set(ByVal value As String)
            _classNotes = value
        End Set
    End Property

    Private _pickupLocation As String
    Public Property PickupLocation() As String
        Get
            Return _pickupLocation
        End Get
        Set(ByVal value As String)
            _pickupLocation = value
        End Set
    End Property

    Private _dropOffLocation As String
    Public Property DropOffLocation() As String
        Get
            Return _dropOffLocation
        End Get
        Set(ByVal value As String)
            _dropOffLocation = value
        End Set
    End Property

    Private _pickupDateTime As DateTime
    Public Property PickupDateTime() As DateTime
        Get
            Return _pickupDateTime
        End Get
        Set(ByVal value As DateTime)
            _pickupDateTime = value
        End Set
    End Property

    Private _dropOffDateTime As DateTime
    Public Property DropOffDateTime() As DateTime
        Get
            Return _dropOffDateTime
        End Get
        Set(ByVal value As DateTime)
            _dropOffDateTime = value
        End Set
    End Property

    Private _modeldesc As String
    Public Property ModelDesc() As String
        Get
            Return _modeldesc
        End Get
        Set(ByVal value As String)
            _modeldesc = value
        End Set
    End Property

    Private _rateID As Integer
    Public Property RateID() As Integer
        Get
            Return _rateID
        End Get
        Set(ByVal value As Integer)
            _rateID = value
        End Set
    End Property

    Private _ClassCode As String
    Public Property ClassCode() As String
        Get
            Return _ClassCode
        End Get
        Set(ByVal value As String)
            _ClassCode = value
        End Set
    End Property

    Private _ClassDescription As String
    Public Property ClassDescription() As String
        Get
            Return _ClassDescription
        End Get
        Set(ByVal value As String)
            _ClassDescription = value
        End Set
    End Property


    Private _rateCode As String
    Public Property RateCode() As String
        Get
            Return _rateCode
        End Get
        Set(ByVal value As String)
            _rateCode = value
        End Set
    End Property


    Private _ratePlan As String
    Public Property RatePlan() As String
        Get
            Return _ratePlan
        End Get
        Set(ByVal value As String)
            _ratePlan = value
        End Set
    End Property

    Private _rentalMonths As Integer
    Public Property RentalMonths() As Integer
        Get
            Return _rentalMonths
        End Get
        Set(ByVal value As Integer)
            _rentalMonths = value
        End Set
    End Property

    Private _ratePerMonth As Double
    Public Property RatePerMonth() As Double
        Get
            Return _ratePerMonth
        End Get
        Set(ByVal value As Double)
            _ratePerMonth = value
        End Set
    End Property

    Private _rentalDays As Integer
    Public Property RentalDays() As Integer
        Get
            Return _rentalDays
        End Get
        Set(ByVal value As Integer)
            _rentalDays = value
        End Set
    End Property

    Private _ratePerDay As Double
    Public Property RatePerDay() As Double
        Get
            Return _ratePerDay
        End Get
        Set(ByVal value As Double)
            _ratePerDay = value
        End Set
    End Property

    Private _RentalWeeks As Double
    Public Property RentalWeeks() As Integer
        Get
            Return _RentalWeeks
        End Get
        Set(ByVal value As Integer)
            _RentalWeeks = value
        End Set
    End Property

    Private _ratePerWeek As Double
    Public Property ratePerWeek() As Double
        Get
            Return _ratePerWeek
        End Get
        Set(ByVal value As Double)
            _ratePerWeek = value
        End Set
    End Property



    Private _rentalHours As Integer
    Public Property RentalHours() As Integer
        Get
            Return _rentalHours
        End Get
        Set(ByVal value As Integer)
            _rentalHours = value
        End Set
    End Property

    Private _ratePerHour As Double
    Public Property RatePerHour() As Double
        Get
            Return _ratePerHour
        End Get
        Set(ByVal value As Double)
            _ratePerHour = value
        End Set
    End Property

    Private _rateDiscount As Double
    Public Property RateDiscount() As Double
        Get
            Return _rateDiscount
        End Get
        Set(ByVal value As Double)
            _rateDiscount = value
        End Set
    End Property

    Private _milesIncluded As Double
    Public Property MilesIncluded() As Double
        Get
            Return _milesIncluded
        End Get
        Set(ByVal value As Double)
            _milesIncluded = value
        End Set
    End Property


    Private _totalFreeMiles As Double
    Public Property TotalFreeMiles() As Double
        Get
            Return _totalFreeMiles
        End Get
        Set(ByVal value As Double)
            _totalFreeMiles = value
        End Set
    End Property


    Private _ratePlusLate As Double
    Public Property RatePlusLate() As Double
        Get
            Return _ratePlusLate
        End Get
        Set(ByVal value As Double)
            _ratePlusLate = value
        End Set
    End Property

    Private _gstRate As Double
    Public Property GSTRate() As Double
        Get
            Return _gstRate
        End Get
        Set(ByVal value As Double)
            _gstRate = value
        End Set
    End Property

    Private _gstAmount As Double
    Public Property GSTAmount() As Double
        Get
            Return _gstAmount
        End Get
        Set(ByVal value As Double)
            _gstAmount = value
        End Set
    End Property

    Private _pstRate As Double
    Public Property PSTRate() As Double
        Get
            Return _pstRate
        End Get
        Set(ByVal value As Double)
            _pstRate = value
        End Set
    End Property

    Private _pstAmount As Double
    Public Property PSTAmount() As Double
        Get
            Return _pstAmount
        End Get
        Set(ByVal value As Double)
            _pstAmount = value
        End Set
    End Property

    Private _totalTaxes As Double
    Public Property TotalTaxes() As Double
        Get
            Return _totalTaxes
        End Get
        Set(ByVal value As Double)
            _totalTaxes = value
        End Set
    End Property

    Private _totalExtras As Double
    Public Property TotalExtras() As Double
        Get
            Return _totalExtras
        End Get
        Set(ByVal value As Double)
            _totalExtras = value
        End Set
    End Property

    Private _totalCharges As Double
    Public Property TotalCharges() As Double
        Get
            Return _totalCharges
        End Get
        Set(ByVal value As Double)
            _totalCharges = value
        End Set
    End Property

    Private _image As String
    Public Property Image() As String
        Get
            Return _image
        End Get
        Set(ByVal value As String)
            _image = value
        End Set
    End Property


    Private _orderNum As Double
    Public Property OrderNum() As Double
        Get
            Return _orderNum
        End Get
        Set(ByVal value As Double)
            _orderNum = value
        End Set
    End Property

    Private _Passengers As Integer
    Public Property Passengers() As Integer
        Get
            Return _Passengers
        End Get
        Set(ByVal value As Integer)
            _Passengers = value
        End Set
    End Property

    Private _Luggage As Integer
    Public Property Luggage() As Integer
        Get
            Return _Luggage
        End Get
        Set(ByVal value As Integer)
            _Luggage = value
        End Set
    End Property

    Private _PromoCode As String
    Public Property PromoCode() As String
        Get
            Return _PromoCode
        End Get
        Set(ByVal value As String)
            _PromoCode = value
        End Set
    End Property

    Private _PromoPrice As Double
    Public Property PromoPrice() As Double
        Get
            Return _PromoPrice
        End Get
        Set(ByVal value As Double)
            _PromoPrice = value
        End Set
    End Property

    Private _extrasCodes As ArrayList
    Public Property ExtrasCodes() As ArrayList
        Get
            Return _extrasCodes
        End Get
        Set(ByVal value As ArrayList)
            _extrasCodes = value
        End Set
    End Property

    Private _extraRates As List(Of ExtraRate)
    Public Property ExtraRates() As List(Of ExtraRate)
        Get
            Return _extraRates
        End Get
        Set(value As List(Of ExtraRate))
            _extraRates = value
        End Set
    End Property

    Private _renterFirstName As String
    Public Property RenterFirstName() As String
        Get
            Return _renterFirstName
        End Get
        Set(value As String)
            _renterFirstName = value
        End Set
    End Property

    Private _renterLastName As String
    Public Property RenterLastName() As String
        Get
            Return _renterLastName
        End Get
        Set(value As String)
            _renterLastName = value
        End Set
    End Property

    Private _renterEmail As String
    Public Property RenterEmail() As String
        Get
            Return _renterEmail
        End Get
        Set(value As String)
            _renterEmail = value
        End Set
    End Property

    Private _renterPhone As String
    Public Property RenterPhone() As String
        Get
            Return _renterPhone
        End Get
        Set(value As String)
            _renterPhone = value
        End Set
    End Property

    Private _confirmationNumber As String
    Public Property ConfirmationNumber() As String
        Get
            Return _confirmationNumber
        End Get
        Set(value As String)
            _confirmationNumber = value
        End Set
    End Property


End Class

Public Class ExtraRate1
    Public Sub New(ByVal extraCode As String, ByVal extraDescription As String, ByVal extraAmount As Double, ByVal qty As Int16)
        Me.ExtraCode = extraCode
        Me.ExtraDescription = extraDescription
        Me.ExtraAmount = extraAmount
        Me.Qty = qty
    End Sub

    Private _extraCode As String
    Public Property ExtraCode() As String
        Get
            Return _extraCode
        End Get
        Set(ByVal value As String)
            _extraCode = value
        End Set
    End Property

    Private _extraDescription As String
    Public Property ExtraDescription() As String
        Get
            Return _extraDescription
        End Get
        Set(ByVal value As String)
            _extraDescription = value
        End Set
    End Property

    Private _extraAmount As Double
    Public Property ExtraAmount() As Double
        Get
            Return _extraAmount
        End Get
        Set(ByVal value As Double)
            _extraAmount = value
        End Set
    End Property

    Private _qty As Integer
    Public Property Qty() As Integer
        Get
            Return _qty
        End Get
        Set(ByVal value As Integer)
            _qty = value
        End Set
    End Property


    Private _address As String
    Public Property address() As String
        Get
            Return _address
        End Get
        Set(ByVal value As String)
            _address = value
        End Set
    End Property

    Private _city As String
    Public Property city() As String
        Get
            Return _city
        End Get
        Set(ByVal value As String)
            _city = value
        End Set
    End Property

    Private _state As String
    Public Property state() As String
        Get
            Return _state
        End Get
        Set(ByVal value As String)
            _state = value
        End Set
    End Property

    Private _zip As String
    Public Property zip() As String
        Get
            Return _zip
        End Get
        Set(ByVal value As String)
            _zip = value
        End Set
    End Property

    Private _country As String
    Public Property country() As String
        Get
            Return _country
        End Get
        Set(ByVal value As String)
            _country = value
        End Set
    End Property

    Private _license As String
    Public Property license() As String
        Get
            Return _license
        End Get
        Set(ByVal value As String)
            _license = value
        End Set
    End Property

    Private _licenseexp As String
    Public Property licenseexp() As String
        Get
            Return _licenseexp
        End Get
        Set(ByVal value As String)
            _licenseexp = value
        End Set
    End Property

    Private _dob As String
    Public Property dob() As String
        Get
            Return _dob
        End Get
        Set(ByVal value As String)
            _dob = value
        End Set
    End Property

    Private _phone As String
    Public Property phone() As String
        Get
            Return _phone
        End Get
        Set(ByVal value As String)
            _phone = value
        End Set
    End Property

    Private _firstname As String
    Public Property firstname() As String
        Get
            Return _firstname
        End Get
        Set(ByVal value As String)
            _firstname = value
        End Set
    End Property

    Private _lastname As String
    Public Property lastname() As String
        Get
            Return _lastname
        End Get
        Set(ByVal value As String)
            _lastname = value
        End Set
    End Property

    Private _email As String
    Public Property email() As String
        Get
            Return _email
        End Get
        Set(ByVal value As String)
            _email = value
        End Set
    End Property

    'Private _extraDescription As String
    'Public Property ExtraDescription() As String
    '    Get
    '        Return _extraDescription
    '    End Get
    '    Set(ByVal value As String)
    '        _extraDescription = value
    '    End Set
    'End Property



End Class

