﻿Imports Microsoft.VisualBasic

Public Class RentalLocation

    Public Sub New(ByVal rentalLocationID As String, ByVal rentalLocationName As String, ByVal rentalVehicleProvider As String, ByVal latitude As String, ByVal longitude As String)
        Me.RentalLocationID = rentalLocationID
        Me.RentalLocationName = rentalLocationName
        Me.RentalVehicleProvider = rentalVehicleProvider
        Me.Latitude = latitude
        Me.Longitude = longitude
    End Sub

    Private _rentalLocationID As String
    Public Property RentalLocationID() As String
        Get
            Return _rentalLocationID
        End Get
        Set(ByVal value As String)
            _rentalLocationID = value
        End Set
    End Property

    Private _rentalLocationName As String
    Public Property RentalLocationName() As String
        Get
            Return _rentalLocationName
        End Get
        Set(ByVal value As String)
            _rentalLocationName = value
        End Set
    End Property

    Private _rentalVehicleProvider As String
    Public Property RentalVehicleProvider() As String
        Get
            Return _rentalVehicleProvider
        End Get
        Set(ByVal value As String)
            _rentalVehicleProvider = value
        End Set
    End Property

    Private _latitude As String
    Public Property Latitude() As String
        Get
            Return _latitude
        End Get
        Set(ByVal value As String)
            _latitude = value
        End Set
    End Property

    Private _longitude As String
    Public Property Longitude() As String
        Get
            Return _longitude
        End Get
        Set(ByVal value As String)
            _longitude = value
        End Set
    End Property

End Class

