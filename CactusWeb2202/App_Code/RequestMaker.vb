﻿Imports System.Net
Imports System.IO
Imports System.Collections.Generic
Imports System.Data
Imports System.Xml
Imports System.Xml.Linq
Imports System.Net.Mail
Imports System.Text
Imports System.Security.Cryptography

Public Class RequestMaker
    Public Sub New()

    End Sub

    Shared req As WebRequest
    Shared rep As WebResponse
    Shared senderID As String = System.Configuration.ConfigurationManager.AppSettings("senderID")
    Shared Trading As String = System.Configuration.ConfigurationManager.AppSettings("TradingPartnerCode")
    Shared customer As String = System.Configuration.ConfigurationManager.AppSettings("CustomerNumber")
	Shared Passcode As String = System.Configuration.ConfigurationManager.AppSettings("Passcode")


    Shared url As String = System.Configuration.ConfigurationManager.AppSettings("url")

    Public Shared Function RateRequest(ByVal pickUpdateTime As String, ByVal dropOffDateTime As String, ByVal classCode As String, ByVal sid As String, ByVal address As String, ByVal rentallocation As String, ByVal returnlocation As String, ByVal discount As String, ByVal remoteaddress As String) As String
        Dim result As String
        Dim textBuilder As New Text.StringBuilder()

        textBuilder.AppendLine("<TRNXML Version=""1.0.0"">")
        textBuilder.AppendLine("<Dategmtime TimeZone=""ET"">" + DateTime.Now.ToString + "</Dategmtime>")
        textBuilder.AppendLine("<Sender>")
        textBuilder.AppendLine("<SenderID>" & senderID & "</SenderID>")
        textBuilder.AppendLine("</Sender>")
        textBuilder.AppendLine("<Recipient>")
        textBuilder.AppendLine("<RecipientID>TRN</RecipientID>")
        textBuilder.AppendLine("</Recipient>")
        textBuilder.AppendLine("<TradingPartner>")
        textBuilder.AppendLine("<TradingPartnerCode>" & Trading & "</TradingPartnerCode>")
        textBuilder.AppendLine("</TradingPartner>")
        textBuilder.AppendLine("<Customer>")
        textBuilder.AppendLine("<CustomerNumber>" & customer & "</CustomerNumber>")
        textBuilder.AppendLine("<Passcode>" & Passcode & "</Passcode>")
        'textBuilder.AppendLine("<SID>" + sid + "</SID>")
        textBuilder.AppendLine("<RemoteAddress>" + remoteaddress + "</RemoteAddress>")
        textBuilder.AppendLine("</Customer>")
        textBuilder.AppendLine("<Message>")
        textBuilder.AppendLine("<MessageID>REQRAT</MessageID>")
        textBuilder.AppendLine("<MessageDesc>Request Rates</MessageDesc>")
        textBuilder.AppendLine("</Message>")
        textBuilder.AppendLine("<Payload>")
        'textBuilder.AppendLine("<BusinessHours>0</BusinessHours>")
        textBuilder.AppendLine("<RentalLocationID>" & rentallocation & "</RentalLocationID>")
        textBuilder.AppendLine("<ReturnLocationID>" & returnlocation & "</ReturnLocationID>")
        textBuilder.AppendLine("<PickupDateTime>" + pickUpdateTime + "</PickupDateTime>")
        textBuilder.AppendLine("<ReturnDateTime>" + dropOffDateTime + "</ReturnDateTime>")
        textBuilder.AppendLine("<RateSource>WebLink</RateSource>")
        textBuilder.AppendLine("<Prepaid>Y</Prepaid>")
        textBuilder.AppendLine("<ClassCode></ClassCode>")


        If Trim(discount).Length = 5 Then
            textBuilder.AppendLine("<DiscountCode>" & discount & "</DiscountCode>")
        Else

            textBuilder.AppendLine("<CompanyNumber>" & discount & "</CompanyNumber>")
        End If

        '   textBuilder.AppendLine("<RateCode>" & ratecode & "</RateCode>")

        ' textBuilder.AppendLine("<CompanyNumber>FXWEK</CompanyNumber>")
        textBuilder.AppendLine("<TotalPricing>1</TotalPricing>")
        textBuilder.AppendLine("</Payload>")
        textBuilder.AppendLine("</TRNXML>")

        result = textBuilder.ToString()
        Return result


    End Function

    Public Shared Function RateRequestClass(ByVal classCode As String) As String
        Dim result As String
        Dim textBuilder As New Text.StringBuilder()

        textBuilder.AppendLine("<TRNXML Version=""1.0.0"">")
        textBuilder.AppendLine("<Dategmtime TimeZone=""ET"">" + DateTime.Now.ToString + "</Dategmtime>")
        textBuilder.AppendLine("<Sender>")
        textBuilder.AppendLine("<SenderID>" & senderID & "</SenderID>")
        textBuilder.AppendLine("</Sender>")
        textBuilder.AppendLine("<Recipient>")
        textBuilder.AppendLine("<RecipientID>TRN</RecipientID>")
        textBuilder.AppendLine("</Recipient>")
        textBuilder.AppendLine("<TradingPartner>")
        textBuilder.AppendLine("<TradingPartnerCode>" & Trading & "</TradingPartnerCode>")
        textBuilder.AppendLine("</TradingPartner>")
        textBuilder.AppendLine("<Customer>")
        textBuilder.AppendLine("<CustomerNumber>" & customer & "</CustomerNumber>")
        textBuilder.AppendLine("<Passcode>" & Passcode & "</Passcode>")
        'textBuilder.AppendLine("<SID>" + sid + "</SID>")
        'textBuilder.AppendLine("<RemoteAddress>" + address + "</RemoteAddress>")
        textBuilder.AppendLine("</Customer>")
        textBuilder.AppendLine("<Message>")
        textBuilder.AppendLine("<MessageID>REQCLS</MessageID>")
        textBuilder.AppendLine("<MessageDesc>Request Rates</MessageDesc>")
        textBuilder.AppendLine("</Message>")
        textBuilder.AppendLine("<Payload>")
        'textBuilder.AppendLine("<BusinessHours>0</BusinessHours>")
        textBuilder.AppendLine("<RateSource>WebLink</RateSource>")
        textBuilder.AppendLine("<ClassCode>" + classCode + "</ClassCode>")
        textBuilder.AppendLine("</Payload>")
        textBuilder.AppendLine("</TRNXML>")

        result = textBuilder.ToString()
        Return result



    End Function

    Public Shared Function RateRequestByID(ByVal pickUpdateTime As String, _
                                               ByVal dropOffDateTime As String, _
                                               ByVal classCode As String, ByVal rateID As String, ByVal sid As String, _
                                               ByVal address As String, ByVal rentallocation As String, ByVal returnlocation As String, ByVal extraCodes As String, ByVal discount As String) As String
        Dim result As String
        Dim textBuilder As New Text.StringBuilder()
        textBuilder.AppendLine("<TRNXML Version=""1.0.0"">")
        textBuilder.AppendLine("<Dategmtime TimeZone=""ET"">" + DateTime.Now.ToString + "</Dategmtime>")
        textBuilder.AppendLine("<Sender>")
        textBuilder.AppendLine("<SenderID>" & senderID & "</SenderID>")
        textBuilder.AppendLine("</Sender>")
        textBuilder.AppendLine("<Recipient>")
        textBuilder.AppendLine("<RecipientID>TRN</RecipientID>")
        textBuilder.AppendLine("</Recipient>")
        textBuilder.AppendLine("<TradingPartner>")
        textBuilder.AppendLine("<TradingPartnerCode>" & Trading & "</TradingPartnerCode>")
        textBuilder.AppendLine("</TradingPartner>")
        textBuilder.AppendLine("<Customer>")
        textBuilder.AppendLine("<CustomerNumber>" & customer & "</CustomerNumber>")
        textBuilder.AppendLine("<Passcode>" & Passcode & "</Passcode>")
        'textBuilder.AppendLine("<SID>" + sid + "</SID>")
        'textBuilder.AppendLine("<RemoteAddress>" + address + "</RemoteAddress>")
        textBuilder.AppendLine("</Customer>")
        textBuilder.AppendLine("<SID>" + sid + "</SID>")
        textBuilder.AppendLine("<RemoteAddress>" + address + "</RemoteAddress>")
        textBuilder.AppendLine("</Customer>")
        textBuilder.AppendLine("<Message>")
        textBuilder.AppendLine("<MessageID>REQRAT</MessageID>")
        textBuilder.AppendLine("<MessageDesc>Request Rates</MessageDesc>")
        textBuilder.AppendLine("</Message>")
        textBuilder.AppendLine("<PayLoad>")
        'textBuilder.AppendLine("<BusinessHours>0</BusinessHours>")
        textBuilder.AppendLine("<DiscountCode>" & discount & "</DiscountCode>")
        textBuilder.AppendLine("<RentalLocationID>" + rentallocation + "</RentalLocationID>")
        textBuilder.AppendLine("<ReturnLocationID>" + returnlocation + "</ReturnLocationID>")
        textBuilder.AppendLine("<PickupDateTime>" + pickUpdateTime + "</PickupDateTime>")
        textBuilder.AppendLine("<ReturnDateTime>" + dropOffDateTime + "</ReturnDateTime>")
        textBuilder.AppendLine("<PickupDateTime>" + pickUpdateTime + "</PickupDateTime>")
        textBuilder.AppendLine("<ReturnDateTime>" + dropOffDateTime + "</ReturnDateTime>")
        textBuilder.AppendLine("<RateSource>WebLink</RateSource>")
        textBuilder.AppendLine("<ClassCode>" + classCode + "</ClassCode>")
        textBuilder.AppendLine("<RateCode></RateCode>")
        textBuilder.AppendLine("<RateID>" + rateID + "</RateID>")

        If Not String.IsNullOrEmpty(extraCodes) Then

            Dim c() As Char = {"^"}

            Dim codes() As String = extraCodes.Split(c, StringSplitOptions.RemoveEmptyEntries)

            For Each code As String In codes
                textBuilder.AppendLine("<ExtraCode>" & code & "</ExtraCode>")
            Next

        End If

        textBuilder.AppendLine("<TotalPricing>1</TotalPricing>")
        textBuilder.AppendLine("</PayLoad>")
        textBuilder.AppendLine("</TRNXML>")

        result = textBuilder.ToString()
        Return result
    End Function


    Public Shared Function LocationRequest() As String
        Dim result As String
        Dim textBuilder As New Text.StringBuilder()
        textBuilder.AppendLine("<TRNXML Version=""1.0.0"">")
        textBuilder.AppendLine("<Dategmtime TimeZone=""ET"">" + DateTime.Now.ToString + "</Dategmtime>")
        textBuilder.AppendLine("<Sender>")
        textBuilder.AppendLine("<SenderID>" & senderID & "</SenderID>")
        textBuilder.AppendLine("</Sender>")
        textBuilder.AppendLine("<Recipient>")
        textBuilder.AppendLine("<RecipientID>TRN</RecipientID>")
        textBuilder.AppendLine("</Recipient>")
        textBuilder.AppendLine("<TradingPartner>")
        textBuilder.AppendLine("<TradingPartnerCode>" & Trading & "</TradingPartnerCode>")
        textBuilder.AppendLine("</TradingPartner>")
        textBuilder.AppendLine("<Customer>")
        textBuilder.AppendLine("<CustomerNumber>" & customer & "</CustomerNumber>")
        textBuilder.AppendLine("<Passcode>" & Passcode & "</Passcode>")
        textBuilder.AppendLine("</Customer>")
        textBuilder.AppendLine("<Message>")
        textBuilder.AppendLine("<MessageID>REQLOC</MessageID>")
        textBuilder.AppendLine("<MessageDesc>Request Locations</MessageDesc>")
        textBuilder.AppendLine("</Message>")
        textBuilder.AppendLine("<Payload>")
        'textBuilder.AppendLine("<BusinessHours>0</BusinessHours>")
        textBuilder.AppendLine("<RentalLocationID></RentalLocationID>")
        textBuilder.AppendLine("<RateSource>WebLink</RateSource>")
        'textBuilder.AppendLine("<RateCode>" + rateCode + "</RateCode>")
        textBuilder.AppendLine("<TotalPricing>1</TotalPricing>")
        textBuilder.AppendLine("</Payload>")
        textBuilder.AppendLine("</TRNXML>")
        result = textBuilder.ToString()
        Return result
    End Function



    Public Shared Function RequestVehicles() As String
        Dim result As String
        Dim textBuilder As New Text.StringBuilder()
        textBuilder.AppendLine("<TRNXML Version=""1.0.0"">")
        textBuilder.AppendLine("<Dategmtime TimeZone=""ET"">" + DateTime.Now.ToString + "</Dategmtime>")
        textBuilder.AppendLine("<Sender>")
        textBuilder.AppendLine("<SenderID>" & senderID & "</SenderID>")
        textBuilder.AppendLine("</Sender>")
        textBuilder.AppendLine("<Recipient>")
        textBuilder.AppendLine("<RecipientID>TRN</RecipientID>")
        textBuilder.AppendLine("</Recipient>")
        textBuilder.AppendLine("<TradingPartner>")
        textBuilder.AppendLine("<TradingPartnerCode>" & Trading & "</TradingPartnerCode>")
        textBuilder.AppendLine("</TradingPartner>")
        textBuilder.AppendLine("<Customer>")
        textBuilder.AppendLine("<CustomerNumber>" & customer & "</CustomerNumber>")
        textBuilder.AppendLine("<Passcode>" & Passcode & "</Passcode>")
        textBuilder.AppendLine("</Customer>")
        textBuilder.AppendLine("<Message>")
        textBuilder.AppendLine("<MessageID>REQCLS</MessageID>")
        textBuilder.AppendLine("<MessageDesc></MessageDesc>")
        textBuilder.AppendLine("</Message>")
        textBuilder.AppendLine("<Payload>")
        textBuilder.AppendLine("<ClassCode></ClassCode>")
        textBuilder.AppendLine("</Payload>")
        textBuilder.AppendLine("</TRNXML>")
        result = textBuilder.ToString()
        Return result
    End Function

    Public Shared Function RequestClass(ByVal cls As String) As String
        Dim result As String
        Dim textBuilder As New Text.StringBuilder()
        textBuilder.AppendLine("<TRNXML Version=""1.0.0"">")
        textBuilder.AppendLine("<Dategmtime TimeZone=""ET"">" + DateTime.Now.ToString + "</Dategmtime>")
        textBuilder.AppendLine("<Sender>")
        textBuilder.AppendLine("<SenderID>" & senderID & "</SenderID>")
        textBuilder.AppendLine("</Sender>")
        textBuilder.AppendLine("<Recipient>")
        textBuilder.AppendLine("<RecipientID>TRN</RecipientID>")
        textBuilder.AppendLine("</Recipient>")
        textBuilder.AppendLine("<TradingPartner>")
        textBuilder.AppendLine("<TradingPartnerCode>" & Trading & "</TradingPartnerCode>")
        textBuilder.AppendLine("</TradingPartner>")
        textBuilder.AppendLine("<Customer>")
        textBuilder.AppendLine("<CustomerNumber>" & customer & "</CustomerNumber>")
        textBuilder.AppendLine("<Passcode>" & Passcode & "</Passcode>")
        'textBuilder.AppendLine("<SID>" + sid + "</SID>")
        'textBuilder.AppendLine("<RemoteAddress>" + address + "</RemoteAddress>")
        textBuilder.AppendLine("</Customer>")
        textBuilder.AppendLine("<Message>")
        textBuilder.AppendLine("<MessageID>REQCLS</MessageID>")
        textBuilder.AppendLine("<MessageDesc></MessageDesc>")
        textBuilder.AppendLine("</Message>")
        textBuilder.AppendLine("<PayLoad>")
        'textBuilder.AppendLine("<BusinessHours>0</BusinessHours>")
        textBuilder.AppendLine("<ClassCode>" & cls & "</ClassCode>")
        '  textBuilder.AppendLine("<ReturnLocationID>TAL</ReturnLocationID>")

        textBuilder.AppendLine("</PayLoad>")
        textBuilder.AppendLine("</TRNXML>")

        result = textBuilder.ToString()
        Return result
    End Function


    Public Shared Function RequestExtras(ByVal rentallocation As String, cls As String) As String
        Dim result As String
        Dim textBuilder As New Text.StringBuilder()
        textBuilder.AppendLine("<TRNXML Version=""1.0.0"">")
        textBuilder.AppendLine("<Dategmtime TimeZone=""ET"">" + DateTime.Now.ToString + "</Dategmtime>")
        textBuilder.AppendLine("<Sender>")
        textBuilder.AppendLine("<SenderID>" & senderID & "</SenderID>")
        textBuilder.AppendLine("</Sender>")
        textBuilder.AppendLine("<Recipient>")
        textBuilder.AppendLine("<RecipientID>TRN</RecipientID>")
        textBuilder.AppendLine("</Recipient>")
        textBuilder.AppendLine("<TradingPartner>")
        textBuilder.AppendLine("<TradingPartnerCode>" & Trading & "</TradingPartnerCode>")
        textBuilder.AppendLine("</TradingPartner>")
        textBuilder.AppendLine("<Customer>")
        textBuilder.AppendLine("<CustomerNumber>" & customer & "</CustomerNumber>")
        textBuilder.AppendLine("<Passcode>" & Passcode & "</Passcode>")
        'textBuilder.AppendLine("<SID>" + sid + "</SID>")
        'textBuilder.AppendLine("<RemoteAddress>" + address + "</RemoteAddress>")
        textBuilder.AppendLine("</Customer>")
        textBuilder.AppendLine("<Message>")
        textBuilder.AppendLine("<MessageID>REQEXT</MessageID>")
        textBuilder.AppendLine("<MessageDesc></MessageDesc>")
        textBuilder.AppendLine("</Message>")
        textBuilder.AppendLine("<PayLoad>")
        'textBuilder.AppendLine("<BusinessHours>0</BusinessHours>")
        textBuilder.AppendLine("<RentalLocationID>" & rentallocation & "</RentalLocationID>")
        ' textBuilder.AppendLine("<ReturnLocationID>TAL</ReturnLocationID>")

        textBuilder.AppendLine("</PayLoad>")
        textBuilder.AppendLine("</TRNXML>")

        result = textBuilder.ToString()
        Return result
    End Function

    Public Shared Function RateRequestByExtras(ByVal pickUpdateTime As String, _
                                           ByVal dropOffDateTime As String, _
                                           ByVal classCode As String, ByVal rateCode As String, ByVal sid As String, _
                                           ByVal address As String, ByVal rentallocation As String, ByVal returnlocation As String, ByVal rateID As String, ByVal strPrepaid As String) As String
        Dim result As String
        Dim textBuilder As New Text.StringBuilder()
        textBuilder.AppendLine("<TRNXML Version=""1.0.0"">")
        textBuilder.AppendLine("<Dategmtime TimeZone=""ET"">" + DateTime.Now.ToString + "</Dategmtime>")
        textBuilder.AppendLine("<Sender>")
        textBuilder.AppendLine("<SenderID>" & senderID & "</SenderID>")
        textBuilder.AppendLine("</Sender>")
        textBuilder.AppendLine("<Recipient>")
        textBuilder.AppendLine("<RecipientID>TRN</RecipientID>")
        textBuilder.AppendLine("</Recipient>")
        textBuilder.AppendLine("<TradingPartner>")
        textBuilder.AppendLine("<TradingPartnerCode>" & Trading & "</TradingPartnerCode>")
        textBuilder.AppendLine("</TradingPartner>")
        textBuilder.AppendLine("<Customer>")
        textBuilder.AppendLine("<CustomerNumber>" & customer & "</CustomerNumber>")
        textBuilder.AppendLine("<Passcode>" & Passcode & "</Passcode>")
        'textBuilder.AppendLine("<SID>" + sid + "</SID>")
        'textBuilder.AppendLine("<RemoteAddress>" + address + "</RemoteAddress>")
        textBuilder.AppendLine("</Customer>")
        textBuilder.AppendLine("<Message>")
        textBuilder.AppendLine("<MessageID>REQBIL</MessageID>")
        textBuilder.AppendLine("<MessageDesc />")
        textBuilder.AppendLine("</Message>")
        textBuilder.AppendLine("<PayLoad>")
        'textBuilder.AppendLine("<BusinessHours>0</BusinessHours>")
        textBuilder.AppendLine("<RentalLocationID>" & rentallocation & "</RentalLocationID>")
        textBuilder.AppendLine("<ReturnLocationID>" & returnlocation & "</ReturnLocationID>")
        textBuilder.AppendLine("<PickupDateTime>" + pickUpdateTime + "</PickupDateTime>")
        textBuilder.AppendLine("<ReturnDateTime>" + dropOffDateTime + "</ReturnDateTime>")
        ' textBuilder.AppendLine("<RateSource>RMS.NET</RateSource>")
        textBuilder.AppendLine("<RateID>" + rateID + "</RateID>")
        textBuilder.AppendLine("<ClassCode>" + classCode + "</ClassCode>")
        textBuilder.AppendLine("<ExtraCode>CDW</ExtraCode>")
        'textBuilder.AppendLine("<ExtraCode>CHILDSEAT</ExtraCode>")
        'textBuilder.AppendLine("<Prepaid>Y</Prepaid>")
        textBuilder.AppendLine("</PayLoad>")
        textBuilder.AppendLine("</TRNXML>")

        result = textBuilder.ToString()
		
        Return result
    End Function

    Public Shared Function MakeRateRequest(ByVal para As String) As String
        ' Dim uri As String = url

        req = WebRequest.Create(url)
        req.Method = "POST"
        req.ContentType = "text/xml"
        Dim writer As New StreamWriter(req.GetRequestStream())
        writer.Write(para)
        writer.Close()
        rep = req.GetResponse()

        Dim receivestream As Stream = rep.GetResponseStream()

        Dim encode As Encoding = Encoding.GetEncoding("utf-8")

        Dim readstream As New StreamReader(receivestream, encode)

        Dim read(256) As [Char]

        Dim result As String

        result = readstream.ReadToEnd()

        Return result
    End Function


    Public Shared Function AddRezRequest(ByVal pickUpdateTime As String, _
                                             ByVal dropOffDateTime As String, _
                                             ByVal rateID As String, ByVal classCode As String, _
                                             ByVal comments As String, _
                                             ByVal sid As String, ByVal address As String, _
                                             ByVal first As String, ByVal last As String, ByVal email As String, _
                                             ByVal cell As String, ByVal RAddress1 As String, _
                                             ByVal RAddress2 As String, ByVal city As String, _
                                             ByVal state As String, ByVal zip As String, _
                                             ByVal comf As String, ByVal rentallocation As String, ByVal returnlocation As String, ByVal country As String, ByVal extraCodes As String, ByVal discount As String, ByVal credittype As String, ByVal creditno As String, ByVal creditcode As String, ByVal creditexp As String, IsExtra As Boolean, ByVal isPrepaidType As String, ByVal prePaidAmount As String, ByVal arrivaltime As String, ByVal ArrivalofFlight As String, ByVal NeedHotel As Boolean, ByVal DateOfBirth As String
                                             ) As String
        'ByVal comf As String, ByVal rentallocation As String, ByVal returnlocation As String, ByVal country As String, ByVal extraCodes As String, ByVal discount As String, ByVal credittype As String, ByVal creditno As String, ByVal creditcode As String, ByVal creditexp As String, Licensenumber As String, DOB As String, airline As String, flight As String, licenseexpdate As String, IsExtra As Boolean) As String
        'ByVal comf As String, ByVal rentallocation As String, ByVal returnlocation As String, ByVal country As String, ByVal extraCodes As String, ByVal discount As String, dob As String, IsExtra As Boolean) As String
        Dim result As String
        Dim textBuilder As New Text.StringBuilder()
        textBuilder.AppendLine("<TRNXML Version=""1.0.0"">")
        textBuilder.AppendLine("<Dategmtime TimeZone=""ET"">" + DateTime.Now.ToString + "</Dategmtime>")
        textBuilder.AppendLine("<Sender>")
        textBuilder.AppendLine("<SenderID>" & senderID & "</SenderID>")
        textBuilder.AppendLine("</Sender>")
        textBuilder.AppendLine("<Recipient>")
        textBuilder.AppendLine("<RecipientID>TRN</RecipientID>")
        textBuilder.AppendLine("</Recipient>")
        textBuilder.AppendLine("<TradingPartner>")
        textBuilder.AppendLine("<TradingPartnerCode>WEB01</TradingPartnerCode>")
        textBuilder.AppendLine("</TradingPartner>")
        textBuilder.AppendLine("<Customer>")
        textBuilder.AppendLine("<CustomerNumber>" & customer & "</CustomerNumber>")
        textBuilder.AppendLine("<Passcode>" & Passcode & "</Passcode>")
        textBuilder.AppendLine("<SID>" + sid + "</SID>")
        textBuilder.AppendLine("<RemoteAddress>" + address + "</RemoteAddress>")
        textBuilder.AppendLine("</Customer>")
        textBuilder.AppendLine("<Message>")
        textBuilder.AppendLine("<MessageID>ADDREZ</MessageID>")
        textBuilder.AppendLine("<MessageDesc>Add Reservation</MessageDesc>")
        textBuilder.AppendLine("</Message>")
        textBuilder.AppendLine("<Payload>")
        If comf <> "" Then
            textBuilder.AppendLine("<ConfirmNum>" + comf + "</ConfirmNum>")
        End If
        textBuilder.AppendLine("<RentalLocationID>" & rentallocation & "</RentalLocationID>")
        textBuilder.AppendLine("<ReturnLocationID>" + returnlocation + "</ReturnLocationID>")
        textBuilder.AppendLine("<PickupDateTime>" + pickUpdateTime + "</PickupDateTime>")
        textBuilder.AppendLine("<ReturnDateTime>" + dropOffDateTime + "</ReturnDateTime>")
        textBuilder.AppendLine("<RateID>" + rateID + "</RateID>")
        textBuilder.AppendLine("<ClassCode>" + classCode + "</ClassCode>")
        If Trim(discount).Length = 5 Then
            textBuilder.AppendLine("<CompanyNumber>" & discount & "</CompanyNumber>")
        Else
            textBuilder.AppendLine("<DiscountCode>" & discount & "</DiscountCode>")
        End If
        '  textBuilder.AppendLine("<RentalComments>" + comments + "</RentalComments>")
        textBuilder.AppendLine("<RenterFirst>" + first + "</RenterFirst>")
        textBuilder.AppendLine("<RenterLast>" + last + "</RenterLast>")
        textBuilder.AppendLine("<EmailAddress>" + email + "</EmailAddress>")
        textBuilder.AppendLine("<RenterHomePhone>" + cell + "</RenterHomePhone>")
        textBuilder.AppendLine("<RenterAddress1>" + RAddress1 + "</RenterAddress1>")
        textBuilder.AppendLine("<RenterAddress2>" + RAddress2 + "</RenterAddress2>")
        textBuilder.AppendLine("<RenterCity>" + city + "</RenterCity>")
        textBuilder.AppendLine("<RenterState>" + state + "</RenterState>")
        textBuilder.AppendLine("<RenterZip>" + zip + "</RenterZip>")
        textBuilder.AppendLine("<RenterCountry>" + country + "</RenterCountry>")

        textBuilder.AppendLine("<RateCode>" + discount + "</RateCode>")

        textBuilder.AppendLine("<CCType>" + credittype + "</CCType>")
        textBuilder.AppendLine("<CCNumber>" + creditno + "</CCNumber>")
        textBuilder.AppendLine("<CCExp>" + creditexp + "</CCExp>")
        ' textBuilder.AppendLine("<CardDepositAmount>" + rateCode + "</CardDepositAmount>")
        textBuilder.AppendLine("<CardAuthNumber>" + creditcode + "</CardAuthNumber>")
        'textBuilder.AppendLine("<LicenseNumber>" + Licensenumber + "</LicenseNumber>")
        'textBuilder.AppendLine("<LicenseState>" + licensestate + "</LicenseState>")
        'textBuilder.AppendLine("<LicenseExpDate>" + licenseexpdate + "</LicenseExpDate>")
        'textBuilder.AppendLine("<Airline>" + airline + "</Airline>")
        'textBuilder.AppendLine("<Flight>" + flight + "</Flight>")
        textBuilder.AppendLine("<IATA>" + arrivaltime + "</IATA>")
        textBuilder.AppendLine("<RenterDOB>" + DateOfBirth + "</RenterDOB>")

        If Not String.IsNullOrEmpty(extraCodes) Then

            Dim c() As Char = {"^"}

            Dim codes() As String = extraCodes.Split(c, StringSplitOptions.RemoveEmptyEntries)

            For Each code As String In codes
                textBuilder.AppendLine("<ExtraCode>" & code & "</ExtraCode>")
            Next
        Else
            If IsExtra = True Then
                textBuilder.AppendLine("<ExtraCode></ExtraCode>")
            End If

        End If


        'Line added by ravi for Pay Now. date 22 aug 2015
        If Not String.IsNullOrEmpty(isPrepaidType) And Not IsNothing(isPrepaidType) And isPrepaidType = "pnow" Then

            textBuilder.AppendLine("<PrepaidAmount>")
            textBuilder.AppendLine("<CCType>" + credittype + "</CCType>")
            textBuilder.AppendLine("<CCNumber>" + creditno + "</CCNumber>")
            textBuilder.AppendLine("<CCExp>" + creditexp + "</CCExp>")
            textBuilder.AppendLine("<Prepaid>" & isPrepaidType.ToUpper() & "</Prepaid>")
            textBuilder.AppendLine("<PrepaidAmount>" & prePaidAmount & "</PrepaidAmount>")
            textBuilder.AppendLine("</PrepaidAmount>")

        End If

        'textBuilder.AppendLine("<RateCode>" + rateCode + "</RateCode>")
        textBuilder.AppendLine("<TotalPricing>1</TotalPricing>")
        textBuilder.AppendLine("</Payload>")
        textBuilder.AppendLine("</TRNXML>")

        result = textBuilder.ToString()
        Return result
    End Function

    Public Shared Function CancelReservation(ByVal confno As String, ByVal sid As String, ByVal address As String) As String
        Dim result As String
        Dim textBuilder As New Text.StringBuilder()
        textBuilder.AppendLine("<TRNXML Version=""1.0.0"">")
        textBuilder.AppendLine("<Dategmtime TimeZone=""ET"">" + DateTime.Now.ToString + "</Dategmtime>")
        textBuilder.AppendLine("<Sender>")
        textBuilder.AppendLine("<SenderID>" & senderID & "</SenderID>")
        textBuilder.AppendLine("</Sender>")
        textBuilder.AppendLine("<Recipient>")
        textBuilder.AppendLine("<RecipientID>TRN</RecipientID>")
        textBuilder.AppendLine("</Recipient>")
        textBuilder.AppendLine("<TradingPartner>")
        textBuilder.AppendLine("<TradingPartnerCode>WEB01</TradingPartnerCode>")
        textBuilder.AppendLine("</TradingPartner>")
        textBuilder.AppendLine("<Customer>")
        textBuilder.AppendLine("<CustomerNumber>" & customer & "</CustomerNumber>")
        textBuilder.AppendLine("<Passcode>" & Passcode & "</Passcode>")
        textBuilder.AppendLine("<SID>" + sid + "</SID>")
        textBuilder.AppendLine("<RemoteAddress>" + address + "</RemoteAddress>")
        textBuilder.AppendLine("</Customer>")
        textBuilder.AppendLine("<Message>")
        textBuilder.AppendLine("<MessageID>REQCAN</MessageID>")
        textBuilder.AppendLine("<MessageDesc>Request Rates</MessageDesc>")
        textBuilder.AppendLine("</Message>")
        textBuilder.AppendLine("<Payload>")
        textBuilder.AppendLine("<ConfirmNum>" & confno & "</ConfirmNum>")
        textBuilder.AppendLine("</Payload>")
        textBuilder.AppendLine("</TRNXML>")

        result = textBuilder.ToString()
        Return result
    End Function


    Public Shared Function EditReservation(ByVal confno As String, ByVal sid As String, ByVal address As String) As String
        Dim result As String
        Dim textBuilder As New Text.StringBuilder()
        textBuilder.AppendLine("<TRNXML Version=""1.0.0"">")
        textBuilder.AppendLine("<Dategmtime TimeZone=""ET"">" + DateTime.Now.ToString + "</Dategmtime>")
        textBuilder.AppendLine("<Sender>")
        textBuilder.AppendLine("<SenderID>" & senderID & "</SenderID>")
        textBuilder.AppendLine("</Sender>")
        textBuilder.AppendLine("<Recipient>")
        textBuilder.AppendLine("<RecipientID>TRN</RecipientID>")
        textBuilder.AppendLine("</Recipient>")
        textBuilder.AppendLine("<TradingPartner>")
        textBuilder.AppendLine("<TradingPartnerCode>" & Trading & "</TradingPartnerCode>")
        textBuilder.AppendLine("</TradingPartner>")
        textBuilder.AppendLine("<Customer>")
        textBuilder.AppendLine("<CustomerNumber>" & customer & "</CustomerNumber>")
        textBuilder.AppendLine("<Passcode>" & Passcode & "</Passcode>")
        textBuilder.AppendLine("<SID>" + sid + "</SID>")
        textBuilder.AppendLine("<RemoteAddress>" + address + "</RemoteAddress>")
        textBuilder.AppendLine("</Customer>")
        textBuilder.AppendLine("<Message>")
        textBuilder.AppendLine("<MessageID>REQREZ</MessageID>")
        textBuilder.AppendLine("<MessageDesc></MessageDesc>")
        textBuilder.AppendLine("</Message>")
        textBuilder.AppendLine("<PayLoad>")
        textBuilder.AppendLine("<ConfirmNum>" & confno & "</ConfirmNum>")
        textBuilder.AppendLine("</PayLoad>")
        textBuilder.AppendLine("</TRNXML>")

        result = textBuilder.ToString()
        Return result
    End Function

    ' Private _Classes As Dictionary(Of String, String)
    Public Shared ReadOnly Property Classes() As Dictionary(Of String, String)
        Get
            Dim _Classes As New Dictionary(Of String, String)
            _Classes.Add("A", "Mini")
            _Classes.Add("B", "Economy Car")
            _Classes.Add("C", "Compact")
            _Classes.Add("D", "MINI VAN")
            _Classes.Add("E", "Intermediate")


            _Classes.Add("F", "STANDARD CAR")
            _Classes.Add("G", "FULL SIZE CAR")
            _Classes.Add("H", "PREMIUM CAR")

            _Classes.Add("I", "ALL TERRAIN")


            _Classes.Add("J", "ALL TERRAIN CONVERTIBLE")
            _Classes.Add("K", "INTERMEDIATE SUV")
            _Classes.Add("L", "STANDARD SUV")
            _Classes.Add("FFAR", "FULL SIZE SUV")


            _Classes.Add("FVAR", "FULL SIZE VAN")
            _Classes.Add("SQBR", "STANDARD PICK UP")
            _Classes.Add("PFAR", "PREMIUM SUV")
            _Classes.Add("IVAR", "INTERMEDIATE VAN")
            _Classes.Add("CFAR", "COMPACT SUV")
            Return _Classes
        End Get
        'Set(ByVal value As Dictionary(Of String, String))
        '    _Classes = value
        'End Set
    End Property

    Public Shared ReadOnly Property linkPages() As Dictionary(Of String, String)
        Get
            Dim _links As New Dictionary(Of String, String)
            _links.Add("CCAR", "vehicles-compact.asp")
            _links.Add("SCAR", "vehicles-standard.asp")
            _links.Add("FCAR", "vehicles-fullsize.asp")
            _links.Add("FQAR", "vehicles-fullsizePIckup.asp")
            _links.Add("MVAR", "vehicles-minivan.asp")
            _links.Add("PFAR", "vehicles-suv.asp")
            _links.Add("PJAR", "vehicles-Jeep.asp")
            _links.Add("STAR", "vehicles-convertible.asp")
            _links.Add("WCAR", "vehicles-luxury.asp")
            Return _links
        End Get
    End Property

    Public Shared ReadOnly Property ClassOrders() As Dictionary(Of String, Integer)
        Get
            Dim orders As New Dictionary(Of String, Integer)
            orders.Add("A", 1)
            orders.Add("B", 2)
            orders.Add("C", 3)
            orders.Add("D", 4)
            orders.Add("M", 5)
            orders.Add("H", 6)
            orders.Add("T", 7)
            orders.Add("I", 8)
            orders.Add("K", 9)
            orders.Add("E", 10)
            orders.Add("F", 11)
            orders.Add("G", 12)
            orders.Add("J", 13)

            Return orders
        End Get
    End Property

    Public Shared Function GetClasses(ByVal ClassCode As String) As String
        Return Classes(ClassCode)
    End Function



    Public Function GetLocation(ByVal loc) As String
        If loc = "SLA" Then
            loc = "Salta Centro - Downtown"
		Else If loc = "AEPSLA" Then
			loc = "Aeropuerto Salta"
		Else If loc = "MDZ" Then
			loc = "Mendoza Centro - Downtown"
		Else If loc = "AEPMDZ" Then
			loc = "Aeropuerto Mendoza"
		Else If loc = "BRC" Then
			loc = "Bariloche - Downtown"
		Else If loc = "BRCAEP" Then
			loc = "Aeropuerto Bariloche"
        End If
        Return loc

    End Function

    Public Sub RetainVehicleInfo()
        Dim pickupdate, dropoffdatedate As String
        Dim pickuptime, dropofftime As String

        pickuptime = Trim(HttpContext.Current.Request.QueryString("putime"))
        dropofftime = Trim(HttpContext.Current.Request.QueryString("putime"))

        Dim ploc As String
        Dim dloc As String
        Dim loc As New RequestMaker


        ploc = Trim(HttpContext.Current.Request.QueryString("ploc"))

        dloc = Trim(HttpContext.Current.Request.QueryString("rloc"))


        HttpContext.Current.Response.Redirect("Default.aspx?pudt=" & Trim(HttpContext.Current.Request.QueryString("pudt")) & "&dodt=" & Trim(HttpContext.Current.Request.QueryString("dodt")) & "&dotime=" & dropofftime & "&putime=" & pickuptime & "&ploc=" & ploc & "&rloc=" & dloc)

    End Sub


    Public Sub ChangeVehicle()
        Dim pickupdate, dropoffdatedate As String
        Dim pickuptime, dropofftime As String



        pickuptime = Trim(HttpContext.Current.Request.QueryString("putime"))
        dropofftime = Trim(HttpContext.Current.Request.QueryString("putime"))

        Dim ploc As String
        Dim dloc As String
        Dim loc As New RequestMaker


        ploc = Trim(HttpContext.Current.Request.QueryString("ploc"))

        dloc = Trim(HttpContext.Current.Request.QueryString("rloc"))

        ' pickupdate = requ
        '  pickupdate = pickupdate.Insert(4, "/").Insert(2, "/")
        '  dropoffdatedate = dropoffdatedate.Insert(4, "/").Insert(2, "/")

        HttpContext.Current.Response.Redirect("list.aspx?pudt=" & Trim(HttpContext.Current.Request.QueryString("pudt")) & "&dodt=" & Trim(HttpContext.Current.Request.QueryString("dodt")) & "&dotime=" & dropofftime & "&putime=" & pickuptime & "&ploc=" & ploc & "&rloc=" & dloc)

    End Sub


    Function processDateString(ByVal value As String) As String
        Dim re() As String
        Dim c() As Char = {"/"c}
        re = value.Split(c)
        If re(0).Length < 2 Then
            re(0) = "0" + re(0)
        End If
        If re(1).Length < 2 Then
            re(1) = "0" + re(1)
        End If
        Return re(0) + re(1) + re(2)
    End Function


    Public Shared Function RateRequestByExtras(ByVal pickUpdateTime As String, _
                                          ByVal dropOffDateTime As String, _
                                          ByVal classCode As String, ByVal rateCode As String, ByVal sid As String, _
                                          ByVal address As String, ByVal rentallocation As String, ByVal returnlocation As String, ByVal rateID As String, ByRef chkListExtras As CheckBoxList, ByVal discount As String, ByVal prepaid As String) As String
        Dim result As String
        Dim textBuilder As New Text.StringBuilder()
        textBuilder.AppendLine("<TRNXML Version=""1.0.0"">")
        textBuilder.AppendLine("<Dategmtime TimeZone=""ET"">" + DateTime.Now.ToString + "</Dategmtime>")
        textBuilder.AppendLine("<Sender>")
        textBuilder.AppendLine("<SenderID>" & senderID & "</SenderID>")
        textBuilder.AppendLine("</Sender>")
        textBuilder.AppendLine("<Recipient>")
        textBuilder.AppendLine("<RecipientID>TRN</RecipientID>")
        textBuilder.AppendLine("</Recipient>")
        textBuilder.AppendLine("<TradingPartner>")
        textBuilder.AppendLine("<TradingPartnerCode>" & Trading & "</TradingPartnerCode>")
        textBuilder.AppendLine("</TradingPartner>")
        textBuilder.AppendLine("<Customer>")
        textBuilder.AppendLine("<CustomerNumber>" & customer & "</CustomerNumber>")
        textBuilder.AppendLine("<Passcode>" & Passcode & "</Passcode>")
        'textBuilder.AppendLine("<SID>" + sid + "</SID>")
        'textBuilder.AppendLine("<RemoteAddress>" + address + "</RemoteAddress>")
        textBuilder.AppendLine("</Customer>")
        textBuilder.AppendLine("<Message>")
        textBuilder.AppendLine("<MessageID>REQBIL</MessageID>")
        textBuilder.AppendLine("<MessageDesc />")
        textBuilder.AppendLine("</Message>")
        textBuilder.AppendLine("<PayLoad>")
        textBuilder.AppendLine("<DiscountCode>" & discount & "</DiscountCode>")
        'textBuilder.AppendLine("<BusinessHours>0</BusinessHours>")
        textBuilder.AppendLine("<RentalLocationID>" & rentallocation & "</RentalLocationID>")
        textBuilder.AppendLine("<ReturnLocationID>" & returnlocation & "</ReturnLocationID>")
        textBuilder.AppendLine("<PickupDateTime>" + pickUpdateTime + "</PickupDateTime>")
        textBuilder.AppendLine("<ReturnDateTime>" + dropOffDateTime + "</ReturnDateTime>")
        textBuilder.AppendLine("<RateID>" + rateID + "</RateID>")
        textBuilder.AppendLine("<ClassCode>" + classCode + "</ClassCode>")
        textBuilder.AppendLine("<RateSource>RMS.NET</RateSource>")
        'Get prepaid rates
        If prepaid = "y" Then
            textBuilder.AppendLine("<Prepaid>Y</Prepaid>")
        End If
        'textBuilder.AppendLine("<ExtraCode>CDW</ExtraCode>")
        'textBuilder.AppendLine("<ExtraCode>CHILDSEAT</ExtraCode>")

        For Each item As ListItem In chkListExtras.Items
            If item.Selected Then
                textBuilder.AppendLine("<ExtraCode>" & item.Value & "</ExtraCode>")
            End If
        Next

        textBuilder.AppendLine("</PayLoad>")
        textBuilder.AppendLine("</TRNXML>")

        result = textBuilder.ToString()
        Return result
    End Function

    Public Shared Function PolicyRequest(ByVal rentallocation As String) As String
        Dim result As String
        Dim textBuilder As New Text.StringBuilder()
        textBuilder.AppendLine("<TRNXML Version=""1.0.0"">")
        textBuilder.AppendLine("<Dategmtime TimeZone=""ET"">" + DateTime.Now.ToString + "</Dategmtime>")
        textBuilder.AppendLine("<Sender>")
        textBuilder.AppendLine("<SenderID>" & senderID & "</SenderID>")
        textBuilder.AppendLine("</Sender>")
        textBuilder.AppendLine("<Recipient>")
        textBuilder.AppendLine("<RecipientID>TRN</RecipientID>")
        textBuilder.AppendLine("</Recipient>")
        textBuilder.AppendLine("<TradingPartner>")
        textBuilder.AppendLine("<TradingPartnerCode>" & Trading & "</TradingPartnerCode>")
        textBuilder.AppendLine("</TradingPartner>")
        textBuilder.AppendLine("<Customer>")
        textBuilder.AppendLine("<CustomerNumber>" & customer & "</CustomerNumber>")
        textBuilder.AppendLine("<Passcode>" & Passcode & "</Passcode>")
        'textBuilder.AppendLine("<SID>" + sid + "</SID>")
        'textBuilder.AppendLine("<RemoteAddress>" + address + "</RemoteAddress>")
        textBuilder.AppendLine("</Customer>")
        textBuilder.AppendLine("<Message>")
        textBuilder.AppendLine("<MessageID>REQPOL</MessageID>")
        textBuilder.AppendLine("<MessageDesc />")
        textBuilder.AppendLine("</Message>")
        textBuilder.AppendLine("<PayLoad>")
        textBuilder.AppendLine("<RentalLocationID>" & rentallocation & "</RentalLocationID>")
        textBuilder.AppendLine("</PayLoad>")
        textBuilder.AppendLine("</TRNXML>")

        result = textBuilder.ToString()
        Return result
    End Function


    Public Shared Function GetExtras(ByVal para As String) As IEnumerable
        ' Dim uri As String = "https://weblink.tsdasp.net/hld01xml.asp"

        Dim uri As String = url

        req = WebRequest.Create(uri)
        req.Method = "POST"
        req.ContentType = "text/xml"
        Dim writer As New StreamWriter(req.GetRequestStream())
        writer.Write(para)
        writer.Close()

        'rep = req.GetResponse()

        Dim res As HttpWebResponse = DirectCast(req.GetResponse(), HttpWebResponse)

        Dim dataStream As Stream = res.GetResponseStream()
        Dim enc As Encoding = Encoding.GetEncoding("utf-8")
        Dim reader As New StreamReader(dataStream, enc)

        Dim responseFromServer As String = reader.ReadToEnd()

        reader.Close()
        res.Close()

        Dim xDoc As New XmlDocument()
        xDoc.LoadXml(responseFromServer)
        Dim nodeReader As New XmlNodeReader(xDoc)
        Dim rssDoc As System.Xml.Linq.XDocument = XDocument.Load(nodeReader)

        Dim q = From item In rssDoc.Descendants("DailyExtra") _
            Select ExtraCode = item.Element("ExtraCode").Value, ExtraDesc = item.Element("ExtraDesc").Value & " ( $ " & item.Element("ExtraAmount").Value & ")"
        Return q

    End Function


    Public Shared Function GetExtras2(ByVal para As String) As IEnumerable
        ' Dim uri As String = "https://weblink.tsdasp.net/hld01xml.asp"

        Dim uri As String = url

        req = WebRequest.Create(uri)
        req.Method = "POST"
        req.ContentType = "text/xml"
        Dim writer As New StreamWriter(req.GetRequestStream())
        writer.Write(para)
        writer.Close()

        'rep = req.GetResponse()

        Dim res As HttpWebResponse = DirectCast(req.GetResponse(), HttpWebResponse)

        Dim dataStream As Stream = res.GetResponseStream()
        Dim enc As Encoding = Encoding.GetEncoding("utf-8")
        Dim reader As New StreamReader(dataStream, enc)

        Dim responseFromServer As String = reader.ReadToEnd()

        reader.Close()
        res.Close()

        Dim xDoc As New XmlDocument()
        xDoc.LoadXml(responseFromServer)
        Dim nodeReader As New XmlNodeReader(xDoc)
        Dim rssDoc As System.Xml.Linq.XDocument = XDocument.Load(nodeReader)

        Dim q = From item In rssDoc.Descendants("DailyExtra") _
              Select ExtraCode = item.Element("ExtraCode").Value, ExtraDesc = "" & item.Element("ExtraDesc").Value, ExtraOnRequest = "" & item.Element("ExtraOnRequest").Value, ExtraDesc2 = item.Element("ExtraAmount").Value
        Return q

    End Function





    Public Shared Function ValidateRequest(lastname As String, cancel As String, sessionID As String, sessionaddress As String) As Boolean
        Dim req1 As String = RequestMaker.EditReservation(cancel, sessionID, sessionaddress)
        Dim rep1 As String = RequestMaker.MakeRateRequest(req1)
        Dim xml2 As XDocument = XDocument.Parse(rep1)

        Dim TsdLastName As String = xml2...<RenterLast>.Value
        If Len(lastname) > 0 Then
            If Trim(TsdLastName).ToLower = lastname.ToLower Then
                Return True
            Else
                Return False
            End If
        End If

    End Function

    Public Sub GetAddress(ByVal loc, ByRef address, ByRef city, ByRef state, ByRef zip, ByRef phone)
        If loc = "Perryville Mo" Then
            address = "Perryville MO"
            ' city = "Perryville"
            ' state = " Missouri"
            ' zip = "63775"
            ' phone = "573-300-4602"

        ElseIf loc = "Perryville Mo Airport" Then 'ElseIf loc = "Perryville MoO Airport" Then
            address = "Perryville MO Airport "
            'city = "CALEDONIA"
            'state = "MI"
            ' zip = "49316"
            ' phone = "616-698-9595"
        ElseIf loc = "New Madrid Mo" Then
            address = "New Madrid MO"
        ElseIf loc = "Ste Genevieve Mo" Then
            address = "Ste Genevieve MO"
        ElseIf loc = "Jackson mo" Then
            address = "Jackson MO"

        ElseIf loc = "Cape Girardeau Mo Airport" Then
            address = "Cape Girardeau MO Airport"
        End If


    End Sub



    'Implement Fuction For Hot deals
    Public Shared Function RequestDeals() As String
        Dim result As String
        Dim textBuilder As New Text.StringBuilder()
        textBuilder.AppendLine("<TRNXML Version=""1.0.0"">")
        textBuilder.AppendLine("<Dategmtime TimeZone=""ET"">" + DateTime.Now.ToString + "</Dategmtime>")
        textBuilder.AppendLine("<Sender>")
        textBuilder.AppendLine("<SenderID>" & senderID & "</SenderID>")
        textBuilder.AppendLine("</Sender>")
        textBuilder.AppendLine("<Recipient>")
        textBuilder.AppendLine("<RecipientID>TRN</RecipientID>")
        textBuilder.AppendLine("</Recipient>")
        textBuilder.AppendLine("<TradingPartner>")
        textBuilder.AppendLine("<TradingPartnerCode>" & Trading & "</TradingPartnerCode>")
        textBuilder.AppendLine("</TradingPartner>")
        textBuilder.AppendLine("<Customer>")
        textBuilder.AppendLine("<CustomerNumber>" & customer & "</CustomerNumber>")
        textBuilder.AppendLine("<Passcode>" & Passcode & "</Passcode>")
        'textBuilder.AppendLine("<SID>" + sid + "</SID>")
        'textBuilder.AppendLine("<RemoteAddress>" + address + "</RemoteAddress>")
        textBuilder.AppendLine("</Customer>")
        textBuilder.AppendLine("<Message>")
        textBuilder.AppendLine("<MessageID>REQHOT</MessageID>")
        textBuilder.AppendLine("<MessageDesc></MessageDesc>")
        textBuilder.AppendLine("</Message>")
        textBuilder.AppendLine("<PayLoad>")
        'textBuilder.AppendLine("<BusinessHours>0</BusinessHours>")
        'textBuilder.AppendLine("<RentalLocationID>" + rentallocation + "</RentalLocationID>")

        textBuilder.AppendLine("</PayLoad>")
        textBuilder.AppendLine("</TRNXML>")

        result = textBuilder.ToString()
        Return result
    End Function

    Public Function HotDeals() As String
        Dim Xml
        Xml = RequestMaker.MakeRateRequest(RequestMaker.RequestDeals())


        Dim str As String
        Dim ratePriceNew As String = ""
        Dim locationsPath = ""
        Dim DateDay = ""
        Dim rateDoller As String = ""
        Dim sbd As New StringBuilder()

        Dim doc As XDocument = XDocument.Parse(Xml)
        Dim rq As New RequestMaker

        '  Response.Write("BEFORE")
        For Each deal As XElement In doc...<HotDetails>
            Dim loc As String = deal...<RentalLocationID>.Value
            Dim rate As String = deal...<BaseRate>.Value & " " & deal...<RatePlan>.Value
            Dim ddt As DateTime = deal...<ReturnDate>.Value
            Dim pdt As DateTime = deal...<PickupDate>.Value
            Dim putime = pdt.ToShortTimeString
            Dim dutime = ddt.ToShortTimeString

            Dim returndate = deal...<ReturnDate>.Value
            Dim pickupdate = deal...<PickupDate>.Value


            str = "pudt=" & pdt.ToShortDateString & "&putime=" & putime & "&dodt=" & ddt.ToShortDateString & "&dotime=" & dutime & "&ploc=" & loc & "&rloc=" & loc & "&src=deals&BackTo=MEX"
            'hploc.NavigateUrl = "list.aspx?" & str()




            rateDoller = Replace(rate, ".00 DAILY", "")
            Dim ratePrice As String = rateDoller

            If ratePrice = ratePriceNew Then
                locationsPath += String.Format("<a href=""list.aspx?" & str & """ >" & rq.GetLocation(loc) & "</a><br>")
                DateDay += pdt.ToString("MMM") & " " & pdt.Day.ToString & " - " & ddt.Day.ToString & " " & ddt.ToString("MMM") & "<br>"
                ratePriceNew = rateDoller

            Else
                If ratePriceNew = "" Then
                    locationsPath += String.Format("<a href=""list.aspx?" & str & """ style='color:white '>" & rq.GetLocation(loc) & "</a><br>")
                    'locationsPath = rq.GetLocation(loc) & "<br>"
                    DateDay = pdt.ToString("MMM") & " " & pdt.Day.ToString & " - " & ddt.Day.ToString & " " & ddt.ToString("MMM") & "<br>"
                    ratePriceNew = rateDoller
                Else
                    sbd.AppendLine(" <div class='col-lg-12'>")
                    sbd.AppendLine("<div class='col-lg-3' style='text-align: left;'>")

                    sbd.AppendLine("<a href=""list.aspx?" & str & """ style='color:white '>" & locationsPath & "</a></div>")
                    sbd.AppendLine("<div class='col-lg-5'> " & DateDay & "</div>")
                    sbd.AppendLine("<div class='col-lg-4'>" & ratePriceNew & "</div>")
                    sbd.AppendLine("</div>")
                    locationsPath = ""
                    DateDay = ""
                    locationsPath = rq.GetLocation(loc) & "<br>"
                    DateDay = pdt.ToString("MMM") & " " & pdt.Day.ToString & " - " & ddt.Day.ToString & " " & ddt.ToString("MMM") & "<br>"
                    ratePriceNew = rateDoller
                End If
            End If




        Next
        sbd.AppendLine(" <div class='col-lg-12'>")
        sbd.AppendLine("<div class='col-lg-3' style='text-align: left;'>")
        sbd.AppendLine(locationsPath + "</div>")
        'sbd.AppendLine("<a href=""list.aspx?" & str & """ style='color:white '>" & locationsPath & "</a></div>")
        sbd.AppendLine("<div class='col-lg-5'> " & DateDay & "</div>")
        sbd.AppendLine("<div class='col-lg-4'>" & rateDoller & "</div>")
        sbd.AppendLine("</div>")
        Return Xml 'sbd.ToString
    End Function

 Public Shared Sub WriteSchema(ByVal fileName As String, ByVal xml As String)
        Dim strFilePath As String = HttpContext.Current.Server.MapPath("~/xmlschemas/" + fileName)
        If Not Directory.Exists(Path.GetDirectoryName(strFilePath)) Then
            Directory.CreateDirectory(Path.GetDirectoryName(strFilePath))
        End If
        System.IO.File.AppendAllText(strFilePath, xml)

    End Sub
    Public Shared Function GetMD5HashData(hasstring As String) As String
        Dim encyroted As String = ""
        Dim mds As MD5 = MD5.Create()

        '  //convert the input text to array of bytes
        Dim hashData As Byte() = mds.ComputeHash(Encoding.Default.GetBytes(hasstring))

        '//create new instance of StringBuilder to save hashed data
        Dim returnValue As StringBuilder = New StringBuilder()
        '//loop for each byte and add it to StringBuilder
        For index = 0 To hashData.Length - 1
            returnValue.Append(hashData(index).ToString("X2"))
        Next
        '// return hexadecimal string
        Return returnValue.ToString()
        Return encyroted
    End Function
  
End Class