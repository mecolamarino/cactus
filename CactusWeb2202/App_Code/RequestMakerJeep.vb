﻿Imports System.Net
Imports System.IO
Imports System.Collections.Generic
Imports System.Data
Imports System.Xml
Imports System.Xml.Linq

Public Class RequestMakerJeep
    Public Sub New()

    End Sub

    Shared req As WebRequest
    Shared rep As WebResponse
    Shared senderID As String = System.Configuration.ConfigurationManager.AppSettings("senderID")
    Shared Trading As String = System.Configuration.ConfigurationManager.AppSettings("TradingPartnerCode")
    Shared customer As String = System.Configuration.ConfigurationManager.AppSettings("CustomerNumber")
    Shared url As String = System.Configuration.ConfigurationManager.AppSettings("url")


    Public Shared Function RateRequest(ByVal pickUpdateTime As String, ByVal dropOffDateTime As String, ByVal classCode As String, ByVal sid As String, ByVal address As String, ByVal rentallocation As String, ByVal returnlocation As String, ByVal discount As String, ratecode As String) As String
        Dim result As String
        Dim textBuilder As New Text.StringBuilder()

        textBuilder.AppendLine("<TRNXML Version=""1.0.0"">")
        textBuilder.AppendLine("<Dategmtime TimeZone=""ET"">" + DateTime.Now.ToString + "</Dategmtime>")
        textBuilder.AppendLine("<Sender>")
        textBuilder.AppendLine("<SenderID>" & senderID & "</SenderID>")
        textBuilder.AppendLine("</Sender>")
        textBuilder.AppendLine("<Recipient>")
        textBuilder.AppendLine("<RecipientID>TRN</RecipientID>")
        textBuilder.AppendLine("</Recipient>")
        textBuilder.AppendLine("<TradingPartner>")
        textBuilder.AppendLine("<TradingPartnerCode>" & Trading & "</TradingPartnerCode>")
        textBuilder.AppendLine("</TradingPartner>")
        textBuilder.AppendLine("<Customer>")
        textBuilder.AppendLine("<CustomerNumber>" & customer & "</CustomerNumber>")
        textBuilder.AppendLine("<Passcode>" & customer & "</Passcode>")
        'textBuilder.AppendLine("<SID>" + sid + "</SID>")
        'textBuilder.AppendLine("<RemoteAddress>" + address + "</RemoteAddress>")
        textBuilder.AppendLine("</Customer>")
        textBuilder.AppendLine("<Message>")
        textBuilder.AppendLine("<MessageID>REQRAT</MessageID>")
        textBuilder.AppendLine("<MessageDesc>Request Rates</MessageDesc>")
        textBuilder.AppendLine("</Message>")
        textBuilder.AppendLine("<Payload>")
        'textBuilder.AppendLine("<BusinessHours>0</BusinessHours>")
        textBuilder.AppendLine("<RentalLocationID>TRAC</RentalLocationID>")
        textBuilder.AppendLine("<ReturnLocationID>TRAC</ReturnLocationID>")
        textBuilder.AppendLine("<PickupDateTime>" + pickUpdateTime + "</PickupDateTime>")
        textBuilder.AppendLine("<ReturnDateTime>" + dropOffDateTime + "</ReturnDateTime>")
        textBuilder.AppendLine("<RateSource>WebLink</RateSource>")
        textBuilder.AppendLine("<ClassCode>" & classCode & "</ClassCode>")
        textBuilder.AppendLine("<RateCode>" & ratecode & "</RateCode>")
        textBuilder.AppendLine("<CompanyNumber>" & discount & "</CompanyNumber>")
        Dim promo, firstletter, prcode
        promo = discount


        '   textBuilder.AppendLine("<RateCode>" & ratecode & "</RateCode>")
        textBuilder.AppendLine("<DiscountCode>" & discount & "</DiscountCode>")
        ' textBuilder.AppendLine("<CompanyNumber>FXWEK</CompanyNumber>")
        textBuilder.AppendLine("<TotalPricing>1</TotalPricing>")
        textBuilder.AppendLine("</Payload>")
        textBuilder.AppendLine("</TRNXML>")

        result = textBuilder.ToString()
        Return result

        'Dim result As String
        'Dim textBuilder As New Text.StringBuilder()
        'textBuilder.AppendLine("<TRNXML Version=""1.0.0"">")
        'textBuilder.AppendLine("<Dategmtime TimeZone=""ET"">" + DateTime.Now.ToString + "</Dategmtime>")
        'textBuilder.AppendLine("<Sender>")
        'textBuilder.AppendLine("<SenderID>HLD01</SenderID>")
        'textBuilder.AppendLine("</Sender>")
        'textBuilder.AppendLine("<Recipient>")
        'textBuilder.AppendLine("<RecipientID>TRN</RecipientID>")
        'textBuilder.AppendLine("</Recipient>")
        'textBuilder.AppendLine("<TradingPartner>")
        'textBuilder.AppendLine("<TradingPartnerCode>WEB01</TradingPartnerCode>")
        'textBuilder.AppendLine("</TradingPartner>")
        'textBuilder.AppendLine("<Customer>")
        'textBuilder.AppendLine("<CustomerNumber>41480</CustomerNumber>")
        'textBuilder.AppendLine("<Passcode>41480</Passcode>")
        'textBuilder.AppendLine("<SID>" + sid + "</SID>")
        'textBuilder.AppendLine("<RemoteAddress>" + address + "</RemoteAddress>")
        'textBuilder.AppendLine("</Customer>")
        'textBuilder.AppendLine("<Message>")
        'textBuilder.AppendLine("<MessageID>REQRAT</MessageID>")
        'textBuilder.AppendLine("<MessageDesc>Request Rates</MessageDesc>")
        'textBuilder.AppendLine("</Message>")
        'textBuilder.AppendLine("<Payload>")
        ''textBuilder.AppendLine("<BusinessHours>0</BusinessHours>")
        'textBuilder.AppendLine("<RentalLocationID>001</RentalLocationID>")
        'textBuilder.AppendLine("<ReturnLocationID>001</ReturnLocationID>")
        'textBuilder.AppendLine("<PickupDateTime>" + pickUpdateTime + "</PickupDateTime>")
        'textBuilder.AppendLine("<ReturnDateTime>" + dropOffDateTime + "</ReturnDateTime>")
        'textBuilder.AppendLine("<RateSource>RMS.NET</RateSource>")
        'textBuilder.AppendLine("<ClassCode>" + classCode + "</ClassCode>")
        ''textBuilder.AppendLine("<RateCode>" + rateCode + "</RateCode>")
        'textBuilder.AppendLine("<TotalPricing>1</TotalPricing>")
        'textBuilder.AppendLine("</Payload>")
        'textBuilder.AppendLine("</TRNXML>")

        'result = textBuilder.ToString()
        'Return result

    End Function
    Public Shared Function RequestClass(ByVal cls As String) As String
        Dim result As String
        Dim textBuilder As New Text.StringBuilder()
        textBuilder.AppendLine("<TRNXML Version=""1.0.0"">")
        textBuilder.AppendLine("<Dategmtime TimeZone=""ET"">" + DateTime.Now.ToString + "</Dategmtime>")
        textBuilder.AppendLine("<Sender>")
        textBuilder.AppendLine("<SenderID>" & senderID & "</SenderID>")
        textBuilder.AppendLine("</Sender>")
        textBuilder.AppendLine("<Recipient>")
        textBuilder.AppendLine("<RecipientID>TRN</RecipientID>")
        textBuilder.AppendLine("</Recipient>")
        textBuilder.AppendLine("<TradingPartner>")
        textBuilder.AppendLine("<TradingPartnerCode>" & Trading & "</TradingPartnerCode>")
        textBuilder.AppendLine("</TradingPartner>")
        textBuilder.AppendLine("<Customer>")
        textBuilder.AppendLine("<CustomerNumber>" & customer & "</CustomerNumber>")
        textBuilder.AppendLine("<Passcode>" & customer & "</Passcode>")
        'textBuilder.AppendLine("<SID>" + sid + "</SID>")
        'textBuilder.AppendLine("<RemoteAddress>" + address + "</RemoteAddress>")
        textBuilder.AppendLine("</Customer>")
        textBuilder.AppendLine("<Message>")
        textBuilder.AppendLine("<MessageID>REQCLS</MessageID>")
        textBuilder.AppendLine("<MessageDesc></MessageDesc>")
        textBuilder.AppendLine("</Message>")
        textBuilder.AppendLine("<PayLoad>")
        'textBuilder.AppendLine("<BusinessHours>0</BusinessHours>")
        textBuilder.AppendLine("<ClassCode>" & cls & "</ClassCode>")
        '  textBuilder.AppendLine("<ReturnLocationID>TAL</ReturnLocationID>")

        textBuilder.AppendLine("</PayLoad>")
        textBuilder.AppendLine("</TRNXML>")

        result = textBuilder.ToString()
        Return result



    End Function
    Public Shared Function RateRequestByID(ByVal pickUpdateTime As String, _
                                           ByVal dropOffDateTime As String, _
                                           ByVal classCode As String, ByVal rateID As String, ByVal sid As String, _
                                           ByVal address As String, ByVal rentallocation As String, ByVal returnlocation As String, ByVal extraCodes As String, ByVal discount As String) As String
        Dim result As String
        Dim textBuilder As New Text.StringBuilder()
        textBuilder.AppendLine("<TRNXML Version=""1.0.0"">")
        textBuilder.AppendLine("<Dategmtime TimeZone=""ET"">" + DateTime.Now.ToString + "</Dategmtime>")
        textBuilder.AppendLine("<Sender>")
        textBuilder.AppendLine("<SenderID>" & senderID & "</SenderID>")
        textBuilder.AppendLine("</Sender>")
        textBuilder.AppendLine("<Recipient>")
        textBuilder.AppendLine("<RecipientID>TRN</RecipientID>")
        textBuilder.AppendLine("</Recipient>")
        textBuilder.AppendLine("<TradingPartner>")
        textBuilder.AppendLine("<TradingPartnerCode>" & Trading & "</TradingPartnerCode>")
        textBuilder.AppendLine("</TradingPartner>")
        textBuilder.AppendLine("<Customer>")
        textBuilder.AppendLine("<CustomerNumber>" & customer & "</CustomerNumber>")
        textBuilder.AppendLine("<Passcode>" & customer & "</Passcode>")
        'textBuilder.AppendLine("<SID>" + sid + "</SID>")
        'textBuilder.AppendLine("<RemoteAddress>" + address + "</RemoteAddress>")
        textBuilder.AppendLine("</Customer>")
        textBuilder.AppendLine("<SID>" + sid + "</SID>")
        textBuilder.AppendLine("<RemoteAddress>" + address + "</RemoteAddress>")
        textBuilder.AppendLine("</Customer>")
        textBuilder.AppendLine("<Message>")
        textBuilder.AppendLine("<MessageID>REQRAT</MessageID>")
        textBuilder.AppendLine("<MessageDesc>Request Rates</MessageDesc>")
        textBuilder.AppendLine("</Message>")
        textBuilder.AppendLine("<PayLoad>")
        'textBuilder.AppendLine("<BusinessHours>0</BusinessHours>")
        textBuilder.AppendLine("<DiscountCode>" & discount & "</DiscountCode>")
        textBuilder.AppendLine("<RentalLocationID>TRAC</RentalLocationID>")
        textBuilder.AppendLine("<ReturnLocationID>TRAC</ReturnLocationID>")
        textBuilder.AppendLine("<PickupDateTime>" + pickUpdateTime + "</PickupDateTime>")
        textBuilder.AppendLine("<ReturnDateTime>" + dropOffDateTime + "</ReturnDateTime>")
        textBuilder.AppendLine("<PickupDateTime>" + pickUpdateTime + "</PickupDateTime>")
        textBuilder.AppendLine("<ReturnDateTime>" + dropOffDateTime + "</ReturnDateTime>")
        textBuilder.AppendLine("<RateSource>WebLink</RateSource>")
        textBuilder.AppendLine("<ClassCode>" + classCode + "</ClassCode>")
        textBuilder.AppendLine("<RateCode></RateCode>")
        textBuilder.AppendLine("<RateID>" + rateID + "</RateID>")

        If Not String.IsNullOrEmpty(extraCodes) Then

            Dim c() As Char = {"^"}

            Dim codes() As String = extraCodes.Split(c, StringSplitOptions.RemoveEmptyEntries)

            For Each code As String In codes
                textBuilder.AppendLine("<ExtraCode>" & code & "</ExtraCode>")
            Next

        End If

        textBuilder.AppendLine("<TotalPricing>1</TotalPricing>")
        textBuilder.AppendLine("</PayLoad>")
        textBuilder.AppendLine("</TRNXML>")

        result = textBuilder.ToString()
        Return result
    End Function

    Public Shared Function RateRequestByCode(ByVal pickUpdateTime As String, _
                                           ByVal dropOffDateTime As String, _
                                           ByVal classCode As String, ByVal rateCode As String, ByVal sid As String, _
                                           ByVal address As String, ByVal rentallocation As String, ByVal returnlocation As String) As String
        Dim result As String
        Dim textBuilder As New Text.StringBuilder()
        textBuilder.AppendLine("<TRNXML Version=""1.0.0"">")
        textBuilder.AppendLine("<Dategmtime TimeZone=""ET"">" + DateTime.Now.ToString + "</Dategmtime>")
        textBuilder.AppendLine("<Sender>")
        textBuilder.AppendLine("<SenderID>" & senderID & "</SenderID>")
        textBuilder.AppendLine("</Sender>")
        textBuilder.AppendLine("<Recipient>")
        textBuilder.AppendLine("<RecipientID>TRN</RecipientID>")
        textBuilder.AppendLine("</Recipient>")
        textBuilder.AppendLine("<TradingPartner>")
        textBuilder.AppendLine("<TradingPartnerCode>" & Trading & "</TradingPartnerCode>")
        textBuilder.AppendLine("</TradingPartner>")
        textBuilder.AppendLine("<Customer>")
        textBuilder.AppendLine("<CustomerNumber>" & customer & "</CustomerNumber>")
        textBuilder.AppendLine("<Passcode>" & customer & "</Passcode>")
        'textBuilder.AppendLine("<SID>" + sid + "</SID>")
        'textBuilder.AppendLine("<RemoteAddress>" + address + "</RemoteAddress>")
        textBuilder.AppendLine("</Customer>")
        'textBuilder.AppendLine("<SID>" + sid + "</SID>")
        'textBuilder.AppendLine("<RemoteAddress>" + address + "</RemoteAddress>")
        textBuilder.AppendLine("<Message>")
        textBuilder.AppendLine("<MessageID>REQRAT</MessageID>")
        textBuilder.AppendLine("<MessageDesc>Request Rates</MessageDesc>")
        textBuilder.AppendLine("</Message>")
        textBuilder.AppendLine("<Payload>")
        'textBuilder.AppendLine("<BusinessHours>0</BusinessHours>")
        textBuilder.AppendLine("<RentalLocationID>TRAC</RentalLocationID>")
        textBuilder.AppendLine("<ReturnLocationID>TRAC</ReturnLocationID>")
        textBuilder.AppendLine("<PickupDateTime>" + pickUpdateTime + "</PickupDateTime>")
        textBuilder.AppendLine("<ReturnDateTime>" + dropOffDateTime + "</ReturnDateTime>")
        textBuilder.AppendLine("<RateSource>WebLink</RateSource>")
        textBuilder.AppendLine("<ClassCode>" + classCode + "</ClassCode>")
        'textBuilder.AppendLine("<RateCode>" + rateCode + "</RateCode>")
        textBuilder.AppendLine("<TotalPricing>1</TotalPricing>")
        textBuilder.AppendLine("</Payload>")
        textBuilder.AppendLine("</TRNXML>")

        result = textBuilder.ToString()
        Return result
    End Function


    Public Shared Function RequestRezLoc() As String
        Dim result As String
        Dim textBuilder As New Text.StringBuilder()
        textBuilder.AppendLine("<TRNXML Version=""1.0.0"">")
        textBuilder.AppendLine("<Dategmtime TimeZone=""ET"">" + DateTime.Now.ToString + "</Dategmtime>")
        textBuilder.AppendLine("<Sender>")
        textBuilder.AppendLine("<SenderID>" & senderID & "</SenderID>")
        textBuilder.AppendLine("</Sender>")
        textBuilder.AppendLine("<Recipient>")
        textBuilder.AppendLine("<RecipientID>TRN</RecipientID>")
        textBuilder.AppendLine("</Recipient>")
        textBuilder.AppendLine("<TradingPartner>")
        textBuilder.AppendLine("<TradingPartnerCode>" & Trading & "</TradingPartnerCode>")
        textBuilder.AppendLine("</TradingPartner>")
        textBuilder.AppendLine("<Customer>")
        textBuilder.AppendLine("<CustomerNumber>" & customer & "</CustomerNumber>")
        textBuilder.AppendLine("<Passcode>" & customer & "</Passcode>")
        'textBuilder.AppendLine("<SID>" + sid + "</SID>")
        'textBuilder.AppendLine("<RemoteAddress>" + address + "</RemoteAddress>")
        textBuilder.AppendLine("</Customer>")
        textBuilder.AppendLine("<Message>")
        textBuilder.AppendLine("<MessageID>REQLOC</MessageID>")
        textBuilder.AppendLine("<MessageDesc>Request Locations</MessageDesc>")
        textBuilder.AppendLine("</Message>")
        textBuilder.AppendLine("<PayLoad>")
        textBuilder.AppendLine("<RentalLocationID></RentalLocationID>")
        textBuilder.AppendLine("<BusinessHours>0</BusinessHours>")
        textBuilder.AppendLine("<IgnoreStatus></IgnoreStatus>")
        '  textBuilder.AppendLine("<ReturnLocationID>TAL</ReturnLocationID>")
        textBuilder.AppendLine("</PayLoad>")
        textBuilder.AppendLine("</TRNXML>")
        result = textBuilder.ToString()
        Return result
    End Function
    Public Shared Function RequestExtras(ByVal rentallocation As String, cls As String) As String
        Dim result As String
        Dim textBuilder As New Text.StringBuilder()
        textBuilder.AppendLine("<TRNXML Version=""1.0.0"">")
        textBuilder.AppendLine("<Dategmtime TimeZone=""ET"">" + DateTime.Now.ToString + "</Dategmtime>")
        textBuilder.AppendLine("<Sender>")
        textBuilder.AppendLine("<SenderID>" & senderID & "</SenderID>")
        textBuilder.AppendLine("</Sender>")
        textBuilder.AppendLine("<Recipient>")
        textBuilder.AppendLine("<RecipientID>TRN</RecipientID>")
        textBuilder.AppendLine("</Recipient>")
        textBuilder.AppendLine("<TradingPartner>")
        textBuilder.AppendLine("<TradingPartnerCode>" & Trading & "</TradingPartnerCode>")
        textBuilder.AppendLine("</TradingPartner>")
        textBuilder.AppendLine("<Customer>")
        textBuilder.AppendLine("<CustomerNumber>" & customer & "</CustomerNumber>")
        textBuilder.AppendLine("<Passcode>" & customer & "</Passcode>")
        'textBuilder.AppendLine("<SID>" + sid + "</SID>")
        'textBuilder.AppendLine("<RemoteAddress>" + address + "</RemoteAddress>")
        textBuilder.AppendLine("</Customer>")
        textBuilder.AppendLine("<Message>")
        textBuilder.AppendLine("<MessageID>REQEXT</MessageID>")
        textBuilder.AppendLine("<MessageDesc></MessageDesc>")
        textBuilder.AppendLine("</Message>")
        textBuilder.AppendLine("<PayLoad>")
        'textBuilder.AppendLine("<BusinessHours>0</BusinessHours>")
        textBuilder.AppendLine("<RentalLocationID>TRAC</RentalLocationID>")
        textBuilder.AppendLine("<ClassCode>" & cls & "</ClassCode>")
        '  textBuilder.AppendLine("<ReturnLocationID>TAL</ReturnLocationID>")

        textBuilder.AppendLine("</PayLoad>")
        textBuilder.AppendLine("</TRNXML>")

        result = textBuilder.ToString()
        Return result
    End Function


    'Public Shared Function RateRequestByExtras(ByVal pickUpdateTime As String, _
    '                                       ByVal dropOffDateTime As String, _
    '                                       ByVal classCode As String, ByVal rateCode As String, ByVal sid As String, _
    '                                       ByVal address As String, ByVal rentallocation As String, ByVal returnlocation As String, ByVal rateID As String) As String
    '    Dim result As String
    '    Dim textBuilder As New Text.StringBuilder()
    '    textBuilder.AppendLine("<TRNXML Version=""1.0.0"">")
    '    textBuilder.AppendLine("<Dategmtime TimeZone=""ET"">" + DateTime.Now.ToString + "</Dategmtime>")
    '    textBuilder.AppendLine("<Sender>")
    '    textBuilder.AppendLine("<SenderID>" & senderID & "</SenderID>")
    '    textBuilder.AppendLine("</Sender>")
    '    textBuilder.AppendLine("<Recipient>")
    '    textBuilder.AppendLine("<RecipientID>TRN</RecipientID>")
    '    textBuilder.AppendLine("</Recipient>")
    '    textBuilder.AppendLine("<TradingPartner>")
    '    textBuilder.AppendLine("<TradingPartnerCode>" & Trading & "</TradingPartnerCode>")
    '    textBuilder.AppendLine("</TradingPartner>")
    '    textBuilder.AppendLine("<Customer>")
    '    textBuilder.AppendLine("<CustomerNumber>" & customer & "</CustomerNumber>")
    '    textBuilder.AppendLine("<Passcode>" & customer & "</Passcode>")
    '    'textBuilder.AppendLine("<SID>" + sid + "</SID>")
    '    'textBuilder.AppendLine("<RemoteAddress>" + address + "</RemoteAddress>")
    '    textBuilder.AppendLine("</Customer>")
    '    textBuilder.AppendLine("<Message>")
    '    textBuilder.AppendLine("<MessageID>REQBIL</MessageID>")
    '    textBuilder.AppendLine("<MessageDesc />")
    '    textBuilder.AppendLine("</Message>")
    '    textBuilder.AppendLine("<PayLoad>")
    '    'textBuilder.AppendLine("<BusinessHours>0</BusinessHours>")
    '    textBuilder.AppendLine("<RentalLocationID>" + rentallocation + "</RentalLocationID>")
    '    textBuilder.AppendLine("<ReturnLocationID>" + returnlocation + "</ReturnLocationID>")
    '    textBuilder.AppendLine("<PickupDateTime>" + pickUpdateTime + "</PickupDateTime>")
    '    textBuilder.AppendLine("<ReturnDateTime>" + dropOffDateTime + "</ReturnDateTime>")
    '    ' textBuilder.AppendLine("<RateSource>RMS.NET</RateSource>")
    '    textBuilder.AppendLine("<RateID>" + rateID + "</RateID>")
    '    textBuilder.AppendLine("<ClassCode>" + classCode + "</ClassCode>")
    '    textBuilder.AppendLine("<ExtraCode>CDW</ExtraCode>")
    '    'textBuilder.AppendLine("<ExtraCode>CHILDSEAT</ExtraCode>")
    '    textBuilder.AppendLine("</PayLoad>")
    '    textBuilder.AppendLine("</TRNXML>")

    '    result = textBuilder.ToString()
    '    Return result
    'End Function



    Public Shared Function MakeRateRequest(ByVal para As String) As String


        Dim uri As String = url

        req = WebRequest.Create(uri)
        req.Method = "POST"
        req.ContentType = "text/xml"
        Dim writer As New StreamWriter(req.GetRequestStream())
        writer.Write(para)
        writer.Close()
        rep = req.GetResponse()

        Dim receivestream As Stream = rep.GetResponseStream()

        Dim encode As Encoding = Encoding.GetEncoding("utf-8")

        Dim readstream As New StreamReader(receivestream, encode)

        Dim read(256) As [Char]

        Dim result As String

        result = readstream.ReadToEnd()

        Return result
    End Function

    Public Shared Function RequestExtras3(ByVal rentallocation As String) As String
        Dim result As String
        Dim textBuilder As New Text.StringBuilder()
        textBuilder.AppendLine("<TRNXML Version=""1.0.0"">")
        textBuilder.AppendLine("<Dategmtime TimeZone=""ET"">" + DateTime.Now.ToString + "</Dategmtime>")
        textBuilder.AppendLine("<Sender>")
        textBuilder.AppendLine("<SenderID>" & senderID & "</SenderID>")
        textBuilder.AppendLine("</Sender>")
        textBuilder.AppendLine("<Recipient>")
        textBuilder.AppendLine("<RecipientID>TRN</RecipientID>")
        textBuilder.AppendLine("</Recipient>")
        textBuilder.AppendLine("<TradingPartner>")
        textBuilder.AppendLine("<TradingPartnerCode>" & Trading & "</TradingPartnerCode>")
        textBuilder.AppendLine("</TradingPartner>")
        textBuilder.AppendLine("<Customer>")
        textBuilder.AppendLine("<CustomerNumber>" & customer & "</CustomerNumber>")
        textBuilder.AppendLine("<Passcode>" & customer & "</Passcode>")
        'textBuilder.AppendLine("<SID>" + sid + "</SID>")
        'textBuilder.AppendLine("<RemoteAddress>" + address + "</RemoteAddress>")
        textBuilder.AppendLine("</Customer>")
        textBuilder.AppendLine("<Message>")
        textBuilder.AppendLine("<MessageID>REQEXT</MessageID>")
        textBuilder.AppendLine("<MessageDesc></MessageDesc>")
        textBuilder.AppendLine("</Message>")
        textBuilder.AppendLine("<PayLoad>")
        'textBuilder.AppendLine("<BusinessHours>0</BusinessHours>")
        textBuilder.AppendLine("<RentalLocationID>" & rentallocation & "</RentalLocationID>")
        ' textBuilder.AppendLine("<ClassCode>" & cls & "</ClassCode>")
        '  textBuilder.AppendLine("<ReturnLocationID>TAL</ReturnLocationID>")

        textBuilder.AppendLine("</PayLoad>")
        textBuilder.AppendLine("</TRNXML>")

        result = textBuilder.ToString()
        Return result
    End Function

    Public Shared Function AddRezRequest(ByVal pickUpdateTime As String, _
                                          ByVal dropOffDateTime As String, ByVal rateID As String, ByVal classCode As String, ByVal sid As String, ByVal address As String, ByVal first As String, ByVal last As String, ByVal email As String, ByVal cell As String, ByVal RAddress1 As String, ByVal RAddress2 As String, ByVal city As String, ByVal state As String, ByVal zip As String, ByVal comf As String, ByVal extraCodes As ArrayList, ByVal discount As String, ByVal credittype As String, ByVal creditno As String, ByVal creditcode As String, ByVal creditexp As String, Licensenumber As String, licensestate As String, DOB As String) As String
        Dim result As String
        Dim textBuilder As New Text.StringBuilder()
        textBuilder.AppendLine("<TRNXML Version=""1.0.0"">")
        textBuilder.AppendLine("<Dategmtime TimeZone=""ET"">" + DateTime.Now.ToString + "</Dategmtime>")
        textBuilder.AppendLine("<Sender>")
        textBuilder.AppendLine("<SenderID>" & senderID & "</SenderID>")
        textBuilder.AppendLine("</Sender>")
        textBuilder.AppendLine("<Recipient>")
        textBuilder.AppendLine("<RecipientID>TRN</RecipientID>")
        textBuilder.AppendLine("</Recipient>")
        textBuilder.AppendLine("<TradingPartner>")
        textBuilder.AppendLine("<TradingPartnerCode>WEB01</TradingPartnerCode>")
        textBuilder.AppendLine("</TradingPartner>")
        textBuilder.AppendLine("<Customer>")
        textBuilder.AppendLine("<CustomerNumber>" & customer & "</CustomerNumber>")
        textBuilder.AppendLine("<Passcode>" & customer & "</Passcode>")
        textBuilder.AppendLine("<SID>" + sid + "</SID>")
        textBuilder.AppendLine("<RemoteAddress>" + address + "</RemoteAddress>")
        textBuilder.AppendLine("</Customer>")
        textBuilder.AppendLine("<Message>")
        textBuilder.AppendLine("<MessageID>ADDREZ</MessageID>")
        textBuilder.AppendLine("<MessageDesc>Add Reservation</MessageDesc>")
        textBuilder.AppendLine("</Message>")
        textBuilder.AppendLine("<Payload>")
        If comf <> "" Then
            textBuilder.AppendLine("<ConfirmNum>" + comf + "</ConfirmNum>")
        End If
        textBuilder.AppendLine("<RentalLocationID>TRAC</RentalLocationID>")
        textBuilder.AppendLine("<ReturnLocationID>TRAC</ReturnLocationID>")
        textBuilder.AppendLine("<PickupDateTime>" + pickUpdateTime + "</PickupDateTime>")
        textBuilder.AppendLine("<ReturnDateTime>" + dropOffDateTime + "</ReturnDateTime>")
        textBuilder.AppendLine("<RateID>" + rateID + "</RateID>")
        textBuilder.AppendLine("<ClassCode>" + classCode + "</ClassCode>")
        textBuilder.AppendLine("<CompanyNumber>" & discount & "</CompanyNumber>")
        textBuilder.AppendLine("<DiscountCode>" & discount & "</DiscountCode>")
        ' textBuilder.AppendLine("<RentalComments>" + comments + "</RentalComments>")
        textBuilder.AppendLine("<RenterFirst>" + first + "</RenterFirst>")
        textBuilder.AppendLine("<RenterLast>" + last + "</RenterLast>")
        textBuilder.AppendLine("<EmailAddress>" + email + "</EmailAddress>")
        textBuilder.AppendLine("<RenterHomePhone>" + cell + "</RenterHomePhone>")
        textBuilder.AppendLine("<CCType>" + credittype + "</CCType>")
        textBuilder.AppendLine("<CCNumber>" + creditno + "</CCNumber>")
        textBuilder.AppendLine("<CCExp>" + creditexp + "</CCExp>")
        ' textBuilder.AppendLine("<CardDepositAmount>" + rateCode + "</CardDepositAmount>")
        textBuilder.AppendLine("<CardAuthNumber>" + creditcode + "</CardAuthNumber>")
        textBuilder.AppendLine("<LicenseNumber>" + Licensenumber + "</LicenseNumber>")
        textBuilder.AppendLine("<LicenseState>" + licensestate + "</LicenseState>>")

        textBuilder.AppendLine("<RenterDOB>" + DOB + "</RenterDOB>")

        If extraCodes IsNot Nothing Then
            For Each extraCode As String In extraCodes
                textBuilder.AppendLine("<ExtraCode>" & extraCode & "</ExtraCode>")
            Next
        Else
            If Not String.IsNullOrEmpty(comf) Then
                textBuilder.AppendLine("<ExtraCode></ExtraCode>")
            End If
        End If

        'textBuilder.AppendLine("<RateCode>" + rateCode + "</RateCode>")
        textBuilder.AppendLine("<TotalPricing>1</TotalPricing>")
        textBuilder.AppendLine("</Payload>")
        textBuilder.AppendLine("</TRNXML>")

        result = textBuilder.ToString()
        Return result
    End Function

    Public Shared Function CancelReservation(ByVal confno As String, ByVal sid As String, ByVal address As String) As String
        Dim result As String
        Dim textBuilder As New Text.StringBuilder()
        textBuilder.AppendLine("<TRNXML Version=""1.0.0"">")
        textBuilder.AppendLine("<Dategmtime TimeZone=""ET"">" + DateTime.Now.ToString + "</Dategmtime>")
        textBuilder.AppendLine("<Sender>")
        textBuilder.AppendLine("<SenderID>" & senderID & "</SenderID>")
        textBuilder.AppendLine("</Sender>")
        textBuilder.AppendLine("<Recipient>")
        textBuilder.AppendLine("<RecipientID>TRN</RecipientID>")
        textBuilder.AppendLine("</Recipient>")
        textBuilder.AppendLine("<TradingPartner>")
        textBuilder.AppendLine("<TradingPartnerCode>WEB01</TradingPartnerCode>")
        textBuilder.AppendLine("</TradingPartner>")
        textBuilder.AppendLine("<Customer>")
        textBuilder.AppendLine("<CustomerNumber>" & customer & "</CustomerNumber>")
        textBuilder.AppendLine("<Passcode>" & customer & "</Passcode>")
        textBuilder.AppendLine("<SID>" + sid + "</SID>")
        textBuilder.AppendLine("<RemoteAddress>" + address + "</RemoteAddress>")
        textBuilder.AppendLine("</Customer>")
        textBuilder.AppendLine("<Message>")
        textBuilder.AppendLine("<MessageID>REQCAN</MessageID>")
        textBuilder.AppendLine("<MessageDesc>Request Rates</MessageDesc>")
        textBuilder.AppendLine("</Message>")
        textBuilder.AppendLine("<Payload>")
        textBuilder.AppendLine("<ConfirmNum>" & confno & "</ConfirmNum>")
        textBuilder.AppendLine("</Payload>")
        textBuilder.AppendLine("</TRNXML>")

        result = textBuilder.ToString()
        Return result
    End Function

    Public Shared Function EditReservation(ByVal confno As String, ByVal sid As String, ByVal address As String) As String
        Dim result As String
        Dim textBuilder As New Text.StringBuilder()
        textBuilder.AppendLine("<TRNXML Version=""1.0.0"">")
        textBuilder.AppendLine("<Dategmtime TimeZone=""ET"">" + DateTime.Now.ToString + "</Dategmtime>")
        textBuilder.AppendLine("<Sender>")
        textBuilder.AppendLine("<SenderID>" & senderID & "</SenderID>")
        textBuilder.AppendLine("</Sender>")
        textBuilder.AppendLine("<Recipient>")
        textBuilder.AppendLine("<RecipientID>TRN</RecipientID>")
        textBuilder.AppendLine("</Recipient>")
        textBuilder.AppendLine("<TradingPartner>")
        textBuilder.AppendLine("<TradingPartnerCode>" & Trading & "</TradingPartnerCode>")
        textBuilder.AppendLine("</TradingPartner>")
        textBuilder.AppendLine("<Customer>")
        textBuilder.AppendLine("<CustomerNumber>" & customer & "</CustomerNumber>")
        textBuilder.AppendLine("<Passcode>" & customer & "</Passcode>")
        textBuilder.AppendLine("<SID>" + sid + "</SID>")
        textBuilder.AppendLine("<RemoteAddress>" + address + "</RemoteAddress>")
        textBuilder.AppendLine("</Customer>")
        textBuilder.AppendLine("<Message>")
        textBuilder.AppendLine("<MessageID>REQREZ</MessageID>")
        textBuilder.AppendLine("<MessageDesc></MessageDesc>")
        textBuilder.AppendLine("</Message>")
        textBuilder.AppendLine("<PayLoad>")
        textBuilder.AppendLine("<ConfirmNum>" & confno & "</ConfirmNum>")
        textBuilder.AppendLine("</PayLoad>")
        textBuilder.AppendLine("</TRNXML>")

        result = textBuilder.ToString()
        Return result
    End Function

    ' Private _Classes As Dictionary(Of String, String)
    Public Shared ReadOnly Property Classes() As Dictionary(Of String, String)
        Get
            Dim _Classes As New Dictionary(Of String, String)
            _Classes.Add("ECAR", "Mazda 2 – Publicity Vehicle")
            _Classes.Add("CCAR", "Mazda 2 or similar")
            _Classes.Add("ICAR", "Mazda 3 or similar")
            _Classes.Add("SCAR", "Toyota Corolla  or similar")
            _Classes.Add("FCAR", "Toyota Camry  or similar")
            _Classes.Add("PCAR", "Chrysler 300 or similar")
            _Classes.Add("LCAR", "Maxima (Leather, Roof, Bluetooth)")
            _Classes.Add("IFAR", "Nissan Xterra 5 Passenger ")
            _Classes.Add("FFAR", "Nissan Pathfinder 7 Passenger")
            _Classes.Add("PFAR", "Hummer H3")
            _Classes.Add("MVAR", "Minivan – Toyota Sienna – 7 Passenger")
            _Classes.Add("XVAR", "Toyota Sienna – 8 Passenger")
            Return _Classes
        End Get
        'Set(ByVal value As Dictionary(Of String, String))
        '    _Classes = value
        'End Set
    End Property


    Public Shared ReadOnly Property ClassesDesc() As Dictionary(Of String, String)
        Get
            Dim _Classes As New Dictionary(Of String, String)
            _Classes.Add("ECAR", "Economy")
            _Classes.Add("CCAR", "Compact")
            _Classes.Add("ICAR", "Intermediate")
            _Classes.Add("SCAR", "Midsize ")
            _Classes.Add("FCAR", "Fullsize")
            _Classes.Add("PCAR", "Premium ")
            _Classes.Add("LCAR", "Luxury ")
            _Classes.Add("IFAR", "SUV 4X4 Standard – Nissan Xterra 5 Passenger ")
            _Classes.Add("FFAR", "SUV 4X4 Fullsize – Nissan Pathfinder 7 Passenger")
            _Classes.Add("PFAR", "Premium3")
            _Classes.Add("MVAR", "Minivan – Toyota Sienna – 7 Passenger")
            _Classes.Add("XVAR", "Minivan – Toyota Sienna – 8 Passenger")
            Return _Classes
        End Get
        'Set(ByVal value As Dictionary(Of String, String))
        '    _Classes = value
        'End Set
    End Property

    Public Shared ReadOnly Property ClassesFr() As Dictionary(Of String, String)
        Get
            Dim _Classes As New Dictionary(Of String, String)
            _Classes.Add("ECAR", "Mazda 2 – Véhicule Publicitaire")
            _Classes.Add("CCAR", "Mazda 2 ou semblable")
            _Classes.Add("ICAR", "Mazda 3 ou semblable")
            _Classes.Add("SCAR", "Toyota Corolla ou semblable")
            _Classes.Add("FCAR", "Toyota Camry ou semblable")
            _Classes.Add("PCAR", "Chrysler 300 ou semblable")
            _Classes.Add("LCAR", "Maxima (Cuir, Toit, Bluetooth)")
            _Classes.Add("IFAR", "Nissan Rogue 5 Passagers")
            _Classes.Add("FFAR", "Nissan Pathfinder 7 Passagers")
            _Classes.Add("PFBR", "Hummer H3")
            _Classes.Add("MVAR", "Toyota Sienna – 7 Passagers")
            _Classes.Add("XVAR", "Toyota Sienna – 8 Passagers")
            Return _Classes
        End Get
        'Set(ByVal value As Dictionary(Of String, String))
        '    _Classes = value
        'End Set
    End Property





    Public Shared ReadOnly Property ClassesModelFr() As Dictionary(Of String, String)
        Get
            Dim _Classes As New Dictionary(Of String, String)
            _Classes.Add("ECAR", "Econo. 4 Portes, Auto, CD, AC, Véhicule Publicitaire")
            _Classes.Add("CCAR", "Compacte 4 Portes, Auto, CD, AC, Groupe Elec.")
            _Classes.Add("ICAR", "Intermédiaire 4 Portes, Auto, CD, AC, Groupe Elec.")
            _Classes.Add("SCAR", "Standard 4 Portes, Auto, CD, AC, Groupe Elec.")
            _Classes.Add("FCAR", "Pleine Grandeur  4 Portes, Auto, CD, AC, Groupe Elec")
            _Classes.Add("PCAR", "Prestige 4 Portes, Auto, CD, AC, Groupe Elec")
            _Classes.Add("LCAR", "De Luxe 4 Portes, Auto, CD, AC")
            _Classes.Add("IFAR", "VUS 4X4 – 5 Passagers, Toutes Equipée")
            _Classes.Add("FFAR", "VUS 4X4 Pleine Grandeur – Toutes Equipée")
            _Classes.Add("PFBR", "VUS 4X4 Premium – Hummer, Toutes Equipée")
            _Classes.Add("MVAR", "Mini Fourgonnette – Toutes Equipée")
            _Classes.Add("XVAR", "Mini Fourgonnette – Toutes Equipée")
            Return _Classes
        End Get
        'Set(ByVal value As Dictionary(Of String, String))
        '    _Classes = value
        'End Set
    End Property



    Public Shared ReadOnly Property ClassOrders() As Dictionary(Of String, Double)
        Get
            Dim orders As New Dictionary(Of String, Double)
            orders.Add("HYE", 1)
            orders.Add("HYA", 2)
            orders.Add("PTC", 3)
            orders.Add("PTCC", 4)
            orders.Add("JC42", 5)
            orders.Add("P42B", 6)
            orders.Add("P44B", 7)
            orders.Add("P42A", 8)
            orders.Add("P44A", 9)
            orders.Add("P42P", 10)
            orders.Add("P44P", 11)
            orders.Add("L44R", 12)
            orders.Add("L44A", 13)
            orders.Add("L44P", 14)
            orders.Add("CEX2", 15)
            orders.Add("CEX4", 16)
            orders.Add("MTE2", 17)
            orders.Add("MTE4", 18)
            orders.Add("W24C", 19)
            orders.Add("R24H", 20)
            orders.Add("W44C", 21)
            orders.Add("W44H", 22)
            orders.Add("X44K", 23)
            orders.Add("X44E", 24)
            orders.Add("RANA", 25)
            orders.Add("RANX", 26)
            orders.Add("NF22", 27)
            orders.Add("DR22", 28)
            orders.Add("FF2D", 29)
            orders.Add("DK44", 30)
            orders.Add("NFP2", 31)
            orders.Add("NFP4", 32)
            orders.Add("DR42", 33)
            orders.Add("DR4A", 34)
            orders.Add("DR4G", 35)
            orders.Add("DR44", 36)
            orders.Add("V08B", 37)
            orders.Add("V12B", 38)
            orders.Add("V15B", 39)
            orders.Add("V12A", 40)
            orders.Add("V15A", 41)
            orders.Add("NPR2", 42)
            orders.Add("WCBT", 43)
            orders.Add("GCVB", 44)
            orders.Add("DD42", 45)
            orders.Add("DD44", 46)
            orders.Add("FF44", 47)
            orders.Add("TFBT", 48)
            '   orders.Add("VO8B", 49)









            Return orders
        End Get
    End Property

    Public Shared ReadOnly Property linkPages() As Dictionary(Of String, String)
        Get
            Dim _links As New Dictionary(Of String, String)
            _links.Add("CCAR", "vehicles-compact.asp")
            _links.Add("SCAR", "vehicles-standard.asp")
            _links.Add("FCAR", "vehicles-fullsize.asp")
            _links.Add("FQAR", "vehicles-fullsizePIckup.asp")
            _links.Add("MVAR", "vehicles-minivan.asp")
            _links.Add("PFAR", "vehicles-suv.asp")
            _links.Add("PJAR", "vehicles-Jeep.asp")
            _links.Add("STAR", "vehicles-convertible.asp")
            _links.Add("WCAR", "vehicles-luxury.asp")
            Return _links
        End Get
    End Property



    Public Shared Function GetClasses(ByVal ClassCode As String) As String
        Return Classes(ClassCode)
    End Function



    Public Function GetLocation(ByVal loc) As String
        If loc = "MAIN" Then
            loc = "MAIN OFFICE. PO BOX 1781"
        ElseIf loc = "INTL" Then
            loc = "PHILLIP GOLDSON AIRPORT-LADYVILLE"
        End If
        Return loc

    End Function


    Public Sub GetAddress(ByVal loc, ByRef address, ByRef city, ByRef state, ByRef zip, ByRef phone)
        If loc = "TMR" Then
            address = "NA"
            city = "NA"
            state = "NA"
            zip = "NA"
            phone = "NA"

        ElseIf loc = "YUL" Then
            address = "NA"
            city = "NA"
            state = "NA"
            zip = "NA "
            phone = "NA"

        ElseIf loc = "AMT" Then
            address = "NA"
            city = "NA"
            state = "NA"
            zip = "NA"
            phone = "NA"
        ElseIf loc = "ATW" Then
            address = "NA"
            city = "NA"
            state = "NA"
            zip = "NA"
            phone = "NA"
        End If

    End Sub


    Public Sub RetainVehicleInfo()
        Dim pickupdate, dropoffdatedate As String
        Dim pickuptime, dropofftime As String



        pickuptime = Trim(HttpContext.Current.Request.QueryString("putime"))
        dropofftime = Trim(HttpContext.Current.Request.QueryString("putime"))

        Dim ploc As String
        Dim dloc As String
        Dim loc As New RequestMaker


        ploc = Trim(HttpContext.Current.Request.QueryString("ploc"))

        dloc = Trim(HttpContext.Current.Request.QueryString("rloc"))


        HttpContext.Current.Response.Redirect("search.aspx?pudt=" & Trim(HttpContext.Current.Request.QueryString("pudt")) & "&dodt=" & Trim(HttpContext.Current.Request.QueryString("dodt")) & "&dotime=" & dropofftime & "&putime=" & pickuptime & "&ploc=" & ploc & "&rloc=" & dloc)

    End Sub


    Public Sub ChangeVehicle()
        Dim pickupdate, dropoffdatedate As String
        Dim pickuptime, dropofftime As String
        Dim disc


        pickuptime = Trim(HttpContext.Current.Request.QueryString("putime"))
        dropofftime = Trim(HttpContext.Current.Request.QueryString("putime"))
        disc = Trim(HttpContext.Current.Request.QueryString("disc"))

        Dim ploc As String
        Dim dloc As String
        Dim loc As New RequestMaker


        ploc = Trim(HttpContext.Current.Request.QueryString("ploc"))

        dloc = Trim(HttpContext.Current.Request.QueryString("rloc"))


        HttpContext.Current.Response.Redirect("list.aspx?pudt=" & Trim(HttpContext.Current.Request.QueryString("pudt")) & "&dodt=" & Trim(HttpContext.Current.Request.QueryString("dodt")) & "&dotime=" & dropofftime & "&putime=" & pickuptime & "&ploc=" & ploc & "&rloc=" & dloc & "&disc=" & disc)

    End Sub


    Function processDateString(ByVal value As String) As String
        Dim re() As String
        Dim c() As Char = {"/"c}
        re = value.Split(c)
        If re(0).Length < 2 Then
            re(0) = "0" + re(0)
        End If
        If re(1).Length < 2 Then
            re(1) = "0" + re(1)
        End If
        Return re(0) + re(1) + re(2)
    End Function



    Public Shared Function RateRequestByExtras(ByVal pickUpdateTime As String, _
                                          ByVal dropOffDateTime As String, _
                                          ByVal classCode As String, ByVal rateCode As String, ByVal sid As String, _
                                          ByVal address As String, ByVal rentallocation As String, ByVal returnlocation As String, ByVal rateID As String, ByRef chkListExtras As CheckBoxList, ByVal discount As String) As String
        Dim result As String
        Dim textBuilder As New Text.StringBuilder()
        textBuilder.AppendLine("<TRNXML Version=""1.0.0"">")
        textBuilder.AppendLine("<Dategmtime TimeZone=""ET"">" + DateTime.Now.ToString + "</Dategmtime>")
        textBuilder.AppendLine("<Sender>")
        textBuilder.AppendLine("<SenderID>" & senderID & "</SenderID>")
        textBuilder.AppendLine("</Sender>")
        textBuilder.AppendLine("<Recipient>")
        textBuilder.AppendLine("<RecipientID>TRN</RecipientID>")
        textBuilder.AppendLine("</Recipient>")
        textBuilder.AppendLine("<TradingPartner>")
        textBuilder.AppendLine("<TradingPartnerCode>" & Trading & "</TradingPartnerCode>")
        textBuilder.AppendLine("</TradingPartner>")
        textBuilder.AppendLine("<Customer>")
        textBuilder.AppendLine("<CustomerNumber>" & customer & "</CustomerNumber>")
        textBuilder.AppendLine("<Passcode>" & customer & "</Passcode>")
        'textBuilder.AppendLine("<SID>" + sid + "</SID>")
        'textBuilder.AppendLine("<RemoteAddress>" + address + "</RemoteAddress>")
        textBuilder.AppendLine("</Customer>")
        textBuilder.AppendLine("<Message>")
        textBuilder.AppendLine("<MessageID>REQBIL</MessageID>")
        textBuilder.AppendLine("<MessageDesc />")
        textBuilder.AppendLine("</Message>")
        textBuilder.AppendLine("<PayLoad>")
        ' textBuilder.AppendLine("<DiscountCode>" & discount & "</DiscountCode>")
        'textBuilder.AppendLine("<BusinessHours>0</BusinessHours>")
        textBuilder.AppendLine("<RentalLocationID>TRAC</RentalLocationID>")
        textBuilder.AppendLine("<ReturnLocationID>TRAC</ReturnLocationID>")
        textBuilder.AppendLine("<PickupDateTime>" + pickUpdateTime + "</PickupDateTime>")
        textBuilder.AppendLine("<ReturnDateTime>" + dropOffDateTime + "</ReturnDateTime>")
        ' textBuilder.AppendLine("<RateSource>RMS.NET</RateSource>")
        textBuilder.AppendLine("<RateID>" + rateID + "</RateID>")
        textBuilder.AppendLine("<ClassCode>" + classCode + "</ClassCode>")
        ' textBuilder.AppendLine("<CompanyNumber>" & discount & "</CompanyNumber>")
        'textBuilder.AppendLine("<ExtraCode>CDW</ExtraCode>")
        'textBuilder.AppendLine("<ExtraCode>CHILDSEAT</ExtraCode>")
        textBuilder.AppendLine("<DiscountCode>" & discount & "</DiscountCode>")
        For Each item As ListItem In chkListExtras.Items
            If item.Selected Then
                textBuilder.AppendLine("<ExtraCode>" & item.Value & "</ExtraCode>")
            End If


        Next

        textBuilder.AppendLine("</PayLoad>")
        textBuilder.AppendLine("</TRNXML>")

        result = textBuilder.ToString()
        Return result
    End Function


    Public Shared Function GetExtras(ByVal para As String) As IEnumerable
        ' Dim uri As String = "https://weblink.tsdasp.net/41594xml.asp"

        Dim uri As String = url

        req = WebRequest.Create(uri)
        req.Method = "POST"
        req.ContentType = "text/xml"
        Dim writer As New StreamWriter(req.GetRequestStream())
        writer.Write(para)
        writer.Close()

        'rep = req.GetResponse()

        Dim res As HttpWebResponse = DirectCast(req.GetResponse(), HttpWebResponse)

        Dim dataStream As Stream = res.GetResponseStream()
        Dim enc As Encoding = Encoding.GetEncoding("utf-8")
        Dim reader As New StreamReader(dataStream, enc)

        Dim responseFromServer As String = reader.ReadToEnd()

        reader.Close()
        res.Close()

        Dim xDoc As New XmlDocument()
        xDoc.LoadXml(responseFromServer)
        Dim nodeReader As New XmlNodeReader(xDoc)
        Dim rssDoc As System.Xml.Linq.XDocument = XDocument.Load(nodeReader)

        Dim q = From item In rssDoc.Descendants("DailyExtra") _
            Select ExtraCode = item.Element("ExtraCode").Value, ExtraDesc = item.Element("ExtraDesc").Value & " ( $ " & item.Element("ExtraAmount").Value & ")"
        Return q

    End Function


    Public Shared Function GetExtras2(ByVal para As String) As IEnumerable
        ' Dim uri As String = "https://weblink.tsdasp.net/41594xml.asp"

        Dim uri As String = url

        req = WebRequest.Create(uri)
        req.Method = "POST"
        req.ContentType = "text/xml"
        Dim writer As New StreamWriter(req.GetRequestStream())
        writer.Write(para)
        writer.Close()

        'rep = req.GetResponse()

        Dim res As HttpWebResponse = DirectCast(req.GetResponse(), HttpWebResponse)

        Dim dataStream As Stream = res.GetResponseStream()
        Dim enc As Encoding = Encoding.GetEncoding("utf-8")
        Dim reader As New StreamReader(dataStream, enc)

        Dim responseFromServer As String = reader.ReadToEnd()

        reader.Close()
        res.Close()

        Dim xDoc As New XmlDocument()
        xDoc.LoadXml(responseFromServer)
        Dim nodeReader As New XmlNodeReader(xDoc)
        Dim rssDoc As System.Xml.Linq.XDocument = XDocument.Load(nodeReader)

        Dim q = From item In rssDoc.Descendants("DailyExtra") _
            Select ExtraCode = item.Element("ExtraCode").Value, ExtraDesc = "" & item.Element("ExtraDesc").Value, ExtraDesc2 = "$ " & item.Element("ExtraAmount").Value
        Return q

    End Function
    Public Shared Function GetExtrasFr(ByVal para As String) As IEnumerable
        ' Dim uri As String = "https://weblink.tsdasp.net/hld01xml.asp"

        Dim uri As String = url

        req = WebRequest.Create(uri)
        req.Method = "POST"
        req.ContentType = "text/xml"
        Dim writer As New StreamWriter(req.GetRequestStream())
        writer.Write(para)
        writer.Close()

        'rep = req.GetResponse()

        Dim res As HttpWebResponse = DirectCast(req.GetResponse(), HttpWebResponse)

        Dim dataStream As Stream = res.GetResponseStream()
        Dim enc As Encoding = Encoding.GetEncoding("utf-8")
        Dim reader As New StreamReader(dataStream, enc)

        Dim responseFromServer As String = reader.ReadToEnd()

        reader.Close()
        res.Close()

        Dim xDoc As New XmlDocument()
        xDoc.LoadXml(responseFromServer)
        Dim nodeReader As New XmlNodeReader(xDoc)
        Dim rssDoc As System.Xml.Linq.XDocument = XDocument.Load(nodeReader)
        Dim extrafr

        Dim q = From item In rssDoc.Descendants("DailyExtra") _
            Select ExtraCode = item.Element("ExtraCode").Value, ExtraDesc = GetCode(item.Element("ExtraCode").Value) & " ( $ " & item.Element("ExtraAmount").Value & ")"
        Return q

    End Function

    Shared Function GetCode(ByVal cd) As String
        Dim code
        If cd = "NAV" Then
            code = "SYSTÈME DE NAVIGATION (G.P.S.)"
        ElseIf cd = "1000K" Then
            code = "1000 KM SUPPLÉMENTAIRES"


        ElseIf cd = "500K" Then
            code = "500 KM SUPPLÉMENTAIRES"

        ElseIf cd = "ADDDR" Then
            code = "CONDUCTEUR ADDITIONNEL"
        ElseIf cd = "KST1" Then
            code = "SIÈGE D'ENFANT"
        ElseIf cd = "KST2" Then
            code = "SIÈGE D'APPOINT"
        ElseIf cd = "USA" Then
            code = "NO. DE JOURS AUX ÉTATS-UNIS"
        ElseIf cd = "UNDERAGE" Then
            code = "CONDUCTEUR MOINS DE 24, PLUS DE 21"
        ElseIf cd = "CDW - PAP" Then
            code = "ASSURANCE EDC – $0 FRANCHISE + PAP "
        ElseIf cd = "CDW" Then
            code = "PROTECTIONS EDC  (500$ FRANCHISE)"

        ElseIf cd = "PAP" Then
            code = "PROTECTIONS SUPP. PAP (0$ FRANCHISE)"


        ElseIf cd = "PEC" Then
            code = "PROTECTIONS SUPP. CEP (0$ FRANCHISE)"



        ElseIf cd = "PLT" Then
            code = "Frais d'Immatriculation"
        ElseIf cd = "RDP" Then
            code = "Récupération Régionale – Pneus"
        ElseIf cd = "DATA PLAN" Then
            code = "DONNÉES 3G, TEXTOS INT’L & APPELS INTERNATIONAUX"
        ElseIf cd = "CELL" Then
            code = "CELL – APPELS LOCAUX ILLIMITÉS"
        End If
        Return code
    End Function



    Public Function Check15Promo(ByVal promo As String) As Boolean
        Dim fourdigits As String
        Dim i As Integer
        Dim firstLetter As String
        ' $ 15 promo card should have 5 characters (1 letter either y or z and 4 integer digits)
        If Len(Trim(promo)) = 5 Then
            firstLetter = promo.Substring(0, 1).ToLower
            If firstLetter = "y" Or firstLetter = "z" Or firstLetter = "x" Then
                fourdigits = promo.Substring(1, 4)
                If Integer.TryParse(fourdigits, i) = True Then
                    HttpContext.Current.Trace.Write("INTEGER")
                    Return True
                Else
                    HttpContext.Current.Trace.Write("NOT INTEGER")
                    Return False
                End If
            Else
                HttpContext.Current.Trace.Write("FALSE and NOW CHECK FREE ITEM")
                Return False
            End If
            HttpContext.Current.Trace.Write("FIRST LTER" & firstLetter)
            HttpContext.Current.Trace.Write("Length" & Len(promo))
            HttpContext.Current.Trace.Write("Four digits" & fourdigits)
        ElseIf Len(Trim(promo)) = 0 Then
            Return True
        Else
            Return False
        End If
    End Function


    Public Function CheckFreePromo(ByVal promo As String) As Boolean

        ' First check if there is a 15 promo card item entered..if not then check if the customer entered any valid promotional item code

        Dim code
        code = promo.ToLower
        If Len(Trim(promo)) > 2 Then
            If code = "guzzo" Then
                HttpContext.Current.Trace.Write("INSIDE FREE")
                Return True
            ElseIf code = "guzzo1" Then
                Return True
            ElseIf code = "bluetooth" Then
                Return True
            ElseIf code = "usb" Then
                Return True
            Else
                HttpContext.Current.Trace.Write("FALSE FREE")
                Return False
            End If
        Else
            Return True
        End If

    End Function

    Public Shared Function CheckMinDays(ByVal pdate, ByVal ddate) As Boolean

        Dim pickupdate As DateTime = Convert.ToDateTime(pdate)
        Dim returndate As DateTime = Convert.ToDateTime(ddate)
        Dim totaldays As TimeSpan = returndate - pickupdate
        Dim days As Integer = totaldays.Days

        If days < 3 Then

            Return False
        Else
            Return True
        End If
    End Function

    Public Shared Function CheckTuesday(ByVal pickupdate As String) As Boolean
        Dim pdate As DateTime = DateTime.Parse(pickupdate)
        ' Trace.Write("PDATE" & pdate)
        Dim day

        day = pdate.DayOfWeek
        ' Trace.Write("day" & day)

        If day = 2 Then
            Return True
        Else
            Return False
        End If
    End Function


    Public Function CheckGuzzo() As Boolean
        'Dim flgGuz As Boolean


    End Function

    Public Shared ReadOnly Property PromoDict() As Dictionary(Of String, String)
        Get
            Dim _promo As New Dictionary(Of String, String)
            _promo.Add("usb", "USB Key to Keep")
            _promo.Add("bluetooth", "Bluetooth to Keep")
            _promo.Add("guzzo", "1 Movie Ticket")
            _promo.Add("guzzo1", "1 Movie Ticket")
            Return _promo
        End Get
    End Property



    Public Shared ReadOnly Property PromoDictFr() As Dictionary(Of String, String)
        Get
            Dim _promo As New Dictionary(Of String, String)
            _promo.Add("usb", "Clé USB à Conserver")
            _promo.Add("bluetooth", "Bluetooth  à Conserver")
            _promo.Add("guzzo", "1 Billet de Cinéma")
            _promo.Add("guzzo1", "1 Billet de Cinéma")
            Return _promo
        End Get
    End Property

    Public Shared Function ValidateRequest(lastname As String, cancel As String, sessionID As String, sessionaddress As String) As Boolean
        Dim req1 As String = RequestMaker.EditReservation(cancel, sessionID, sessionaddress)
        Dim rep1 As String = RequestMaker.MakeRateRequest(req1)
        Dim xml2 As XDocument = XDocument.Parse(rep1)

        Dim TsdLastName As String = xml2...<RenterLast>.Value

        If Trim(TsdLastName).ToLower = lastname.ToLower Then
            Return True
        Else
            Return False
        End If
    End Function

    Public Shared Function RequestRezTerms(locationID As String) As String
        Dim result As String
        Dim textBuilder As New Text.StringBuilder()
        textBuilder.AppendLine("<TRNXML Version=""1.0.0"">")
        textBuilder.AppendLine("<Dategmtime TimeZone=""ET"">" + DateTime.Now.ToString + "</Dategmtime>")
        textBuilder.AppendLine("<Sender>")
        textBuilder.AppendLine("<SenderID>" & senderID & "</SenderID>")
        textBuilder.AppendLine("</Sender>")
        textBuilder.AppendLine("<Recipient>")
        textBuilder.AppendLine("<RecipientID>TRN</RecipientID>")
        textBuilder.AppendLine("</Recipient>")
        textBuilder.AppendLine("<TradingPartner>")
        textBuilder.AppendLine("<TradingPartnerCode>" & Trading & "</TradingPartnerCode>")
        textBuilder.AppendLine("</TradingPartner>")
        textBuilder.AppendLine("<Customer>")
        textBuilder.AppendLine("<CustomerNumber>" & customer & "</CustomerNumber>")
        textBuilder.AppendLine("<Passcode>" & customer & "</Passcode>")
        'textBuilder.AppendLine("<SID>" + sid + "</SID>")
        'textBuilder.AppendLine("<RemoteAddress>" + address + "</RemoteAddress>")
        textBuilder.AppendLine("</Customer>")
        textBuilder.AppendLine("<Message>")
        textBuilder.AppendLine("<MessageID>REQPOL</MessageID>")
        textBuilder.AppendLine("<MessageDesc>Request Policy</MessageDesc>")
        textBuilder.AppendLine("</Message>")
        textBuilder.AppendLine("<PayLoad>")
        textBuilder.AppendLine("<RentalLocationID>" & locationID & "</RentalLocationID>")
        '  textBuilder.AppendLine("<ReturnLocationID>TAL</ReturnLocationID>")
        textBuilder.AppendLine("</PayLoad>")
        textBuilder.AppendLine("</TRNXML>")
        result = textBuilder.ToString()
        Return result
    End Function
End Class