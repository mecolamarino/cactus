﻿Imports System.Globalization
Imports System.IO
Imports System.Net
Imports System.Xml
Imports Microsoft.VisualBasic
Imports System
Imports System.Collections

Public Class RezCentralRequests

#Region " Constants "

    Shared ReadOnly senderID As String = System.Configuration.ConfigurationManager.AppSettings("senderID")
    Shared ReadOnly tradingPartnerCode As String = System.Configuration.ConfigurationManager.AppSettings("TradingPartnerCode")
    Shared ReadOnly customerNumber As String = System.Configuration.ConfigurationManager.AppSettings("CustomerNumber")
    Shared ReadOnly passcode As String = ConfigurationManager.AppSettings("Passcode")
    Shared ReadOnly url As String = System.Configuration.ConfigurationManager.AppSettings("url")

#End Region

#Region " Properties "
    Public Shared ReadOnly Property Classes() As Dictionary(Of String, String)
        Get
            Dim _Classes As New Dictionary(Of String, String)
            _Classes.Add("ECAR", "Mazda 2 – Publicity Vehicle")
            _Classes.Add("CCAR", "Mazda 2 or similar")
            _Classes.Add("ICAR", "Mazda 3 or similar")
            _Classes.Add("SCAR", "Toyota Corolla  or similar")
            _Classes.Add("FCAR", "Toyota Camry  or similar")
            _Classes.Add("PCAR", "Chrysler 300 or similar")
            _Classes.Add("LCAR", "Maxima (Leather, Roof, Bluetooth)")
            _Classes.Add("IFAR", "Nissan Xterra 5 Passenger ")
            _Classes.Add("FFAR", "Nissan Pathfinder 7 Passenger")
            _Classes.Add("PFAR", "Hummer H3")
            _Classes.Add("MVAR", "Minivan – Toyota Sienna or similar – 7 Passenger")
            _Classes.Add("XVAR", "Toyota Sienna – 8 Passenger")
            Return _Classes
        End Get
    End Property

    Public Shared ReadOnly Property ClassesDesc() As Dictionary(Of String, String)
        Get
            Dim _Classes As New Dictionary(Of String, String)
            _Classes.Add("ECAR", "Economy")
            _Classes.Add("CCAR", "Compact")
            _Classes.Add("ICAR", "Intermediate")
            _Classes.Add("SCAR", "Midsize ")
            _Classes.Add("FCAR", "Fullsize")
            _Classes.Add("PCAR", "Premium ")
            _Classes.Add("LCAR", "Luxury ")
            _Classes.Add("IFAR", "SUV 4X4 Standard – Nissan Xterra 5 Passenger ")
            _Classes.Add("FFAR", "SUV 4X4 Fullsize – Nissan Pathfinder 7 Passenger")
            _Classes.Add("PFAR", "Premium3")
            _Classes.Add("MVAR", "Minivan – Toyota Sienna or similar – 7 Passenger")
            _Classes.Add("XVAR", "Minivan – Toyota Sienna – 8 Passenger")
            Return _Classes
        End Get
    End Property
#End Region

#Region " Methods "

    Public Shared Function GetLocations() As IEnumerable(Of RentalLocation)

        Dim locReq As String = "<TRNXML Version=""1.0.0""><Dategmtime TimeZone=""ET"">" + DateTime.Now.ToString + "</Dategmtime><Sender><SenderID>" + senderID + "</SenderID></Sender><Recipient><RecipientID>TRN</RecipientID></Recipient><TradingPartner><TradingPartnerCode>" + tradingPartnerCode + "</TradingPartnerCode></TradingPartner><Customer><CustomerNumber>" + customerNumber + "</CustomerNumber><Passcode>" + passcode + "</Passcode></Customer><Message><MessageID>REQLOC</MessageID><MessageDesc>Request Locations</MessageDesc></Message><Payload><RentalLocationID></RentalLocationID><BusinessHours>1</BusinessHours></Payload></TRNXML>"

        Dim res As WebResponse = GetRezCentralResponse(locReq)

        Dim receivestream As Stream = res.GetResponseStream()
        Dim encode As Encoding = Encoding.GetEncoding("utf-8")
        Dim readstream As New StreamReader(receivestream, encode)

        Dim xDoc As XDocument = XDocument.Parse(readstream.ReadToEnd())

        readstream.Close()
        receivestream.Close()

        Dim ti As TextInfo = CultureInfo.CurrentCulture.TextInfo

        Dim p = From item In xDoc.Descendants("RentalLocation") _
                       Select New RentalLocation(item.Element("RentalLocationID").Value, ti.ToTitleCase(item.Element("RentalLocationName").Value.ToLower()), item.Element("RentalVehicleProvider").Value, item.Element("Latitude").Value, item.Element("Longitude").Value)

        Return p

    End Function

    Public Shared Function MakeRateRequest(ByVal para As String) As String
        Dim res As WebResponse = GetRezCentralResponse(para)

        Dim receivestream As Stream = res.GetResponseStream()

        Dim encode As Encoding = Encoding.GetEncoding("utf-8")

        Dim readstream As New StreamReader(receivestream, encode)

        Dim read(256) As [Char]

        Dim result As String

        result = readstream.ReadToEnd()

        Return result
    End Function

    Public Shared Function RateRequest(ByVal pickUpdateTime As String, ByVal dropOffDateTime As String, ByVal rentallocation As String, ByVal returnlocation As String, ByVal discount As String, ByVal Classcode As String) As String
        Dim result As String
        Dim textBuilder As New Text.StringBuilder()

        textBuilder.AppendLine("<TRNXML Version=""1.0.0"">")
        textBuilder.AppendLine("<Dategmtime TimeZone=""ET"">" + DateTime.Now.ToString + "</Dategmtime>")
        textBuilder.AppendLine("<Sender>")
        textBuilder.AppendLine("<SenderID>" & senderID & "</SenderID>")
        textBuilder.AppendLine("</Sender>")
        textBuilder.AppendLine("<Recipient>")
        textBuilder.AppendLine("<RecipientID>TRN</RecipientID>")
        textBuilder.AppendLine("</Recipient>")
        textBuilder.AppendLine("<TradingPartner>")
        textBuilder.AppendLine("<TradingPartnerCode>" & tradingPartnerCode & "</TradingPartnerCode>")
        textBuilder.AppendLine("</TradingPartner>")
        textBuilder.AppendLine("<Customer>")
        textBuilder.AppendLine("<CustomerNumber>" & customerNumber & "</CustomerNumber>")
        textBuilder.AppendLine("<Passcode>" & customerNumber & "</Passcode>")
        textBuilder.AppendLine("</Customer>")
        textBuilder.AppendLine("<Message>")
        textBuilder.AppendLine("<MessageID>REQRAT</MessageID>")
        textBuilder.AppendLine("<MessageDesc>Request Rates</MessageDesc>")
        textBuilder.AppendLine("</Message>")
        textBuilder.AppendLine("<Payload>")
        textBuilder.AppendLine("<RentalLocationID>" + rentallocation + "</RentalLocationID>")
        textBuilder.AppendLine("<ReturnLocationID>" + returnlocation + "</ReturnLocationID>")
        textBuilder.AppendLine("<PickupDateTime>" + pickUpdateTime + "</PickupDateTime>")
        textBuilder.AppendLine("<ReturnDateTime>" + dropOffDateTime + "</ReturnDateTime>")
        textBuilder.AppendLine("<RateSource>WebLink</RateSource>")
        textBuilder.AppendLine("<ClassCode></ClassCode>")
        '  textBuilder.AppendLine("<CompanyNumber>" & discount & "</CompanyNumber>")
        ' textBuilder.AppendLine("<DiscountCode>" & discount & "</DiscountCode>")
        ' textBuilder.AppendLine("<ExtraCode>" & fee & "</ExtraCode>")
        textBuilder.AppendLine("<TotalPricing>1</TotalPricing>")
        textBuilder.AppendLine("</Payload>")
        textBuilder.AppendLine("</TRNXML>")

        result = textBuilder.ToString()
        Return result

    End Function

    Public Shared Function RequestBill(ByVal rateID As String, ByVal pickUpdateTime As String, ByVal dropOffDateTime As String, ByVal rentallocation As String, ByVal returnlocation As String) As String
        Dim result As String
        Dim textBuilder As New Text.StringBuilder()

        textBuilder.AppendLine("<TRNXML Version=""1.0.0"">")
        textBuilder.AppendLine("<Dategmtime TimeZone=""ET"">" + DateTime.Now.ToString + "</Dategmtime>")
        textBuilder.AppendLine("<Sender>")
        textBuilder.AppendLine("<SenderID>" & senderID & "</SenderID>")
        textBuilder.AppendLine("</Sender>")
        textBuilder.AppendLine("<Recipient>")
        textBuilder.AppendLine("<RecipientID>TRN</RecipientID>")
        textBuilder.AppendLine("</Recipient>")
        textBuilder.AppendLine("<TradingPartner>")
        textBuilder.AppendLine("<TradingPartnerCode>" & tradingPartnerCode & "</TradingPartnerCode>")
        textBuilder.AppendLine("</TradingPartner>")
        textBuilder.AppendLine("<Customer>")
        textBuilder.AppendLine("<CustomerNumber>" & customerNumber & "</CustomerNumber>")
        textBuilder.AppendLine("<Passcode>" & customerNumber & "</Passcode>")
        textBuilder.AppendLine("</Customer>")
        textBuilder.AppendLine("<Message>")
        textBuilder.AppendLine("<MessageID>REQBIL</MessageID>")
        textBuilder.AppendLine("<MessageDesc>Request Bill</MessageDesc>")
        textBuilder.AppendLine("</Message>")
        textBuilder.AppendLine("<Payload>")
        textBuilder.AppendLine("<RentalLocationID>" + rentallocation + "</RentalLocationID>")
        textBuilder.AppendLine("<ReturnLocationID>" + returnlocation + "</ReturnLocationID>")
        textBuilder.AppendLine("<PickupDateTime>" + pickUpdateTime + "</PickupDateTime>")
        textBuilder.AppendLine("<ReturnDateTime>" + dropOffDateTime + "</ReturnDateTime>")
        textBuilder.AppendLine("<RateID>" + rateID + "</RateID>")
        textBuilder.AppendLine("<ClassCode></ClassCode>")
        textBuilder.AppendLine("<ExtraCode></ExtraCode>")

        textBuilder.AppendLine("<TotalPricing>1</TotalPricing>")
        textBuilder.AppendLine("</Payload>")
        textBuilder.AppendLine("</TRNXML>")

        result = textBuilder.ToString()
        Return result

    End Function

    Public Shared Function RequestClass(ByVal cls As String) As String
        Dim result As String
        Dim textBuilder As New Text.StringBuilder()
        textBuilder.AppendLine("<TRNXML Version=""1.0.0"">")
        textBuilder.AppendLine("<Dategmtime TimeZone=""ET"">" + DateTime.Now.ToString + "</Dategmtime>")
        textBuilder.AppendLine("<Sender>")
        textBuilder.AppendLine("<SenderID>" & senderID & "</SenderID>")
        textBuilder.AppendLine("</Sender>")
        textBuilder.AppendLine("<Recipient>")
        textBuilder.AppendLine("<RecipientID>TRN</RecipientID>")
        textBuilder.AppendLine("</Recipient>")
        textBuilder.AppendLine("<TradingPartner>")
        textBuilder.AppendLine("<TradingPartnerCode>" & tradingPartnerCode & "</TradingPartnerCode>")
        textBuilder.AppendLine("</TradingPartner>")
        textBuilder.AppendLine("<Customer>")
        textBuilder.AppendLine("<CustomerNumber>" & customerNumber & "</CustomerNumber>")
        textBuilder.AppendLine("<Passcode>" & customerNumber & "</Passcode>")
        'textBuilder.AppendLine("<SID>" + sid + "</SID>")
        'textBuilder.AppendLine("<RemoteAddress>" + address + "</RemoteAddress>")
        textBuilder.AppendLine("</Customer>")
        textBuilder.AppendLine("<Message>")
        textBuilder.AppendLine("<MessageID>REQCLS</MessageID>")
        textBuilder.AppendLine("<MessageDesc></MessageDesc>")
        textBuilder.AppendLine("</Message>")
        textBuilder.AppendLine("<PayLoad>")
        'textBuilder.AppendLine("<BusinessHours>0</BusinessHours>")
        textBuilder.AppendLine("<ClassCode>" & cls & "</ClassCode>")
        '  textBuilder.AppendLine("<ReturnLocationID>TAL</ReturnLocationID>")

        textBuilder.AppendLine("</PayLoad>")
        textBuilder.AppendLine("</TRNXML>")

        result = textBuilder.ToString()
        Return result
    End Function

    Public Shared Function RequestExtras(ByVal rentallocation As String, ByVal classCode As String) As String
        Dim result As String
        Dim textBuilder As New Text.StringBuilder()
        textBuilder.AppendLine("<TRNXML Version=""1.0.0"">")
        textBuilder.AppendLine("<Dategmtime TimeZone=""ET"">" + DateTime.Now.ToString + "</Dategmtime>")
        textBuilder.AppendLine("<Sender>")
        textBuilder.AppendLine("<SenderID>" & senderID & "</SenderID>")
        textBuilder.AppendLine("</Sender>")
        textBuilder.AppendLine("<Recipient>")
        textBuilder.AppendLine("<RecipientID>TRN</RecipientID>")
        textBuilder.AppendLine("</Recipient>")
        textBuilder.AppendLine("<TradingPartner>")
        textBuilder.AppendLine("<TradingPartnerCode>" & tradingPartnerCode & "</TradingPartnerCode>")
        textBuilder.AppendLine("</TradingPartner>")
        textBuilder.AppendLine("<Customer>")
        textBuilder.AppendLine("<CustomerNumber>" & customerNumber & "</CustomerNumber>")
        textBuilder.AppendLine("<Passcode>" & customerNumber & "</Passcode>")
        textBuilder.AppendLine("</Customer>")
        textBuilder.AppendLine("<Message>")
        textBuilder.AppendLine("<MessageID>REQEXT</MessageID>")
        textBuilder.AppendLine("<MessageDesc></MessageDesc>")
        textBuilder.AppendLine("</Message>")
        textBuilder.AppendLine("<PayLoad>")
        textBuilder.AppendLine("<RentalLocationID>" & rentallocation & "</RentalLocationID>")
        textBuilder.AppendLine("<ClassCode>" & classCode & "</ClassCode>")

        textBuilder.AppendLine("</PayLoad>")
        textBuilder.AppendLine("</TRNXML>")

        result = textBuilder.ToString()
        Return result
    End Function

    

    Public Shared Function CancelReservation(ByVal confno As String, ByVal sid As String, ByVal address As String) As String
        Dim result As String
        Dim textBuilder As New Text.StringBuilder()
        textBuilder.AppendLine("<TRNXML Version=""1.0.0"">")
        textBuilder.AppendLine("<Dategmtime TimeZone=""ET"">" + DateTime.Now.ToString + "</Dategmtime>")
        textBuilder.AppendLine("<Sender>")
        textBuilder.AppendLine("<SenderID>" & senderID & "</SenderID>")
        textBuilder.AppendLine("</Sender>")
        textBuilder.AppendLine("<Recipient>")
        textBuilder.AppendLine("<RecipientID>TRN</RecipientID>")
        textBuilder.AppendLine("</Recipient>")
        textBuilder.AppendLine("<TradingPartner>")
        textBuilder.AppendLine("<TradingPartnerCode>WEB01</TradingPartnerCode>")
        textBuilder.AppendLine("</TradingPartner>")
        textBuilder.AppendLine("<Customer>")
        textBuilder.AppendLine("<CustomerNumber>" & customerNumber & "</CustomerNumber>")
        textBuilder.AppendLine("<Passcode>" & customerNumber & "</Passcode>")
        textBuilder.AppendLine("<SID>" + sid + "</SID>")
        textBuilder.AppendLine("<RemoteAddress>" + address + "</RemoteAddress>")
        textBuilder.AppendLine("</Customer>")
        textBuilder.AppendLine("<Message>")
        textBuilder.AppendLine("<MessageID>REQCAN</MessageID>")
        textBuilder.AppendLine("<MessageDesc>Request Rates</MessageDesc>")
        textBuilder.AppendLine("</Message>")
        textBuilder.AppendLine("<Payload>")
        textBuilder.AppendLine("<ConfirmNum>" & confno & "</ConfirmNum>")
        textBuilder.AppendLine("</Payload>")
        textBuilder.AppendLine("</TRNXML>")

        result = textBuilder.ToString()
        Return result
    End Function

    Public Shared Function EditReservation(ByVal confirmationCode As String, ByVal sessionID As String, ByVal hostAddress As String) As String
        Dim result As String
        Dim textBuilder As New Text.StringBuilder()
        textBuilder.AppendLine("<TRNXML Version=""1.0.0"">")
        textBuilder.AppendLine("<Dategmtime TimeZone=""ET"">" + DateTime.Now.ToString + "</Dategmtime>")
        textBuilder.AppendLine("<Sender>")
        textBuilder.AppendLine("<SenderID>" & senderID & "</SenderID>")
        textBuilder.AppendLine("</Sender>")
        textBuilder.AppendLine("<Recipient>")
        textBuilder.AppendLine("<RecipientID>TRN</RecipientID>")
        textBuilder.AppendLine("</Recipient>")
        textBuilder.AppendLine("<TradingPartner>")
        textBuilder.AppendLine("<TradingPartnerCode>" & tradingPartnerCode & "</TradingPartnerCode>")
        textBuilder.AppendLine("</TradingPartner>")
        textBuilder.AppendLine("<Customer>")
        textBuilder.AppendLine("<CustomerNumber>" & customerNumber & "</CustomerNumber>")
        textBuilder.AppendLine("<Passcode>" & customerNumber & "</Passcode>")
        textBuilder.AppendLine("<SID>" + sessionID + "</SID>")
        textBuilder.AppendLine("<RemoteAddress>" + hostAddress + "</RemoteAddress>")
        textBuilder.AppendLine("</Customer>")
        textBuilder.AppendLine("<Message>")
        textBuilder.AppendLine("<MessageID>REQREZ</MessageID>")
        textBuilder.AppendLine("<MessageDesc></MessageDesc>")
        textBuilder.AppendLine("</Message>")
        textBuilder.AppendLine("<PayLoad>")
        textBuilder.AppendLine("<ConfirmNum>" & confirmationCode & "</ConfirmNum>")
        textBuilder.AppendLine("</PayLoad>")
        textBuilder.AppendLine("</TRNXML>")

        result = textBuilder.ToString()
        Return result
    End Function

    Public Shared Function RateRequestByExtras(ByVal pickUpdateTime As String, _
                                          ByVal dropOffDateTime As String, _
                                          ByVal classCode As String, ByVal rateCode As String, ByVal sid As String, _
                                          ByVal address As String, ByVal rentallocation As String, ByVal returnlocation As String, ByVal rateID As String, ByRef extrasCodes As ArrayList, ByVal discount As String) As String
        Dim result As String
        Dim textBuilder As New Text.StringBuilder()
        textBuilder.AppendLine("<TRNXML Version=""1.0.0"">")
        textBuilder.AppendLine("<Dategmtime TimeZone=""ET"">" + DateTime.Now.ToString + "</Dategmtime>")
        textBuilder.AppendLine("<Sender>")
        textBuilder.AppendLine("<SenderID>" & senderID & "</SenderID>")
        textBuilder.AppendLine("</Sender>")
        textBuilder.AppendLine("<Recipient>")
        textBuilder.AppendLine("<RecipientID>TRN</RecipientID>")
        textBuilder.AppendLine("</Recipient>")
        textBuilder.AppendLine("<TradingPartner>")
        textBuilder.AppendLine("<TradingPartnerCode>" & tradingPartnerCode & "</TradingPartnerCode>")
        textBuilder.AppendLine("</TradingPartner>")
        textBuilder.AppendLine("<Customer>")
        textBuilder.AppendLine("<CustomerNumber>" & customerNumber & "</CustomerNumber>")
        textBuilder.AppendLine("<Passcode>" & customerNumber & "</Passcode>")
        'textBuilder.AppendLine("<SID>" + sid + "</SID>")
        'textBuilder.AppendLine("<RemoteAddress>" + address + "</RemoteAddress>")
        textBuilder.AppendLine("</Customer>")
        textBuilder.AppendLine("<Message>")
        textBuilder.AppendLine("<MessageID>REQBIL</MessageID>")
        textBuilder.AppendLine("<MessageDesc />")
        textBuilder.AppendLine("</Message>")
        textBuilder.AppendLine("<PayLoad>")
        textBuilder.AppendLine("<RentalLocationID>" + rentallocation + "</RentalLocationID>")
        textBuilder.AppendLine("<ReturnLocationID>" + returnlocation + "</ReturnLocationID>")
        textBuilder.AppendLine("<PickupDateTime>" + pickUpdateTime + "</PickupDateTime>")
        textBuilder.AppendLine("<ReturnDateTime>" + dropOffDateTime + "</ReturnDateTime>")
        textBuilder.AppendLine("<RateID>" + rateID + "</RateID>")
        textBuilder.AppendLine("<ClassCode>" + classCode + "</ClassCode>")
        textBuilder.AppendLine("<CompanyNumber>" & discount & "</CompanyNumber>")
        ' textBuilder.AppendLine("<DiscountCode>" & discount & "</DiscountCode>")
        For Each code As String In extrasCodes
            textBuilder.AppendLine("<ExtraCode>" & code & "</ExtraCode>")
        Next

        textBuilder.AppendLine("</PayLoad>")
        textBuilder.AppendLine("</TRNXML>")

        result = textBuilder.ToString()
        Return result
    End Function

    Public Shared Function GetExtras(ByVal para As String) As IEnumerable(Of LocationExtras)

        Dim res As HttpWebResponse = DirectCast(GetRezCentralResponse(para), HttpWebResponse)

        Dim dataStream As Stream = res.GetResponseStream()
        Dim enc As Encoding = Encoding.GetEncoding("utf-8")
        Dim reader As New StreamReader(dataStream, enc)

        Dim responseFromServer As String = reader.ReadToEnd()

        reader.Close()
        res.Close()

        Dim xDoc As New XmlDocument()
        xDoc.LoadXml(responseFromServer)
        Dim nodeReader As New XmlNodeReader(xDoc)
        Dim rssDoc As System.Xml.Linq.XDocument = XDocument.Load(nodeReader)

        Dim q = From item In rssDoc.Descendants("DailyExtra") _
                Select New LocationExtras(item.Element("ExtraCode").Value, item.Element("ExtraDesc").Value, "$" & item.Element("ExtraAmount").Value)

        Return q

    End Function

    Shared Function GetCode(ByVal cd) As String
        Dim code
        If cd = "NAV" Then
            code = "SYSTÈME DE NAVIGATION (G.P.S.)"
        ElseIf cd = "1000K" Then
            code = "1000 KM SUPPLÉMENTAIRES"
        ElseIf cd = "500K" Then
            code = "500 KM SUPPLÉMENTAIRES"
        ElseIf cd = "ADDDR" Then
            code = "CONDUCTEUR ADDITIONNEL"
        ElseIf cd = "KST1" Then
            code = "SIÈGE D'ENFANT"
        ElseIf cd = "KST2" Then
            code = "SIÈGE D'APPOINT"
        ElseIf cd = "USA" Then
            code = "NO. DE JOURS AUX ÉTATS-UNIS"
        ElseIf cd = "UNDERAGE" Then
            code = "CONDUCTEUR MOINS DE 24, PLUS DE 21"
        ElseIf cd = "CDW - PAP" Then
            code = "ASSURANCE EDC – $0 FRANCHISE + PAP "
        ElseIf cd = "CDW" Then
            code = "PROTECTIONS EDC  (500$ FRANCHISE)"
        ElseIf cd = "PAP" Then
            code = "PROTECTIONS SUPP. PAP (0$ FRANCHISE)"
        ElseIf cd = "PEC" Then
            code = "PROTECTIONS SUPP. CEP (0$ FRANCHISE)"
        ElseIf cd = "PLT" Then
            code = "Frais d'Immatriculation"
        ElseIf cd = "RDP" Then
            code = "Récupération Régionale – Pneus"
        ElseIf cd = "DATA PLAN" Then
            code = "DONNÉES 3G, TEXTOS INT’L & APPELS INTERNATIONAUX"
        ElseIf cd = "CELL" Then
            code = "CELL – APPELS LOCAUX ILLIMITÉS"
        ElseIf cd = "UNL" Then
            code = "KILOMÉTRAGE ILLIMITÉ (Économique -> Pleine Grandeur)"
        ElseIf cd = "DBL" Then
            code = "DOUBLEZ VOS KMS (Premium, Luxe, VUS, Mini Van)"
        ElseIf cd = "CDW1" Then
            code = "PROTECTIONS EDC  (500$ FRANCHISE)"
        End If
        Return code
    End Function

    Public Shared Function ModifyTimeRequest(ByVal pickUpdateTime As String, ByVal dropOffDateTime As String, ByVal sid As String, ByVal address As String, ByVal comf As String) As String
        Dim result As String
        Dim textBuilder As New Text.StringBuilder()
        textBuilder.AppendLine("<TRNXML Version=""1.0.0"">")
        textBuilder.AppendLine("<Dategmtime TimeZone=""ET"">" + DateTime.Now.ToString + "</Dategmtime>")
        textBuilder.AppendLine("<Sender>")
        textBuilder.AppendLine("<SenderID>" & senderID & "</SenderID>")
        textBuilder.AppendLine("</Sender>")
        textBuilder.AppendLine("<Recipient>")
        textBuilder.AppendLine("<RecipientID>TRN</RecipientID>")
        textBuilder.AppendLine("</Recipient>")
        textBuilder.AppendLine("<TradingPartner>")
        textBuilder.AppendLine("<TradingPartnerCode>WEB01</TradingPartnerCode>")
        textBuilder.AppendLine("</TradingPartner>")
        textBuilder.AppendLine("<Customer>")
        textBuilder.AppendLine("<CustomerNumber>" & customerNumber & "</CustomerNumber>")
        textBuilder.AppendLine("<Passcode>" & customerNumber & "</Passcode>")
        textBuilder.AppendLine("<SID>" + sid + "</SID>")
        textBuilder.AppendLine("<RemoteAddress>" + address + "</RemoteAddress>")
        textBuilder.AppendLine("</Customer>")
        textBuilder.AppendLine("<Message>")
        textBuilder.AppendLine("<MessageID>ADDREZ</MessageID>")
        textBuilder.AppendLine("<MessageDesc>Add Reservation</MessageDesc>")
        textBuilder.AppendLine("</Message>")
        textBuilder.AppendLine("<Payload>")
        textBuilder.AppendLine("<ConfirmNum>" + comf + "</ConfirmNum>")
        textBuilder.AppendLine("<PickupDateTime>" + pickUpdateTime + "</PickupDateTime>")
        textBuilder.AppendLine("<ReturnDateTime>" + dropOffDateTime + "</ReturnDateTime>")
        ' textBuilder.AppendLine("<CompanyNumber>321312</CompanyNumber>")
        'textBuilder.AppendLine("<RateCode>" + rateCode + "</RateCode>")
        textBuilder.AppendLine("</Payload>")
        textBuilder.AppendLine("</TRNXML>")

        result = textBuilder.ToString()
        Return result
    End Function

    Public Shared Function ValidateReservation(ByVal confirmationCode As String, ByVal lastName As String, sessionID As String, hostAddress As String) As Boolean
        Dim req1 As String = EditReservation(confirmationCode, sessionID, hostAddress)
        Dim rep1 As String = MakeRateRequest(req1)

        Dim xml2 As XDocument = XDocument.Parse(rep1)

        Dim tsdLastName As String = xml2...<RenterLast>.Value

        If Len(lastName) > 0 Then
            If Trim(TsdLastName).ToLower = lastName.ToLower Then
                Return True
            Else
                Return False
            End If
        End If
    End Function

    Private Shared Function GetRezCentralResponse(ByVal xmlRequestString As String) As WebResponse
        Dim req As WebRequest

        req = WebRequest.Create(url)
        req.Method = "POST"
        req.ContentType = "text/xml"
        Dim writer As New StreamWriter(req.GetRequestStream())
        writer.Write(xmlRequestString)
        writer.Close()

        Return req.GetResponse()

    End Function


    Public Shared Function AddRezRequest(ByVal pickUpdateTime As String, _
                                          ByVal dropOffDateTime As String, ByVal rateID As String, ByVal classCode As String, ByVal sid As String, ByVal address As String, ByVal first As String, ByVal last As String, ByVal email As String, ByVal cell As String, ByVal RAddress1 As String, ByVal RAddress2 As String, ByVal city As String, ByVal state As String, ByVal zip As String, ByVal comf As String, ByVal extraCodes As ArrayList, ByVal discount As String, Licensenumber As String, DOB As String, comments As String, country As String, licenseexpdate As String, pickuplocation As String, droplocation As String, airline As String, flight As String, arrivaltime As String) As String
        Dim result As String
        Dim textBuilder As New Text.StringBuilder()
        textBuilder.AppendLine("<TRNXML Version=""1.0.0"">")
        textBuilder.AppendLine("<Dategmtime TimeZone=""ET"">" + DateTime.Now.ToString + "</Dategmtime>")
        textBuilder.AppendLine("<Sender>")
        textBuilder.AppendLine("<SenderID>" & senderID & "</SenderID>")
        textBuilder.AppendLine("</Sender>")
        textBuilder.AppendLine("<Recipient>")
        textBuilder.AppendLine("<RecipientID>TRN</RecipientID>")
        textBuilder.AppendLine("</Recipient>")
        textBuilder.AppendLine("<TradingPartner>")
        textBuilder.AppendLine("<TradingPartnerCode>WEB01</TradingPartnerCode>")
        textBuilder.AppendLine("</TradingPartner>")
        textBuilder.AppendLine("<Customer>")
        textBuilder.AppendLine("<CustomerNumber>" & customerNumber & "</CustomerNumber>")
        textBuilder.AppendLine("<Passcode>" & customerNumber & "</Passcode>")
        textBuilder.AppendLine("<SID>" + sid + "</SID>")
        textBuilder.AppendLine("<RemoteAddress>" + address + "</RemoteAddress>")
        textBuilder.AppendLine("</Customer>")
        textBuilder.AppendLine("<Message>")
        textBuilder.AppendLine("<MessageID>ADDREZ</MessageID>")
        textBuilder.AppendLine("<MessageDesc>Add Reservation</MessageDesc>")
        textBuilder.AppendLine("</Message>")
        textBuilder.AppendLine("<Payload>")
        If comf <> "" Then
            textBuilder.AppendLine("<ConfirmNum>" + comf + "</ConfirmNum>")
        End If
        textBuilder.AppendLine("<RentalLocationID>" & pickuplocation & "</RentalLocationID>")
        textBuilder.AppendLine("<ReturnLocationID>" & droplocation & "</ReturnLocationID>")
        textBuilder.AppendLine("<PickupDateTime>" + pickUpdateTime + "</PickupDateTime>")
        textBuilder.AppendLine("<ReturnDateTime>" + dropOffDateTime + "</ReturnDateTime>")
        textBuilder.AppendLine("<RateID>" + rateID + "</RateID>")
        textBuilder.AppendLine("<ClassCode>" + classCode + "</ClassCode>")
        textBuilder.AppendLine("<CompanyNumber>" & discount & "</CompanyNumber>")
        'textBuilder.AppendLine("<DiscountCode>" & discount & "</DiscountCode>")
        textBuilder.AppendLine("<RentalComments>" + comments + "</RentalComments>")
        textBuilder.AppendLine("<RenterFirst>" + first + "</RenterFirst>")
        textBuilder.AppendLine("<RenterLast>" + last + "</RenterLast>")
        textBuilder.AppendLine("<EmailAddress>" + email + "</EmailAddress>")
        textBuilder.AppendLine("<RenterHomePhone>" + cell + "</RenterHomePhone>")
        textBuilder.AppendLine("<RenterAddress1>" + RAddress1 + "</RenterAddress1>")
        textBuilder.AppendLine("<RenterCity>" + city + "</RenterCity>")
        textBuilder.AppendLine("<RenterState>" + state + "</RenterState>")
        textBuilder.AppendLine("<RenterZip>" + zip + "</RenterZip>")
        textBuilder.AppendLine("<RenterCountry>" + country + "</RenterCountry>")


        'textBuilder.AppendLine("<CCType>" + credittype + "</CCType>")
        'textBuilder.AppendLine("<CCNumber>" + creditno + "</CCNumber>")
        'textBuilder.AppendLine("<CCExp>" + creditexp + "</CCExp>")
        '' textBuilder.AppendLine("<CardDepositAmount>" + rateCode + "</CardDepositAmount>")
        'textBuilder.AppendLine("<CardAuthNumber>" + creditcode + "</CardAuthNumber>")
        textBuilder.AppendLine("<LicenseNumber>" + Licensenumber + "</LicenseNumber>")
        'textBuilder.AppendLine("<LicenseState>" + licensestate + "</LicenseState>")
        textBuilder.AppendLine("<LicenseExpDate>" + licenseexpdate + "</LicenseExpDate>")
        textBuilder.AppendLine("<Airline>" + airline + "</Airline>")
        textBuilder.AppendLine("<Flight>" + flight + "</Flight>")
        textBuilder.AppendLine("<IATA>" + arrivaltime + "</IATA>")
        textBuilder.AppendLine("<RenterDOB>" + DOB + "</RenterDOB>")

        If extraCodes IsNot Nothing Then
            For Each extraCode As String In extraCodes
                textBuilder.AppendLine("<ExtraCode>" & extraCode & "</ExtraCode>")
            Next
        Else
            If Not String.IsNullOrEmpty(comf) Then
                textBuilder.AppendLine("<ExtraCode></ExtraCode>")
            End If
        End If

        'textBuilder.AppendLine("<RateCode>" + rateCode + "</RateCode>")
        textBuilder.AppendLine("<TotalPricing>1</TotalPricing>")
        textBuilder.AppendLine("</Payload>")
        textBuilder.AppendLine("</TRNXML>")

        result = textBuilder.ToString()
        Return result
    End Function



    Public Shared Function GetLocation(ByVal loc) As String
        If loc = "TUC" Then
            loc = "VALUE MAX UNION CITY"
        ElseIf loc = "SDQC1" Then
            loc = "Downtown Santo Domingo"
        ElseIf loc = "SDQC2" Then
            loc = "Santo Domingo Headquarters"
        ElseIf loc = "PUJ" Then
            loc = "Punta Cana Airport"
        ElseIf loc = "STI" Then
            loc = "Cibaro Airport (STI)"
        ElseIf loc = "POP" Then
            loc = "Gregorio Luper&#243;n International Airport"

        End If
        Return loc

    End Function

#End Region

End Class
