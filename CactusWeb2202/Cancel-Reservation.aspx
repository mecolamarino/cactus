<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Cancel-Reservation.aspx.vb"  MasterPageFile="MastarList.master" Inherits="CancelReservation" culture="auto" meta:resourcekey="PageResource1" uiculture="auto" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <title>Cancel my Reservation | Savannah Car Rentals</title>
</asp:Content>
<asp:Content ContentPlaceHolderID="ContentPlaceHolder1" ID="cnt" runat="server">	
	<div class="col-sm-12" style="background:#fff; padding-bottom:20px;">
    <h1>Already have a Reservation?</h1>
    <h2>
      <asp:Label ID="lblCancelText" 
             runat="server" Text="Cancel Reservation" 
            meta:resourcekey="lblCancelTextResource1"  ></asp:Label>
    </h2>
    
    
     <asp:Panel ID="pnlshow" runat="server" Visible="False" 
        meta:resourcekey="pnlshowResource1">
        <div style="font-size:24px; color:#F00;"><asp:Label ID="lblmessage" runat="server" 
                meta:resourcekey="lblmessageResource1"></asp:Label></div>
    </asp:Panel>   
  
    <div class="one_third flex_column">
  <asp:TextBox ID="txtcancel" onFocus="this.value=''" value="" name="number"  style="border:1px solid #828177; padding: 3px;" 
            runat="server" meta:resourcekey="txtcancelResource1" placeholder="Confirmation number"></asp:TextBox></div>
	
    <div class="one_fourth first flex_column" style="text-transform:capitalize;"></div>
    <div class="one_third flex_column" style="margin-top:15px;"><asp:TextBox ID="txtlastname"  name="number" 
            style="border:1px solid #828177; padding: 3px;" runat="server" 
            meta:resourcekey="txtlastnameResource1" placeholder="Last name"></asp:TextBox></div>
    
     
        <br />
<br />

				
   <asp:Button ID="btnConf" runat="server"  CssClass="btn-next" 
        Text="Cancel Reservation" meta:resourcekey="btnConfResource1" />

</div>

  </asp:Content>
  