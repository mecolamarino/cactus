﻿Imports System.Xml
Imports System.Xml.Serialization
Imports System.Linq
Imports System.Xml.Linq
Imports System.Threading
Imports System.Globalization
Partial Class CancelReservation
    Inherits System.Web.UI.Page

    Protected Overrides Sub InitializeCulture()
        Culture = Common.MyCulture

        'set culture to current thread
        Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(Common.MyCulture)
        Thread.CurrentThread.CurrentUICulture = New CultureInfo(Common.MyCulture)

        'call base class
        MyBase.InitializeCulture()
    End Sub

    Protected Sub btnConf_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnConf.Click
        Me.pnlshow.Visible = True



        If txtcancel.Text = "" Or txtlastname.Text = "" Then
            lblmessage.Text = "Invalid confirmation number/Last Name"
            ' Return
        Else

            If RequestMaker.ValidateRequest(Me.txtlastname.Text, txtcancel.Text, Session.SessionID, Request.UserHostAddress) Then

                Dim reqXml As String = RequestMaker.CancelReservation(Me.txtcancel.Text, Me.Session.SessionID, Request.UserHostAddress)
                Dim repXml As String = RequestMaker.MakeRateRequest(reqXml)

                Dim xml As XDocument = XDocument.Parse(repXml)

                lblmessage.Text = xml...<MessageDescription>.Value

                Dim req As String = RequestMaker.EditReservation(Me.txtcancel.Text, Me.Session.SessionID, Request.UserHostAddress)
                Dim rep As String = RequestMaker.MakeRateRequest(req)
                Dim xml2 As XDocument = XDocument.Parse(rep)

                Dim email As String = xml2...<EmailAddress>.Value

                ' SendEmail(email, Me.txtcancel.Text)
                Trace.Write("Email", email)

                txtcancel.Text = ""
            Else
                lblmessage.Text = "Invalid confirmation number/Last Name"
            End If
            End If

    End Sub

    Public Sub SendEmail(ByVal email, ByVal conf)

        '   Dim body As String

        Dim fromaddress As String = System.Configuration.ConfigurationManager.AppSettings("fromaddress")
        Dim toaddress As String
        Dim msg As String
        msg = ""
        Dim body



        Dim html As New StringBuilder


        body = "Your Reservation (" & conf & ") has been cancelled.<br>Thank You"

        'Patient Health Information

        Dim mm As New System.Net.Mail.MailMessage(fromaddress, email)

        mm.Subject = "Reservation Cancellation Notice"
        mm.Body = body
        mm.IsBodyHtml = True
        mm.Priority = System.Net.Mail.MailPriority.High

        Dim smtp As New System.Net.Mail.SmtpClient
        '(4) Send the MailMessage (will use the Web.config settings)
        smtp.Send(mm)
        Response.Write("SENT")
    End Sub
   

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Request.QueryString("err") Is Nothing Then
        Else
            If Request.QueryString("err") = "1" Then
                lblmessage.Text = "The confirmation number is invalid!"
            ElseIf Request.QueryString("err") = "2" Then
                lblmessage.Text = "This reservation was cancelled already!"
            ElseIf Request.QueryString("err") = "3" Then
                lblmessage.Text = "Your reservation has been modified. Please enter your confirmation number and click on modify button to view or modify your  reservation"
            End If
        End If
    End Sub


End Class
