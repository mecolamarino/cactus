﻿Imports System.Xml.Linq
Imports System.Threading
Imports System.Globalization
Partial Class EditRes
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub

    Protected Overrides Sub InitializeCulture()
        Culture = Common.MyCulture

        'set culture to current thread
        Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(Common.MyCulture)
        Thread.CurrentThread.CurrentUICulture = New CultureInfo(Common.MyCulture)

        'call base class
        MyBase.InitializeCulture()
    End Sub

    Protected Sub Page_PreLoad(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreLoad

        If RequestMaker.ValidateRequest(Request.QueryString("last"), Request.QueryString("con"), Session.SessionID, Request.UserHostAddress) Then

            Dim req As String = RequestMaker.EditReservation(Request.QueryString("con"), Me.Session.SessionID, Request.UserHostAddress)
            Dim rep As String = RequestMaker.MakeRateRequest(req)
            Dim picklocation As String
            Dim droplocation As String

            Dim xml As XDocument = XDocument.Parse(rep)

            If xml.Root...<MessageID>.Value = "RSPERR" Then
                Response.Redirect("modify-reservation.aspx?err=1") '''err = 1, reservation does not exist, 2, reservation is cancelled
                Return
            End If

            If xml.Root...<ReservationStatus>.Value = "CANCELLED" Then
                Response.Redirect("modify-reservation.aspx?err=2") '''err = 1, reservation does not exist, 2, reservation is cancelled
                Return
            End If


            Dim pudt As String = xml...<PickupDateTime>.Value
            Dim dodt As String = xml...<ReturnDateTime>.Value
            Dim classCode As String = xml...<ClassCode>.Value
            Dim classDescription As String = xml...<ModelDesc>.Value
            Dim classID As String = xml...<RateID>.Value
            picklocation = xml...<RentalLocationID>.Value
            droplocation = xml...<ReturnLocationID>.Value
            Dim img = xml...<ClassImageURL>.value

            Dim cls As New RequestMaker
            ' Dim rateQ As String = RequestMaker.RateRequestByID(pudt, dodt, classCode, classID, Me.Session.SessionID, Request.UserHostAddress, picklocation, droplocation, "", "")

            ' Dim rateP As String = RequestMaker.MakeRateRequest(rateQ)

            'Dim rateXml As XDocument = XDocument.Parse(rateP)
        
            lblname2.Text = xml...<RenterFirst>.Value + " " + xml...<RenterLast>.Value
            lbladdress2.Text = xml...<RenterAddress1>.Value
            lblstate2.Text = xml...<RenterState>.Value
            lblzip2.Text = xml...<RenterZIP>.Value
            lblcity2.Text = xml...<RenterCity>.Value
            lblphone2.Text = xml...<RenterHomePhone>.Value
            lblemail.Text = xml...<EmailAddress>.Value
            'lbllicense.Text = xml...<LicenseNumber>.Value
            ' lblexpiredate.Text = xml...<LicenseExpDate>.Value
            lbldob.Text = xml...<RenterDOB>.Value
            lblcomments2.Text = xml...<RentalComments>.Value
            lblcdescription.Text = classDescription
            '  lblmdescription.Text = vd.ClassDescription
            imgcar2.ImageUrl = img

            lblpick.Text = RezCentralRequests.GetLocation(picklocation)
            lblrloc.Text = RezCentralRequests.GetLocation(droplocation)


            lblddate.Text = dodt.Insert(4, "/").Insert(2, "/")
            lblpdate.Text = pudt.Insert(4, "/").Insert(2, "/")
            lblconf.Text = Request.QueryString("con")


            Dim Totalweekrate As Double
            Dim TotalDayrate As Double
            Dim WeekDiscount As Double
            Dim pr As New RequestMaker
            Dim dayrate As Double
            Dim ratediscount As Double
            Dim days
            Dim frate
            Dim stotal As Double
            Dim sbd As New StringBuilder()
            ' sbd.AppendLine("<table style=""width: 98%; height: 66px; margin-bottom: 0px;"" align=""center"">")

            Dim lstExtra As New List(Of ExtraRate)

            Dim extraCodes As String = ""
            Dim amount
            Dim am
            Dim totalTax As Decimal = 0



            Dim q = From item In xml.Descendants("DailyExtra") _
              Select ExtraDesc = item.Element("ExtraDesc").Value, ExtraAmount = item.Element("ExtraAmount").Value, ExtraCode = item.Element("ExtraCode").Value

            '  Dim extraCodes As String = ""
            Trace.Write("OUTSIDE")
            For Each item In q.ToList()
                Trace.Write("INSIDE")
                sbd.AppendLine("<tr><td>" + item.ExtraDesc + "</td><td>$" + item.ExtraAmount + "</td></tr>")
                '   End If

                If Not String.IsNullOrEmpty(extraCodes) Then
                    extraCodes &= "^"
                End If

                '   extraCodes &= item.ExtraCode
            Next



            If xml...<Tax2Rate>.Value <> "" Then
                ' sbd.AppendLine("<tr><td>GST Federal Tax</td><td>$" + xml...<Tax2Charge>.Value + "</td></tr>")
            End If

            If xml...<Tax3Rate>.Value <> "" Then
                '  sbd.AppendLine("<tr><td>PST Provincial Tax</td><td>$" + xml...<Tax3Charge>.Value + "</td></tr>")
            End If




            If CInt(xml...<RateDiscount>.Value) <> 0 Then
                sbd.AppendLine("<tr><td>Discount</td><td>$" + xml...<RateDiscount>.Value + "</td></tr>")
            End If

            sbd.AppendLine("<tr><td>Estimated Total:</td><td>$" + xml...<TotalCharges>.Value + "</td></tr>")

            ' sbd.AppendLine("<tr><td><strong>Security Deposit Required</strong></td><td>" + deposit.ToString() + "</td></tr>")
            'sbd.AppendLine("<tr><td>Due on Pickup</td><td>" + (deposit + total).ToString() + "</td></tr>")
            lbltotal.Text = String.Format("{0:$#,##0.00}", xml...<TotalCharges>.Value)
            '  lblratedetail2.Text = sbd.ToString

            ' Else
            ' Response.Redirect("Modify-Reservation.aspx?err=4")
            ' End If
        Else
            Response.Redirect("Modify-Reservation.aspx?err=4")
        End If

    End Sub




    'Protected Sub btnChange2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnChange2.Click


    '    Dim req As String = RequestMaker.EditReservation(Request.QueryString("con"), _
    '                        Me.Session.SessionID, Request.UserHostAddress)
    '    Dim rep As String = RequestMaker.MakeRateRequest(req)
    '    Session("con") = Request.QueryString("con")
    '    Dim xml As XDocument = XDocument.Parse(rep)

    '    Dim pudt As String = xml...<PickupDateTime>.Value
    '    Dim dodt As String = xml...<ReturnDateTime>.Value
    '    Dim picklocation = xml...<RentalLocationID>.Value
    '    Dim droplocation = xml...<ReturnLocationID>.Value
    '    Dim qsr As String = "?pudt=" + pudt.Insert(4, "/").Insert(2, "/") + "&dodt=" + dodt.Insert(4, "/").Insert(2, "/") + _
    '                        "&classCode=" + xml...<ClassCode>.Value + "&rateID=" + xml...<RateID>.Value & "&ploc=" + picklocation + "&rloc=" + droplocation + ""
    '    Response.Redirect("list.aspx" + qsr)
    'End Sub

    Protected Sub Page_PreInit(sender As Object, e As System.EventArgs) Handles Me.PreInit
        If Common.MyCulture = "en-US" Then
            Me.Page.MasterPageFile = "MasterPage.master"
        Else
            Me.Page.MasterPageFile = "MasterPage-SP.master"
        End If
    End Sub
End Class
