﻿<%@ WebHandler Language="C#" Class="Handler" %>

using System;
using System.Web;
using System.Web.SessionState;

public class Handler : IHttpHandler, IRequiresSessionState
{

    public void ProcessRequest(HttpContext context)
    {

        context.Session["Currency"] = context.Request["currency"];
        context.Response.ContentType = "text/plain";
        context.Response.Write("changed");
    }

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

}