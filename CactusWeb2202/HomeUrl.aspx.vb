﻿
Partial Class HomeUrl
    Inherits System.Web.UI.Page


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        FillResponse()
    End Sub

    Private Sub FillResponse()
        Dim SB As StringBuilder = New StringBuilder()
        SB.AppendLine("response_code_pol=" + Request("response_code_pol"))
        SB.AppendLine("phone=" + Request("phone"))
        SB.AppendLine("additional_value=" + Request("additional_value"))
        SB.AppendLine("test=" + Request("test"))
        SB.AppendLine("transaction_date=" + Request("transaction_date"))
        SB.AppendLine("cc_number=" + Request("cc_number"))
        SB.AppendLine("cc_number=" + Request("cc_number"))
        SB.AppendLine("cc_holder=" + Request("cc_holder"))
        SB.AppendLine("error_code_bank=" + Request("error_code_bank"))
        SB.AppendLine("billing_country=" + Request("billing_country"))
        SB.AppendLine("billing_country=" + Request("billing_country"))
        SB.AppendLine("bank_referenced_name=" + Request("bank_referenced_name"))
        SB.AppendLine("description=" + Request("description"))
        SB.AppendLine("administrative_fee_tax=" + Request("administrative_fee_tax"))
        SB.AppendLine("value=" + Request("value"))
        SB.AppendLine("administrative_fee=" + Request("administrative_fee"))
        SB.AppendLine("payment_method_type=" + Request("payment_method_type"))
        SB.AppendLine("office_phone=" + Request("office_phone"))
        SB.AppendLine("email_buyer=" + Request("email_buyer"))
        SB.AppendLine("response_message_pol=" + Request("response_message_pol"))
        SB.AppendLine("error_message_bank=" + Request("error_message_bank"))
        SB.AppendLine("shipping_city=" + Request("shipping_city"))
        SB.AppendLine("transaction_id=" + Request("transaction_id"))
        SB.AppendLine("sign=" + Request("sign"))
        SB.AppendLine("tax=" + Request("tax"))
        SB.AppendLine("payment_method=" + Request("payment_method"))
        SB.AppendLine("billing_address=" + Request("billing_address"))
        SB.AppendLine("payment_method_name=" + Request("payment_method_name"))
        SB.AppendLine("pse_bank=" + Request("pse_bank"))
        SB.AppendLine("state_pol=" + Request("state_pol"))
        SB.AppendLine("date=" + Request("date"))
        SB.AppendLine("nickname_buyer=" + Request("nickname_buyer"))
        SB.AppendLine("reference_pol=" + Request("reference_pol"))
        SB.AppendLine("currency=" + Request("currency"))
        SB.AppendLine("risk=" + Request("risk"))
        SB.AppendLine("shipping_address=" + Request("shipping_address"))
        SB.AppendLine("bank_id=" + Request("bank_id"))
        SB.AppendLine("date=" + Request("date"))
        SB.AppendLine("payment_request_state=" + Request("payment_request_state"))
        SB.AppendLine("customer_number=" + Request("customer_number"))
        SB.AppendLine("administrative_fee_base=" + Request("administrative_fee_base"))
        SB.AppendLine("attempts=" + Request("attempts"))
        SB.AppendLine("merchant_id=" + Request("merchant_id"))
        SB.AppendLine("exchange_rate=" + Request("exchange_rate"))
        SB.AppendLine("shipping_country=" + Request("shipping_country"))
        SB.AppendLine("installments_number=" + Request("installments_number"))
        SB.AppendLine("franchise=" + Request("franchise"))
        SB.AppendLine("payment_method_id=" + Request("payment_method_id"))
        SB.AppendLine("extra1=" + Request("extra1"))
        SB.AppendLine("extra2=" + Request("extra2"))
        SB.AppendLine("antifraudMerchantId=" + Request("antifraudMerchantId"))
        SB.AppendLine("extra3=" + Request("extra3"))
        SB.AppendLine("nickname_seller=" + Request("nickname_seller"))
        SB.AppendLine("ip=" + Request("ip"))
        SB.AppendLine("airline_code=" + Request("airline_code"))
        SB.AppendLine("billing_city=" + Request("billing_city"))
        SB.AppendLine("pse_reference1=" + Request("pse_reference1"))
        SB.AppendLine("reference_sale=" + Request("reference_sale"))
        SB.AppendLine("pse_reference3=" + Request("pse_reference3"))
        SB.AppendLine("pse_reference2=" + Request("pse_reference2"))
        RequestMaker.WriteSchema("PaytamConfrimationResponse.txt", SB.ToString())
    End Sub

End Class


'pse_reference1=
'reference_sale=2015-05-27 13:04:37
'pse_reference3=
'pse_reference2=