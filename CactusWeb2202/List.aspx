<%@ Page Language="VB" AutoEventWireup="false" Trace="false" MasterPageFile="MastarList.master" CodeFile="List.aspx.vb" Inherits="List" %>

<%@ Register Src="Controls/address.ascx" TagName="address" TagPrefix="ad" %>

<asp:Content ID="Content3" ContentPlaceHolderID="head" runat="Server">
</asp:Content>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="ContentPlaceHolder1">

    <div class="col-xs-12 itin-box">
        <div class="col-sm-3 top-itin">
            <strong>Your Itinerary</strong>
        </div>

        <div class="col-sm-3 top-itin">
            <strong>Pick Up</strong><br />
            <strong>Location: </strong>
            <asp:Label ID="lblploc" runat="server"></asp:Label><br />
            <strong>Date: </strong>
            <asp:Label ID="lblpickupdateText" runat="server" meta:resourcekey="lblpickupdateTextResource1"></asp:Label>
            <asp:Label ID="lblptime" runat="server" meta:resourcekey="lblptimeResource1"></asp:Label>
        </div>

        <div class="col-sm-3 top-itin">
            <strong>Drop Off</strong><br />
            <strong>Location: </strong>
            <asp:Label ID="lbldloc" runat="server"></asp:Label><br />
            <strong>Date: </strong>
            <asp:Label ID="lbldtime" runat="server"></asp:Label>
        </div>

        <div class="col-sm-3 top-itin text-center">
            <asp:LinkButton ID="lnkChange" Text="&laquo; Modify" CssClass="grey-btn" runat="server"></asp:LinkButton>
        </div>

    </div>
    <div class="modal fade" tabindex="-1" role="dialog" id="noRecordPopup" data-backdrop="false">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">No Vehicles</h4>
                </div>
                <div class="modal-body">
                    <p>No Vehicles are available at this time, please adjust your search&hellip;</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default hide" data-dismiss="modal">Close</button>
                    <a  href="Default.aspx" class="btn btn-primary"  data-dismiss="modal">Modify</a>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->

   

    <%--</asp:Content>

<asp:Content ID="cnt" runat="server" ContentPlaceHolderID="ContentPlaceHolder1">--%>

    <div class="list-holder col-xs-12 clearfix">
        <asp:Label ID="Label3" runat="server" Text=""></asp:Label>
    </div>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="scriptPlaceHolder" runat="Server">
    <asp:Literal ID="ltrlNoVechile" runat="server"></asp:Literal>    
</asp:Content>
