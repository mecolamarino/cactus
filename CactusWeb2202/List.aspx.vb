﻿Imports System.Net
Imports System.IO
Imports System.Xml
Imports System.Xml.Serialization
Imports System.Linq
Imports System.Xml.Linq
Imports System.Web.Script.Serialization
'Imports System.Configuration

Partial Class List
    Inherits System.Web.UI.Page
    Dim listOfCarCode As New List(Of String)

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'AddHandler CBSelectAll.CheckedChanged 
        If Not IsPostBack Then
            BindSessionWithQueryString()
        End If

        BindCarListWithOrder()
        getrates()
        '   End If
    End Sub
    Dim req As WebRequest
    Dim rep As WebResponse

    Protected Sub BindCarListWithOrder()
        listOfCarCode.Add("CCAR")
        listOfCarCode.Add("ICAR")
        listOfCarCode.Add("FCAR")
        listOfCarCode.Add("IXAR")
        listOfCarCode.Add("IFAR") ' New Car code is Added after Ariel Request showing in the last.
        listOfCarCode.Add("IPAR")
        listOfCarCode.Add("FPAR")
        listOfCarCode.Add("MVAR")
    End Sub

    Protected Sub BindSessionWithQueryString()
        Dim htRentalParam As Hashtable = Nothing
        htRentalParam = TryCast(Session("_htRentalParam"), Hashtable)

        If Not String.IsNullOrEmpty(Request.QueryString("PickupDate")) And
            Not String.IsNullOrEmpty(Request.QueryString("DropoffDate")) And
            Not String.IsNullOrEmpty(Request.QueryString("PickupTime")) And
            Not String.IsNullOrEmpty(Request.QueryString("DropoffTime")) Then

            htRentalParam = New Hashtable

            htRentalParam.Add("PickupLocation", Request.QueryString("PickupLocation"))

            Dim dropOffLoc As String
            If Not String.IsNullOrEmpty(Request.QueryString("DropoffLocation")) Then
                dropOffLoc = Request.QueryString("DropoffLocation")
            Else
                dropOffLoc = Request.QueryString("PickupLocation")
            End If

            htRentalParam.Add("DropoffLocation", dropOffLoc)

            htRentalParam.Add("PickupDate", Request.QueryString("PickupDate"))
            htRentalParam.Add("PickupTime", Request.QueryString("PickupTime"))
            htRentalParam.Add("DropoffDate", Request.QueryString("DropoffDate"))
            htRentalParam.Add("DropoffTime", Request.QueryString("DropoffTime"))
            htRentalParam.Add("PromoCode", Request.QueryString("txtdiscount"))
            htRentalParam.Add("ClassCode", Request.QueryString("code"))
            htRentalParam.Add("vechileType", Request.QueryString("vechileType"))

            Session.Add("_htRentalParam", htRentalParam)
            Trace.Write("INSIDE")
        End If


    End Sub



    Protected Sub getrates()
        Dim pickupdate, dropoffdatedate As String
        Dim pickuptime, dropofftime, vechileType As String
        Dim discount As String = ""
        Dim url As String = System.Configuration.ConfigurationManager.AppSettings("Url")
        Dim cl As New RequestMaker
        vechileType = ""
        Dim htRentalParam As Hashtable = Nothing
        htRentalParam = TryCast(Session("_htRentalParam"), Hashtable)
        ' Dim uri As String = url
        Dim val As Integer
        'If Request.QueryString("modify") = "true" Then
        '    If htRentalParam Is Nothing Then
        '        Response.Redirect("SessionExpired.aspx")
        '    Else
        '        txtpickupdate.Text = pickupdate.Substring(0, 8).Insert(4, "/").Insert(2, "/")
        '        txtdropoffdate.Text = dropoffdatedate.Substring(0, 8).Insert(4, "/").Insert(2, "/")
        '        lnkChange.Visible = False
        '    End If
        'Else


        Dim ploc As String
        Dim dloc As String
        Dim loc As New RequestMaker
        Dim cLoc As String

        If Request.QueryString("modify") = "true" Then
            If htRentalParam Is Nothing Then
                Response.Redirect("SessionExpired.aspx")
                Trace.Write("EXPIRED")
            Else
                dropoffdatedate = htRentalParam("DropoffDate")
                pickupdate = htRentalParam("PickupDate")
            End If
        Else
            If htRentalParam Is Nothing Then
                Response.Redirect("SessionExpired.aspx")
                Trace.Write("EXPIRED")
            End If

            vechileType = htRentalParam("vechileType")
            pickuptime = htRentalParam("PickupTime")
            dropofftime = htRentalParam("DropoffTime")
            pickupdate = htRentalParam("PickupDate")
            val = pickupdate.IndexOf("/")
            If val = -1 Then

                dropoffdatedate = htRentalParam("DropoffDate")

                pickupdate = htRentalParam("PickupDate")
                dropoffdatedate = htRentalParam("DropoffDate")

                Trace.Write("dont Insert slash")

            Else
                Trace.Write("VAL" & val)
                Trace.Write("INSERT SLACSH")

                'dropoffdatedate = htRentalParam("DropoffDate") & " " & dropofftime
                'pickupdate = htRentalParam("PickupDate") & " " & pickuptime

                dropoffdatedate = cl.processDateString(htRentalParam("DropoffDate")) & " " & dropofftime
                pickupdate = cl.processDateString(htRentalParam("PickupDate")) & " " & pickuptime
            End If

            End If

            '  discount = Trim(Request.QueryString("disc"))

            discount = htRentalParam("PromoCode")
            'Dim ploc As String
            'Dim dloc As String
            'Dim loc As New RequestMaker

            ploc = htRentalParam("PickupLocation")
            dloc = htRentalParam("DropoffLocation")
            cLoc = htRentalParam("CountryLoc")
            lblploc.Text = loc.GetLocation(ploc)
            lbldloc.Text = loc.GetLocation(dloc)
            lblptime.Text = pickupdate.Insert(2, "/").Insert(5, "/")
            lbldtime.Text = dropoffdatedate.Insert(2, "/").Insert(5, "/")

            '   Dim companyID
            ' companyID = Request.QueryString("companyID")

            Dim classCode As String = htRentalParam("ClassCode")
            Try
                'Dim fileName As String = "D:\DropboxFiles\My Dropbox\E-applied\CarRental\CarRentalDemo\CarRentalDemo\XMLFile2.xml"


                Dim uri As String = url


                req = WebRequest.Create(uri)
                req.Method = "POST"
                req.ContentType = "text/xml"

                Dim writer As New StreamWriter(req.GetRequestStream())
				Dim rateRequestString As String
                ' Dim classes As String = GetSelectedClasses()
                '   Trace.Write("CLASSES" & classes)


                Trace.Write("PICKDATE" & pickupdate)
                Trace.Write("DROP DATE" & dropoffdatedate)


                '  writer.Write(RequestMaker.RateRequest(cl.processDateString(pickupdate) + " " + pickuptime, cl.processDateString(dropoffdatedate) + " " + dropofftime, "", Me.Session.SessionID, Me.Request.UserHostAddress, "OCHS", "OCHS", discount))
            'Debug.WriteLine(RequestMaker.RateRequest(pickupdate, dropoffdatedate, classCode, Me.Session.SessionID, Me.Request.UserHostAddress, ploc, dloc, discount, cLoc))
                'Response.Write("<pre>"+RequestMaker.RateRequest(pickupdate, dropoffdatedate, "", Me.Session.SessionID, Me.Request.UserHostAddress, ploc, dloc, discount)+"</pre>")
				
				rateRequestString = RequestMaker.RateRequest(pickupdate, dropoffdatedate, classCode, Me.Session.SessionID, Me.Request.UserHostAddress, ploc, dloc, discount, cLoc)
				
				
				writer.Write(rateRequestString) 
				RequestMaker.WriteSchema("listCarsRequest.txt", rateRequestString)
                writer.Close()

                rep = req.GetResponse()

                Dim receivestream As Stream = rep.GetResponseStream()
                Dim encode As Encoding = Encoding.GetEncoding("utf-8")
                Dim readstream As New StreamReader(receivestream, encode)
                Dim miles, rentaldays

                Dim read(256) As [Char]
                Dim result As String
                'Dim IsLocal As Boolean
                Dim counter As Integer
                Dim Tcounter As Integer
                counter = 0
                Tcounter = 0
                result = readstream.ReadToEnd()
		
				RequestMaker.WriteSchema("listCarsResponse.txt", result)
				
            'Debug.WriteLine(result)

                Dim xml As XDocument = XDocument.Parse(result)
            ' Trace.Write()
            'Add where clause to get single result
            'Where (ele...<RateAmount>.Value >= 25.6 And ele...<RateAmount>.Value <= 26)
            Dim el2 = From ele In xml...<RateProduct> _
                      Order By CDbl(ele...<RateAmount>.Value) Ascending _
                 Select New Rate(ele.<RateID>.Value, ele.<ClassCode>.Value, ele.<ClassDesc>.Value, ele.<RatePlan>.Value, ele...<RateCode>.Value, ele...<RentalDays>.Value, ele...<TotalPricing>.<RateAmount>.Value, ele...<Prepaid>.<RateAmount>.Value, ele...<PrepaidBaseDiscount>.Value, ele...<FreeMiles>.Value, ele...<TotalFreeMiles>.Value, ele...<RateCharge>.Value, ele...<TotalTaxes>.Value, ele...<TotalPricing>.<TotalCharges>.Value, ele...<Prepaid>.<TotalPricing>.<TotalCharges>.Value, ele...<CH>.Value, ele...<ClassImageURL>.Value, ele...<RateDiscount>.Value, ele.<ModelDesc>.Value, ele...<PerMileAmount>.Value, ele...<Rate1PerDay>.Value)

                Dim el3 = From nor In xml...<NoRatesFound> _
                         Select New NoRate(nor.<Class>.Value, nor.<Description>.Value, nor.<ClassImageURL>.Value, nor.<ModelDesc>.Value, nor.<PickupPhone>.Value)


                If el2.Count = 0 Then
                    Label3.Text = "<div class='col-sm-12'><font color='red'>Unavailable, Please revise your search.<br><br>Thank you, RentMax Car Rental</font></div>"
                    ' Return
                End If
 
                Dim el = From e3 In el2

                Dim nolist = From e4 In el3

                Dim filtercarcodelist As New List(Of Rate)
                Dim filterbycode As Rate
                Dim CarCode As String


                Try
                    For Each item In listOfCarCode
                        For Each CAR In el.ToList()

                            If (CAR.ClassCode = item) Then
                                filtercarcodelist.Add(CAR)
                            End If


                        Next

                    Next



                    filtercarcodelist.AddRange(el.ToList().Where(Function(name) Not listOfCarCode.Contains(name.ClassCode)).ToList())

                Catch ex As Exception
                    Label3.Text += ex.ToString

                End Try



                Session("_vehicleList") = filtercarcodelist

                Dim minDays As Integer = 1 ' el.Min(Function(i) i.RentalDays)
                Dim sbd As New StringBuilder()
                Dim sbdL As New StringBuilder()
                Dim sbdT As New StringBuilder()
                ' sbd.AppendLine("<table width=570 border=0>")
                Dim RegRate As Double

            ' Dim el6 = filtercarcodelist
            Dim el6 = el

            If Not String.IsNullOrEmpty(vechileType) Then
                el6 = el6.Where(Function(name) name.ClassCode.Contains(vechileType) Or name.ClassDescription.Contains(vechileType)).ToList()
            Else

                End If

            ' We need only the records that has days cost greater then zero.
	    
	    'Response.Write(el6.Where(Function(name) (name.PrepaidAmount / name.RentalDays) > 0).Count())
            For Each item As Rate In el6 '.Where(Function(name) (name.PrepaidAmount / name.RentalDays) > 0)

                    Dim carClassInfo As New CarClass(item.ClassCode)


                    ' miles = item.TotalFreeMiles
                    If counter = 0 Then
                        rentaldays = item.RentalDays
                        ' Response.Write("Rental" & rentaldays)
                    End If
                    counter += 1

                    pickupdate = htRentalParam("PickupDate")
                    dropoffdatedate = htRentalParam("DropoffDate")

                    Dim qsr As String = ("?pudt=" + pickupdate + "&putime=" + pickuptime + "&dodt=" + dropoffdatedate + "&dotime=" + dropofftime + "&classCode=" + item.ClassCode + "&rateCode=" + item.RateCode + "&rateID=" + item.RateID.ToString() & "&ploc=" + ploc + "&rloc=" + dloc + "&disc=" & discount & "&modify=" & Request.QueryString("modify"))


                    Trace.Write("inside")

                    sbdL.AppendLine("<div class='col-sm-12 clearfix car-row' style='border-top:1px solid #D0D0D0;padding-top:15px'>")

                    sbdL.AppendLine("<div class='col-sm-4' style='padding:0px;'>")
					
					'Display adjusted model description
                    
					Dim adjModelDesc As String = item.modeldesc
                    adjModelDesc = Replace(adjModelDesc, "OR SIMILAR", "")

                    sbdL.AppendLine("<h3 style=""margin-bottom:0px; margin-top:0px; font-weight:bold;"">" + adjModelDesc + " <small>or Similar</small></h3>")
					
                    sbdL.AppendLine("<p style=""margin-top:0px;"">" + (item.ClassDescription) + "</p>")
					
                    sbdL.AppendLine("<img src='carimages/" + item.ClassCode + ".jpg' alt='" + adjModelDesc + "' title='" + adjModelDesc + "' />")
                    sbdL.AppendLine("</div>")

                    sbdL.AppendLine("<div class='col-sm-2'>")

                    'Display car class info

                    'get doors from model description
                    Dim txtDoors As String = ""
                    Dim iLocDoors, iLastSpace As Integer
                    Dim txtReverseDescription As String = ""

                    iLocDoors = item.ClassDescription.IndexOf("Door")
                    iLastSpace = 0

                    If iLocDoors > 1 Then
                        txtReverseDescription = StrReverse(Left(item.ClassDescription, iLocDoors - 1))
                        iLastSpace = txtReverseDescription.IndexOf(" ")
                        txtDoors = Mid(item.ClassDescription, iLocDoors - iLastSpace, iLastSpace + 1)
                    End If
                    sbdL.AppendLine(String.Format("<div style='width:20px;text-align:center;display:inline-block;margin-right:3px;'><img src='images/car-door.png'></div>{0}<br/>", txtDoors))
                    sbdL.AppendLine(String.Format("<i class='fa fa-user' style='width:20px;text-align:center;'></i> {0}<br/>", carClassInfo.passengers))
                    sbdL.AppendLine(String.Format("<i class='fa fa-suitcase' style='width:20px;text-align:center;'></i> {0}<br/>", carClassInfo.luggage))

                    'Get automatic or manual transmission
                    Dim transmissionType As String = ""
                    If item.ClassDescription.IndexOf("Automatic") > 0 Then
                        transmissionType = "Auto"
                    End If

                    If item.ClassDescription.IndexOf("Manual") > 0 Then
                        transmissionType = "Manual"
                    End If

                    sbdL.AppendLine(String.Format("<i class='fa fa-cog' style='width:20px;text-align:center;'></i> {0}<br/>", transmissionType))
                    'Display mileage text
                    Dim mileageText As String
                    mileageText = String.Format("<i class='fa fa-road' style='width:20px;text-align:center;'></i> {0:00}/{1:00} mpg<br/>", carClassInfo.mpgCity, carClassInfo.mpgHighway)
                    sbdL.AppendLine(mileageText)
                    sbdL.AppendLine("</div>")


                'Pay at counter price
                    Dim origamount As Double
                    Dim rmile As String
                    If item.ratepermile = "0.00" Then
                        rmile = ""
                    Else
                        rmile = " ($" & item.ratepermile & " /mile beyond)"
                    End If

                    Dim tmiles
                    If item.TotalFreeMiles = "0" And item.ratepermile = "0.00" Then
                        tmiles = "Unlimited"
                    Else
                        If item.TotalFreeMiles <> "0" Then
                            tmiles = item.TotalFreeMiles
                        Else
                            tmiles = "None"
                        End If

                    End If


                sbdL.AppendLine("<div class='col-sm-3 pay-box'> <span class='pricelist-counter'>" + item.RatePerDay.ToString("C") & "</span> USD per day<br>")

                    If item.RateDiscount < 0 Then
                        origamount = item.TotalCharge - (item.RateDiscount)

                        sbdL.AppendLine("<strong>Total Cost (Before Discount):</strong> " + origamount.ToString("C") & "")
                        sbdL.AppendLine("<strong>Discounted Total:</strong> " + item.TotalCharge.ToString("C") & " <br><small>(Taxes/Surcharges Inc.)</small>")
                    Else
                        sbdL.AppendLine(item.TotalCharge.ToString("C") & " est. total <br><span class='small'>(Taxes/Surcharges Inc.)</span>")
                    End If

                    sbdL.AppendLine("<br>")
                    'sbdL.AppendLine("<strong>Free Miles:</strong> " + tmiles.ToString & rmile & "")
                    sbdL.AppendLine("<br>")
                    sbdL.AppendLine("<a href='RateDetails.aspx" + qsr + "' class='btn-counter'>Pay at Counter</a>")
                    sbdL.AppendLine("</div>")

                'Pay Now price
                    ' If item.ClassCode = "PCAR" Or item.ClassCode = "SSAR" Or item.ClassCode = "STAR" Or item.ClassCode = "XSAR" Or item.ClassCode = "XVAR" Then

                'Else
                Dim origPrepaidAmount As Double
                Dim prepaidAmount As Double

                origPrepaidAmount = item.PrepaidAmount
                Dim origPrepaidRatePerDay As Double = item.PrepaidAmount / item.RentalDays

                'Limit discount to base price if too large, or zero if not found

                sbdL.AppendLine("<div class='col-sm-3' style='padding-bottom:15px'> <span class='pricelist'>" + origPrepaidRatePerDay.ToString("C") & "</span> USD per day<br>")

                    If item.RateDiscount < 0 Then
                    'origamount = item.TotalCharge - (item.RateDiscount)
                    prepaidAmount = item.PrepaidAmount - item.RateDiscount

                    sbdL.AppendLine("<strong>Total Cost (Before Discount):</strong> " + origPrepaidAmount.ToString("C") & "")
                    sbdL.AppendLine("<strong>Discounted Total:</strong> " + prepaidAmount.ToString("C") & " <br><small>(Taxes/Surcharges Inc.)</small>")
                    Else
                    sbdL.AppendLine(item.PrepaidTotalCharge.ToString("C") & " est. total <br><span class='small'>(Taxes/Surcharges Inc.)</span>")
                    End If

 

                    sbdL.AppendLine("<br>")
                   ' sbdL.AppendLine("<strong>Free Miles:</strong> " + tmiles.ToString & rmile & "")
                    sbdL.AppendLine("<br>")
                sbdL.AppendLine("<a href='RateDetails.aspx" + qsr + "&prepaid=y" + "' class='btn-next'>Pay Now</a>")
                'Response.Redirect("RateDetails.aspx" + qsr)
                sbdL.AppendLine("<br>")

                'Calculate savings
                Dim saveamount = item.TotalCharge - item.PrepaidTotalCharge
                sbdL.AppendLine("<div style='font-size:20px;color:red; padding-top:15px;'>" & String.Format("Save ${0:C}!", Convert.ToString(saveamount.ToString("0.00"))) & "</div>")
                    sbdL.AppendLine("</div>")
                    sbdL.AppendLine("</div>")



                    'sbdL.AppendLine("<div class='col-sm-2'>")
                    'sbdL.AppendLine("<div>Discount Per Day</div>")
                    'sbdL.AppendLine("<div>Est Discount Total</div>")
                    'sbdL.AppendLine("<a href='RateDetails.aspx" + qsr + "&prepaid=y" + "' class='btn-next'>Pay Now</a>")


                    'sbdL.AppendLine("<div style=""color:#f60707;"">Enter Savings Here</div>")
                    'sbdL.AppendLine("</div>")


                    'sbdL.AppendLine("<div class='col-sm-2'>")

                    'sbdL.AppendLine("</div>")

                    '' End If

                    'sbdL.AppendLine("</div>")


                    ' Next


                    ' For Each item As NoRate In nolist.ToList()

                    'Dim carModel As String() = item.modeldesc.Split(new String() { "or" }, StringSplitOptions.None)
                    'Dim carModelName as String = carModel(0).TrimEnd()

                    'sbdL.AppendLine("<div class='col-sm-12 clearfix car-row'>")

                    'sbdL.AppendLine("<div class='col-sm-2 text-center' style='padding:0px;'>")
                    'sbdL.AppendLine("<img src='carimages/" + item.ClassCode + ".jpg' alt='" + carModelName + "' title='" + carModelName + "' />")
                    'sbdL.AppendLine("</div>")

                    'sbdL.AppendLine("<div class='col-sm-10'>")

                    'sbdL.AppendLine("<strong style=""color:#ff0000;"">" + carModelName + " <!--small>or Similar</small--> is currently unavailable at this time, please contact us at 808-419-7401 for more information</strong>")
                    'sbdL.AppendLine("</div>")

                    ' sbdL.AppendLine("</div>")

            Next

            If el6.Count() > 0 Then
                ltrlNoVechile.Text = " <script type='text/javascript'> $(document).ready(function () { $('#noRecordPopup').modal('show');});</script>"
            End If

                sbd.AppendLine("<tr valign=top><td >" + sbdL.ToString() + "</td><td>" + sbdT.ToString())
                sbd.AppendLine("</td></tr></table>")

                Label3.Text += sbd.ToString()
            Catch ex As Exception
                Trace.Write("EX" & ex.ToString)
                Label3.Text += ex.ToString
            End Try
    End Sub

    Function SerializeRate(ByVal rate As Rate) As String
        Dim result As String = ""
        Dim xs As New XmlSerializer(GetType(Rate))
        Dim wrt As New StringWriter()
        xs.Serialize(wrt, rate)
        result = wrt.ToString()
        wrt.Close()
        result = result.Replace("<?xml version=""1.0"" encoding=""utf-16""?>", "")
        result = result.Replace("<Rate xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"">", "")
        Return result

    End Function

    Function processDateString(ByVal value As String) As String
        Dim re() As String
        Dim c() As Char = {"/"c}
        re = value.Split(c)
        If re(0).Length < 2 Then
            re(0) = "0" + re(0)
        End If
        If re(1).Length < 2 Then
            re(1) = "0" + re(1)
        End If
        Return re(0) + re(1) + re(2)
    End Function


    Protected Sub lnkChange_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkChange.Click
        Dim vh As New RequestMaker
        vh.RetainVehicleInfo()
    End Sub


End Class
