﻿Imports System.Net
Imports System.IO

Partial Class List
    Inherits System.Web.UI.Page

    Dim xml As XDocument



    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'AddHandler CBSelectAll.CheckedChanged 
        ' If Not IsPostBack Then
        getrates()

        'GetClassDetail("CFAR")
        '   End If
    End Sub
    Dim req As WebRequest
    Dim rep As WebResponse

    Protected Sub getrates()
        Try

            Dim htRentalParam As Hashtable = Nothing

            htRentalParam = TryCast(Session("_htRentalParam"), Hashtable)

            If htRentalParam Is Nothing Then
                Response.Redirect("SessionExpired.aspx")
            Else
                Dim uri As String = ConfigurationManager.AppSettings("Url")

                Dim req As WebRequest
                Dim rep As WebResponse

                req = WebRequest.Create(uri)
                req.Method = "POST"
                req.ContentType = "text/xml"

                Dim writer As New StreamWriter(req.GetRequestStream())

                Dim pickupLocID As String, dropOffLocID As String, pickupDate As String, dropOffDate As String, pickupTime As String, dropOffTime As String, promoCode As String, classcode As String, age As String

                pickupLocID = htRentalParam("PickupLocation")
                dropOffLocID = htRentalParam("DropoffLocation")
                pickupDate = htRentalParam("PickupDate")
                dropOffDate = htRentalParam("DropoffDate")
                pickupTime = htRentalParam("PickupTime")
                dropOffTime = htRentalParam("DropoffTime")
                promoCode = htRentalParam("PromoCode")
                classcode = htRentalParam("ClassCode")

                'age = htRentalParam("Age")



                Trace.Write("INSIDE")
                writer.Write(RezCentralRequests.RateRequest(Common.ProcessDateString(pickupDate) + " " + pickupTime, Common.ProcessDateString(dropOffDate) + " " + dropOffTime, pickupLocID, dropOffLocID, promoCode, classcode))
                writer.Close()

                rep = req.GetResponse()

                Dim receivestream As Stream = rep.GetResponseStream()
                Dim encode As Encoding = Encoding.GetEncoding("utf-8")
                Dim readstream As New StreamReader(receivestream, encode)

                Dim read(256) As [Char]
                Dim result As String
                result = readstream.ReadToEnd()

                xml = XDocument.Parse(result)

                'Dim el2 = From ele In xml...<RateProduct> _
                '          Order By CDbl(ele...<RateAmount>.Value) Ascending _
                '          Select New Rate(ele.<RateID>.Value,
                '                          pickupLocID,
                '                          dropOffLocID,
                '                          DateTime.ParseExact(pickupDate + " " + pickupTime, "M/d/yyyy hh:mm tt", Nothing),
                '                          DateTime.ParseExact(dropOffDate + " " + dropOffTime, "M/d/yyyy hh:mm tt", Nothing),
                '                          ele.<ClassCode>.Value,
                '                          ele.<ClassDesc>.Value,
                '                          ele.<ModelDesc>.Value,
                '                          ele.<RatePlan>.Value,
                '                          ele...<RateCode>.Value,
                '                          ele...<Months>.Value,
                '                          ele...<AmtPerMonth>.Value,
                '                          ele...<RentalDays>.Value,
                '                          ele...<Rate1PerDay>.Value,
                '                          ele...<Hours>.Value,
                '                          ele...<AmtPerHour>.Value,
                '                          ele...<Weeks>.Value,
                '                          ele...<AmtPerWeek>.Value,
                '                          ele...<FreeMiles>.Value,
                '                          ele...<TotalFreeMiles>.Value,
                '                          ele...<RatePlusLate>.Value,
                '                          ele...<Tax2Rate>.Value,
                '                          ele...<Tax2Charge>.Value,
                '                          ele...<Tax3Rate>.Value,
                '                          ele...<Tax3Charge>.Value,
                '                          ele...<TotalTaxes>.Value,
                '                          ele...<TotalExtras>.Value,
                '                          ele...<TotalCharges>.Value,
                '                          ele...<ClassImageURL>.Value,
                '                          ele...<RateDiscount>.Value,
                '                          ele.<Passengers>.Value,
                '                          ele.<Luggage>.Value,
                '                          promoCode,
                '                          ele.<ClassNotes>.Value,
                '                          ele.<MPGCity>.Value,
                '                          ele.<MPGHighway>.Value,
                '                          ele...<RateCharge>.Value,
                '                          ele...<Rate1Days>.Value,
                '                          ele...<Rate1PerDay>.Value)
                Dim el2 = From ele In xml...<RateProduct> _
                          Order By CDbl(ele...<RateAmount>.Value) Ascending _
                         Select New Rate(ele.<RateID>.Value, ele.<ClassCode>.Value, ele.<ClassDesc>.Value, ele.<RatePlan>.Value, ele...<RateCode>.Value, ele...<RentalDays>.Value, ele...<RateAmount>.Value, ele...<FreeMiles>.Value, ele...<TotalFreeMiles>.Value, ele...<RateCharge>.Value, ele...<TotalTaxes>.Value, ele...<TotalCharges>.Value, ele...<CH>.Value, ele...<ClassImageURL>.Value, ele...<RateDiscount>.Value, ele.<ModelDesc>.Value, ele...<PerMileAmount>.Value)

                Dim el3 = From nor In xml...<NoRatesFound> _
                          Select New NoRate(nor.<Class>.Value, nor.<Description>.Value, nor.<ClassImageURL>.Value, nor.<ModelDesc>.Value, nor.<PickupPhone>.Value)


                Dim el = From e3 In el2
                Dim nolist = From e4 In el3

                Session("_vehicleList") = el

                gvListing.DataSource = el
                gvListing.DataBind()

                If el.Count() <= 0 Then
                    ' pnlNoResults.Visible = True
                    ' gvListing.DataSource = Nothing
                    ' gvListing.DataBind()
                Else
                    'pnlRecommend.Visible = True
                End If

            End If

        Catch ex As Exception
            Trace.Write("EX" & ex.ToString)
        End Try
    End Sub

    Function FormatClassDesc(ByVal classDesc As String) As String
        Dim returnValue As String = ""

        Dim str() As String = classDesc.Split(" ")

        Dim i As Integer = 1
        For Each s As String In str
            If i = 1 Then
                returnValue = "<strong>" & s & "</strong><br/>"
                i += 1
            Else
                returnValue += s & " "
            End If
        Next

        Return Server.HtmlEncode(returnValue)
    End Function
    Protected Sub lnkBtnMoreDet_Click(ByVal sender As Object, ByVal e As EventArgs)

        Dim rateId As Integer
        Integer.TryParse(DirectCast(sender, LinkButton).CommandArgument, rateId)

        If rateId > 0 Then
            Dim vl = DirectCast(Session("_vehicleList"), IEnumerable(Of Rate))

            Dim vd = vl.Where(Function(x) x.RateID = rateId)

            Session("_vehicleDetail") = vd.FirstOrDefault()

            Response.Redirect("Add-Extras.aspx")
        End If
    End Sub
    Protected Sub lnkBtnBookThisCar_Click(ByVal sender As Object, ByVal e As EventArgs)
        Dim rateId As Integer
        Integer.TryParse(DirectCast(sender, LinkButton).CommandArgument, rateId)

        If rateId > 0 Then
            Dim vl = DirectCast(Session("_vehicleList"), IEnumerable(Of Rate))

            Dim vd = vl.Where(Function(x) x.RateID = rateId)

            Session("_vehicleDetail") = vd.FirstOrDefault()

            Response.Redirect("Add-Extras.aspx")
        End If
    End Sub

    Public Shared Function GetRatePerPeriod(months As Integer, ratePerMonth As Double, weeks As Integer, ratePerWeek As Double, days As Integer, ratePerDay As Double, hours As Integer, ratePerHour As Double) As String
        If months > 0 Then
            Return ratePerMonth.ToString("$#,##0.00") + "/" + HttpContext.GetGlobalResourceObject("GlobalResource", "Month")
        ElseIf weeks > 0 Then
            If days >= 7 Then
                Return ratePerWeek.ToString("$#,##0.00") + "/" + HttpContext.GetGlobalResourceObject("GlobalResource", "Week")
            End If

        ElseIf days > 0 Then
            Return ratePerDay.ToString("$#,##0.00") + "/" + HttpContext.GetGlobalResourceObject("GlobalResource", "Day")
        ElseIf hours > 0 Then
            Return ratePerHour.ToString("$#,##0.00") + "/" + HttpContext.GetGlobalResourceObject("GlobalResource", "Hour")
        Else
            Return ""
        End If
    End Function


    Public Shared Function GetDescription(carcode As String) As String

        Dim desc

        desc = HttpContext.GetGlobalResourceObject("GlobalResource", carcode)
        Return desc
    End Function
End Class




