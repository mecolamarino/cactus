﻿
Partial Class MastarList
    Inherits System.Web.UI.MasterPage

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load

        If Not Page.IsPostBack Then


            If Session("Currency") Is Nothing Or Session("Currency") = 1 Then
                ddlCurrency.SelectedValue = "ARS"
            Else
                ddlCurrency.SelectedValue = "USD"
            End If
        End If

    End Sub
    
    Protected Sub btnChangeCurrency_Click(sender As Object, e As EventArgs) Handles btnChangeCurrency.Click
        Session("Currency") = hdnFeildCurrencyAmount.Value
        ' Response.Redirect(Request.Url.ToString())
        hdnFeildUrl.Value = Request.Url.ToString()

    End Sub
End Class

