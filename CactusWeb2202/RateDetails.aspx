<%@ Page Language="VB" AutoEventWireup="false" CodeFile="RateDetails.aspx.vb" Inherits="RateDetails" Trace="false" MasterPageFile="MastarList.master" Culture="auto" meta:resourcekey="PageResource1" UICulture="auto" %>

<%@ Register Src="Controls/address.ascx" TagName="address" TagPrefix="ad" %>
<asp:Content ID="Content3" ContentPlaceHolderID="head" runat="Server">
    <title>Rate Details</title>
    <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css">
        <style type="text/css">
        .plcy
        {
            height: 200px;
            overflow-x: auto;
        }
    </style>
</asp:Content>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="ContentPlaceHolder1">

        <div class="col-sm-10 col-sm-offset-1">

            <div class="col-sm-4">
                <div class="side-bar">

                    <asp:Label ID="LabelcarInfo" runat="server" Visible="false" meta:resourcekey="LabelcarInfo"></asp:Label>
                    <div class="about-car">
							<div class="title">
								<h2>Sed&aacute;n 4 puertas</h2>
								<span>Chevrolet Classic</span>
							</div>
							<figure>
								<img src="images/about-car.png" alt="">
							</figure>
                            <ul class="hidden-sm hidden-xs">
								
								<li>
                           
									<figure>
										
									</figure>
									<h3>Rendimiento:</h3>
								</li>
                                <li>
                           
									<figure>
										<img src="images/about-1.png" alt="">
									</figure>
									<span>6,5lts / 100km</span>
								</li>
							</ul>
							
							<ul class="hidden-sm hidden-xs">
								<li>
                           
									<figure>
										
									</figure>
									<h3>Equipamiento:</h3>
								</li>
								<li>
									<figure>
										<img src="images/about-3.png" alt="">
									</figure>
									<span>Aire Acondicionado</span>
								</li>
								<li>
									<figure>
										<img src="images/about-4.png" alt="">
									</figure>
									<span>Caja de Cambios Manual</span>
								</li>
							</ul>
							<ul class="hidden-sm hidden-xs full">
								<li>
                           
									<figure>
										
									</figure>
									<h3>Capacidad</h3>
								</li>
								<li>
									<figure>
										<img src="images/about-6.png" alt="">
									</figure>
									<span>5 Pasajeros </span>
								</li>
								<li>
									<figure>
										<img src="images/about-7.png" alt="">
									</figure>
									<span>2 Valijas Grandes <br>y 2 chicas</span>
								</li>
							</ul>
						</div>

                    <div class="rental-information">

                        <table>
                            <thead>
                                <tr>
                                    <th>Info del Alquiler</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>
                                        <span>Punto de Entrega:</span>
                                        <h3>
                                            <asp:Label ID="lblploc" runat="server" meta:resourcekey="lblplocResource1"></asp:Label></h3>
                                        <p>
                                            <asp:Label ID="lblpickupdateText" runat="server" meta:resourcekey="lblpickupdateTextResource1"></asp:Label>
                                            <asp:Label ID="lblptime" runat="server" meta:resourcekey="lblptimeResource1"></asp:Label>
                                        </p>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <span>Punto de Devoluci�n</span>
                                        <h3>
                                            <asp:Label ID="lbldloc" runat="server" meta:resourcekey="lbldlocResource1"></asp:Label></h3>
                                        <p>
                                            <asp:Label ID="lbldropffDatetext" runat="server" meta:resourcekey="lbldropffDatetextResource1"></asp:Label>
                                            <asp:Label ID="lbldtime" runat="server" meta:resourcekey="lbldtimeResource1"></asp:Label>
                                        </p>
                                    </td>
                                </tr>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>D�as de alquiler:
                                        <asp:Label ID="LabelRPeriod" runat="server" meta:resourcekey="LabelRPeriod"></asp:Label></th>
                                </tr>
                            </tfoot>
                        </table>
                        
                        </div>
                        
                        <table class="detail-user hidden-sm hidden-xs">
                            <thead>
                                
                                <tr>
                                    <th colspan="2">Detalles <span style="display:inline-block; float:right;">TOTAL PARCIAL</span></th>
                                </tr>
                            </thead>
                            <tbody>


                                <asp:Label ID="lblRateDetails" runat="server" meta:resourcekey="Label1Resource1"></asp:Label>
                                <asp:Label ID="lblExTraCharges" runat="server" meta:resourcekey="Label1Resource1"></asp:Label>


                            </tbody>
                            <tfoot>
                                <tr>
                                    <td colspan="2">Pago Total 
                                        <div style="display:inline-block; float:right; text-align:right;"><asp:Label ID="lblcost" CssClass="important-alert" runat="server"></asp:Label><br>
                                        Impuesto incluido
                                        </div>
                                    </td>
                                </tr>
                            </tfoot>
                        </table>

                    <div class="info">
                        <div class="info-cancel">
                            <figure>
                                <img src="images/icon-1.png" alt="">
                            </figure>
                            <h3>Cancelaci&oacute;n Gratuita</h3>
                            <span>Modifica
                        o cancela tu reserva sin costo antes de 24 de Mayo 2015</span></div>
                        <div class="info-attention">
                            <figure>
                                <img src="images/icon-2.png" alt="">
                            </figure>
                            <h3>Edad m&iacute;nima de conductor 21 a&ntilde;os.
                            </h3>
                            <span>Si tiene entre 18 a 21 a�os debera pagar un adicional
                            </span>
                            <div class="info-attention-contact">
                                <ul>
                                    <li>
                                        <a href="javascript:if(window.print)window.print()"><i class="fa fa-print"></i>
                                        <span>Imprimir esta reserva</span></a>
                                    </li>
                                    <li>
                                        <i class="fa fa-envelope"></i>
                                        <span>Recomendar esta oferta</span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                  </div>
                </div>

            <!-------------------- ---------------------------->

            <div class="col-sm-8">
                <div class="detail-check">
                    <table class="detail-sevr">
                        <thead>
                            <tr>
                                <th>1.Rate</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>
                                    <asp:RadioButton AutoPostBack="true" runat="server" OnCheckedChanged="plater_CheckedChanged" Text="Pagar en Destino" ID="plater" name="plater" value="plater" GroupName="pay-when" CssClass=""></asp:RadioButton>
                                    <figure>
                                        <a type="button" data-toggle="modal" data-target="#myModal">
                                            <img src="images/icon-i.png" alt="">
                                            </a>
                                    </figure>
                                    <div class="modal fade" id="myModal" role="dialog" style="display: none;">
                                        <div class="modal-dialog">
                                            <!-- Modal content-->
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal">�</button>
                                                    <figure>
                                                        <img src="images/icon-i-2.png" alt="">
                                                    </figure>
                                                    <h4 class="modal-title">Lorem Ipsum is</h4>
                                                </div>
                                                <div class="modal-body">
                                                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <p>
                                        por d&iacute;a: <span id="pay-ltr-price"><%Extentions.GetCurrency %>&nbsp;<asp:Label ID="labelTotal2" Text="" runat="server"></asp:Label>
                                            
                                    </p>
                                </td>
                            </tr>
                            
							
                            
                            <tr class="pay-later-close">
                                <td>
                                    <figure>
                                        <img src="images/check.png" alt="">
                                    </figure>
                                    <span>Cambios de Reserva y cancelaci�n.
                                  </span>
                                </td>
                            </tr>
                            <tr class="pay-later-close">
                                <td>
                                    <figure>
                                        <img src="images/check.png" alt="">
                                    </figure>
                                    <span>Kilometros ilimitados.
                                  </span>
                                </td>
                            </tr>
                            <tr class="pay-later-close">
                                <td>
                                    <figure>
                                        <img src="images/check.png" alt="">
                                    </figure>
                                    <span>Cobertura Parcial por colisi�n y robo
                                  </span>
                                </td>
                            </tr>
                            <tr class="pay-later-close">
                                <td>
                                    <figure>
                                        <img src="images/check.png" alt="">
                                    </figure>
                                    <span>Impuestrosincluidos.
                                  </span>
                                </td>
                            </tr>
                            <tr class="pay-later-close">
                                <td>
                                    <figure>
                                        <img src="images/check.png" alt="">
                                    </figure>
                                    <span>Factura por correo elecr�nico.										</span>
                                </td>
                            </tr>
                            <tr class="pay-later-close">
                                <td>
                                    <figure>
                                        <img src="images/check.png" alt="">
                                    </figure>
                                    <span>xxxxxxxxxxxxxxxx</span>
                                </td>
                            </tr>
                            <tr class="pay-later-close">
                                <td>
                                    <figure>
                                        <img src="images/check.png" alt="">xxxxxxxxxxxxxxxx</figure></td>
                            </tr>
                            
                            
                            
                            <tr id="pay-now-break">
                                <td>
                                    <asp:RadioButton runat="server"  AutoPostBack="true" OnCheckedChanged="pnow_CheckedChanged" Text="Pagar ahora Online" ID="pnow" name="pnow" value="pnow" ClientIDMode="Static" GroupName="pay-when" CssClass="" Checked="True"></asp:RadioButton>

                                    <figure>
                                        <a type="button" data-toggle="modal" data-target="#myModal2">
                                            <img src="images/icon-i.png" alt=""></a>
                                    </figure>
                                    <figure>
                                        <img src="images/logo-check.png" alt="">
                                    </figure>

                                </td>
                                
                                <td>
                                    <p>
                                        por d&iacute;a: <span id="pay-nw-price"><%Extentions.GetCurrency()%> <asp:Label ID="lblpynow" Text="" runat="server"></asp:Label>
                                        <br>
                                        <em><%Extentions.GetCurrency %> <asp:Label ID="lblsalesfull" Text="" runat="server"></asp:Label> </em></span>
                                    </p>
                               </td>

                            </tr>
                            
                            <tr class="pay-now-close">
                                <td>
                                    <figure>
                                        <img src="images/check.png" alt="">
                                    </figure>
                                    <span>Unlimited Millage
                                    </span>
                                </td>
                            </tr>
                            <tr class="pay-now-close">
                                <td>
                                    <figure>
                                        <img src="images/check.png" alt="">
                                    </figure>
                                    <span>Free amendments
                                    </span>
                                </td>
                            </tr>
                            <tr class="pay-now-close">
                                <td>
                                    <figure>
                                        <img src="images/check.png" alt="">
                                    </figure>
                                    <span>Free Cancellation
                                    </span>
                                </td>
                            </tr>
                            <tr class="pay-now-close">
                                <td>
                                    <figure>
                                        <img src="images/check.png" alt="">
                                    </figure>
                                    <span>Collision dammage Waiver (CDW)
                                    </span>
                                </td>
                            </tr>
                            <tr class="pay-now-close">
                                <td>
                                    <figure>
                                        <img src="images/check.png" alt="">
                                    </figure>
                                    <span>All taxes included										</span>
                                </td>
                            </tr>
                            <tr class="pay-now-close">
                                <td>
                                    <figure>
                                        <img src="images/check.png" alt="">
                                    </figure>
                                    <span>Invoice by email
                                    </span>
                                </td>
                            </tr>
                            <tr class="pay-now-close">
                                <td>
                                    <figure>
                                        <img src="images/check.png" alt="">
                                    </figure>
                                    <span>Road assistance 24/7
                                    </span>
                                </td>
                            </tr>
                            
                            <%--<tr id="divEmailonPaynow" style="display: none">

                                <td>
                                    <span>Email</span>


                                    <asp:TextBox ID="txtEmail" runat="server" ClientIDMode="Static" Width="250"></asp:TextBox>

                                </td>

                            </tr>--%>
                        </tbody>
                    </table>
                   
                    <table class="detail-price">
                        <thead>
                            <tr>
                                <th>2.Tipos de Cobertura</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>&nbsp;</td>
                                <td><p>Cobertura <span class="color">FULL</span></p></td>
                                <td><p>Cobertura B&aacute;sica GRATIS</p></td>
                            </tr>
                            <tr>
								<td>&iquest;Que cubre?</td>
								<td id="CDW-Full">
									
								</td>
								<td id="CDW-Basic">
                                	<asp:CheckBox runat="server" ID="cdwbasic"></asp:CheckBox>
								</td>
							</tr>
                            <tr>
                                <td>Da&ntilde;os a terceros</td>
                                <td>
                                    <figure>
                                        <img src="images/check.png" alt="">
                                    </figure>
                                </td>
                                <td>
                                    <figure>
                                        <img src="images/check.png" alt="">
                                    </figure>
                                </td>
                            </tr>
                            <tr>
                                <td> Asistencia 24/7 </td>
                                <td>
                                    <figure>
                                        <img src="images/check.png" alt="">
                                    </figure>
                                </td>
                                <td>
                                    <figure>
                                        <img src="images/check.png" alt="">
                                    </figure>
                                </td>
                            </tr>
                            <tr>
									<td>
										Gastos de remolque, taxi</td>
									<td>
										<figure>
											<img src="images/check.png" alt="">
											
										</figure>
									</td>
									<td>
										<figure>
											<img src="images/close.png" alt="">
										</figure>
									</td>
								</tr>
								<tr>
									<td>
										Ventanas, espejo, ruedas y neum&aacute;ticos
									</td>
                                <td>
                                    <figure>
                                        <img src="images/check.png" alt="">
                                    </figure>
                                </td>
                                <td>
                                    <figure>
                                        <img src="images/close.png" alt="">
                                    </figure>
                                </td>
                            </tr>
                            <tr>
									<td>
										P�rdida de beneficios durante la reparaci�n del coche
                                </td>
                                <td>
                                    <figure>
                                        <img src="images/close.png" alt="">
                                    </figure>
                                </td>
                                <td>
                                    <figure>
                                        <img src="images/close.png" alt="">
                                    </figure>
                                </td>
                            </tr>
                            <tr>
                                <td>Da&ntilde;os por robo o vuelvo </td>
                                <td>
                                    <figure>
                                        <img src="images/close.png" alt="">
                                    </figure>
                                </td>
                                <td>
                                    <figure>
                                        <img src="images/close.png" alt="">
                                    </figure>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                Deducible por da�os parciales al vehiculo
                                </td>
                                
                                <td id="ttlCost"> </td>
                                
                                <td>
                                    <%--<span>ARS $20.00</span>--%>
                                    <asp:Label ID="lblCobertura_Basic" runat="server" Text="Label"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <th>
                                    <div class="detail-price-foot">
                                        <span>SU COBERTURA EXPLICADO</span>
                                        <figure>
                                            <a type="button" data-toggle="modal" data-target="#myModal3">
                                                <img src="images/icon-i.png" alt=""></a>
                                        </figure>
                                        <div class="modal fade" id="myModal3" role="dialog" style="display: none;">
                                            <div class="modal-dialog">
                                                <!-- Modal content-->
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal">�</button>
                                                        <figure>
                                                            <img src="images/icon-i-2.png" alt="">
                                                        </figure>
                                                        <h4 class="modal-title">YOUR CDW EXPLAINED</h4>
                                                    </div>
                                                    <div class="modal-body">
                                                        <p>
                                                            The deductible shows you the amount of money you have to pay for in case of partial damage to the vehicle. It is important that the deductible is as low as possible so that in case of an accident the insurance will cover most or all the damage.
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </th>
                            </tr>
                        </tbody>
                    </table>


                    <h2><asp:Label ID="lblextrasText" Visible="false" Text="Choose Extras" runat="server"></asp:Label></h2>

                    <asp:Repeater ID="rpExtras" runat="server">

                        <HeaderTemplate>
                            <table class="detail-fitting">
                                <thead>
                                  <tr>
                                        <th>3.Extras y accesorios recomendados (dependiendo de la disponibilidad)</th></tr></thead></HeaderTemplate>

                        <ItemTemplate>
                           
                            <asp:Panel ID="PanelRepeater" runat="server">
                            <tr class="<%# Eval("ExtraCode") %>" style="display:<%# Eval("ShowonRepeater")%>">

                                <td>
                                    <asp:CheckBox Font-Size="X-Small" ID="chkExtra" ToolTip='<%# Eval("ExtraCode") %>' runat="server" AutoPostBack="True" ClientIDMode="Static" Checked ='<%# Eval("DefaultChecked") %>' OnCheckedChanged="chkExtra_OnCheckedChanged" meta:resourcekey="chkExtraResource1" />
                                  

		<label>
            <asp:Label ID="Label1" Text='<%# Eval("ExtraDesc") %>' runat="server" meta:resourcekey="Label1Resource2"></asp:Label>
        </label>

                                    <asp:DropDownList OnSelectedIndexChanged="DropDownList1_SelectedIndexChanged" AutoPostBack="true" ClientIDMode="Static" runat="server" Width="55px" ID="txtQuantity">

                                        <asp:ListItem Text="1" Value="1" />
                                        <asp:ListItem Text="2" Value="2" />
                                        <asp:ListItem Text="3" Value="3" />

                                    </asp:DropDownList>
                                </td>

                                <td id="<%# Eval("ExtraCode") %>-cost"><asp:Label ID="Label2" Text='<%# Eval("ExtraDesc2") %>' runat="server" meta:resourcekey="Label2Resource1"></asp:Label>
                                   
                                </td>
							</asp:panel>
                        </ItemTemplate>

                        <FooterTemplate>
                            </tbody>
      </table>
                        </FooterTemplate>

                    </asp:Repeater>

                </div>

                <div class="row">
                    <div class="col-md-12 col-sm-6">
                        <div class="total">
                            <h4>Total a Pagar</h4>
                            <span><%Extentions.GetCurrency()%> <asp:Label ID="labelTotal" Text="" runat="server"></asp:Label>
                                <br>
                                <em>Tax included</em>
                            </span>
                        </div>
                    </div>
                    <div class="col-md-12 col-sm-6">
                        <div class="continue">
                            <asp:Label ID="LabelsubBtn" Text="" runat="server"> </asp:Label>
                            <div class="continue-box hidden-sm hidden-xs">
                                <figure>
                                    <img src="images/icon-ctn.png" alt="">
                                </figure>
                                <span>Cancelaci&oacute;n Gratuita</span>
                            </div>
                        </div>
                    </div>
                    <!--          <asp:CheckBox ID="chkInsurance" ClientIDMode="Static"  runat="server"/>Insurance
                 END EXTRAS TABLE -->


                    <div class="col-sm-6">
                        <asp:Literal ID="Label1" runat="server" meta:resourcekey="Label1Resource1" Visible="False"></asp:Literal>

                        <asp:Label Visible="False" ID="leftContent" runat="server" meta:resourcekey="leftContentResource1"></asp:Label>

                        <asp:Label ID="lblcontent" Font-Size="X-Small" runat="server" meta:resourcekey="lblcontentResource1"></asp:Label>

                        <asp:Label ID="lblbook" runat="server" meta:resourcekey="lblbookResource1"></asp:Label>
                    </div>
                </div>
            </div>
        </div>
    
    <script>
		$(function(){
		
        $(".CDW #chkExtra").detach().appendTo("#CDW-Full");
		var CDWCost = $("#CDW-cost").html()
		$("#ttlCost").append(CDWCost)
		
		$(".CDW").detach();
		
		$(".AEP.FEE").detach();
		$(".DROP").detach();
		

		if($("#ContentPlaceHolder1_plater").is( ":checked" )) {
			$(".pay-later-close").each(function(index, element) {
            	$(this).slideDown();
        	});
		} else {
			$(".pay-later-close").each(function(index, element) {
            	$(this).slideUp();
				$("#pay-ltr-price").addClass("grey-price-tag")
				$("#ContentPlaceHolder1_labelTotal2").addClass("grey-price-tag")
        	});
		}
		
		if($("#pnow").is( ":checked" )) {
			$(".pay-now-close").each(function(index, element) {
            	$(this).slideDown();
				
        	});
		} else {
			$(".pay-now-close").each(function(index, element) {
            	$(this).slideUp();
				$("#pay-nw-price").addClass("grey-price-tag")
				$("#ContentPlaceHolder1_lblpynow").addClass("grey-price-tag")
				$("em").hide();
        	});
		}
		
		
		$(".rez-step1").addClass("active");

   
		});
		
		$("#ContentPlaceHolder1_cdwbasic").on("click", function() {
			$("#CDW-Full input").trigger("click");
		});
	</script>
</asp:Content>
