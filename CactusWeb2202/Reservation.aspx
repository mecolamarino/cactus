<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Reservation.aspx.vb" Trace="false" Inherits="Reservation" MasterPageFile="MastarList.master" %>

<%@ Register Src="Controls/address.ascx" TagName="address" TagPrefix="ad" %>

<asp:Content ContentPlaceHolderID="head" runat="server" ID="header">
    <title>Reservation</title>

<style>
  .error {
	  color: red;
  }
  

  .plcy {
	  height:200px;
	  overflow-x:auto;
	  width:100%;
	  border:1px solid #ccc;
	  margin-top:15px; 
	  margin-bottom:15px;
	  padding: 5px; 
  }
  
  .please-wait {
		position:fixed;
		top:0px;
		left:0px;
		width:100vw;
		height:100vh;
		background:rgba(192,192,192,0.8);
		z-index:99999999999;
   }
   
   .loading-msg {
	   font-size:3em;
	   line-height:1em;
	   position:absolute;
	   width:50%;
	   height:50%;
	   margin:auto;
	   top:0px;
	   bottom:0px;
	   right:0px;
	   left:0px;
	   text-align:center;
   }
    .chbx
    { margin-right :500px;
      margin-top :10px;
        float: right ;
        position: relative;
    }
</style>
    <!--script>
        function ValidatePage() {
            try
			{
			 Page_ClientValidate();
            if (typeof (Page_ClientValidate) == 'function') {
                Page_ClientValidate();
				console.log("Inside");
            }

            if (Page_IsValid) {             
                $('#btnReserve').hide();
                $('#bWait').show();
                return true;
            }
            else {               
             return false;

            }
			}
			catch(ex)
			{
			console.log(ex);
			 return true;
			
			}
        }
		</script-->
		<script>
$(function(){
	$(".dateOfBirth").prop('required',true);
})

</script>
</asp:Content>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="ContentPlaceHolder1">

    <section id="content-changer" class="info-user padding-section">

        <asp:Panel ID="pnlLeft" runat="server" Visible="true">

            <!--div class='please-wait'>
      <div class='loading-msg'>
      <p>Processing Request... Please Wait<br>
      <i class='fa fa-spinner fa-pulse'></i></p>
      </div>
    </div-->

            <div class="container">
                <div class="row">
                    <div class="col-md-4">
                        <div class="side-bar">
                            <div class="about-car">
                                <div class="title">
                                    <h2>Sed&aacute;n 4 puertas</h2>
                                    <span>Chevrolet Classic</span>

                                  <asp:Label ID="carModelName" Visible="false" runat="server"></asp:Label><asp:Label ID="lblcls" runat="server" Visible="false"></asp:Label>

                              </div>
                                <figure>
								<img src="images/about-car.png" alt="">
								</figure>
                                <ul class="hidden-sm hidden-xs">

                                    <li>
                                        <figure>
                                          
                                        </figure>
                                       <h3>Rendimiento:</h3>
								</li>
                                <li>
                           
									<figure>
										<img src="images/about-1.png" alt="">
									</figure>
									<span>6,5lts / 100km</span>
								</li>
							</ul>
							
							<ul class="hidden-sm hidden-xs">
								<li>
                           
									<figure>
										
									</figure>
									<h3>Equipamiento:</h3>
								</li>
								<li>
									<figure>
										<img src="images/about-3.png" alt="">
									</figure>
									<span>Aire Acondicionado</span>
								</li>
								<li>
									<figure>
										<img src="images/about-4.png" alt="">
									</figure>
									<span>Caja de Cambios Manual</span>
								</li>
							</ul>
							<ul class="hidden-sm hidden-xs full">
								<li>
                           
									<figure>
										
									</figure>
									<h3>Capacidad</h3>
								</li>
								<li>
									<figure>
										<img src="images/about-6.png" alt="">
									</figure>
									<span>5 Pasajeros </span>
								</li>
								<li>
									<figure>
										<img src="images/about-7.png" alt="">
									</figure>
									<span>2 Valijas Grandes <br>y 2 chicas</span>
								</li>
							</ul>
                            </div>
                            
                            <div id="Remove-side-bar">
                            
                            <div class="rental-information">

                                <table>
                                    <thead>
                                        <tr>
                                            <th>Info del Alquiler</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>Punto de Entrega:
                                              <h3>
                                              <asp:Label ID="lblploc" runat="server"></asp:Label></h3>
                                                <p>
                                                    <asp:Label ID="lblpickupdateText" runat="server" meta:resourcekey="lblpickupdateTextResource1"></asp:Label>
                                                    <asp:Label ID="lblptime" runat="server" meta:resourcekey="lblptimeResource1"></asp:Label>
                                                </p>
                                          </td>
                                        </tr>
                                        <tr>
                                            <td>Punto de Devoluci&oacute;n
                                              <h3>
                                              <asp:Label ID="lbldloc" runat="server"></asp:Label></h3>
                                                <p>
                                                    <asp:Label ID="lbldropffDatetext" runat="server" meta:resourcekey="lbldropffDatetextResource1"></asp:Label>
                                                    <asp:Label ID="lbldtime" runat="server" meta:resourcekey="lbldtimeResource1"></asp:Label>
                                                </p>
                                          </td>
                                        </tr>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th>D&iacute;as de alquiler:
                                              <asp:Label ID="LabelRPeriod" runat="server" meta:resourcekey="LabelRPeriod"></asp:Label>
                                          D&iacute;as</th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>

                            <table class="detail-user hidden-sm hidden-xs">
                                <thead>
                                      <th colspan="2">Detalles <span style="display:inline-block; float:right;">TOTAL PARCIAL</span></th>
                                </thead>
                                <tbody>


                                    <asp:Label ID="Label1" runat="server" meta:resourcekey="Label1Resource1"></asp:Label>
                                    
                                    <asp:Label ID="Label2" runat="server" meta:resourcekey="Label1Resource1"></asp:Label>




                                </tbody>
                                <tfoot>
                                    <tr>
                                       <td colspan="2">Pago Total 
                                        <div style="display:inline-block; float:right; text-align:right;"><asp:Label ID="lblcost" CssClass="important-alert" runat="server"></asp:Label><br>
                                        Impuesto incluido
                                        </div>
                                    </td>
                                    </tr>
                                </tfoot>
                            </table>
                            <asp:LinkButton CausesValidation="false" ID="lnkChange" Text="&laquo; Start Over" CssClass="grey-btn" runat="server" Visible="false"></asp:LinkButton>
                            <div class="info">
                              <div class="info-cancel">
                                <figure>
                                        <img src="/images/icon-1.png" alt="">
                                  </figure>
                                  <h3>Cancelaci&oacute;n Gratuita</h3>
                                  <span>Modifica
                        o cancela tu reserva sin costo antes MES A&Ntilde;O Y D&Iacute;A</span>
                                </div>
                                <div class="info-attention">
                                  <figure>
                                        <img src="/images/icon-2.png" alt="">
                                    </figure>
                                    <h3>Edad m&iacute;nima de conductor 21 a&ntilde;os.</h3>
                                    <span>Si tiene entre 18 a 21 a&ntilde;os debera pagar un adicional</span>
<div class="info-attention-contact">
                        <ul>
                                            <li><a href="javascript:if(window.print)window.print()"><i class="fa fa-print"></i> <span>Imprimir esta reserva</a></span></li>
                                            <li><i class="fa fa-envelope"></i> <span>Recomendar esta oferta</span></li>
                      </ul>
                                  </div>
                              </div>
                                </div>
                            </div>
                        </div>


                    </div>

                    <asp:Panel runat="server" ID="pnlReserve">
                        <asp:Label ID="lblMessage" runat="server" Style="color: red; font-weight: bold; font-size: xx-large"></asp:Label>


                        <asp:Label ID="lblconftext" Text="Confirmation Number:" Visible="false" runat="server"></asp:Label>
                        <asp:Label ID="lblmconf" runat="server"></asp:Label>



                        <div class="col-md-8">
                            <div class="your-data">
                                <table class="data-user">
                                    <thead>
                                        <tr>
                                            <th>4. Informaci&oacute;n del conductor</th>
                                        </tr>
                                    </thead>
                                    <tbody>




                                        <tr>
                                            <td><span>Nombre*:</span></td>
                                            <td>
                                                <asp:TextBox runat="server" placeholder="Nombre" CssClass="txtinputfld " ID="tbFirstName" required="required"></asp:TextBox>

                                            </td>
                                        </tr>

                                        <tr>
                                            <td><span>Apellido*:</span></td>
                                            <td>
                                                <asp:TextBox runat="server" placeholder="Apellido" CssClass="txtinputfld" ID="tbLastName" required="required"></asp:TextBox>

                                            </td>
                                        </tr>
                                        <tr>
                                            <td><br />
                                            Fecha de cumplea&ntilde;os</td>
                                            <td>
                                                <div class="data-item">
                                                    <label>Mes</label>

                                                    <asp:DropDownList ID="selectMonth" runat="server">
                                                    </asp:DropDownList>
                                                </div>
                                                <div class="data-item">
                                                    <label>Fecha</label>
                                                    <asp:DropDownList ID="selectDay" runat="server">
                                                    </asp:DropDownList>
                                                </div>
                                                <div class="data-item">
                                                    <label>A&ntilde;o</label>

                                                    <asp:DropDownList ID="selectYear" runat="server">
                                                    </asp:DropDownList>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><span>Email</span></td>
                                            <td>
                                                <asp:TextBox runat="server" placeholder="Email" CssClass="txtinputfld" ID="tbEmail" required></asp:TextBox></td>
                                            <td>
                                            </td>
                                        </tr>
                                        
                                        <tr>
                                            <td><span>Confirmar Email</span></td>
                                            <td>
                                                <asp:TextBox runat="server" CssClass="txtinputfld" ID="txtConfemail" placeholder="Confirmar Email" required="required"></asp:TextBox></td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td>
                                                <asp:CompareValidator ID="comparePasswords" CssClass="error" runat="server" ControlToCompare="tbEmail" ControlToValidate="txtConfemail" ErrorMessage="The email adddresses do not match up" Display="Dynamic" /></td>
                                        </tr>
                                        <tr>
                                            <td><span>Tel&eacute;fono</span></td>
                                            <td>

                                                <asp:TextBox runat="server" CssClass="txtinputfld" type="tel" ID="tbCell" placeholder="+54"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <asp:Panel ID="pnlInfo" runat="server" Visible="false">
                                        <tr>
                                            <td><span>Direcci&oacute;n:</span></td>
                                            <td>
                                                <asp:TextBox runat="server" ID="tbSA" placeholder="Direcci&oacute;n"></asp:TextBox>
                                            </td>
                                        </tr>
                                        
                                        <tr>
                                            <td><span>
                                                Ciudad / Pueblo:</span></td>
                                            <td>
                                                <asp:TextBox runat="server" ID="tbCity" placeholder="Ciudad"></asp:TextBox>
                                            </td>
                                        </tr>
                                        
                                        <tr >
                                            <td><span>
                                            Estado:</span></td>
                                            <td>
                                                <asp:TextBox runat="server" ID="tbState" placeholder="Estado"></asp:TextBox>
                                            </td>
                                        </tr>
                                        
                                <tr>
									<td><span> C&oacute;digo postal:</span></td>
									<td>
                                <asp:TextBox runat="server" ID="tbZip" placeholder=" C&oacute;digo postal"></asp:TextBox>
                            	</td>
								</tr>
                             </asp:Panel>
                                <tr> </tr>
                               

							</tbody>
						</table>
    
                                <div id="cardDetail" runat="server" visible="true">
                                <table class="card-user">
							<thead>
								<tr>
									<th><div id="PTitle"> </div></th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>
										<span>
Por favor, seleccione su tarjeta:</span>
									</td>
									<td><asp:DropDownList ID="drpcard" runat="server" style="margin-top:7px;">
                                    
                                            <asp:ListItem Value="#" Text="seleccione su tarjeta"></asp:ListItem>
                                            <asp:ListItem Value="VI" Text="VISA"></asp:ListItem>
                                            <asp:ListItem Value="MC" Text="Master Card"></asp:ListItem>
                                            <asp:ListItem Value="AX" Text="American Express"></asp:ListItem>
                                            <asp:ListItem Value="DS" Text="Discover"></asp:ListItem>
                                        </asp:DropDownList>
							
									</td>
								</tr>
								<tr>
									<td>
										<span>N&uacute;mero de tarjeta de cr&eacute;dito:</span>
									</td>
									<td>
										<div class="secur">
											<asp:TextBox runat="server" ID="txtCreditCard" Width="200" required="required"></asp:TextBox>
										</div>
										<div class="input-box hidden-pay">
											<span class="hidden-sm hidden-xs hidden-md">
											No hay cargos ser&aacute;n procesadas a esta tarjeta ! S&oacute;lo necesitamos esta informaci&oacute;n para garantizar su reserva
											</span>
										</div>
									</td>
								</tr>
								<tr>
									<td>
										<span>
V&aacute;lido hasta el:</span>
									</td>
									<td style="padding-top:9px;">
										<div class="data-item">
											<label>Mes</label>
											<asp:DropDownList ID="txtExpMonth" runat="server">
                        <asp:ListItem Value="01" Text="01"></asp:ListItem>
                        <asp:ListItem Value="02" Text="02"></asp:ListItem>
                        <asp:ListItem Value="03" Text="03"></asp:ListItem>
                        <asp:ListItem Value="04" Text="04"></asp:ListItem>
                        <asp:ListItem Value="05" Text="05"></asp:ListItem>
                        <asp:ListItem Value="06" Text="06"></asp:ListItem>
                        <asp:ListItem Value="07" Text="07"></asp:ListItem>
                        <asp:ListItem Value="08" Text="08"></asp:ListItem>
                        <asp:ListItem Value="09" Text="09"></asp:ListItem>
                        <asp:ListItem Value="10" Text="10"></asp:ListItem>
                        <asp:ListItem Value="11" Text="11"></asp:ListItem>
                        <asp:ListItem Value="12" Text="12"></asp:ListItem>
                        </asp:DropDownList>
										</div>
										<div class="data-item">
											<label>A&Ntilde;o</label>
									<asp:DropDownList ID="txtExpYear" runat="server">
			<asp:ListItem Value="15" Text="15"></asp:ListItem>
            <asp:ListItem Value="16" Text="16"></asp:ListItem>
            <asp:ListItem Value="17" Text="17"></asp:ListItem>
            <asp:ListItem Value="18" Text="18"></asp:ListItem>
            <asp:ListItem Value="19" Text="19"></asp:ListItem>
            <asp:ListItem Value="20" Text="20"></asp:ListItem>
            <asp:ListItem Value="21" Text="21"></asp:ListItem>
            <asp:ListItem Value="22" Text="22"></asp:ListItem>
            <asp:ListItem Value="23" Text="23"></asp:ListItem>
            <asp:ListItem Value="24" Text="24"></asp:ListItem>
            <asp:ListItem Value="25" Text="25"></asp:ListItem>
            <asp:ListItem Value="26" Text="26"></asp:ListItem>
            <asp:ListItem Value="27" Text="27"></asp:ListItem>
            <asp:ListItem Value="28" Text="28"></asp:ListItem>
            <asp:ListItem Value="29" Text="29"></asp:ListItem>
            <asp:ListItem Value="30" Text="30"></asp:ListItem>     
                    </asp:DropDownList>
										</div>
									</td>
								</tr>
								<tr>
									<td><span>CVV:</span></td>
									<td>
										<asp:TextBox runat="server" ID="txtcode"></asp:TextBox>
                    
               						
										<figure>
											<a data-toggle="modal" data-target="#myModal2"><img src="/images/icon-i.png" alt=""></a>
										</figure>
										<div class="modal fade" id="myModal2" role="dialog">
										    <div class="modal-dialog">
										      <!-- Modal content-->
										      	<div class="modal-content">
										        	<div class="modal-header">
										          		<button type="button" class="close" data-dismiss="modal">&tiempo;</button>
										          		<figure>
										          			<img src="/images/icon-i-2.png" alt="">
										          		</figure>
										          		<h4 class="modal-title">Lorem Ipsum is</h4>
										        	</div>
											        <div class="modal-body">
											          	<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also </p>
											        </div>
										      	</div>
										    </div>
										</div>
									</td>
								</tr>
								<tr class="hidden-pay">
									<td>
										<figure>
											<img src="/images/icon-3.png" alt="">
										</figure>
										<span>
										No hay cargos ser&aacute;n procesadas a esta tarjeta ! S&oacute;lo necesitamos esta informaci&oacute;n para garantizar su reserva
									</span>
									</td>
								</tr>
							</tbody>
						</table>
                                </div>
						<table class="info-users">
							<thead>
								<tr>
									<th>6. Informaci&oacute;n adicional</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>
										<span>
&iquest;Conoce su vuelo de llegada ?:</span>
                                        <asp:TextBox ID="arrivalflight" runat="server"></asp:TextBox>
									</td>
								</tr>
								<tr>
									<td>
										<span>&iquest;Necesita un hotel ?:</span>
                                        <asp:RadioButton ID="radioHotelYes" runat="server" GroupName ="Hotel" Text="S&iacute;"/>
                                      
                                          <asp:RadioButton ID="radioHotelNo" runat="server" GroupName ="Hotel" Text="No"/>
									</td>
								</tr>
								<tr>
									<td>
										<span>
Comentarios:</span>
                                        <asp:TextBox ID="Comments" runat="server" TextMode ="MultiLine"></asp:TextBox>
									</td>
								</tr>
							</tbody>
						</table>
						<table class="condition-user">
							<thead>
								<th>7.
T&eacute;rminos y Condiciones</th>
							</thead>
							<tbody>
								<tr>
									<td>
										<asp:CheckBox ID="chkpolicy" runat="server" required="required" />
                            
                                
										<p>
He le&iacute;do y acepto la informaci&oacute;n de alquiler y los t&eacute;rminos y condiciones .</p>
                                        
                                        
                                        <asp:Label ID="lblchk" ForeColor="red" runat="server" Font-Names="Arial" Text=""></asp:Label>
                                        
                                        <asp:Literal ID="ltrlPolicy" runat="server" Text=""></asp:Literal>
									</td>
								</tr>
							</tbody>
						</table>
    
                        
						<div class="info">
							<div class="info-cancel">
								<figure>
									<img src="/images/icon.png" alt="">
								</figure>
								<h3>
									
Enmiendas gratis
								</h3>
								<span>
									
Si cambia de opini&oacute;n , puede modificar o cancelar su reserva de forma gratuita antes :<br>
 									<asp:Label Text="" ID="lblfreecncl" runat="server"></asp:Label>
								</span>
							</div>
						</div>
						<div class="continue">
                        
                        
                        <asp:Button runat="server" CausesValidation="true" OnClientClick="return ValidatePage()" CssClass="bnt-continue" ID="btnReserve" ClientIDMode="Static" Text="" ></asp:Button>

							

                                </div>
                                <!-- <div class="total">
							<h4>Total:</h4>
							<span>
								ARS $1435,34 <br>
								<em>iva incluido</em>
							</span>
						</div> -->
					</div>
					<div class="col-md-12">
						<div class="infor hidden-md hidden-lg">
							<div class="row">
								<div class="col-sm-6">
								  <div class="info-cancel">
									<figure>
										<img src="/images/icon-1.png" alt="">
									</figure>
									<h3>
									
Cancelaci&oacute;n gratis
								</h3>
								<span>
									Modifica
                        o cancela tu reserva sin costo antes MES A&Ntilde;O Y D&Iacute;A<
								</span>
								</div>
								</div>
								<div class="col-sm-6">
									<div class="info-i">
									  <div class="info-item">
											<figure>
												<img src="/images/icon-5.png" alt="">
											</figure>
											<span>					
									Edad mínima de conductor 21 a&ntilde;os.
								
								<b>
									Si tiene entre 18 a 21 a&ntilde;os debera pagar un adicional
								</b>
												
											</span>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="info-attention hidden-md hidden-lg">
							<div class="info-attention-contact">
								<ul>
										<li>
											<a href="#" class="fa fa-print"></a>
											<span>Imprimir esta reserva</span>
										</li>
										<li>
											<a href="#" class="fa fa-file-pdf-o"></a>
											<span>Comparte esta oferta</span>
										</li>
									</ul>
							</div>
						</div>
					</div>
				</div>
		

<br style="clear:both;">

	

    




    <!-- Right Bar-->
    <div class="home-righ1t">

        <div class="page">
            

                
                
                <div class="col-sm-12" style="background:#fff; padding:20px 0px;">
                

                
                
                <div class="col-sm-12">
                	<div class="col-sm-12">
                    <asp:TextBox runat="server" ID="txtconf" Width="195px" Visible="false"></asp:TextBox><br />
                            <asp:Label ID="conNum" runat="server"></asp:Label>
                    </div>
             	</div>
                
               
     

                </div>
        </div>
        
			<script>
			function getParameterByName(name, url) {
    		if (!url) url = window.location.href;
    		name = name.replace(/[\[\]]/g, "\\$&");
    		var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        	results = regex.exec(url);
    		if (!results) return null;
    		if (!results[2]) return '';
    		return decodeURIComponent(results[2].replace(/\+/g, " "));
			}
			
			var paymentType = getParameterByName('paymentType');

			$(function(){
			$(".rez-step2").addClass("active");	
			
			if (paymentType == "pnow") {
			$(".hidden-pay").hide();
			$("#PTitle").html("5. Pago")
			} else {
			$(".hidden-pay").show();
			$("#PTitle").html("5. Garant&iacute;a de reserva")
			}
			
			});
			</script>
            
            </asp:Panel>

            <asp:Panel ID="pnlsummary" runat="server" Visible="false">
            
              <div style="margin-bottom: 10px;" style="display:none;">
                <p><asp:Label ID="lblclass" Font-Bold="true" runat="server"></asp:Label></p>
                
                <asp:Label ID="lblimage" Width="400" Visible="false" runat="server"></asp:Label>
                    
                <asp:Label ID="lblreservationdetail" runat="server"></asp:Label>
                </div>
                
                <div class="col-md-8">
                <div class="row">
                <div class="col-md-12">
							<div class="code">
								<ul>
									<li>Reservation</li>
									<li>
										<asp:Label CssClass="color" ID="lblconf" runat="server"></asp:Label>
                                        	<asp:Label CssClass="color" ID="lblTransactionId" runat="server"></asp:Label>
										<span>Pago Total <b class="color"> 	<asp:Label CssClass="color" ID="lblPageTotalAmount" runat="server"></asp:Label></b></span>

									</li>  
                                  
                                    
									<li>
										<div class="desc">
											
Verifique su direcci&oacute;n de correo electr&oacute;nico , le hemos enviado la confirmaci&oacute;n de la reserva. Si usted no recibi&oacute; nuestro correo electr&oacute;nico, <a href="#" class="color" onClick="window.print();">
haga clic aqu&iacute;</a> 
para descargarlo.
										</div>
									</li>
								</ul>
							</div>
						</div>
                        
                 <div class="col-md-6" id="moved-rental-info">

				 </div>
                 
                 <div class="col-md-6" id="moved-extra-details">
                 
                 </div>
                 
                 <div class="col-md-12">
							<div class="info-important">
								<table>
									<thead>
										<tr>
											<th>INFORMACI&Oacute;N importante para recoger el coche</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td>
												<h4>
Cuando recoja el coche, por favor aseg&uacute;rese de que las liebres los documentos pollowing :</h4>
												<ul>
													<li>
														<span>Tarjeta de cr&eacute;dito:</span>
														<figure>
															<img src="images/visa-icon.png" alt="">
														</figure>
													</li>
													<li>
														<span>
Licencia de Conducir v&aacute;lida hasta el alquiler completo</span>
														<figure>
															<img src="images/ser-img.png" alt="">
														</figure>
													</li>
													<li>
														<span>
Identificaci&oacute;n Personal o Pasaporte</span>
														<figure>
															<img src="images/ser-img2.png" alt="">
														</figure>
													</li>
												</ul>
												<p><span>Informaci&oacute;n importante:</span> 
Tarjeta de cr&eacute;dito : Para recoger el coche que tendr&aacute; que proporcionar un visado v&aacute;lido o tarjeta de cr&eacute;dito MASTERCARD . Vamos a congelar AR $ 4.500 para la duraci&oacute;n de la licencia rental.Driver : Usted tendr&aacute; que presentar una licencia de conducir v&aacute;lida de su pa&iacute;s de residencia. No se requiere licencia de conducir internacional .</p>
											</td>
										</tr>
									</tbody>
								</table>
							</div>
					</div>
                    
                    <div class="col-md-12">
							<div class="info">
								<div class="info-cancel col-xs-12">
									<h3>
										
Enmiendas gratis
									</h3>
									<span>
										
Si cambia de opini&oacute;n , puede modificar o cancelar su reserva de forma gratuita antes : <br>
                                         <asp:Label Text="" ID="lblfreecncl1" runat="server"></asp:Label>
									</span>
								</div>
							</div>
							<div class="infor hidden-md hidden-lg">
								<div class="row">
									<div class="col-sm-6">
											<div class="info-cancel">
										<figure>
											<img src="images/icon-1.png" alt="">
										</figure>
										<h3>
											Enmiendas gratis
										</h3>
										<span>
											
Modificar o cancelar su reserva sin coste antes MES D&Iacute;A A&Ntilde;O
										</span>
									</div>
									</div>
									<div class="col-sm-6">
										<div class="info-i">
											<div class="info-item">
												<figure>
													<img src="images/icon-5.png" alt="">
												</figure>
												<span>
													
La edad mínima del conductor 21 a&ntilde;os .
													<b>Si el conductor adicional es entre 18 y 21 a&ntilde;os deben pagar un adicional .</b>
												</span>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="info-attention">
								<div class="info-attention-contact">
									<ul>
										<li>
											<a href="#" class="fa fa-print"></a>
											<span>Imprimir esta reserva</span>
										</li>
										<li>
											<a href="#" class="fa fa-file-pdf-o"></a>
											<span>Comparte esta oferta</span>
										</li>
									</ul>
								</div>
							</div>
						</div>
                      </div>
                    </div>
                   
				  <script>
                  $(function(){
                  $(".rez-step3").addClass("active");	
				  
				  $("#moved-rental-info").html($(".rental-information"));
				  $("#moved-extra-details").html($(".detail-user"));
				  
				  $("#content-changer").removeClass("info-user")
				  $("#content-changer").addClass("ready")
				  
				  $("#Remove-side-bar").hide();
                  });
                  </script>

                    </asp:Panel>
                    
                    <asp:Panel ID="pnlPaymentSection" runat="server" Visible="false">
                        <div class="col-sm-8" style="background: #fff; padding-top: 20px; padding-bottom: 20px;">
                            <span>
                                <h2>Su reservaion no est&aacute; completa hasta que haya enviado el pago a trav&eacute;s de nuestro sistema de pago en l&iacute;nea,</h2>
                            </span>
                            <h3>


                                <asp:HyperLink runat="server" ID="lnkpayment" class="bnt-continue" >Haga clic aqu&iacute; para continuar Pay U</asp:HyperLink>
                            </h3>
                        </div>
                        
                     <script>
					  $(function(){
					  $(".rez-step3").addClass("active");	
					  });
					  </script>
                    </asp:Panel>
                </div>

    </div>
    </div>
   </asp:Panel>

	</section>
</asp:Content>