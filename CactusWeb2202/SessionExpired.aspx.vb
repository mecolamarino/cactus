﻿Imports System.IO
Imports System.Net
Imports System.Threading
Imports System.Globalization
Partial Class SessionExpired
    Inherits System.Web.UI.Page

    Protected Overrides Sub InitializeCulture()
        Culture = Common.MyCulture

        'set culture to current thread
        Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(Common.MyCulture)
        Thread.CurrentThread.CurrentUICulture = New CultureInfo(Common.MyCulture)

        'call base class
        MyBase.InitializeCulture()

    End Sub
End Class
