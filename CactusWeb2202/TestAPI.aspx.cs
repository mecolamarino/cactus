﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class TestAPI : System.Web.UI.Page
{
    public static string cactusMerchantId = System.Configuration.ConfigurationManager.AppSettings["cactusMerchantId"];
    public static string cactusAPIKey = System.Configuration.ConfigurationManager.AppSettings["cactusAPIKey"];

    string req;
    string signature;
    protected void Page_Load(object sender, EventArgs e)
    {
         
        string refrencecode="payment_test_"+new Random().Next(99,999);


       string  hashstring = String.Format("{0}~{1}~{2}~{3}~{4}",cactusAPIKey , cactusMerchantId, refrencecode, 100, "ARS");
       signature = RequestMaker.GetMD5HashData(hashstring);
      
        req = PayulatamAPI.CreditCardsPayment(refrencecode,
            signature, 100, "ARS", "1",
            "Test", "test1groovy@gmail.com", "7563126", "5415668464654",
            "Viamonte", "1366", "Buenos Aires", "Buenos Aires", "AR", "000000", "7563126", "Viamonte", "1366",
            "Buenos Aires", "Buenos Aires", "AR", "0000000", "7563126", "1", "Test", "test1groovy@gmail.com", "7563126", "5415668464654",
            "Avenida entre rios", "452", "La Plata", "Buenos Aires", "AR", "64000", "7563126", "4850110000000000",
            "321", "2016/12", "Test", "VISA","AR",Session.SessionID,"192.168.1.4","",Request.UserAgent);
      string res = PayulatamAPI.MakePaymentRequest(req);
        lblMessage.Text=res.ToString();
    }
       
}