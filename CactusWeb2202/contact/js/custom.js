jQuery(document).ready(function($) {
  $(".main-slider").owlCarousel({
    autoPlay: 3000, //Set AutoPlay to 3 seconds
    pagination: true,
    singleItem : true,
    transitionStyle : "fade",
    });
  $(".testimonial-slide").owlCarousel({
    autoPlay: 3000, //Set AutoPlay to 3 seconds
    pagination: false,
    singleItem : true,
    transitionStyle : "fade",
    });
  var owl = $(".testimonial-slide");
  $(".bnt-next").click(function(){
    owl.trigger('owl.next');
  })
  $(".bnt-prev").click(function(){
    owl.trigger('owl.prev');
  })
  $(".logo-slider").owlCarousel({
    autoPlay: 3000, //Set AutoPlay to 3 seconds
    pagination: false,
    transitionStyle : "fade",
    itemsDesktop : [1920,6], //5 items between 1000px and 901px
    itemsDesktopSmall : [991,4], // betweem 900px and 601px
    itemsTablet: [767,4],
    itemsMobile: [570,2]
  });

    // var str = "";
    // $( "#myselect" ).each(function() {
    //   $("#location_map").removeClass("localtion_main");
    //   str = $( "#myselect" ).val();
    //   $("#location_map").addClass(str);
    // });
  
  $( "#myselect" ).change(function() {
    var str = "";
    $( "#myselect" ).each(function() {
      str = $( "#myselect" ).val();
    });
    $("#location_map").removeClass("localtion_main");
    $("#location_map").removeClass("localtion1");
    $("#location_map").removeClass("localtion2");
    $("#location_map").removeClass("localtion3");
    $("#location_map").removeClass("localtion4");
    $("#location_map").removeClass("localtion5");
    $("#location_map").removeClass("localtion6");
    $("#location_map").addClass(str);

    $("#location_map").gMap({
      maptype: google.maps.MapTypeId.ROADMAP,
      zoom: 5,
      markers:
        [
          {
            latitude: -24.790673,
            longitude: -65.409844,
            html: "<strong>Cactus Alquiler de Autos Salta</strong><br/>Buenos Aires 82,Salta",
            popup: true,
          },
          {
            latitude: -24.8204749,
            longitude: -65.4274758,
            html: "<strong>CACTUS ALQUILER DE AUTOS SALT AEROPUERTO</strong><br/>Av. Alfredo Palacios 2430,4400 Salta",
            popup: true,
          },
          {
            latitude: -32.892075,
            longitude: -68.8405037,
            html: "<strong>Cactus RENT A CAR</strong><br/>Pres. de la Reta 992,Capital,Mendoza",
            popup: true,
          },
          {
            latitude: -32.8324109,
            longitude: -68.796206,
            html: "<strong>Aeropuerto Internacional Governor Francisco Gabrielli</strong><br/>Ruta Nac. Nº 40 Acceso Norte Km. 15,5539 Las Heras,Mendoza",
            popup: true,
          },
          {
            latitude: -41.1357909,
            longitude: -71.3114854,
            html: "<strong>Cactus Alquiler de Autos Bariloche</strong><br/>Ada Maria Elflein 59,8400 San Carlos de Bariloche,Río Negro",
            popup: true,
          }, 
          {
            latitude: -41.1497822,
            longitude: -71.16194,
            html: "<strong>Aeropuerto de San Carlos de Bariloche</strong><br/>Ruta Provincial Nº 80 s/n,8400 San Carlos de Bariloche,Río Negro",
            popup: true,
          }
        ],
      panControl: true,
      zoomControl: true,
      mapTypeControl: true,
      scaleControl: true,
      streetViewControl: true,
      scrollwheel: false,
      styles: [ { "stylers": [ { "hue": "" }, { "gamma": 1 }, { "saturation": 1 } ] } ],
      onComplete: function() {
      // Resize and re-center the map on window resize event
      var gmap = $("#location_map").data('gmap').gmap;
      window.onresize = function(){
        google.maps.event.trigger(gmap, 'resize');
          $("#location_map").gMap('fixAfterResize');
        };
      }
    });
    $(".localtion1").gMap({
    maptype: google.maps.MapTypeId.ROADMAP,
    zoom: 10,
    markers:
      [
        {
          latitude: -24.790673,
          longitude: -65.409844,
          html: "<strong>Cactus Alquiler de Autos Salta</strong><br/>Buenos Aires 82,Salta",
          popup: true,
        }
      ],
    panControl: true,
    zoomControl: true,
    mapTypeControl: true,
    scaleControl: true,
    streetViewControl: true,
    scrollwheel: false,
    styles: [ { "stylers": [ { "hue": "" }, { "gamma": 1 }, { "saturation": 1 } ] } ],
    onComplete: function() {
    // Resize and re-center the map on window resize event
    var gmap = $("#location_map").data('gmap').gmap;
    window.onresize = function(){
      google.maps.event.trigger(gmap, 'resize');
        $("#location_map").gMap('fixAfterResize');
      };
    }
    });
    $(".localtion2").gMap({
      maptype: google.maps.MapTypeId.ROADMAP,
      zoom: 10,
      markers:
        [
          {
            latitude: -24.8204749,
            longitude: -65.4274758,
            html: "<strong>CACTUS ALQUILER DE AUTOS SALT AEROPUERTO</strong><br/>Av. Alfredo Palacios 2430,4400 Salta",
            popup: true,
          }
        ],
      panControl: true,
      zoomControl: true,
      mapTypeControl: true,
      scaleControl: true,
      streetViewControl: true,
      scrollwheel: false,
      styles: [ { "stylers": [ { "hue": "" }, { "gamma": 1 }, { "saturation": 1 } ] } ],
      onComplete: function() {
      // Resize and re-center the map on window resize event
      var gmap = $("#location_map").data('gmap').gmap;
      window.onresize = function(){
        google.maps.event.trigger(gmap, 'resize');
          $("#location_map").gMap('fixAfterResize');
        };
      }
    });
    $(".localtion3").gMap({
      maptype: google.maps.MapTypeId.ROADMAP,
      zoom: 10,
      markers:
        [
          {
            latitude: -32.892075,
            longitude: -68.8405037,
            html: "<strong>Cactus RENT A CAR</strong><br/>Pres. de la Reta 992,Capital,Mendoza",
            popup: true,
          }
        ],
      panControl: true,
      zoomControl: true,
      mapTypeControl: true,
      scaleControl: true,
      streetViewControl: true,
      scrollwheel: false,
      styles: [ { "stylers": [ { "hue": "" }, { "gamma": 1 }, { "saturation": 1 } ] } ],
      onComplete: function() {
      // Resize and re-center the map on window resize event
      var gmap = $("#location_map").data('gmap').gmap;
      window.onresize = function(){
        google.maps.event.trigger(gmap, 'resize');
          $("#location_map").gMap('fixAfterResize');
        };
      }
    });
    $(".localtion4").gMap({
      maptype: google.maps.MapTypeId.ROADMAP,
      zoom: 10,
      markers:
        [
          {
            latitude: -32.8324109,
            longitude: -68.796206,
            html: "<strong>Aeropuerto Internacional Governor Francisco Gabrielli</strong><br/>Ruta Nac. Nº 40 Acceso Norte Km. 15,5539 Las Heras,Mendoza",
            popup: true,
          },
        ],
      panControl: true,
      zoomControl: true,
      mapTypeControl: true,
      scaleControl: true,
      streetViewControl: true,
      scrollwheel: false,
      styles: [ { "stylers": [ { "hue": "" }, { "gamma": 1 }, { "saturation": 1 } ] } ],
      onComplete: function() {
      // Resize and re-center the map on window resize event
      var gmap = $("#location_map").data('gmap').gmap;
      window.onresize = function(){
        google.maps.event.trigger(gmap, 'resize');
          $("#location_map").gMap('fixAfterResize');
        };
      }
    });
    $(".localtion5").gMap({
      maptype: google.maps.MapTypeId.ROADMAP,
      zoom: 10,
      markers:
        [
          {
            latitude: -41.1357909,
            longitude: -71.3114854,
            html: "<strong>Cactus Alquiler de Autos Bariloche</strong><br/>Ada Maria Elflein 59,8400 San Carlos de Bariloche,Río Negro",
            popup: true,
          }        
        ],
      panControl: true,
      zoomControl: true,
      mapTypeControl: true,
      scaleControl: true,
      streetViewControl: true,
      scrollwheel: false,
      styles: [ { "stylers": [ { "hue": "" }, { "gamma": 1 }, { "saturation": 1 } ] } ],
      onComplete: function() {
      // Resize and re-center the map on window resize event
      var gmap = $("#location_map").data('gmap').gmap;
      window.onresize = function(){
        google.maps.event.trigger(gmap, 'resize');
          $("#location_map").gMap('fixAfterResize');
        };
      }
    });
    $(".localtion6").gMap({
      maptype: google.maps.MapTypeId.ROADMAP,
      zoom: 10,
      markers:
        [
          {
            latitude: -41.1497822,
            longitude: -71.16194,
            html: "<strong>Aeropuerto de San Carlos de Bariloche</strong><br/>Ruta Provincial Nº 80 s/n,8400 San Carlos de Bariloche,Río Negro",
            popup: true,
          }
        ],
      panControl: true,
      zoomControl: true,
      mapTypeControl: true,
      scaleControl: true,
      streetViewControl: true,
      scrollwheel: false,
      styles: [ { "stylers": [ { "hue": "" }, { "gamma": 1 }, { "saturation": 1 } ] } ],
      onComplete: function() {
      // Resize and re-center the map on window resize event
      var gmap = $("#location_map").data('gmap').gmap;
      window.onresize = function(){
        google.maps.event.trigger(gmap, 'resize');
          $("#location_map").gMap('fixAfterResize');
        };
      }
    });
  })


  .trigger( "change" );

  


});



jQuery(document).ready(function($) {
  var width = $(window).width();
  if (width < 992) {
    $(".feature").addClass("arrow_box_light");
  };
  if (width < 768) {
    $(".client-2").addClass("arrow_box_dark");
    $(".client-2").addClass("bg-dark");
  };
});

jQuery(document).ready(function($) {
  
  $( "#myselect" ).change(function () {
    var str_info = "";
    $(".info-local").removeClass(str_info);
    $( "#myselect" ).each(function() {
      str_info += $( "#myselect" ).val() + "-info";
    });
    $(".info-local").removeClass("show-info");
    $("." + str_info).addClass("show-info");
  })
  .change();
});

// jQuery(document).ready(function($) {
  
//   $( "#province" ).change(function () {
//     var str_pro = "";
//     $( "#province" ).each(function() {
//       str_info = $( "#province" ).val();

//     });
//     str_info = $( "#province" ).val();

//     $("#location_map").removeClass("localtion_main");
//     $("#location_map").removeClass("localtion1");
//     $("#location_map").removeClass("localtion2");
//     $("#location_map").removeClass("localtion3");
//     $("#location_map").removeClass("localtion4");
//     $("#location_map").removeClass("localtion5");
//     $("#location_map").removeClass("localtion6");    
//     if (str_info="localtion_main") {
//       $("#location_map").removeClass("localtion_main");
//     };

//   })
//   .change();
// });

jQuery(document).ready(function($) {
  // Initializing arrays with city names.
  var SALTA = [{
  display: "Salta Centro",value: "localtion1"},
  {display: "Salta Aeropuerto",value: "localtion2"}];
  var MENDOZA = [{
  display: "Mendoza Centro",value: "localtion3"},
  {display: "Mendoza Aeropuerto",value: "localtion4"}];
  var BARILOCHE = [{
  display: "Bariloche Centro",value: "localtion5"},
  {display: "Bariloche Aeropuerto", value: "localtion6"}];
  // Function executes on change of first select option field.
  $("#province").change(function() {
    var select = $("#province option:selected").val();
    switch (select) {
    case "SALTA":
    city(SALTA);
    break;
    case "MENDOZA":
    city(MENDOZA);
    break;
    case "BARILOCHE":
    city(BARILOCHE);
    break;
    default:
    $("#myselect").empty();
    $("#myselect").append("<option>--Select--</option>");
    break;
  }
  });
  // Function To List out Cities in Second Select tags
  function city(arr) {
    $("#myselect").empty(); //To reset cities
    $("#myselect").append("<option value='localtion_main'>--Select--</option>");
    $(arr).each(function(i) { //to list cities
    $("#myselect").append("<option value=\"" + arr[i].value + "\">" + arr[i].display + "</option>")
    });
  }
});

