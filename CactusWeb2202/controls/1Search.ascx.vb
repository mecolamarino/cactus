﻿
Partial Class controls_Search
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
           


                txtpickupdate.Text = Request.QueryString("PickupDate")
                txtdropoffdate.Text = Request.QueryString("DropoffDate")
                drpPickuptime.SelectedValue = Request.QueryString("PickupTime")
                drpDropOfftime.SelectedValue = Request.QueryString("DropoffTime")
                vechiletype.SelectedValue = Request.QueryString("vechileType")
               

              
              If Not String.IsNullOrEmpty(Request.QueryString("PickupDate")) And Not String.IsNullOrEmpty(Request.QueryString("DropoffDate")) And Not String.IsNullOrEmpty(Request.QueryString("PickupTime")) And Not String.IsNullOrEmpty(Request.QueryString("DropoffTime")) Then
                Dim htRentalParam As New Hashtable
	        	htRentalParam.Add("PickupLocation", drppickuploc.SelectedItem.Value)
                htRentalParam.Add("DropoffLocation", drpreturnloc.SelectedItem.Value)
				'htRentalParam.Add("PickupLocation", "KNIS")
                'htRentalParam.Add("DropoffLocation", "KNIS")
                htRentalParam.Add("PickupDate", Request.QueryString("PickupDate"))
                htRentalParam.Add("PickupTime", Request.QueryString("PickupTime"))
                htRentalParam.Add("DropoffDate", Request.QueryString("DropoffDate"))
                htRentalParam.Add("DropoffTime", Request.QueryString("DropoffTime"))
                htRentalParam.Add("PromoCode", txtdiscount.Text)
                htRentalParam.Add("ClassCode", Request.QueryString("code"))
                htRentalParam.Add("vechileType", Request.QueryString("vechileType"))
                '  htRentalParam.Add("Age", rdoAge.SelectedItem.Value)
                Session.Add("_htRentalParam", htRentalParam)
                Trace.Write("INSIDE")
                Response.Redirect("list.aspx")
            Else
                'Display car name and image if user selects a car from the fleet (7/14/2015)

                Dim carCode As String = Request.QueryString("classCode")
                If Not String.IsNullOrEmpty(carCode) Then
                    Select Case carCode
                        Case "ecar" 'Economic
                            lblCarName.Text = "Nissan Versa"
                            imgCarPicture.ImageUrl = "/carimages/ecar.jpg"

                        Case "CCAR" 'Compact
                            lblCarName.Text = "Nissan Sentra"
                            imgCarPicture.ImageUrl = "/carimages/CCAR.jpg"

                        Case "ICAR" 'Intermediate
                            lblCarName.Text = "Nissan Altima"
                            imgCarPicture.ImageUrl = "/carimages/ICAR.jpg"

                        Case "IXAR" 'Intermediate SUV
                            lblCarName.Text = "Nissan Rogue"
                            imgCarPicture.ImageUrl = "/carimages/IXAR.jpg"

                        Case "IPAR" 'Pickup
                            lblCarName.Text = "Nissan Frontier"
                            imgCarPicture.ImageUrl = "/carimages/IPAR.jpg"

                        Case "FPAR" 'Full Size Pickup
                            lblCarName.Text = "Nissan Titan"
                            imgCarPicture.ImageUrl = "/carimages/FPAR.jpg"

                        Case "MVAR" 'Minivan
                            lblCarName.Text = "Nissan Quest"
                            imgCarPicture.ImageUrl = "/carimages/MVAR.jpg"

                            'Case "CARG"
                            '    lblCarName.Text = "CARGO VAN"
                            '    imgCarPicture.ImageUrl = "/carimages/CARG.jpg"
                            'Case "15PA"
                            '    lblCarName.Text = "15 PASSENGER VAN"
                            '    imgCarPicture.ImageUrl = "/carimages/15PA.jpg"
                            'Case "SSED"
                            '    lblCarName.Text = "Volkswagen Jetta Standard Size Sedan"
                            '    imgCarPicture.ImageUrl = "/carimages/SSED.jpg"
                            'Case "PSED"
                            '    lblCarName.Text = "Volkswagen Passat Premium Size Sedan"
                            '    imgCarPicture.ImageUrl = "/carimages/PSED.jpg"
                            'Case "SP11"
                            '    lblCarName.Text = "SPRINTER CARGO VAN"
                            '    imgCarPicture.ImageUrl = "/carimages/SP11.jpg"
                            'Case "CR-V"
                            '    lblCarName.Text = "CR-V"
                            '    imgCarPicture.ImageUrl = "/carimages/CR-V.jpg"
                            'Case "SUV"
                            '    lblCarName.Text = "SUV"
                            '    imgCarPicture.ImageUrl = "/carimages/SUV.jpg"
                            'Case "12SP"
                            '    lblCarName.Text = "12 Passenger Sprinter Van"
                            '    imgCarPicture.ImageUrl = "/carimages/12SP.jpg"
                            'Case "LUX"
                            '    lblCarName.Text = "LUX"
                            '    imgCarPicture.ImageUrl = "/carimages/LUX.jpg"

                            'Case "LSUV"
                            '    lblCarName.Text = "CADILLAC ESCALADE"
                            '    imgCarPicture.ImageUrl = "/carimages/LSUV.jpg"


                            'Case "8PV"
                            '    lblCarName.Text = "Executive Eight Passenger Sprinter Van"
                            '    imgCarPicture.ImageUrl = "/carimages/8PV.jpg"

                    End Select
                End If
            End If
        End If
    End Sub


    Protected Sub BtnSubmit_Click(sender As Object, e As EventArgs)


        If drppickuploc.SelectedItem.Value = "Select" Then
            lblmsg.Text = "Please select Location"
        Else

            Dim str As String = ""
            Dim TurnDiscount = True

            'Dim nowdate As DateTime = Convert.ToDateTime(Now).ToShortDateString
            'Dim pickupdate As DateTime = Convert.ToDateTime(txtpickupdate.Text)
            'Dim totaldays As TimeSpan = pickupdate - nowdate
            '  Dim days As Integer = totaldays.Days
            Dim discountcode As String
            Dim returnloc As String
            Dim pickloc As String
            '  Dim ratecode As String = rdoRate.SelectedItem.Value.ToString
            Dim htRentalParam As New Hashtable

            If drpreturnloc.SelectedItem.Value = "Same" Then
                returnloc = drppickuploc.SelectedItem.Value
            Else
                returnloc = drpreturnloc.SelectedItem.Value
            End If
            
			htRentalParam.Add("PickupLocation", drppickuploc.SelectedItem.Value)
            htRentalParam.Add("DropoffLocation", returnloc)
			'htRentalParam.Add("PickupLocation", "KNIS")
            'htRentalParam.Add("DropoffLocation", "KNIS")
            htRentalParam.Add("PickupDate", txtpickupdate.Text)
            htRentalParam.Add("PickupTime", drpPickuptime.SelectedItem.Value)
            htRentalParam.Add("DropoffDate", txtdropoffdate.Text)
            htRentalParam.Add("DropoffTime", drpDropOfftime.SelectedItem.Value)
            htRentalParam.Add("PromoCode", txtdiscount.Text)
            htRentalParam.Add("ClassCode", Request.QueryString("code"))
            htRentalParam.Add("vechileType", vechiletype.SelectedItem.Value)
            '  htRentalParam.Add("Age", rdoAge.SelectedItem.Value)
            Session.Add("_htRentalParam", htRentalParam)
            Trace.Write("INSIDE")
            Response.Redirect("list.aspx")
        End If
    End Sub
End Class
