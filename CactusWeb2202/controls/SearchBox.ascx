﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="SearchBox.ascx.vb" Inherits="controls_Search" %>



<style>
    .ui-datepicker {
        font-size: 85%;
    }

    .error {
        color: red;
    }

    .search_steps {
        background-color: #f00;
        color: #fff;
        font-size: 16pt;
        padding: 5px 10px;
    }
</style>
<div>
    <div class="cell-reservation-block-gray">
        <div class="">
            <div class="page">
                <div>Pick up</div>
                <asp:Label ID="lblmsg" ForeColor="white" Font-Bold="true" runat="server"
                    meta:resourcekey="lblmsg"></asp:Label>
                <div style="display: none">
                    <asp:DropDownList ID="drppickuploc" runat="server">
                        <asp:ListItem Value="15 PASSENGER VAN">15 PASSENGER VAN</asp:ListItem>
                        <asp:ListItem Value="SPRINTER PASSENGER VANS">SPRINTER PASSENGER VANS</asp:ListItem>
                        <asp:ListItem Value="SPRINTER CARGO VAN">SPRINTER CARGO VAN</asp:ListItem>
                        <asp:ListItem Value="CARGO VAN">CARGO VAN</asp:ListItem>
                        <asp:ListItem Value="MINI VAN">MINI VAN</asp:ListItem>
                        <asp:ListItem Value="LEXUS LX 570">LEXUS LX 570 - SUV</asp:ListItem>
                        <asp:ListItem Value="MERCEDES-BENZ M350">MERCEDES-BENZ M350 - SUV</asp:ListItem>
                        <asp:ListItem Value="CHEVY SUBURBAN">CHEVY SUBURBAN - SUV</asp:ListItem>
                        <asp:ListItem Value="CHEVY TAHOE">CHEVY TAHOE - SUV</asp:ListItem>
                        <asp:ListItem Value="CADILLAC ESCALADE">CADILLAC ESCALADE - SUV</asp:ListItem>
                        <asp:ListItem Value="FORD F150 PICKUP">FORD F150 PICKUP</asp:ListItem>
                        <asp:ListItem Value="HONDA PILOT">HONDA PILOT - SUV</asp:ListItem>
                        <asp:ListItem Value="HONDA CR-V">HONDA CR-V - SUV</asp:ListItem>
                        <asp:ListItem Value="NISSAN MURANO">NISSAN MURANO - SUV</asp:ListItem>
                        <asp:ListItem Value="NISSAN ROGUE">NISSAN ROGUE - SUV</asp:ListItem>
                        <asp:ListItem Value="JAGUAR XJ">JAGUAR XJ</asp:ListItem>
                        <asp:ListItem Value="SEDAN">STANDARD SEDAN</asp:ListItem>
                        <asp:ListItem Value="SEDAN">PREMIUM SEDAN</asp:ListItem>
                        <asp:ListItem Value="all">ALL VEHICLES</asp:ListItem>
                        <%--<asp:ListItem Value="SDQ" meta:resourcekey="ListItemResourcesdq" Text="Las Americas Airport (SDQ)"></asp:ListItem>
                        <asp:ListItem Value="PAR" Text="PAR" Selected="True"></asp:ListItem>--%>
                    </asp:DropDownList>
                </div>


                <%-- <input name="" type="text" value="12/2/2014" /> --%>
                <div>
                    <asp:TextBox ID="txtpickupdate" runat="server" Width="148" placeholder=" Picup Date" autocomplete="off" ClientIDMode="Static"></asp:TextBox>



                    <%--   <select name="asdf" size="dsf"><option value="#">8:00 am</option></select>--%>
                    <asp:DropDownList ID="drpPickuptime" runat="server">
                        <asp:ListItem Value="00:00 am" Text="00:00 am"></asp:ListItem>
                        <asp:ListItem Value="00:30 am" Text="00:30 am"></asp:ListItem>
                        <asp:ListItem Value="01:00 am" Text="01:00 am"></asp:ListItem>
                        <asp:ListItem Value="01:30 am" Text="01:30 am"></asp:ListItem>
                        <asp:ListItem Value="02:00 am" Text="02:00 am"></asp:ListItem>
                        <asp:ListItem Value="02:30 am" Text="02:30 am"></asp:ListItem>
                        <asp:ListItem Value="03:00 am" Text="03:00 am"></asp:ListItem>
                        <asp:ListItem Value="03:30 am" Text="03:30 am"></asp:ListItem>
                        <asp:ListItem Value="04:00 am" Text="04:00 am"></asp:ListItem>
                        <asp:ListItem Value="04:30 am" Text="04:30 am"></asp:ListItem>
                        <asp:ListItem Value="05:00 am" Text="05:00 am"></asp:ListItem>
                        <asp:ListItem Value="05:30 am" Text="05:30 am"></asp:ListItem>
                        <asp:ListItem Value="06:00 am" Text="06:00 am"></asp:ListItem>
                        <asp:ListItem Value="06:30 am" Text="06:30 am"></asp:ListItem>

                        <asp:ListItem Value="07:00 am" Text="07:00 am"></asp:ListItem>
                        <asp:ListItem Value="07:30 am" Text="07:30 am"></asp:ListItem>

                        <asp:ListItem Value="08:00 am" Text="08:00 am"></asp:ListItem>
                        <asp:ListItem Value="08:30 am" Text="08:30 am"></asp:ListItem>

                        <asp:ListItem Value="09:00 am" Text="09:00 am"></asp:ListItem>

                        <asp:ListItem Value="09:30 am" Selected="true" Text="09:30 am"></asp:ListItem>
                        <asp:ListItem Value="10:00 am" Text="10:00 am"></asp:ListItem>
                        <asp:ListItem Value="10:30 am" Text="10:30 am"></asp:ListItem>
                        <asp:ListItem Value="11:00 am" Text="11:00 am"></asp:ListItem>
                        <asp:ListItem Value="11:30 am" Text="11:30 am"></asp:ListItem>
                        <asp:ListItem Value="12:00 pm" Text="12:00 pm"></asp:ListItem>
                        <asp:ListItem Value="12:30 pm" Text="12:30 pm"></asp:ListItem>
                        <asp:ListItem Value="01:00 pm" Text="01:00 pm"></asp:ListItem>
                        <asp:ListItem Value="01:30 pm" Text="01:30 pm"></asp:ListItem>
                        <asp:ListItem Value="02:00 pm" Text="02:00 pm"></asp:ListItem>
                        <asp:ListItem Value="02:30 pm" Text="02:30 pm"></asp:ListItem>

                        <asp:ListItem Value="03:00 pm" Text="03:00 pm"></asp:ListItem>
                        <asp:ListItem Value="03:30 pm" Text="03:30 pm"></asp:ListItem>
                        <asp:ListItem Value="04:00 pm" Text="04:00 pm"></asp:ListItem>
                        <asp:ListItem Value="04:30 pm" Text="04:30 pm"></asp:ListItem>
                        <asp:ListItem Value="05:00 pm" Text="05:00 pm"></asp:ListItem>
                        <asp:ListItem Value="05:30 pm" Text="05:30 pm"></asp:ListItem>
                        <asp:ListItem Value="06:00 pm" Text="06:00 pm"></asp:ListItem>
                        <asp:ListItem Value="06:30 pm" Text="06:30 pm"></asp:ListItem>



                        <asp:ListItem Value="07:00 pm" Text="07:00 pm"></asp:ListItem>
                        <asp:ListItem Value="07:30 pm" Text="07:30 pm"></asp:ListItem>
                        <asp:ListItem Value="08:00 pm" Text="08:00 pm"></asp:ListItem>
                        <asp:ListItem Value="08:30 pm" Text="08:30 pm"></asp:ListItem>

                        <asp:ListItem Value="09:00 pm" Text="09:00 pm"></asp:ListItem>
                        <asp:ListItem Value="09:30 pm" Text="09:30 pm"></asp:ListItem>
                        <asp:ListItem Value="10:00 pm" Text="10:00 pm"></asp:ListItem>
                        <asp:ListItem Value="10:30 pm" Text="10:30 pm"></asp:ListItem>
                        <asp:ListItem Value="11:00 pm" Text="11:00 pm"></asp:ListItem>
                        <asp:ListItem Value="11:30 pm" Text="11:30 pm"></asp:ListItem>


                    </asp:DropDownList>
                    <br />
                    <asp:RequiredFieldValidator CssClass="error" ID="RequiredFieldValidator1"
                        ControlToValidate="txtpickupdate" runat="server"
                        ErrorMessage="Pickup Date is Required"
                        meta:resourcekey="RequiredFieldValidator1Resource1"></asp:RequiredFieldValidator>

                </div>
                <div style="margin-top: 15px;">Return</div>
                <div style="display: none">
                    <asp:DropDownList ID="drpreturnloc" runat="server">
                        <asp:ListItem Value="Same" meta:resourcekey="ListItemResourceSame" Text="Same as Pickup"></asp:ListItem>
                        <asp:ListItem Value="15 PASSENGER VAN">15 PASSENGER VAN</asp:ListItem>
                        <asp:ListItem Value="SPRINTER PASSENGER VANS">SPRINTER PASSENGER VANS</asp:ListItem>
                        <asp:ListItem Value="SPRINTER CARGO VAN">SPRINTER CARGO VAN</asp:ListItem>
                        <asp:ListItem Value="CARGO VAN">CARGO VAN</asp:ListItem>
                        <asp:ListItem Value="MINI VAN">MINI VAN</asp:ListItem>
                        <asp:ListItem Value="LEXUS LX 570">LEXUS LX 570 - SUV</asp:ListItem>
                        <asp:ListItem Value="MERCEDES-BENZ M350">MERCEDES-BENZ M350 - SUV</asp:ListItem>
                        <asp:ListItem Value="CHEVY SUBURBAN">CHEVY SUBURBAN - SUV</asp:ListItem>
                        <asp:ListItem Value="CHEVY TAHOE">CHEVY TAHOE - SUV</asp:ListItem>
                        <asp:ListItem Value="CADILLAC ESCALADE">CADILLAC ESCALADE - SUV</asp:ListItem>
                        <asp:ListItem Value="FORD F150 PICKUP">FORD F150 PICKUP</asp:ListItem>
                        <asp:ListItem Value="HONDA PILOT">HONDA PILOT - SUV</asp:ListItem>
                        <asp:ListItem Value="HONDA CR-V">HONDA CR-V - SUV</asp:ListItem>
                        <asp:ListItem Value="NISSAN MURANO">NISSAN MURANO - SUV</asp:ListItem>
                        <asp:ListItem Value="NISSAN ROGUE">NISSAN ROGUE - SUV</asp:ListItem>
                        <asp:ListItem Value="JAGUAR XJ">JAGUAR XJ</asp:ListItem>
                        <asp:ListItem Value="SEDAN">STANDARD SEDAN</asp:ListItem>
                        <asp:ListItem Value="SEDAN">PREMIUM SEDAN</asp:ListItem>
                        <asp:ListItem Value="all">ALL VEHICLES</asp:ListItem>
                        <%--<asp:ListItem Value="SDQ" meta:resourcekey="ListItemResourcesdq" Text="Las Americas Airport (SDQ)"></asp:ListItem>

                        <asp:ListItem Value="PAR" Text="PAR" Selected="True"></asp:ListItem>--%>
                    </asp:DropDownList>
                </div>

                <div>
                    <asp:TextBox ID="txtdropoffdate" Width="148" runat="server" placeholder="Drop Date" autocomplete="off" ClientIDMode="Static"></asp:TextBox>

                    <asp:DropDownList ID="drpDropOfftime" runat="server"
                        meta:resourcekey="drpDropOfftimeResource1">
                        <asp:ListItem Value="00:00 am" Text="00:00 am"></asp:ListItem>
                        <asp:ListItem Value="00:30 am" Text="00:30 am"></asp:ListItem>
                        <asp:ListItem Value="01:00 am" Text="01:00 am"></asp:ListItem>
                        <asp:ListItem Value="01:30 am" Text="01:30 am"></asp:ListItem>
                        <asp:ListItem Value="02:00 am" Text="02:00 am"></asp:ListItem>
                        <asp:ListItem Value="02:30 am" Text="02:30 am"></asp:ListItem>
                        <asp:ListItem Value="03:00 am" Text="03:00 am"></asp:ListItem>
                        <asp:ListItem Value="03:30 am" Text="03:30 am"></asp:ListItem>
                        <asp:ListItem Value="04:00 am" Text="04:00 am"></asp:ListItem>
                        <asp:ListItem Value="04:30 am" Text="04:30 am"></asp:ListItem>
                        <asp:ListItem Value="05:00 am" Text="05:00 am"></asp:ListItem>
                        <asp:ListItem Value="05:30 am" Text="05:30 am"></asp:ListItem>
                        <asp:ListItem Value="06:00 am" Text="06:00 am"></asp:ListItem>
                        <asp:ListItem Value="06:30 am" Text="06:30 am"></asp:ListItem>

                        <asp:ListItem Value="07:00 am" Text="07:00 am"></asp:ListItem>
                        <asp:ListItem Value="07:30 am" Text="07:30 am"></asp:ListItem>

                        <asp:ListItem Value="08:00 am" Text="08:00 am"></asp:ListItem>
                        <asp:ListItem Value="08:30 am" Text="08:30 am"></asp:ListItem>

                        <asp:ListItem Value="09:00 am" Text="09:00 am"></asp:ListItem>

                        <asp:ListItem Value="09:30 am" Selected="true" Text="09:30 am"></asp:ListItem>
                        <asp:ListItem Value="10:00 am" Text="10:00 am"></asp:ListItem>
                        <asp:ListItem Value="10:30 am" Text="10:30 am"></asp:ListItem>
                        <asp:ListItem Value="11:00 am" Text="11:00 am"></asp:ListItem>
                        <asp:ListItem Value="11:30 am" Text="11:30 am"></asp:ListItem>
                        <asp:ListItem Value="12:00 pm" Text="12:00 pm"></asp:ListItem>
                        <asp:ListItem Value="12:30 pm" Text="12:30 pm"></asp:ListItem>
                        <asp:ListItem Value="01:00 pm" Text="01:00 pm"></asp:ListItem>
                        <asp:ListItem Value="01:30 pm" Text="01:30 pm"></asp:ListItem>
                        <asp:ListItem Value="02:00 pm" Text="02:00 pm"></asp:ListItem>
                        <asp:ListItem Value="02:30 pm" Text="02:30 pm"></asp:ListItem>

                        <asp:ListItem Value="03:00 pm" Text="03:00 pm"></asp:ListItem>
                        <asp:ListItem Value="03:30 pm" Text="03:30 pm"></asp:ListItem>
                        <asp:ListItem Value="04:00 pm" Text="04:00 pm"></asp:ListItem>
                        <asp:ListItem Value="04:30 pm" Text="04:30 pm"></asp:ListItem>
                        <asp:ListItem Value="05:00 pm" Text="05:00 pm"></asp:ListItem>
                        <asp:ListItem Value="05:30 pm" Text="05:30 pm"></asp:ListItem>
                        <asp:ListItem Value="06:00 pm" Text="06:00 pm"></asp:ListItem>
                        <asp:ListItem Value="06:30 pm" Text="06:30 pm"></asp:ListItem>



                        <asp:ListItem Value="07:00 pm" Text="07:00 pm"></asp:ListItem>
                        <asp:ListItem Value="07:30 pm" Text="07:30 pm"></asp:ListItem>
                        <asp:ListItem Value="08:00 pm" Text="08:00 pm"></asp:ListItem>
                        <asp:ListItem Value="08:30 pm" Text="08:30 pm"></asp:ListItem>

                        <asp:ListItem Value="09:00 pm" Text="09:00 pm"></asp:ListItem>
                        <asp:ListItem Value="09:30 pm" Text="09:30 pm"></asp:ListItem>
                        <asp:ListItem Value="10:00 pm" Text="10:00 pm"></asp:ListItem>
                        <asp:ListItem Value="10:30 pm" Text="10:30 pm"></asp:ListItem>
                        <asp:ListItem Value="11:00 pm" Text="11:00 pm"></asp:ListItem>
                        <asp:ListItem Value="11:30 pm" Text="11:30 pm"></asp:ListItem>

                    </asp:DropDownList>
                    <br />
                    <asp:RequiredFieldValidator CssClass="error" ID="RequiredFieldValidator2"
                        ControlToValidate="txtdropoffdate" runat="server"
                        ErrorMessage="Drop off Date is Required"
                        meta:resourcekey="RequiredFieldValidator2Resource1"></asp:RequiredFieldValidator>

                </div>

                <div style="margin-top: 15px;">Discount Code</div>

                <asp:TextBox ID="txtdiscount" runat="server" placeholder="Optional"></asp:TextBox>
                <div style="margin-top: 15px;">
                    <asp:Button CssClass="gen_button" ID="BtnSubmit"
                        runat="server" Text="Get Rates"
                        meta:resourcekey="BtnSubmitResource1" OnClick="BtnSubmit_Click" />
                </div>


            </div>

        </div>

        <div class="cell-already-have">
            Have a Reservation? <a href="#" class="small_button">Modify</a>

            <a href="#" class="small_button">Cancel</a>
        </div>

    </div>

</div>
