﻿<%@ Control Language="VB" %>
 
<link href="../css/ProcessBar.css" rel="stylesheet" type="text/css" />
<script type="text/javascript">
            //  register for our events
            Sys.WebForms.PageRequestManager.getInstance().add_beginRequest(beginRequest);
            Sys.WebForms.PageRequestManager.getInstance().add_endRequest(endRequest);    
            
            function beginRequest(sender, args){
                // show the popup
                $find('<%= mdlPopup.ClientID %>').show();        
            }

            function endRequest(sender, args) {
                //  hide the popup
                $find('<%= mdlPopup.ClientID %>').hide();
            }
    </script>

<ajaxToolkit:ModalPopupExtender ID="mdlPopup" runat="server" TargetControlID="pnlPopup"
    PopupControlID="pnlPopup" BackgroundCssClass="modalBackground" />
<asp:Panel ID="pnlPopup" runat="server" CssClass="updateProgress" Style="display: none">
    <div align="center" style="margin-top: 13px;">
        <img src="/images/load.gif" alt=""/>
        <span class="updateProgressMessage">Please wait ...</span>
    </div>
</asp:Panel>
