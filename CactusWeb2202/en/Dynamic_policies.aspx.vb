﻿Imports System.Net
Imports System.IO
Imports System.Xml
Imports System.Xml.Serialization
Imports System.Linq
Imports System.Xml.Linq
Imports System.Web.Script.Serialization
Public Class Dynamic_policies
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim _htRentalParam As Hashtable = Nothing
        _htRentalParam = TryCast(Session("_htRentalParam"), Hashtable)

        Dim pickupLocation As String = _htRentalParam("PickupLocation")
        Dim reqXML As String = RequestMaker.PolicyRequest(pickupLocation)
        Dim url As String = System.Configuration.ConfigurationManager.AppSettings("Url")

        Dim uri As String = url

        Dim req As WebRequest
        Dim rep As WebResponse


        req = WebRequest.Create(uri)
        req.Method = "POST"
        req.ContentType = "text/xml"

        Dim writer As New StreamWriter(req.GetRequestStream())
        writer.Write(reqXML)

        writer.Close()

        rep = req.GetResponse()

        Dim receivestream As Stream = rep.GetResponseStream()
        Dim encode As Encoding = Encoding.GetEncoding("utf-8")
        Dim readstream As New StreamReader(receivestream, encode)

        Dim read(256) As [Char]
        Dim result As String
        'Dim IsLocal As Boolean
        Dim counter As Integer
        Dim Tcounter As Integer
        counter = 0
        Tcounter = 0
        result = readstream.ReadToEnd()
        Dim htmlContent As New StringBuilder()
        Label1.Text = result

    End Sub

End Class