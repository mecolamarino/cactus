<%@ Page Title="" Language="VB" MasterPageFile="MasterPage.master" Trace="false" AutoEventWireup="false" CodeFile="EditRes.aspx.vb" Inherits="EditRes" Culture="auto" meta:resourcekey="PageResource1" UICulture="auto" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <!--content_sidebar div ends-->

<div class="col-sm-12" style="background:#fff; padding-bottom:20px;">

    <div class="text_area">


        <h2>
            <asp:Label ID="lblordertext" Text="Order Summary Review" runat="server"></asp:Label>
        </h2>
        <p>
            <asp:Label ID="lblthankyou"
                Text="Thank you for reserving your vehicle with Ameri Rent. Your confirmation number is "
                runat="server" meta:resourcekey="lblthankyouResource1"></asp:Label>&nbsp;
                    <asp:Label ID="lblconf" runat="server" meta:resourcekey="lblconfResource1"></asp:Label><br />





        </p>
        <div class="col-sm-6">
            <h3 style="margin-top: 0px;">
                <asp:Label ID="lblrentertext" Text="Renter's Information" runat="server"></asp:Label></h3>

            <p style="margin-top: 0px;">
                <strong>
                    <asp:Label ID="lblname2" Text="" runat="server"></asp:Label>
                </strong>
                <br />
                <asp:Label ID="lbladdress2" Text="" runat="server"></asp:Label>
                <br />
                <asp:Label ID="lblcity2" Text="" runat="server"></asp:Label>,
                <asp:Label ID="lblstate2" Text="" runat="server"></asp:Label>,
                <asp:Label ID="lblzip2" Text="" runat="server"></asp:Label>, 
       <asp:Label ID="lblcountry2" Text="" runat="server"></asp:Label>
            </p>

            <p style="margin-top: 0px;">
                <strong>

                    <asp:Label ID="lblphonetext" Text="Phone" runat="server"></asp:Label></strong>:<asp:Label ID="lblphone2" Text="" runat="server"></asp:Label>
                <br />

                <strong>
                    <asp:Label ID="lblemailtext" Text="Email" runat="server"></asp:Label></strong>:
                <asp:Label ID="lblemail" Text="Email" runat="server"></asp:Label>
            </p>

            <p style="margin-top: 0px;">
                <%-- <strong><asp:label ID="lbllicensetext"  Text="License #" runat="server"></asp:label></strong>: <asp:label ID="lbllicense"  Text="" runat="server"></asp:label> <br />
      
      <strong><asp:label ID="lblexpirestext"  Text="Expires" runat="server"></asp:label></strong>: <asp:label ID="lblexpiredate"  Text="" runat="server"></asp:label> <br />
                --%>
                <strong>
                    <asp:Label ID="lbldobtext" Text="Date of Birth" runat="server"></asp:Label></strong>:<asp:Label ID="lbldob" Text="" runat="server"></asp:Label>:
            </p>

            <p style="margin-top: 0px;">
                <strong>
                    <asp:Label ID="lblcommenttext" Text="Comments:" runat="server"></asp:Label></strong><br />
                <asp:Label ID="lblcomments2" Text="" runat="server"></asp:Label>
            </p>


        </div>

        <div class="col-sm-6">
            </strong>
     <p>
         <asp:Label ID="lblpickuptext" Text="Pickup:" runat="server"></asp:Label><br />
         <strong>
             <asp:Label ID="lblpick" Text="" runat="server"></asp:Label></strong>
         <br />
         <asp:Label ID="lblpdate" Text="" runat="server"></asp:Label>
     </p>
            <p>
                <asp:Label ID="lbldroptext" Text="Dropoff:" runat="server"></asp:Label><br />
                <strong>
                    <asp:Label ID="lblrloc" Text="" runat="server"></asp:Label></strong>
                <br />
                <asp:Label ID="lblddate" Text="" runat="server"></asp:Label>
            </p>


            <span class="dateFormat">
                <asp:Image ID="imgcar2" Width="250" runat="server"
                    meta:resourcekey="imgcar2Resource1" /><br />
                <p>
                    <asp:Label ID="lblcdescription" Text="" runat="server"></asp:Label><br />
                    <asp:Label ID="lblmdescription" Text="" runat="server"></asp:Label>
                </p>
            </span>




            <h3>
                <asp:Label ID="lbltotaltext" Text="Total:" runat="server"></asp:Label>
                <asp:Label ID="lbltotal" Text="" runat="server"></asp:Label></h3>

            <p>
                <asp:CheckBox ID="chkTerms" Enabled="false" Checked="true" runat="server" />
                <asp:Label Visible="true" ID="lblconfirmtext" Text="Customer has agreed to terms and conditions" runat="server" Font-Bold="true"></asp:Label><br>
            </p>


            <asp:PlaceHolder runat="server" Visible="false" ID="plcConfirm">
                <h3><a href="javascript:window.print()">
                    <img src="images/print_icon.gif" width="36" height="35" /><asp:Label ID="lblprinttext" Text="Print Me" runat="server"></asp:Label></a></h3>

                <p><a href="default.aspx">&laquo; Back to Homepage</a></p>
            </asp:PlaceHolder>
        </div>



    </div>
</div>

</asp:Content>

