﻿Imports System.Xml.Linq
Imports System.IO
Imports System.Threading
Imports System.Globalization
Imports System.Net

Partial Class RateDetails
    Inherits System.Web.UI.Page

    Dim xml As String

    Dim polcytext As String

    Dim regular() As String = {"CVAN", "DBAH", "DCAR", "DRAC", "ECAR", "FDAH", "FFDR", "FFDR", "FPBR", "FWAR", "GCAR", "GDAH", "GWDR", "IWAR", "JWDR", "MVAR", "OFBR", "PCAR", "PCDR", "PFDR", "PWAR", "PWDR", "RVAR", "RWAR", "SCAR", "SDAH", "SDBR", "SDDR", "SFAR", "SFDR", "FCAR"}
    Dim luxury() As String = {"LCDR", "LDDR", "LSDR", "LTAR", "LXAR", "LXDR", "RVAR", "GWDR", "LFDR", "LDAR", "XXDR", "XVAR", "WCAR", "WCDR"}
    Dim exotic() As String = {"LCAR", "XCAR", "XCDR"}
    Dim regularlst As New List(Of String)
    Dim exoticlst As New List(Of String)
    Dim luxurylst As New List(Of String)
    'Protected Sub btnRecalculate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRecalculate.Click
    '    loadRateDetails()
    'End Sub


    Public Sub LoadRateDetails()
        Dim xstr

        Dim cls As String
        GetClassDetail(cls)

        'get all the extras
        xstr = RequestMaker.MakeRateRequest(RequestMaker.RequestExtras(Request.QueryString("ploc"), Request.QueryString("classcode")))

        Dim htRentalParam As Hashtable = Nothing
        htRentalParam = TryCast(Session("_htRentalParam"), Hashtable)

        Dim pickupdate, dropoffdatedate As String
        Dim pickuptime, dropofftime As String
        Dim pr As New RequestMaker
        Dim dayrate As Double
        Dim ratediscount As Double
        Dim surcharge As Double
        Dim IsPaynow As String

        pickuptime = Trim(Request.QueryString("putime"))
        dropofftime = Trim(Request.QueryString("dotime"))

        Dim days
        Dim frate
        Dim stotal As Double
        Dim Totalweekrate As Double
        Dim TotalDayrate As Double
        Dim WeekDiscount As Double
        Dim cxml As String

        'Checkbox List
        Dim chkextras As New CheckBoxList
        Dim lstItem As ListItem
        Dim chkExtra As CheckBox
        Dim ddlExtra As DropDownList
        Dim quantity As DropDownList
        Dim extraQty As Integer

        Dim lstExtra As New List(Of ExtraRate)
        '  Dim cxml As String
        For Each item As RepeaterItem In rpExtras.Items
            chkExtra = item.FindControl("chkExtra")
            quantity = item.FindControl("txtQuantity")

            '  ddlExtra = item.FindControl("ddlExtra")

            If (Not chkExtra Is Nothing) And chkExtra.Checked Then

                lstItem = New ListItem(chkExtra.Text, chkExtra.ToolTip)
                lstItem.Selected = chkExtra.Checked
                chkextras.Items.Add(lstItem)
                lstExtra.Add(New ExtraRate(chkExtra.ToolTip, 0, Convert.ToInt16(quantity.SelectedItem.Value), ""))
                extraQty = quantity.Text
                ' lstExtra.Add(New ExtraRate(chkExtra.ToolTip, 0, Convert.ToInt16("1")))
            End If


        Next
        If htRentalParam Is Nothing Then
            Response.Redirect("SessionExpired.aspx")
        Else
            'Trace.Write("pickupdate" & pickupdate.Substring(0, 8).Insert(4, "/").Insert(2, "/"))
        End If

        pickupdate = Request.QueryString("pudt")
        dropoffdatedate = Request.QueryString("dodt")


        pickupdate = pr.processDateString(pickupdate) + " " + pickuptime
        dropoffdatedate = pr.processDateString(dropoffdatedate) + " " + dropofftime


        'dropoffdatedate = pr.processDateString(Trim(Request.QueryString("dodt"))) & " " & Me.Request.QueryString("dotime")

        'pickupdate = pr.processDateString(Trim(Request.QueryString("pudt"))) & " " & Me.Request.QueryString("putime")

        xml = RequestMaker.MakeRateRequest(RequestMaker.RateRequestByExtras(pickupdate, _
                                         dropoffdatedate, Request.QueryString("classCode"), _
                                         Request.QueryString("rateCode"), Me.Session.SessionID, _
                                         Me.Request.UserHostAddress, Request.QueryString("ploc"), Request.QueryString("rloc"), Request.QueryString("rateID"), chkextras, Request.QueryString("disc"), Request.QueryString("prepaid")))

        cxml = RequestMaker.MakeRateRequest(RequestMaker.RateRequestClass(Request.QueryString("classCode")))


        Dim doc As XDocument = XDocument.Parse(xml)

        Dim p = From item In doc.Descendants("DailyExtra") _
     Select ExtraDesc = item.Element("ExtraDesc").Value, ExtraAmount = item.Element("ExtraAmount").Value, ExtraCode = item.Element("ExtraCode").Value
        Dim q = From i In p _
               Join t In lstExtra On i.ExtraCode Equals t.ExtraCode _
               Select ExtraCode = i.ExtraCode, ExtraAmount = i.ExtraAmount, ExtraDesc = i.ExtraDesc, Qty = t.Qty

        Dim classdoc As XDocument = XDocument.Parse(cxml)

        ratediscount = doc...<RateDiscount>.Value
        'Dim Rdays As String = doc...<RentalDays>.Value
        'Session("ReantalDays") = Rdays.ToString()


        Dim carModel As String() = classdoc...<SimilarText>.Value.Split(New String() {"or"}, StringSplitOptions.None)
        Dim carModelName As String = carModel(0).TrimEnd()

        'doc = XDocument.Load(xml)

        Dim rPeriod As New StringBuilder()
        Dim CarInfo As New StringBuilder()
        Dim htmlContent As New StringBuilder()
        Dim TotalCost As New StringBuilder()
        Dim Submitbtn As New StringBuilder()

        '-------------------- TITLE & IMAGES ----------------------------------
        CarInfo.AppendLine("<h2>" + carModelName + "<!--br><small>or Similar</small--></h2>")
        CarInfo.AppendLine("<center><img src='carimages/" + Request.QueryString("classCode") + ".jpg' style='max-width:300px;' alt='" + carModelName + "' title='" + carModelName + "' class='img-responsive' /></center> <br>")

        '------------------------- HEADER --------------------------------------

        htmlContent.AppendLine("<div class='col-xs-12' style='padding:10px 0px;'>")

        htmlContent.AppendLine("<div class='col-xs-12 detail-head' style='padding:10px 0px;'>")
        htmlContent.AppendLine("<div class='col-sm-5'>")
        htmlContent.AppendLine("<h3>Details</h3>")
        htmlContent.AppendLine("</div>")

        htmlContent.AppendLine("<div class='col-sm-3'>")
        htmlContent.AppendLine("<h3>Rate</h3>")
        htmlContent.AppendLine("</div>")

        htmlContent.AppendLine("<div class='col-sm-4'>")
        htmlContent.AppendLine("<h3>SubTotal</h3>")
        htmlContent.AppendLine("</div>")
        htmlContent.AppendLine("</div>")


        If CInt(doc...<Months>.Value) <> 0 Then

            '--------------------------- MONTH -------------------------------------------
            htmlContent.AppendLine("<div class='col-xs-12' style='padding:10px 0px;'>")
            htmlContent.AppendLine("<div class='col-sm-5'>")
            htmlContent.AppendLine(doc...<Months>.Value + " Month(s)")
            rPeriod.AppendLine(doc...<Months>.Value + " Month(s)")
            htmlContent.AppendLine("</div>")

            htmlContent.AppendLine("<div class='col-sm-3'>")
            htmlContent.AppendLine(Extentions.ChangeCurrency(doc...<AmtPerMonth>.Value))
            htmlContent.AppendLine("</div>")

            htmlContent.AppendLine("<div class='col-sm-4'>")
            htmlContent.AppendLine(Extentions.ChangeCurrency(CStr(CDbl(doc...<AmtPerMonth>.Value) * CInt(doc...<Months>.Value))))
            htmlContent.AppendLine("</div>")
            htmlContent.AppendLine("</div>")

        End If

        If CInt(doc...<Weeks>.Value) <> 0 Then

            Totalweekrate = CDbl(doc...<AmtPerWeek>.Value) * CInt(doc...<Weeks>.Value)

            '----------------------------- IF WEEKS ----------------------------------------
            htmlContent.AppendLine("<div class='col-xs-12' style='padding:10px 0px;'>")
            htmlContent.AppendLine("<div class='col-sm-5'>")
            htmlContent.AppendLine(doc...<Weeks>.Value + " Week(s)")
            rPeriod.AppendLine(doc...<Weeks>.Value + " Week(s)")
            htmlContent.AppendLine("</div>")

            htmlContent.AppendLine("<div class='col-sm-3'>")
            htmlContent.AppendLine(Extentions.ChangeCurrency(doc...<AmtPerWeek>.Value))
            htmlContent.AppendLine("</div>")

            htmlContent.AppendLine("<div class='col-sm-4'>")
            htmlContent.AppendLine(Extentions.ChangeCurrency(CStr(CDbl(doc...<AmtPerWeek>.Value) * CInt(doc...<Weeks>.Value))))
            htmlContent.AppendLine("</div>")
            htmlContent.AppendLine("</div>")

        End If

        If CInt(doc...<Rate1Days>.Value) <> 0 Then

            days = doc...<Rate1Days>.Value
            dayrate = doc...<Rate1PerDay>.Value
            TotalDayrate = dayrate * days

            '-------------------------- IF DAYS --------------------------------------
            htmlContent.AppendLine("<div class='col-xs-12' style='padding:10px 0px;'>")
            htmlContent.AppendLine("<div class='col-sm-5'>")
            htmlContent.AppendLine(doc...<Rate1Days>.Value + " Day(s)")
            rPeriod.AppendLine(doc...<Rate1Days>.Value + " Day(s)")
            htmlContent.AppendLine("</div>")

            htmlContent.AppendLine("<div class='col-sm-3'>")
            htmlContent.AppendLine(Extentions.ChangeCurrency(dayrate))
            htmlContent.AppendLine("/per day</div>")

            htmlContent.AppendLine("<div class='col-sm-4'>")
            htmlContent.AppendLine(Extentions.ChangeCurrency(TotalDayrate))
            htmlContent.AppendLine("</div>")
            htmlContent.AppendLine("</div>")

        End If

        If CInt(doc...<Hours>.Value) <> 0 Then

            '------------------------------- IF HOURS --------------------------------
            htmlContent.AppendLine("<div class='col-xs-12' style='padding:10px 0px;'>")
            htmlContent.AppendLine("<div class='col-sm-5'>")
            htmlContent.AppendLine(doc...<Hours>.Value + " Hours")
            rPeriod.AppendLine(doc...<Hours>.Value + " Hours")
            htmlContent.AppendLine("</div>")

            htmlContent.AppendLine("<div class='col-sm-3'>")
            htmlContent.AppendLine(Extentions.ChangeCurrency(doc...<AmtPerHour>.Value))
            htmlContent.AppendLine("</div>")

            htmlContent.AppendLine("<div class='col-sm-4'>")
            htmlContent.AppendLine(Extentions.ChangeCurrency(CStr(CDbl(doc...<AmtPerHour>.Value) * CInt(doc...<Hours>.Value))))
            htmlContent.AppendLine("</div>")
            htmlContent.AppendLine("</div>")

        End If

        If ratediscount <> 0 Then

            '------------------------- DISCOUNT ------------------------------
            htmlContent.AppendLine("<div class='col-xs-12' style='padding:10px 0px;'>")
            htmlContent.AppendLine("<div class='col-sm-5'>")
            htmlContent.AppendLine("Discount")
            htmlContent.AppendLine("</div>")

            htmlContent.AppendLine("<div class='col-sm-3'>")
            htmlContent.AppendLine(" ")
            htmlContent.AppendLine("</div>")

            htmlContent.AppendLine("<div class='col-sm-4'>")
            htmlContent.AppendLine(Extentions.ChangeCurrency(ratediscount))
            htmlContent.AppendLine("</div>")
            htmlContent.AppendLine("</div>")

        End If

        stotal = doc...<RateCharge>.Value + ratediscount

        '------------------------- TOTAL RENTAL RATE ----------------------------------
        'htmlContent.AppendLine("<div class='col-xs-12' style='padding:10px 0px;'>")
        'htmlContent.AppendLine("<div class='col-sm-5'>")
        'htmlContent.AppendLine("Total Rental Rate")
        'htmlContent.AppendLine("</div>")

        'htmlContent.AppendLine("<div class='col-sm-3'>")
        'htmlContent.AppendLine(" ")
        'htmlContent.AppendLine("</div>")

        'htmlContent.AppendLine("<div class='col-sm-4'>")
        'htmlContent.AppendLine(Extentions.ChangeCurrency(stotal))
        'htmlContent.AppendLine("</div>")
        'htmlContent.AppendLine("</div>")


        If doc...<Surcharge>.Value <> "" Then
            surcharge = doc...<Surcharge>.Value

            If stotal > 0 Then


                '-------------------------- SURCHARGE ------------------------------
                htmlContent.AppendLine("<div class='col-xs-12' style='padding:10px 0px;'>")
                htmlContent.AppendLine("<div class='col-sm-5'>")
                htmlContent.AppendLine("Surcharge")
                htmlContent.AppendLine("</div>")

            htmlContent.AppendLine("<div class='col-sm-3'>")
            htmlContent.AppendLine(" ")
            htmlContent.AppendLine("</div>")

            htmlContent.AppendLine("<div class='col-sm-4'>")
            htmlContent.AppendLine(doc...<Surcharge>.Value)
            htmlContent.AppendLine("</div>")
            htmlContent.AppendLine("</div>")
            End If

        End If

        Dim miles
        miles = doc...<TotalFreeMiles>.Value

        If miles = "0" Then
            miles = "UNLIMITED MILES*"
        Else
            miles = miles & " MILES"
        End If


        Dim promo As String
        Dim rq As New RequestMaker
        Dim prflag As Boolean
        prflag = False
        Dim extraCodes As String = ""

        Dim extraAmountWithQty As Integer = 0
        Dim extraAmountNew As Integer = 0
        Dim extra As ExtraRate
        Dim qtyNew As Integer = 0
        Dim btnText = String.Empty

        For Each item2 In p.ToList()
            extra = lstExtra.FirstOrDefault(Function(x) x.ExtraCode = item2.ExtraCode)
            If Not extra Is Nothing Then
                qtyNew = extra.Qty
                extraAmountWithQty += item2.ExtraAmount * qtyNew
                extraAmountNew += item2.ExtraAmount
            Else
                qtyNew = 1

            End If

            '----------------------- EXTRAS -------------------------------------------	
            htmlContent.AppendLine("<div class='col-xs-12' style='padding:10px 0px;'>")
            htmlContent.AppendLine("<div class='col-sm-5'>")
            htmlContent.AppendLine(item2.ExtraDesc)
            htmlContent.AppendLine("</div>")

            htmlContent.AppendLine("<div class='col-sm-3'>")
            htmlContent.AppendLine(" ")
            htmlContent.AppendLine("/per day</div>")

            htmlContent.AppendLine("<div class='col-sm-4'>")
            htmlContent.AppendLine(Extentions.ChangeCurrency((item2.ExtraAmount * qtyNew)))
            htmlContent.AppendLine("</div>")
            htmlContent.AppendLine("</div>")


            If Not String.IsNullOrEmpty(extraCodes) Then
                extraCodes &= "^"
            End If

            extraCodes &= item2.ExtraCode
        Next

        Trace.Write("ExtraCode" & extraCodes)
        Session("ExtraCode") = extraCodes

        ' -------------------- MILES ---------------------------------------
        'htmlContent.AppendLine("<div class='col-xs-12' style='padding:10px 0px;'>")
        'htmlContent.AppendLine("<div class='col-sm-5'>")
        'htmlContent.AppendLine(miles)
        'htmlContent.AppendLine("</div>")

        'htmlContent.AppendLine("<div class='col-sm-3'>")
        'htmlContent.AppendLine(" ")
        'htmlContent.AppendLine("</div>")

        'htmlContent.AppendLine("<div class='col-sm-4'>")
        'htmlContent.AppendLine("$0.00")
        'htmlContent.AppendLine("</div>")
        'htmlContent.AppendLine("</div>")


        '--------------------- TAX 1 ------------------------------------------
        htmlContent.AppendLine("<div class='col-xs-12' style='padding:10px 0px;'>")
        htmlContent.AppendLine("<div class='col-sm-5'>")
        htmlContent.AppendLine(doc...<Tax1Desc>.Value)
        htmlContent.AppendLine("</div>")

        htmlContent.AppendLine("<div class='col-sm-3'>")
        htmlContent.AppendLine(PercentVal((doc...<Tax1Rate>.Value)) + "%")
        htmlContent.AppendLine("</div>")

        htmlContent.AppendLine("<div class='col-sm-4'>")
        htmlContent.AppendLine(Extentions.ChangeCurrency(doc...<Tax1Charge>.Value))
        htmlContent.AppendLine("</div>")
        htmlContent.AppendLine("</div>")


        If doc...<Tax2Rate>.Value <> "" Then
            '--------------------- TAX 2 ------------------------------------------
            htmlContent.AppendLine("<div class='col-xs-12' style='padding:10px 0px;'>")
            htmlContent.AppendLine("<div class='col-sm-5'>")
            htmlContent.AppendLine(doc...<Tax2Desc>.Value)
            htmlContent.AppendLine("</div>")

            htmlContent.AppendLine("<div class='col-sm-3'>")
            htmlContent.AppendLine(Extentions.ChangeCurrency(doc...<Tax2Rate>.Value))
            htmlContent.AppendLine("/per day</div>")

            htmlContent.AppendLine("<div class='col-sm-4'>")
            htmlContent.AppendLine(Extentions.ChangeCurrency(doc...<Tax2Charge>.Value))
            htmlContent.AppendLine("</div>")
            htmlContent.AppendLine("</div>")

        End If

        If doc...<Tax3Rate>.Value <> "" Then
            '--------------------- TAX 3 ------------------------------------------
            htmlContent.AppendLine("<div class='col-xs-12' style='padding:10px 0px;'>")
            htmlContent.AppendLine("<div class='col-sm-5'>")
            htmlContent.AppendLine(doc...<Tax3Desc>.Value)
            htmlContent.AppendLine("</div>")

            htmlContent.AppendLine("<div class='col-sm-3'>")
            htmlContent.AppendLine(Extentions.ChangeCurrency(doc...<Tax3Rate>.Value))
            htmlContent.AppendLine("/per day</div>")

            htmlContent.AppendLine("<div class='col-sm-4'>")
            htmlContent.AppendLine(Extentions.ChangeCurrency(doc...<Tax3Charge>.Value))
            htmlContent.AppendLine("</div>")
            htmlContent.AppendLine("</div>")
        End If

        If doc...<Tax4Rate>.Value <> "" Then
            '--------------------- TAX 4 ------------------------------------------
            htmlContent.AppendLine("<div class='col-xs-12' style='padding:10px 0px;'>")
            htmlContent.AppendLine("<div class='col-sm-5'>")
            htmlContent.AppendLine(doc...<Tax4Desc>.Value)
            htmlContent.AppendLine("</div>")

            htmlContent.AppendLine("<div class='col-sm-2'>")
            htmlContent.AppendLine(Extentions.ChangeCurrency(doc...<Tax4Rate>.Value))
            htmlContent.AppendLine("</div>")

            htmlContent.AppendLine("<div class='col-sm-3'>")
            htmlContent.AppendLine(Extentions.ChangeCurrency(doc...<Tax4Charge>.Value))
            htmlContent.AppendLine("</div>")
            htmlContent.AppendLine("</div>")
        End If

        If doc...<Tax5Rate>.Value <> "" Then
            '--------------------- TAX 5 ------------------------------------------
            htmlContent.AppendLine("<div class='col-xs-12' style='padding:10px 0px;'>")
            htmlContent.AppendLine("<div class='col-sm-5'>")
            htmlContent.AppendLine(doc...<Tax5Desc>.Value)
            htmlContent.AppendLine("</div>")

            htmlContent.AppendLine("<div class='col-sm-3'>")
            htmlContent.AppendLine(Extentions.ChangeCurrency(doc...<Tax5Rate>.Value))
            htmlContent.AppendLine("</div>")

            htmlContent.AppendLine("<div class='col-sm-4'>")
            htmlContent.AppendLine(Extentions.ChangeCurrency(doc...<Tax5Charge>.Value))
            htmlContent.AppendLine("</div>")
            htmlContent.AppendLine("</div>")
        End If





        Dim totalcharge = doc...<TotalCharges>.Value + (extraAmountWithQty - extraAmountNew)

        Dim totalExtraAmount = (totalcharge - extraAmountNew)
        Session("totalExtraAmount") = totalExtraAmount
        ' Implement the Logic of Discount  Amount Here On the behalf of PrepaidType 
        If pnow.Checked = True Then
            Dim discopuntPayNow = Convert.ToDouble(ConfigurationManager.AppSettings("PayNowDiscount"))
            totalcharge = totalcharge - ((totalcharge * discopuntPayNow) / 100)
        End If


        ' Implement the Logic of Prepaid Amount Here On the behalf of PrepaidType QueryString
        Dim prepaidAmount = totalcharge
        htRentalParam = TryCast(Session("_htRentalParam"), Hashtable)

        If htRentalParam Is Nothing Then
        Else

            htRentalParam("PickupDate") = pickupdate
            htRentalParam("ClassCode") = Request.QueryString("classCode")
            htRentalParam("ClassDescription") = cls
            htRentalParam("DropoffDate") = dropoffdatedate
            htRentalParam("PickupLocation") = Request.QueryString("ploc")
            htRentalParam("DropoffLocation") = Request.QueryString("rloc")
            htRentalParam("TotalCharge") = totalcharge
            htRentalParam("PrepaidAmount") = totalcharge

        End If

        Dim qsr As String = ("?pudt=" + Request.QueryString("pudt") + "&putime=" & Request.QueryString("putime") & "&dodt=" + Request.QueryString("dodt") + "&dotime=" + Request.QueryString("dotime") + "&classCode=" + Request.QueryString("classCode") + "&rateCode=" + Request.QueryString("rateID") + "&rateID=" + Request.QueryString("rateID") & "&ploc=" + Request.QueryString("ploc") + "&rloc=" + Request.QueryString("rloc") + "&TotalCost=" & totalcharge & "&disc=" & Request.QueryString("disc") & "&totalExtraAmount=" & totalExtraAmount)

        If Request.QueryString("prepaid") = "y" Then
            qsr = qsr + "&prepaid=" & Request.QueryString("prepaid") & "&prepaidAmount=" & prepaidAmount
            btnText = "Continue"
        Else
            btnText = "Continue"
        End If
        If pnow.Checked = True Then
            qsr = qsr + "&paymentType=pnow"
        End If
        Dim html2 As New StringBuilder
        Dim html3 As New StringBuilder

        htmlContent.AppendLine("<div class='col-xs-12' style='padding:10px 0px;'>")
        htmlContent.AppendLine("<hr>")
        htmlContent.AppendLine("</div>")

        '--------------------- Total ------------------------------------------
        htmlContent.AppendLine("<div class='col-xs-12' style='padding:10px 0px;'>")
        htmlContent.AppendLine("<div class='col-sm-4'>")
        htmlContent.AppendLine("<h3>TOTAL:</h3>")
        htmlContent.AppendLine("</div>")

        htmlContent.AppendLine("<div class='col-sm-4'>")
        If Request.QueryString("prepaid") = "y" Then
            htmlContent.AppendLine("<h3>" & Extentions.ChangeCurrency(prepaidAmount) & "</h3>")
            TotalCost.AppendLine(Extentions.ChangeCurrency(prepaidAmount))
        Else
            htmlContent.AppendLine("<h3>" & Extentions.ChangeCurrency(totalcharge) & "</h3>")
            TotalCost.AppendLine(Extentions.ChangeCurrency(prepaidAmount))
        End If
        htmlContent.AppendLine("</div>")
        'Place prepaid reservation disclaimer here
        htmlContent.AppendLine("<div class='col-sm-4'>")

        htmlContent.AppendLine("<a id='key'   style='margin-top:14px;float:right' class='btn-next' href=""Reservation.aspx" + qsr + """>" + btnText + "</a>")
        Submitbtn.AppendLine("<a id='key'   class='bnt-continue' href=""Reservation.aspx" + qsr + """>" + btnText + "<i class='fa fa-angle-right'></i></a>")
        htmlContent.AppendLine("</div>")
        htmlContent.AppendLine("<div class='col-xs-12' style='padding:10px 0px;'>")
        If Request.QueryString("prepaid") = "y" Then
            htmlContent.AppendLine("<br/>By clicking ""Reserve"" you are pre-paying for your rental and your credit card will be charged once you confirm your reservation.""<br/>")

        End If
        htmlContent.AppendLine("</div>")
        'htmlContent.AppendLine("<div class='col-sm-4'>")
        'htmlContent.AppendLine("<a style='margin-top:14px;float:right' class='btn-next' href=""Reservation.aspx" + qsr + """>" + btnText + "</a>")
        ' htmlContent.AppendLine("<a style='margin-top:14px;' class='btn-next' href=""Dynamic_policies.aspx" + qsr + """>Policies</a>")

        'htmlContent.AppendLine("</div>")
        'htmlContent.AppendLine("<div class='col-xs-12' style='padding:10px 0px;'>")     /// Policy code 
        'htmlContent.AppendLine("<b>POLICIES</b>")
        'htmlContent.AppendLine("<div   class='plcy''  >" + polcytext + "</div>")
        'htmlContent.AppendLine("</div>")
        '------------------ END DIV ------------------------------------
        html2.AppendLine("</div>")


        Dim leftCon As New StringBuilder()
        '  Session("Detail") = htmlContent.ToString()
        '  Session("TotalCost") = TotalCost.ToString()

        Label1.Text = htmlContent.ToString()
        LabelcarInfo.Text = CarInfo.ToString()
        '   LabelRPeriod.Text = Rdays.ToString()

        labelTotal.Text = TotalCost.ToString()
		dim FullCost = doc...<TotalCharges>.Value + (extraAmountWithQty - extraAmountNew)
		labelTotal2.Text = "ARS " + FullCost.ToString("c")
		lblpynow.Text =	"ARS " + (totalcharge - (totalcharge * Convert.ToDouble(ConfigurationManager.AppSettings("PayNowDiscount")) / 100)).ToString("c")
		
		Dim RandomValue As Integer = CInt(Int((6 * Rnd()) + 1))
		
		If RandomValue = 1 Or 5 Then
		lblsalesfull.Text = "ARS " + (FullCost + (FullCost * .10)).ToString("c")
		Else If RandomValue = 2 Or 6 Then
		lblsalesfull.Text = "ARS " + (FullCost + (FullCost * .15)).ToString("c")
		Else
		lblsalesfull.Text = "ARS " + (FullCost + (FullCost * .05)).ToString("c")
		End If
		
		
        LabelsubBtn.Text = Submitbtn.ToString()

        lblcontent.Text = html2.ToString
        lblbook.Text = html3.ToString

        '  leftContent.Text = leftCon.ToString()
        DisplayVehicleInfo()

    End Sub

    Public Sub ShowRateDetails()
        Dim xstr

        Dim cls As String
        GetClassDetail(cls)

        'get all the extras
        xstr = RequestMaker.MakeRateRequest(RequestMaker.RequestExtras(Request.QueryString("ploc"), Request.QueryString("classcode")))

        Dim htRentalParam As Hashtable = Nothing
        htRentalParam = TryCast(Session("_htRentalParam"), Hashtable)

        Dim pickupdate, dropoffdatedate As String
        Dim pickuptime, dropofftime As String
        Dim pr As New RequestMaker
        Dim dayrate As Double
        Dim ratediscount As Double
        Dim surcharge As Double
        Dim IsPaynow As String

        pickuptime = Trim(Request.QueryString("putime"))
        dropofftime = Trim(Request.QueryString("dotime"))

        Dim days
        Dim frate
        Dim stotal As Double
        Dim Totalweekrate As Double
        Dim TotalDayrate As Double
        Dim WeekDiscount As Double
        Dim cxml As String

        'Checkbox List
        Dim chkextras As New CheckBoxList
        Dim lstItem As ListItem
        Dim chkExtra As CheckBox
        Dim ddlExtra As DropDownList
        Dim quantity As DropDownList
        Dim extraQty As Integer

        Dim lstExtra As New List(Of ExtraRate)
        '  Dim cxml As String
        For Each item As RepeaterItem In rpExtras.Items
            chkExtra = item.FindControl("chkExtra")
            quantity = item.FindControl("txtQuantity")

            '  ddlExtra = item.FindControl("ddlExtra")

            If (Not chkExtra Is Nothing) And chkExtra.Checked Then

                lstItem = New ListItem(chkExtra.Text, chkExtra.ToolTip)
                lstItem.Selected = chkExtra.Checked
                chkextras.Items.Add(lstItem)
                lstExtra.Add(New ExtraRate(chkExtra.ToolTip, 0, Convert.ToInt16(quantity.SelectedItem.Value), ""))
                extraQty = quantity.Text
                ' lstExtra.Add(New ExtraRate(chkExtra.ToolTip, 0, Convert.ToInt16("1")))
            End If


        Next
        If htRentalParam Is Nothing Then
            Response.Redirect("SessionExpired.aspx")
        Else
            'Trace.Write("pickupdate" & pickupdate.Substring(0, 8).Insert(4, "/").Insert(2, "/"))
        End If

        pickupdate = Request.QueryString("pudt")
        dropoffdatedate = Request.QueryString("dodt")


        pickupdate = pr.processDateString(pickupdate) + " " + pickuptime
        dropoffdatedate = pr.processDateString(dropoffdatedate) + " " + dropofftime


        'dropoffdatedate = pr.processDateString(Trim(Request.QueryString("dodt"))) & " " & Me.Request.QueryString("dotime")

        'pickupdate = pr.processDateString(Trim(Request.QueryString("pudt"))) & " " & Me.Request.QueryString("putime")

        xml = RequestMaker.MakeRateRequest(RequestMaker.RateRequestByExtras(pickupdate, _
                                         dropoffdatedate, Request.QueryString("classCode"), _
                                         Request.QueryString("rateCode"), Me.Session.SessionID, _
                                         Me.Request.UserHostAddress, Request.QueryString("ploc"), Request.QueryString("rloc"), Request.QueryString("rateID"), chkextras, Request.QueryString("disc"), Request.QueryString("prepaid")))

        cxml = RequestMaker.MakeRateRequest(RequestMaker.RateRequestClass(Request.QueryString("classCode")))


        Dim doc As XDocument = XDocument.Parse(xml)

        Dim p = From item In doc.Descendants("DailyExtra") _
     Select ExtraDesc = item.Element("ExtraDesc").Value, ExtraAmount = item.Element("ExtraAmount").Value, ExtraCode = item.Element("ExtraCode").Value
        Dim q = From i In p _
               Join t In lstExtra On i.ExtraCode Equals t.ExtraCode _
               Select ExtraCode = i.ExtraCode, ExtraAmount = i.ExtraAmount, ExtraDesc = i.ExtraDesc, Qty = t.Qty

        Dim classdoc As XDocument = XDocument.Parse(cxml)

        ratediscount = doc...<RateDiscount>.Value
        Dim Rdays As String = doc...<RentalDays>.Value
        Session("ReantalDays") = Rdays.ToString()


        Dim carModel As String() = classdoc...<SimilarText>.Value.Split(New String() {"or"}, StringSplitOptions.None)
        Dim carModelName As String = carModel(0).TrimEnd()

        'doc = XDocument.Load(xml)

        Dim rPeriod As New StringBuilder()
        Dim CarInfo As New StringBuilder()
        Dim htmlContent As New StringBuilder()
        Dim htmlContentExTraCharges As New StringBuilder()
        Dim TotalCost As New StringBuilder()
        Dim Submitbtn As New StringBuilder()

        '-------------------- TITLE & IMAGES ----------------------------------
        CarInfo.AppendLine("<h2>" + carModelName + "<!--br><small>or Similar</small--></h2>")
        CarInfo.AppendLine("<center><img src='carimages/" + Request.QueryString("classCode") + ".jpg' style='max-width:300px;' alt='" + carModelName + "' title='" + carModelName + "' class='img-responsive' /></center> <br>")

        '------------------------- HEADER --------------------------------------


        If CInt(doc...<Months>.Value) <> 0 Then

            '--------------------------- MONTH -------------------------------------------
            htmlContent.AppendLine("<tr>")
            htmlContent.AppendLine("<td class='col-sm-5'>")
            htmlContent.AppendLine(doc...<Months>.Value + " MONTH(s)")
            rPeriod.AppendLine(doc...<Months>.Value + " MONTH(s)")
            htmlContent.AppendLine("</td>")

            htmlContent.AppendLine("<td class='col-sm-4'>")
            htmlContent.AppendLine(Extentions.ChangeCurrency(CStr(CDbl(doc...<AmtPerMonth>.Value) * CInt(doc...<Months>.Value))))
            htmlContent.AppendLine("</td>")
            htmlContent.AppendLine("</tr>")

        End If

        If CInt(doc...<Weeks>.Value) <> 0 Then

            Totalweekrate = CDbl(doc...<AmtPerWeek>.Value) * CInt(doc...<Weeks>.Value)

            '----------------------------- IF WEEKS ----------------------------------------
            htmlContent.AppendLine("<tr>")
            htmlContent.AppendLine("<td class='col-sm-5'>")
            htmlContent.AppendLine(doc...<Weeks>.Value + " WEEK(s)")
            rPeriod.AppendLine(doc...<Weeks>.Value + " WEEK(s)")
            htmlContent.AppendLine("</td>")

            htmlContent.AppendLine("<td class='col-sm-4'>")
            htmlContent.AppendLine(Extentions.ChangeCurrency(CStr(CDbl(doc...<AmtPerWeek>.Value) * CInt(doc...<Weeks>.Value))))
            htmlContent.AppendLine("</td>")
            htmlContent.AppendLine("</tr>")

        End If

        If CInt(doc...<Rate1Days>.Value) <> 0 Then

            days = doc...<Rate1Days>.Value
            dayrate = doc...<Rate1PerDay>.Value
            TotalDayrate = dayrate * days

            '-------------------------- IF DAYS --------------------------------------
            htmlContent.AppendLine("<tr>")
            htmlContent.AppendLine("<td class='col-sm-5'>")
            htmlContent.AppendLine(doc...<Rate1Days>.Value + " DAY(s)")
            rPeriod.AppendLine(doc...<Rate1Days>.Value + " DAY(s)")
            htmlContent.AppendLine("</td>")


            htmlContent.AppendLine("<td class='col-sm-4'>")
            htmlContent.AppendLine(Extentions.ChangeCurrency(CStr(TotalDayrate)))
            htmlContent.AppendLine("</td>")
            htmlContent.AppendLine("</tr>")

        End If

        If CInt(doc...<Hours>.Value) <> 0 Then

            '------------------------------- IF HOURS --------------------------------
            htmlContent.AppendLine("<tr>")
            htmlContent.AppendLine("<td class='col-sm-5'>")
            htmlContent.AppendLine(doc...<Hours>.Value + " HOURS")
            rPeriod.AppendLine(doc...<Hours>.Value + " HOURS")
            htmlContent.AppendLine("</td>")


            htmlContent.AppendLine("<td class='col-sm-4'>")
            htmlContent.AppendLine(Extentions.ChangeCurrency(CStr(CDbl(doc...<AmtPerHour>.Value) * CInt(doc...<Hours>.Value))))
            htmlContent.AppendLine("</td>")
            htmlContent.AppendLine("</tr>")

            '------------------------- DISCOUNT ------------------------------
            htmlContent.AppendLine("<tr>")
            htmlContent.AppendLine("<td class='col-sm-5'>")
            htmlContent.AppendLine("Discount")
            htmlContent.AppendLine("</td>")



            htmlContent.AppendLine("<td class='col-sm-4'>")
            htmlContent.AppendLine(Extentions.ChangeCurrency(ratediscount.ToString))
            htmlContent.AppendLine("</td>")
            htmlContent.AppendLine("</tr>")

        End If

        stotal = doc...<RateCharge>.Value + ratediscount

        '------------------------- TOTAL RENTAL RATE ----------------------------------
        'htmlContent.AppendLine("<tr>")
        'htmlContent.AppendLine("<td class='col-sm-5'>")
        'htmlContent.AppendLine("Total Rental Rate")
        'htmlContent.AppendLine("</td>")



        'htmlContent.AppendLine("<td class='col-sm-4'>")
        'htmlContent.AppendLine(Extentions.ChangeCurrency(stotal))
        'htmlContent.AppendLine("</td>")
        'htmlContent.AppendLine("</tr>")


        If doc...<Surcharge>.Value <> "" Then
            surcharge = doc...<Surcharge>.Value

            If surcharge > 0 Then
                '-------------------------- SURCHARGE ------------------------------
                htmlContent.AppendLine("<tr>")
                htmlContent.AppendLine("<td class='col-sm-5'>")
                htmlContent.AppendLine("Surcharge")
                htmlContent.AppendLine("</td>")



                htmlContent.AppendLine("<td class='col-sm-4'>")
                htmlContent.AppendLine(Extentions.ChangeCurrency(surcharge))
                htmlContent.AppendLine("</td>")
                htmlContent.AppendLine("</tr>")
            End If
        End If

        Dim miles
        miles = doc...<TotalFreeMiles>.Value

        If miles = "0" Then
            miles = "UNLIMITED MILES*"
        Else
            miles = miles & " MILES"
        End If


        Dim promo As String
        Dim rq As New RequestMaker
        Dim prflag As Boolean
        prflag = False
        Dim extraCodes As String = ""

        Dim extraAmountWithQty As Integer = 0
        Dim extraAmountNew As Integer = 0
        Dim extra As ExtraRate
        Dim qtyNew As Integer = 0
        Dim btnText = String.Empty

        For Each item2 In p.ToList()
            extra = lstExtra.FirstOrDefault(Function(x) x.ExtraCode = item2.ExtraCode)
            If Not extra Is Nothing Then
                qtyNew = extra.Qty
                extraAmountWithQty += item2.ExtraAmount * qtyNew
                extraAmountNew += item2.ExtraAmount
            Else
                qtyNew = 1

            End If

            '----------------------- EXTRAS -------------------------------------------	


            'htmlContent.AppendLine("<tr>")
            'htmlContent.AppendLine("<tr>")
            'htmlContent.AppendLine("<td class='col-sm-5'>")
            'htmlContent.AppendLine(item2.ExtraDesc)
            'htmlContent.AppendLine("</td>")

            'htmlContentExTraCharges.AppendLine("<tr>")
            htmlContentExTraCharges.AppendLine("<tr>")
            htmlContentExTraCharges.AppendLine("<td class='col-sm-5'>")
            htmlContentExTraCharges.AppendLine(item2.ExtraDesc)
            htmlContentExTraCharges.AppendLine("</td>")

            'htmlContent.AppendLine("<td class='col-sm-4'>")
            'htmlContent.AppendLine((item2.ExtraAmount * qtyNew).ToString("C"))
            'htmlContent.AppendLine("</td>")
            'htmlContent.AppendLine("</tr>")

            htmlContentExTraCharges.AppendLine("<td class='col-sm-4'>")
            htmlContentExTraCharges.AppendLine(Extentions.ChangeCurrency(item2.ExtraAmount * qtyNew))
            htmlContentExTraCharges.AppendLine("</td>")
            htmlContentExTraCharges.AppendLine("</tr>")

            If Not String.IsNullOrEmpty(extraCodes) Then
                extraCodes &= "^"
            End If

            extraCodes &= item2.ExtraCode
        Next

        Trace.Write("ExtraCode" & extraCodes)
        Session("ExtraCode") = extraCodes

        ' -------------------- MILES ---------------------------------------

        'htmlContent.AppendLine("<tr>")
        'htmlContent.AppendLine("<td class='col-sm-5'>")
        'htmlContent.AppendLine(miles)
        'htmlContent.AppendLine("</td>")



        'htmlContent.AppendLine("<td class='col-sm-4'>")
        'htmlContent.AppendLine("$0.00")
        'htmlContent.AppendLine("</td>")
        'htmlContent.AppendLine("</tr>")

        


        '--------------------- TAX 1 ------------------------------------------
        htmlContent.AppendLine("<tr>")
        htmlContent.AppendLine("<td class='col-sm-5'>")
        htmlContent.AppendLine(doc...<Tax1Desc>.Value)
        htmlContent.AppendLine("</td>")


        htmlContent.AppendLine("<td class='col-sm-4'>")
        htmlContent.AppendLine(Extentions.ChangeCurrency(doc...<Tax1Charge>.Value))
        htmlContent.AppendLine("</td>")
        htmlContent.AppendLine("</tr>")


        If doc...<Tax2Rate>.Value <> "" Then
            '--------------------- TAX 2 ------------------------------------------
            htmlContent.AppendLine("<tr>")
            htmlContent.AppendLine("<td class='col-sm-5'>")
            htmlContent.AppendLine(doc...<Tax2Desc>.Value)
            htmlContent.AppendLine("</td>")



            htmlContent.AppendLine("<td class='col-sm-4'>")
            htmlContent.AppendLine(Extentions.ChangeCurrency(doc...<Tax2Charge>.Value))
            htmlContent.AppendLine("</td>")
            htmlContent.AppendLine("</tr>")

        End If

        If doc...<Tax3Rate>.Value <> "" Then
            '--------------------- TAX 3 ------------------------------------------
            htmlContent.AppendLine("<tr>")
            htmlContent.AppendLine("<td class='col-sm-5'>")
            htmlContent.AppendLine(doc...<Tax3Desc>.Value)
            htmlContent.AppendLine("</td>")



            htmlContent.AppendLine("<td class='col-sm-4'>")
            htmlContent.AppendLine(Extentions.ChangeCurrency(doc...<Tax3Charge>.Value))
            htmlContent.AppendLine("</td>")
            htmlContent.AppendLine("</tr>")
        End If

        If doc...<Tax4Rate>.Value <> "" Then
            '--------------------- TAX 4 ------------------------------------------
            htmlContent.AppendLine("<tr>")
            htmlContent.AppendLine("<td class='col-sm-5'>")
            htmlContent.AppendLine(doc...<Tax4Desc>.Value)
            htmlContent.AppendLine("</td>")



            htmlContent.AppendLine("<td class='col-sm-3'>")
            htmlContent.AppendLine(Extentions.ChangeCurrency(doc...<Tax4Charge>.Value))
            htmlContent.AppendLine("</td>")
            htmlContent.AppendLine("</tr>")
        End If

        If doc...<Tax5Rate>.Value <> "" Then
            '--------------------- TAX 5 ------------------------------------------
            htmlContent.AppendLine("<tr>")
            htmlContent.AppendLine("<td class='col-sm-5'>")
            htmlContent.AppendLine(doc...<Tax5Desc>.Value)
            htmlContent.AppendLine("</td>")


            htmlContent.AppendLine("<td class='col-sm-4'>")
            htmlContent.AppendLine(Extentions.ChangeCurrency(doc...<Tax5Charge>.Value))
            htmlContent.AppendLine("</td>")
            htmlContent.AppendLine("</tr>")
        End If
		
		Dim totalcharge = doc...<TotalCharges>.Value + (extraAmountWithQty - extraAmountNew)
		
        If pnow.Checked = True Then
            ' -------------------- Pay now discount ---------------------------------------
            htmlContent.AppendLine("<tr>")
            htmlContent.AppendLine("<td class='col-sm-5'>")
            htmlContent.AppendLine("Pay now discount")
            htmlContent.AppendLine("</td>")



            htmlContent.AppendLine("<td class='col-sm-4'>")
            htmlContent.AppendLine(Extentions.ChangeCurrency(totalcharge * Convert.ToDouble(ConfigurationManager.AppSettings("PayNowDiscount")) / 100))
            htmlContent.AppendLine("</td>")
            htmlContent.AppendLine("</tr>")
        End If
        
        If pnow.Checked = True Then
            Dim discopuntPayNow = Convert.ToDouble(ConfigurationManager.AppSettings("PayNowDiscount"))
            totalcharge = totalcharge - ((totalcharge * discopuntPayNow) / 100)
        End If
        ' Implement the Logic of Prepaid Amount Here On the behalf of PrepaidType QueryString
        Dim prepaidAmount = totalcharge
        htRentalParam = TryCast(Session("_htRentalParam"), Hashtable)

        If htRentalParam Is Nothing Then
        Else

            htRentalParam("PickupDate") = pickupdate
            htRentalParam("ClassCode") = Request.QueryString("classCode")
            htRentalParam("ClassDescription") = cls
            htRentalParam("DropoffDate") = dropoffdatedate
            htRentalParam("PickupLocation") = Request.QueryString("ploc")
            htRentalParam("DropoffLocation") = Request.QueryString("rloc")
            htRentalParam("TotalCharge") = totalcharge
            htRentalParam("PrepaidAmount") = totalcharge

        End If

        Dim qsr As String = ("?pudt=" + Request.QueryString("pudt") + "&putime=" & Request.QueryString("putime") & "&dodt=" + Request.QueryString("dodt") + "&dotime=" + Request.QueryString("dotime") + "&classCode=" + Request.QueryString("classCode") + "&rateCode=" + Request.QueryString("rateID") + "&rateID=" + Request.QueryString("rateID") & "&ploc=" + Request.QueryString("ploc") + "&rloc=" + Request.QueryString("rloc") + "&TotalCost=" & totalcharge & "&disc=" & Request.QueryString("disc"))
        If pnow.Checked = True Then
            qsr = qsr + "&paymentType=pnow"
        End If

        'If Request.QueryString("prepaid") = "y" Then
        '    qsr = qsr + "&prepaid=" & Request.QueryString("prepaid") & "&prepaidAmount=" & prepaidAmount
        '    btnText = "Proceed to Pay"
        'Else
        '    btnText = "Confirm Reservation"
        'End If


        Dim html2 As New StringBuilder
        Dim html3 As New StringBuilder

        htmlContent.AppendLine("<div class='col-xs-12' style='padding:10px 0px;'>")
        htmlContent.AppendLine("<hr>")
        htmlContent.AppendLine("</div>")


        'Place prepaid reservation disclaimer here
        htmlContent.AppendLine("<div class='col-sm-4'>")

        htmlContent.AppendLine("<a style='margin-top:14px;float:right' class='btn-next'  href=""Reservation.aspx" + qsr + """>" + btnText + "</a>")
        Submitbtn.AppendLine("<a class='bnt-continue'  href=""Reservation.aspx" + qsr + """>" + btnText + "<i class='fa fa-angle-right'></i></a>")
        htmlContent.AppendLine("</div>")
        htmlContent.AppendLine("<div class='col-xs-12' style='padding:10px 0px;'>")
        If Request.QueryString("prepaid") = "y" Then
            htmlContent.AppendLine("<br/>By clicking ""Reserve"" you are pre-paying for your rental and your credit card will be charged once you confirm your reservation.""<br/>")

        End If
        htmlContent.AppendLine("</div>")
        'htmlContent.AppendLine("<div class='col-sm-4'>")
        'htmlContent.AppendLine("<a style='margin-top:14px;float:right' class='btn-next' href=""Reservation.aspx" + qsr + """>" + btnText + "</a>")
        ' htmlContent.AppendLine("<a style='margin-top:14px;' class='btn-next' href=""Dynamic_policies.aspx" + qsr + """>Policies</a>")

        'htmlContent.AppendLine("</div>")
        'htmlContent.AppendLine("<div class='col-xs-12' style='padding:10px 0px;'>")     /// Policy code 
        'htmlContent.AppendLine("<b>POLICIES</b>")
        'htmlContent.AppendLine("<div   class='plcy''  >" + polcytext + "</div>")
        'htmlContent.AppendLine("</div>")
        '------------------ END DIV ------------------------------------
        html2.AppendLine("</div>")


        Dim leftCon As New StringBuilder()
        Session("Detail") = htmlContent.ToString()
        Session("ExtraChargeDetail") = htmlContentExTraCharges.ToString()
        Session("TotalCost") = TotalCost.ToString()
        lblRateDetails.Text = Session("Detail")
        lblExTraCharges.Text = Session("ExtraChargeDetail")
        lblcost.Text = Extentions.ChangeCurrency(htRentalParam("TotalCharge"))
        Label1.Text = htmlContent.ToString()
        LabelcarInfo.Text = CarInfo.ToString()
        LabelRPeriod.Text = Rdays.ToString()

        labelTotal.Text = TotalCost.ToString()
        labelTotal2.Text = doc...<TotalCharges>.Value + (extraAmountWithQty - extraAmountNew)
		'lblpynow.Text =	totalcharge - (totalcharge * Convert.ToDouble(ConfigurationManager.AppSettings("PayNowDiscount")) / 100))	
        LabelsubBtn.Text = Submitbtn.ToString()

        lblcontent.Text = html2.ToString
        lblbook.Text = html3.ToString

        '  leftContent.Text = leftCon.ToString()
        DisplayVehicleInfo()

    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not Page.IsPostBack Then
            LoadDynamicPolicy()
            Dim q As IEnumerable = From item In RequestMaker.GetExtras(RequestMaker.RequestExtras(Request.QueryString("ploc"), Request.QueryString("Classcode"))) _
                                  Select ExtraCode = item.ExtraCode, ExtraDesc = item.ExtraDesc, _
                                  HasDDL = (New String() {"ADDDR", "KST2", "KST1", "USA"}).Any(Function(i) i = item.ExtraCode)

            Dim p As IEnumerable = From item In RequestMaker.GetExtras2(RequestMaker.RequestExtras(Request.QueryString("ploc"), Request.QueryString("Classcode"))) _
                                Select ExtraCode = item.ExtraCode, ExtraDesc = item.ExtraDesc, ExtraDesc2 = item.ExtraDesc2, _
                                HasDDL = (New String() {"ADDDR", "KST2", "KST1", "USA"}).Any(Function(i) i = item.ExtraCode)

            rpExtras.DataSource = p
            rpExtras.DataBind()
            ShowRateDetails()
            LoadRateDetails()

        End If
    End Sub

    Public Sub DisplayVehicleInfo()
        Dim pickupdate, dropoffdatedate As String
        Dim pickuptime, dropofftime As String
        Dim ploc As String
        Dim dloc As String
        Dim loc As New RequestMaker


        ploc = Trim(Request.QueryString("ploc"))
        dloc = Trim(Request.QueryString("rloc"))

        lblploc.Text = loc.GetLocation(ploc)
        lbldloc.Text = loc.GetLocation(dloc)
        pickuptime = Request.QueryString("putime")
        dropofftime = Request.QueryString("dotime")
        pickupdate = Trim(Request.QueryString("pudt"))
        dropoffdatedate = Trim(Request.QueryString("dodt"))
        'pickupdate = pickupdate.Insert(2, "/").Insert(5, "/")
        'dropoffdatedate = dropoffdatedate.Insert(2, "/").Insert(5, "/")
		
        lblptime.Text = pickupdate
        lbldtime.Text = dropoffdatedate

		
		dim PDatex As String = Trim(Request.QueryString("pudt"))
		dim PTimex As String = pickuptime.Insert(5, ":")
		dim PDatexDAr() As String = Split(PDatex, "/")
		dim PDatexTAr() As String = Split(PTimex, ":")
		Dim PDatexDate As New System.DateTime(PDatexDAr(2), PDatexDAr(0), PDatexDAr(1), PDatexTAr(0), PDatexTAr(1), 0) 
			
		lblptime.Text = PDatexDate.ToString("g")
		
		dim DDatex As String = Trim(Request.QueryString("dodt"))
		dim DTimex As String =  dropofftime.Insert(5, ":")
		dim DDatexDAr() As String = Split(DDatex, "/")
		dim DDatexTAr() As String = Split(DTimex, ":")
		Dim DDatexDate As New System.DateTime(DDatexDAr(2), DDatexDAr(0), DDatexDAr(1), DDatexTAr(0), DDatexTAr(1), 0) 

        lbldtime.Text =  DDatexDate.ToString("g")

        Dim address, city, state, zip, phone
        loc.GetAddress(ploc, address, city, state, zip, phone)

        'lbladdress.Text = address
        'lblcity.Text = city
        'lblstate.Text = state
        'lblzip.Text = zip
        'lblphone.Text = phone


        loc.GetAddress(dloc, address, city, state, zip, phone)

        'lbldaddress.Text = address
        'lbldcity.Text = city
        'lbldstate.Text = state
        'lbldzip.Text = zip
        'lbldphone.Text = phone


        '    lblcode.Text = RequestMaker.Classes(Request.QueryString("classCode"))
    End Sub



    'Protected Sub lnkChange_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkChange.Click
    'Dim vh As New RequestMaker
    'vh.RetainVehicleInfo()
    'End Sub

    Public Function PercentVal(ByVal val) As String
        Dim Pval As Decimal
        Pval = val * 100
        Dim str As String

        str = Pval.ToString("N2")
        Return str

    End Function

    'Protected Sub lblvchange_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lblvchange.Click
    'Dim vh As New RequestMaker
    'vh.ChangeVehicle()
    'End Sub

    Public Function DisplayDisc(cls As String) As String

        Dim DisStr As String
        Dim filename As String
        Dim filemiles As String
        regularlst.AddRange(regular)
        exoticlst.AddRange(exotic)
        luxurylst.AddRange(luxury)

        If regularlst.Contains(cls) = True Then
            filename = "regular.htm"
            ' filemiles = "regular-miles.htm"
        ElseIf exoticlst.Contains(cls) = True Then
            filename = "exotic.htm"
            ' filemiles = "exotic-miles.htm"
        ElseIf luxurylst.Contains(cls) = True Then
            filename = "Luxury.htm"
            '  filemiles = "Luxury-miles.htm"
        End If

        '  Trace.Write("DISTR" & DisStr.ToString)
        DisStr = GetContent(filename)
        Trace.Write("DISTR" & DisStr.ToString)
        Return DisStr.ToString

    End Function

    Public Function DisplayMiles(cls) As String
        Dim DisStr As String
        Dim filename As String
        Dim filemiles As String
        regularlst.AddRange(regular)
        exoticlst.AddRange(exotic)
        luxurylst.AddRange(luxury)

        If regularlst.Contains(cls) = True Then
            filemiles = "Regular-miles.htm"
        ElseIf exoticlst.Contains(cls) = True Then
            filemiles = "Exotic-miles.htm"
        ElseIf luxurylst.Contains(cls) = True Then
            filemiles = "Luxury-miles.htm"
        End If

        '  Trace.Write("DISTR" & DisStr.ToString)
        DisStr = GetContent(filemiles)
        Trace.Write("DISTR" & DisStr.ToString)
        Return DisStr.ToString

    End Function

    Public Function GetContent(filename As String) As String
        Dim fileContent As String = ""
        Dim fileRead As StreamReader = Nothing

        If File.Exists(Server.MapPath("Disclaimers\" & filename)) Then
            fileRead = New StreamReader(HttpContext.Current.Server.MapPath("Disclaimers\" & filename))
            fileContent = fileRead.ReadToEnd()
            fileRead.Close()
        End If



        Trace.Write("filecontnet" & fileContent.ToString)
        Return fileContent.ToString

    End Function

    Protected Sub chkExtra_OnCheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        ShowRateDetails()
        LoadRateDetails()
    End Sub

    Protected Overrides Sub InitializeCulture()
        Culture = Common.MyCulture

        'set culture to current thread
        Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(Common.MyCulture)
        Thread.CurrentThread.CurrentUICulture = New CultureInfo(Common.MyCulture)

        'call base class
        MyBase.InitializeCulture()
    End Sub

    Protected Sub Page_PreInit(sender As Object, e As System.EventArgs) Handles Me.PreInit
        If Common.MyCulture = "en-US" Then
            Me.Page.MasterPageFile = "MastarList.master"
        ElseIf Common.MyCulture = "es-US" Then
            Me.Page.MasterPageFile = "MastarList.master"
        Else
            Me.Page.MasterPageFile = "MastarList.master"
        End If
    End Sub
    Public Sub GetClassDetail(ByRef cls As String)
        Dim xml2 As String
        xml2 = RequestMaker.MakeRateRequest(RequestMaker.RequestClass(Request.QueryString("classCode")))
        Dim doc As XDocument = XDocument.Parse(xml2)
        Dim luggage, capacity, make
        cls = doc...<ClassDescription>.Value
        luggage = doc...<LuggageCount>.Value
        capacity = doc...<PassengerCount>.Value
        make = doc...<SimilarText>.Value

    End Sub

    Protected Sub DropDownList1_SelectedIndexChanged(sender As Object, e As EventArgs)
        Dim chkextras As New CheckBoxList
        Dim lstItem As ListItem
        Dim chkExtra As CheckBox
        Dim quantity As DropDownList

        For Each item As RepeaterItem In rpExtras.Items
            chkExtra = item.FindControl("chkExtra")
            quantity = item.FindControl("txtQuantity")
            'item.ItemIndex


            'chkExtra.Checked = True
            If (Not chkExtra Is Nothing) And quantity.SelectedItem.Value <> "1" Then

                chkExtra.Checked = True
                ShowRateDetails()
                LoadRateDetails()

            End If


        Next
    End Sub

    'Function Created By  pradeep 26-08-2015
    Public Sub LoadDynamicPolicy()
        Dim _htRentalParam As Hashtable = Nothing
        _htRentalParam = TryCast(Session("_htRentalParam"), Hashtable)

        Dim pickupLocation As String = _htRentalParam("PickupLocation")
        Dim reqXML As String = RequestMaker.PolicyRequest(pickupLocation)
        Dim url As String = System.Configuration.ConfigurationManager.AppSettings("Url")

        Dim uri As String = url

        Dim req As WebRequest
        Dim rep As WebResponse


        req = WebRequest.Create(uri)
        req.Method = "POST"
        req.ContentType = "text/xml"

        Dim writer As New StreamWriter(req.GetRequestStream())
        writer.Write(reqXML)

        writer.Close()

        rep = req.GetResponse()

        Dim receivestream As Stream = rep.GetResponseStream()
        Dim encode As Encoding = Encoding.GetEncoding("utf-8")
        Dim readstream As New StreamReader(receivestream, encode)

        Dim read(256) As [Char]
        Dim result As String
        Dim Text As String
        Dim counter As Integer
        Dim Tcounter As Integer
        counter = 0
        Tcounter = 0
        result = readstream.ReadToEnd()
        Dim htmlContent As New StringBuilder()
        Dim doc As XDocument = XDocument.Parse(result)
        Dim p = From item In doc.Descendants("Policy")
        For Each item In p.ToList()
            Text += item.Element("PolicyText").Value + "</br></br>"

        Next


        polcytext = Text

    End Sub


    Protected Sub plater_CheckedChanged(sender As Object, e As EventArgs)
        If Page.IsPostBack = True Then
            ShowRateDetails()
            LoadRateDetails()

        End If

    End Sub

    Protected Sub pnow_CheckedChanged(sender As Object, e As EventArgs)
        If Page.IsPostBack = True Then
            ShowRateDetails()
            LoadRateDetails()
        End If
    End Sub
End Class
