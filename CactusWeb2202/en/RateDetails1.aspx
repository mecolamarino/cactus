<%@ Page Language="VB" AutoEventWireup="false" CodeFile="RateDetails1.aspx.vb" Inherits="RateDetails1" Trace="false" MasterPageFile="MastarList.master" Culture="auto" meta:resourcekey="PageResource1" UICulture="auto" %>

<%@ Register Src="Controls/address.ascx" TagName="address" TagPrefix="ad" %>
<asp:Content ID="Content3" ContentPlaceHolderID="head" runat="Server">
    <title>Rate Details</title>
    <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css">



    <style type="text/css">
        .plcy
        {
            height: 200px;
            overflow-x: auto;
        }
    </style>
</asp:Content>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="ContentPlaceHolder1">

    <section class="checkout padding-section row">

        <div class="col-sm-10 col-sm-offset-1">

            <div class="col-sm-4">
                <div class="side-bar">

                    <asp:Label ID="LabelcarInfo" runat="server" meta:resourcekey="LabelcarInfo"></asp:Label>
                    <div class="car-specs">
                    </div>

                    <div class="rental-information">

                        <table>
                            <thead>
                                <tr>
                                    <th>Rental Details</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>
                                        <span>Pick up Location:</span>
                                        <h3>
                                            <asp:Label ID="lblploc" runat="server" meta:resourcekey="lblplocResource1"></asp:Label></h3>
                                        <p>
                                            <asp:Label ID="lblpickupdateText" runat="server" meta:resourcekey="lblpickupdateTextResource1"></asp:Label>
                                            <asp:Label ID="lblptime" runat="server" meta:resourcekey="lblptimeResource1"></asp:Label>
                                        </p>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <span>Return Location:</span>
                                        <h3>
                                            <asp:Label ID="lbldloc" runat="server" meta:resourcekey="lbldlocResource1"></asp:Label></h3>
                                        <p>
                                            <asp:Label ID="lbldropffDatetext" runat="server" meta:resourcekey="lbldropffDatetextResource1"></asp:Label>
                                            <asp:Label ID="lbldtime" runat="server" meta:resourcekey="lbldtimeResource1"></asp:Label>
                                        </p>
                                    </td>
                                </tr>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>Rental Period:
                                        <asp:Label ID="LabelRPeriod" runat="server" meta:resourcekey="LabelRPeriod"></asp:Label>Days</th>
                                </tr>
                            </tfoot>
                        </table>


                    </div>

                    <div class="info">
                        <div class="info-cancel">
                            <figure>
                                <img src="/development/cactus/www/images/icon-1.png" alt="">
                            </figure>
                            <h3>Free Cancellation</h3>
                            <span>Modify or cancel your reservation at no cost before MONTH DAY YEAR</span>
                        </div>
                        <div class="info-attention">
                            <figure>
                                <img src="/development/cactus/www/images/icon-2.png" alt="">
                            </figure>
                            <h3>Minimum age of driver 21 years.
                            </h3>
                            <span>If the additional driver is between 18 and 21 years must pay an additional
                            </span>
                            <div class="info-attention-contact">
                                <ul>
                                    <li>
                                        <i class="fa fa-print"></i>
                                        <span>Print this reservation</span>
                                    </li>
                                    <li>
                                        <i class="fa fa-envelope"></i>
                                        <span>Share this offer</span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

            <!-------------------- ---------------------------->

            <div class="col-sm-8">
                <div class="detail-check">
                    <table class="detail-sevr">
                        <thead>
                            <tr>
                                <th>1.Rate</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>
                                    <asp:RadioButton AutoPostBack="true" runat="server" OnCheckedChanged="plater_CheckedChanged" Text="Pay at Destination" ID="plater" name="plater" value="plater" GroupName="pay-when" CssClass=""></asp:RadioButton>
                                    <figure>
                                        <a type="button" data-toggle="modal" data-target="#myModal">
                                            <img src="/development/cactus/www/images/icon-i.png" alt=""></a>
                                    </figure>
                                    <div class="modal fade" id="myModal" role="dialog" style="display: none;">
                                        <div class="modal-dialog">
                                            <!-- Modal content-->
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal">�</button>
                                                    <figure>
                                                        <img src="/development/cactus/www/images/icon-i-2.png" alt="">
                                                    </figure>
                                                    <h4 class="modal-title">Lorem Ipsum is</h4>
                                                </div>
                                                <div class="modal-body">
                                                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <p>
                                        per d&iacute;a: <span>ARS $435,34
                                        <br>
                                            <em>ARS $500 </em></span>
                                    </p>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <figure>
                                        <img src="/development/cactus/www/images/check.png" alt="">
                                    </figure>
                                    <span>Unlimited Millage
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <figure>
                                        <img src="/development/cactus/www/images/check.png" alt="">
                                    </figure>
                                    <span>Free amendments
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <figure>
                                        <img src="/development/cactus/www/images/check.png" alt="">
                                    </figure>
                                    <span>Free Cancellation
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <figure>
                                        <img src="/development/cactus/www/images/check.png" alt="">
                                    </figure>
                                    <span>Collision dammage Waiver (CDW)
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <figure>
                                        <img src="/development/cactus/www/images/check.png" alt="">
                                    </figure>
                                    <span>All taxes included										</span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <figure>
                                        <img src="/development/cactus/www/images/check.png" alt="">
                                    </figure>
                                    <span>Invoice by email
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <figure>
                                        <img src="/development/cactus/www/images/check.png" alt="">
                                    </figure>
                                    <span>Road assistance 24/7
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:RadioButton runat="server"  AutoPostBack="true" OnCheckedChanged="pnow_CheckedChanged" Text="Pay NOW Online" ID="pnow" name="pnow" value="pnow" ClientIDMode="Static" GroupName="pay-when" CssClass=""></asp:RadioButton>

                                    <figure>
                                        <a type="button" data-toggle="modal" data-target="#myModal2">
                                            <img src="/development/cactus/www/images/icon-i.png" alt=""></a>
                                    </figure>
                                    <figure>
                                        <img src="/development/cactus/www/images/logo-check.png" alt="">
                                    </figure>

                                </td>

                            </tr>
                            <%--<tr id="divEmailonPaynow" style="display: none">

                                <td>
                                    <span>Email</span>


                                    <asp:TextBox ID="txtEmail" runat="server" ClientIDMode="Static" Width="250"></asp:TextBox>

                                </td>

                            </tr>--%>
                        </tbody>
                    </table>
                   
                    <table class="detail-price">
                        <thead>
                            <tr>
                                <th>2.Protection avalilable</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>List
                                </td>
                                <td>CDW
										<span class="color">FULL</span>
                                    <br>
                                    ARS $150
                                    <br>
                                    per day
										
                                </td>
                                <td>CDW Basic
										GRATIS
                                </td>
                            </tr>
                            <!--tr>
									<td>Whats Include?</td>
									<td>
										<input type="checkbox">
									</td>
									<td>
										<input type="checkbox">
									</td>
								</tr-->
                            <tr>
                                <td>Damage on third parties
                                </td>
                                <td>
                                    <figure>
                                        <img src="www/images/check.png" alt="">
                                    </figure>
                                </td>
                                <td>
                                    <figure>
                                        <img src="www/images/check.png" alt="">
                                    </figure>
                                </td>
                            </tr>
                            <tr>
                                <td>Road Assistance 24/7
                                </td>
                                <td>
                                    <figure>
                                        <img src="www/images/check.png" alt="">
                                    </figure>
                                </td>
                                <td>
                                    <figure>
                                        <img src="www/images/check.png" alt="">
                                    </figure>
                                </td>
                            </tr>
                            <tr>
                                <td>Towing and taxi expenses
                                </td>
                                <td>
                                    <figure>
                                        <img src="www/images/check.png" alt="">
                                    </figure>
                                </td>
                                <td>
                                    <figure>
                                        <img src="www/images/close.png" alt="">
                                    </figure>
                                </td>
                            </tr>
                            <tr>
                                <td>Windows, mirrors and tires
                                </td>
                                <td>
                                    <figure>
                                        <img src="www/images/check.png" alt="">
                                    </figure>
                                </td>
                                <td>
                                    <figure>
                                        <img src="www/images/close.png" alt="">
                                    </figure>
                                </td>
                            </tr>
                            <tr>
                                <td>Loss of profit during car repair
                                </td>
                                <td>
                                    <figure>
                                        <img src="www/images/close.png" alt="">
                                    </figure>
                                </td>
                                <td>
                                    <figure>
                                        <img src="www/images/close.png" alt="">
                                    </figure>
                                </td>
                            </tr>
                            <tr>
                                <td>Rollover or theft
                                </td>
                                <td>
                                    <figure>
                                        <img src="www/images/close.png" alt="">
                                    </figure>
                                </td>
                                <td>
                                    <figure>
                                        <img src="www/images/close.png" alt="">
                                    </figure>
                                </td>
                            </tr>
                            <tr>
                                <td>Deductible for partial damages to the vehicule
                                </td>
                                <td>
                                    <span>Ars $ 1.000</span>
                                </td>
                                <td>
                                    <span>Ars $ 20.000</span>
                                </td>
                            </tr>
                            <tr>
                                <th>
                                    <div class="detail-price-foot">
                                        <span>YOUR CDW EXPLAINED</span>
                                        <figure>
                                            <a type="button" data-toggle="modal" data-target="#myModal3">
                                                <img src="www/images/icon-i.png" alt=""></a>
                                        </figure>
                                        <div class="modal fade" id="myModal3" role="dialog" style="display: none;">
                                            <div class="modal-dialog">
                                                <!-- Modal content-->
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal">�</button>
                                                        <figure>
                                                            <img src="images/icon-i-2.png" alt="">
                                                        </figure>
                                                        <h4 class="modal-title">YOUR CDW EXPLAINED</h4>
                                                    </div>
                                                    <div class="modal-body">
                                                        <p>
                                                            The deductible shows you the amount of money you have to pay for in case of partial damage to the vehicle. It is important that the deductible is as low as possible so that in case of an accident the insurance will cover most or all the damage.
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </th>
                            </tr>
                        </tbody>
                    </table>


                    <h2>
                        <asp:Label ID="lblextrasText" Visible="false" Text="Choose Extras" runat="server"></asp:Label></h2>

                    <asp:Repeater ID="rpExtras" runat="server">

                        <HeaderTemplate>
                            <table class="detail-fitting">
                                <thead>
                                    <tr>
                                        <th>3. Extras and accessories (depending on availability)</th>
                                    </tr>
                                </thead>
                                <tbody>
                        </HeaderTemplate>

                        <ItemTemplate>

                            <tr class="<%# Eval("ExtraCode") %>">

                                <td>
                                    <asp:CheckBox Font-Size="X-Small" ID="chkExtra" ToolTip='<%# Eval("ExtraCode") %>' runat="server" AutoPostBack="True" ClientIDMode="Static" OnCheckedChanged="chkExtra_OnCheckedChanged" meta:resourcekey="chkExtraResource1" />
                                    </asp:CheckBox>

		<label>
            <asp:Label ID="Label1" Text='<%# Eval("ExtraDesc") %>' runat="server" meta:resourcekey="Label1Resource2"></asp:Label>
        </label>

                                    <asp:DropDownList OnSelectedIndexChanged="DropDownList1_SelectedIndexChanged" AutoPostBack="true" ClientIDMode="Static" runat="server" Visible='<%#(Eval("ExtraCode").ToString() <> "UA01" AND Eval("ExtraCode").ToString() <> "SLI" AND Eval("ExtraCode").ToString() <> "RLP" AND Eval("ExtraCode").ToString() <> "PAI/PEC")%>' Width="55px" ID="txtQuantity">

                                        <asp:ListItem Text="1" Value="1" />
                                        <asp:ListItem Text="2" Value="2" />
                                        <asp:ListItem Text="3" Value="3" />

                                    </asp:DropDownList>
                                </td>

                                <td>$<asp:Label ID="Label2" Text='<%# Eval("ExtraDesc2") %>' runat="server" meta:resourcekey="Label2Resource1"></asp:Label>
                                </td>

                        </ItemTemplate>

                        <FooterTemplate>
                            </tbody>
      </table>
                        </FooterTemplate>

                    </asp:Repeater>

                </div>

                <div class="row">
                    <div class="col-md-12 col-sm-6">
                        <div class="total">
                            <h4>Total:</h4>
                            <span>ARS 
        <asp:Label ID="labelTotal" Text="" runat="server"></asp:Label>
                                <br>
                                <em>Tax included</em>
                            </span>
                        </div>
                    </div>
                    <div class="col-md-12 col-sm-6">
                        <div class="continue">
                            <asp:Label ID="LabelsubBtn" Text="" runat="server"> </asp:Label>
                            <div class="continue-box hidden-sm hidden-xs">
                                <figure>
                                    <img src="www/images/icon-ctn.png" alt="">
                                </figure>
                                <span>Free Cancellation</span>
                            </div>
                        </div>
                    </div>
                    <!--          <asp:CheckBox ID="chkInsurance" ClientIDMode="Static"  runat="server"/>Insurance
                 END EXTRAS TABLE -->


                    <div class="col-sm-6">
                        <asp:Literal ID="Label1" runat="server" meta:resourcekey="Label1Resource1" Visible="False"></asp:Literal>

                        <asp:Label Visible="False" ID="leftContent" runat="server" meta:resourcekey="leftContentResource1"></asp:Label>

                        <asp:Label ID="lblcontent" Font-Size="X-Small" runat="server" meta:resourcekey="lblcontentResource1"></asp:Label>

                        <asp:Label ID="lblbook" runat="server" meta:resourcekey="lblbookResource1"></asp:Label>
                    </div>
                </div>
            </div>
        </div>
    </section>
   
</asp:Content>
