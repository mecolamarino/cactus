<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Reservation.aspx.vb" Trace="false" Inherits="Reservation" MasterPageFile="MastarList.master" %>

<%@ Register Src="Controls/address.ascx" TagName="address" TagPrefix="ad" %>

<asp:Content ContentPlaceHolderID="head" runat="server" ID="header">
    <title>Reservation</title>

<style>
  .error {
	  color: red;
  }
  

  .plcy {
	  height:200px;
	  overflow-x:auto;
	  width:100%;
	  border:1px solid #ccc;
	  margin-top:15px; 
	  margin-bottom:15px;
	  padding: 5px; 
  }
  
  .please-wait {
		position:fixed;
		top:0px;
		left:0px;
		width:100vw;
		height:100vh;
		background:rgba(192,192,192,0.8);
		z-index:99999999999;
   }
   
   .loading-msg {
	   font-size:3em;
	   line-height:1em;
	   position:absolute;
	   width:50%;
	   height:50%;
	   margin:auto;
	   top:0px;
	   bottom:0px;
	   right:0px;
	   left:0px;
	   text-align:center;
   }
    .chbx
    { margin-right :500px;
      margin-top :10px;
        float: right ;
        position: relative;
    }
</style>
		<script>
$(function(){
	$(".dateOfBirth").prop('required',true);
})

</script>
</asp:Content>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="ContentPlaceHolder1">

    <section id="content-changer" class="info-user padding-section">

        <asp:Panel ID="pnlLeft" runat="server" Visible="true">

            <div class="container">
                <div class="row">
                    <div class="col-md-4">
                        <div class="side-bar">
                            <div class="about-car">
                                <div class="title">
                                    <h2>Sedan 4 doors</h2>
                                    <span>Chevrolet Classic</span>

                                    <asp:Label ID="carModelName" Visible="false" runat="server"></asp:Label><asp:Label ID="lblcls" runat="server" Visible="false"></asp:Label>

                                </div>
                                <figure>
								<img src="../images/about-car.png" alt="">
								</figure>
                                <ul class="hidden-sm hidden-xs">

                                    <li>
                                        <figure>
                                            <img src="/development/cactus/images/about-1.png" alt="">
                                        </figure>
                                        <span>6,5lts / 100km</span>
                                    </li>
                                </ul>
                                <ul class="hidden-sm hidden-xs">

                                    <li>
                                        <figure>
                                            <img src="/development/cactus/images/about-3.png" alt="">
                                        </figure>
                                        <span>A/C - Heater <span></li>
                                    <li>
                                        <figure>
                                            <img src="/development/cactus/images/about-4.png" alt="">
                                        </figure>
                                        <span>Manual Gear box</span>
                                    </li>
                                </ul>
                                <ul class="hidden-sm hidden-xs full">
                                    <li></li>
                                    <li>
                                        <figure>
                                            <img src="/development/cactus/images/about-6.png" alt="">
                                        </figure>
                                        <span>5 passengers </span>
                                    </li>
                                    <li>
                                        <figure>
                                            <img src="/development/cactus/images/about-7.png" alt="">
                                        </figure>
                                        <span>2 XL luggage and<br>
                                            2 M luggage</span>
                                    </li>
                                </ul>
                            </div>
                            
                            <div id="Remove-side-bar">
                            
                            <div class="rental-information">

                                <table>
                                    <thead>
                                        <tr>
                                            <th>Rental Details</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>
                                                <span>Pick up Location:</span>
                                                <h3>
                                                    <asp:Label ID="lblploc" runat="server"></asp:Label></h3>
                                                <p>
                                                    <asp:Label ID="lblpickupdateText" runat="server" meta:resourcekey="lblpickupdateTextResource1"></asp:Label>
                                                    <asp:Label ID="lblptime" runat="server" meta:resourcekey="lblptimeResource1"></asp:Label>
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <span>Return Location:</span>
                                                <h3>
                                                    <asp:Label ID="lbldloc" runat="server"></asp:Label></h3>
                                                <p>
                                                    <asp:Label ID="lbldropffDatetext" runat="server" meta:resourcekey="lbldropffDatetextResource1"></asp:Label>
                                                    <asp:Label ID="lbldtime" runat="server" meta:resourcekey="lbldtimeResource1"></asp:Label>
                                                </p>
                                            </td>
                                        </tr>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th>Rental days:
                                                <asp:Label ID="LabelRPeriod" runat="server" meta:resourcekey="LabelRPeriod"></asp:Label>Days</th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>

                            <table class="detail-user hidden-sm hidden-xs">
                                <thead>
                                    <th>DETAIL</th>
                                    <th>SUBTOTAL</th>
                                </thead>
                                <tbody>


                                    <asp:Label ID="Label1" runat="server" meta:resourcekey="Label1Resource1"></asp:Label>
                                    
                                    <asp:Label ID="Label2" runat="server" meta:resourcekey="Label1Resource1"></asp:Label>




                                </tbody>
                                <tfoot>
                                    <tr>
                                        <td>Total Payment</td>
                                        <td>
                                            <asp:Label ID="lblcost" CssClass="important-alert" runat="server"></asp:Label><br>
                                            Tax included
                                        </td>
                                    </tr>
                                </tfoot>
                            </table>
                            <asp:LinkButton CausesValidation="false" ID="lnkChange" Text="&laquo; Start Over" CssClass="grey-btn" runat="server" Visible="false"></asp:LinkButton>
                            <div class="info">
                                <div class="info-cancel">
                                    <figure>
                                        <img src="/development/cactus/images/icon-1.png" alt="">
                                    </figure>
                                    <h3>Free Cancellation
                                    </h3>
                                    <span>Modify or cancel your reservation at no cost before MONTH DAY YEAR
                                    </span>
                                </div>
                                <div class="info-attention">
                                    <figure>
                                        <img src="/development/cactus/images/icon-2.png" alt="">
                                    </figure>
                                    <h3>Minimum age of driver 21 years.
                                    </h3>
                                    <span>If the additional driver is between 18 and 21 years must pay an additional
                                    </span>
                                    <div class="info-attention-contact">
                                        <ul>
                                            <li>
                                                <i class="fa fa-print"></i>
                                                <span>Print this reservation</span>
                                            </li>
                                            <li>
                                                <i class="fa fa-envelope"></i>
                                                <span>Share this offer</span>
                                            </li>
                                        </ul>
                                    </div>
                                   </div>
                                </div>
                            </div>
                        </div>


                    </div>

                    <asp:Panel runat="server" ID="pnlReserve">
                        <asp:Label ID="lblMessage" runat="server" Style="color: red; font-weight: bold; font-size: xx-large"></asp:Label>


                        <asp:Label ID="lblconftext" Text="Confirmation Number:" Visible="false" runat="server"></asp:Label>
                        <asp:Label ID="lblmconf" runat="server"></asp:Label>



                        <div class="col-md-8">
                            <div class="your-data">
                                <table class="data-user">
                                    <thead>
                                        <tr>
                                            <th>4. Driver Information</th>
                                        </tr>
                                    </thead>
                                    <tbody>




                                        <tr>
                                            <td><span>Name*:</span></td>
                                            <td>
                                                <asp:TextBox runat="server" placeholder="First Name" CssClass="txtinputfld " ID="tbFirstName" required="required"></asp:TextBox>

                                            </td>
                                        </tr>

                                        <tr>
                                            <td><span>Surname*:</span></td>
                                            <td>
                                                <asp:TextBox runat="server" placeholder="Surname" CssClass="txtinputfld" ID="tbLastName" required="required"></asp:TextBox>

                                            </td>
                                        </tr>
                                        <tr>
                                            <td><span>Date of birth</span></td>
                                            <td>
                                                <div class="data-item">
                                                    <label>Month</label>

                                                    <asp:DropDownList ID="selectMonth" runat="server">
                                                    </asp:DropDownList>
                                                </div>
                                                <div class="data-item">
                                                    <label>Date</label>
                                                    <asp:DropDownList ID="selectDay" runat="server">
                                                    </asp:DropDownList>
                                                </div>
                                                <div class="data-item">
                                                    <label>Year</label>

                                                    <asp:DropDownList ID="selectYear" runat="server">
                                                    </asp:DropDownList>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><span>Email</span></td>
                                            <td>
                                                <asp:TextBox runat="server" placeholder="Email" CssClass="txtinputfld" ID="tbEmail" required="required"></asp:TextBox></td>
                                            <td>
                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ControlToValidate="tbEmail" CssClass="error" ErrorMessage="Input valid email address!">
                                                </asp:RegularExpressionValidator></td>
                                        </tr>
                                        
                                        <tr>
                                            <td><span>Confirm Email</span></td>
                                            <td>
                                                <asp:TextBox runat="server" CssClass="txtinputfld" ID="txtConfemail" placeholder="Confirm Email" required="required"></asp:TextBox></td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td>
                                                <asp:CompareValidator ID="comparePasswords" CssClass="error" runat="server" ControlToCompare="tbEmail" ControlToValidate="txtConfemail" ErrorMessage="The email adddresses do not match up" Display="Dynamic" /></td>
                                        </tr>
                                        <tr>
                                            <td><span>Phone</span></td>
                                            <td>

                                                <asp:TextBox runat="server" CssClass="txtinputfld" type="tel" ID="tbCell" placeholder="+54"></asp:TextBox>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td><span>Street Address:</span></td>
                                            <td>
                                                <asp:TextBox runat="server" ID="tbSA" placeholder="Street Address"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><span>City / Town:</span></td>
                                            <td>
                                                <asp:TextBox runat="server" ID="tbCity" placeholder="City"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><span>State:</span></td>
                                            <td>
                                                <asp:TextBox runat="server" ID="tbState" placeholder="State"></asp:TextBox>
                                            </td>
                                        </tr>

                                <tr>
									<td><span>  Zip Code:</span></td>
									<td>
                                <asp:TextBox runat="server" ID="tbZip" placeholder="Zip Code"></asp:TextBox>
                            	</td>
								</tr>
                                
                                <tr> </tr>
                               

							</tbody>
						</table>
    
                                <div id="cardDetail" runat="server" visible="false">
                                <table class="card-user">
							<thead>
								<tr>
									<th>5. GUARANTEE info</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>
										<span>Please select your card:</span>
									</td>
									<td><asp:DropDownList ID="drpcard" runat="server">
                                    
                                            <asp:ListItem Value="#" Text="Please select your card"></asp:ListItem>
                                            <asp:ListItem Value="VI" Text="VISA"></asp:ListItem>
                                            <asp:ListItem Value="MC" Text="Master Card"></asp:ListItem>
                                            <asp:ListItem Value="AX" Text="American Express"></asp:ListItem>
                                            <asp:ListItem Value="DS" Text="Discover"></asp:ListItem>
                                        </asp:DropDownList>
							
									</td>
								</tr>
								<tr>
									<td>
										<span>Credit Card Number:</span>
									</td>
									<td>
										<div class="secur">
											<asp:TextBox runat="server" ID="txtCreditCard" Width="200" required="required"></asp:TextBox>
										</div>
										<div class="input-box">
											<span class="hidden-sm hidden-xs hidden-md">No charges will be processed to this card! We only need this information to guarantee your reservation</span>
										</div>
									</td>
								</tr>
								<tr>
									<td>
										<span>Valid through:</span>
									</td>
									<td>
										<div class="data-item">
											<label>Month</label>
											<asp:DropDownList ID="txtExpMonth" runat="server">
                        <asp:ListItem Value="01" Text="01"></asp:ListItem>
                        <asp:ListItem Value="02" Text="02"></asp:ListItem>
                        <asp:ListItem Value="03" Text="03"></asp:ListItem>
                        <asp:ListItem Value="04" Text="04"></asp:ListItem>
                        <asp:ListItem Value="05" Text="05"></asp:ListItem>
                        <asp:ListItem Value="06" Text="06"></asp:ListItem>
                        <asp:ListItem Value="07" Text="07"></asp:ListItem>
                        <asp:ListItem Value="08" Text="08"></asp:ListItem>
                        <asp:ListItem Value="09" Text="09"></asp:ListItem>
                        <asp:ListItem Value="10" Text="10"></asp:ListItem>
                        <asp:ListItem Value="11" Text="11"></asp:ListItem>
                        <asp:ListItem Value="12" Text="12"></asp:ListItem>
                        </asp:DropDownList>
										</div>
										<div class="data-item">
											<label>Year</label>
									<asp:DropDownList ID="txtExpYear" runat="server">
			<asp:ListItem Value="15" Text="15"></asp:ListItem>
            <asp:ListItem Value="16" Text="16"></asp:ListItem>
            <asp:ListItem Value="17" Text="17"></asp:ListItem>
            <asp:ListItem Value="18" Text="18"></asp:ListItem>
            <asp:ListItem Value="19" Text="19"></asp:ListItem>
            <asp:ListItem Value="20" Text="20"></asp:ListItem>
            <asp:ListItem Value="21" Text="21"></asp:ListItem>
            <asp:ListItem Value="22" Text="22"></asp:ListItem>
            <asp:ListItem Value="23" Text="23"></asp:ListItem>
            <asp:ListItem Value="24" Text="24"></asp:ListItem>
            <asp:ListItem Value="25" Text="25"></asp:ListItem>
            <asp:ListItem Value="26" Text="26"></asp:ListItem>
            <asp:ListItem Value="27" Text="27"></asp:ListItem>
            <asp:ListItem Value="28" Text="28"></asp:ListItem>
            <asp:ListItem Value="29" Text="29"></asp:ListItem>
            <asp:ListItem Value="30" Text="30"></asp:ListItem>     
                    </asp:DropDownList>
										</div>
									</td>
								</tr>
								<tr>
									<td><span>CVV:</span></td>
									<td>
										<asp:TextBox runat="server" ID="txtcode"></asp:TextBox>
                    
               						
										<figure>
											<a data-toggle="modal" data-target="#myModal2"><img src="/development/cactus/images/icon-i.png" alt=""></a>
										</figure>
										<div class="modal fade" id="myModal2" role="dialog">
										    <div class="modal-dialog">
										      <!-- Modal content-->
										      	<div class="modal-content">
										        	<div class="modal-header">
										          		<button type="button" class="close" data-dismiss="modal">&times;</button>
										          		<figure>
										          			<img src="/development/cactus/images/icon-i-2.png" alt="">
										          		</figure>
										          		<h4 class="modal-title">Lorem Ipsum is</h4>
										        	</div>
											        <div class="modal-body">
											          	<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also </p>
											        </div>
										      	</div>
										    </div>
										</div>
									</td>
								</tr>
								<tr>
									<td>
										<figure>
											<img src="/development/cactus/images/icon-3.png" alt="">
										</figure>
										<span>No charges will be processed to this card! We only need this information to guarantee your reservation</span>
									</td>
								</tr>
							</tbody>
						</table>
                                </div>
						<table class="info-users">
							<thead>
								<tr>
									<th>6. Additional Info</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>
										<span>Do you know your arrival flight?:</span>
                                        <asp:TextBox ID="arrivalflight" runat="server"></asp:TextBox>
									</td>
								</tr>
								<tr>
									<td>
										<span>Need a Hotel?:</span>
                                        <asp:RadioButton ID="radioHotelYes" runat="server" GroupName ="Hotel" Text="Yes"/>
                                      
                                          <asp:RadioButton ID="radioHotelNo" runat="server" GroupName ="Hotel" Text="No"/>
									</td>
								</tr>
								<tr>
									<td>
										<span>Comments:</span>
                                        <asp:TextBox ID="Comments" runat="server" TextMode ="MultiLine"></asp:TextBox>
									</td>
								</tr>
							</tbody>
						</table>
						<table class="condition-user">
							<thead>
								<th>7. Terms and conditions</th>
							</thead>
							<tbody>
								<tr>
									<td>
										<asp:CheckBox ID="chkpolicy" runat="server" required="required" />
                            
                                
										<p>I have read and accepted the rental information and the terms and conditions.</p>
                                        
                                        
                                        <asp:Label ID="lblchk" ForeColor="red" runat="server" Font-Names="Arial" Text=""></asp:Label>
                                        
                                        <asp:Literal ID="ltrlPolicy" runat="server" Text=""></asp:Literal>
									</td>
								</tr>
							</tbody>
						</table>
    
                        
						<div class="info">
							<div class="info-cancel">
								<figure>
									<img src="/development/cactus/images/icon.png" alt="">
								</figure>
								<h3>
									Free Amendments
								</h3>
								<span>
									If you change your mind you can modify or cancel your reservation for free before:<br>
 									<asp:Label Text="" ID="lblfreecncl" runat="server"></asp:Label>
								</span>
							</div>
						</div>
						<div class="continue">
                        
                        
                        <asp:Button runat="server" CausesValidation="true" OnClientClick="return ValidatePage()" CssClass="bnt-continue" ID="btnReserve" ClientIDMode="Static" Text="" ></asp:Button>

							

                                </div>
					</div>
					<div class="col-md-12">
						<div class="infor hidden-md hidden-lg">
							<div class="row">
								<div class="col-sm-6">
										<div class="info-cancel">
									<figure>
										<img src="/development/cactus/images/icon-1.png" alt="">
									</figure>
									<h3>
									Free Cancellation
								</h3>
								<span>
									Modify or cancel your reservation at no cost before MONTH DAY YEAR
								</span>
								</div>
								</div>
								<div class="col-sm-6">
									<div class="info-i">
										<div class="info-item">
											<figure>
												<img src="../images/icon-5.png" alt="">
											</figure>
											<span>					
									Minimum age of driver 21 years.
								
								<b>
									If the additional driver is between 18 and 21 years must pay an additional
								</b>
												
											</span>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="info-attention hidden-md hidden-lg">
							<div class="info-attention-contact">
								<ul>
									<li>
										<a href="#" class="fa fa-print"></a>
										<span>Print this reservation</span>
									</li>
									<li>
										<a href="#" class="fa fa-file-pdf-o"></a>
											<span>Share this offer</span>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
		

<br style="clear:both;">

	

    




    <!-- Right Bar-->
    <div class="home-righ1t">

        <div class="page">
            

                
                
                <div class="col-sm-12" style="background:#fff; padding:20px 0px;">
                

                
                
                <div class="col-sm-12">
                	<div class="col-sm-12">
                    <asp:TextBox runat="server" ID="txtconf" Width="195px" Visible="false"></asp:TextBox><br />
                            <asp:Label ID="conNum" runat="server"></asp:Label>
                    </div>
             	</div>
                
               
     

                </div>
        </div>
        
			<script>
			$(function(){
			$(".rez-step2").addClass("active");	
			});
			</script>
            
            </asp:Panel>

            <asp:Panel ID="pnlsummary" runat="server" Visible="false">
            
              <div style="margin-bottom: 10px;" style="display:none;">
                <p><asp:Label ID="lblclass" Font-Bold="true" runat="server"></asp:Label></p>
                
                <asp:Label ID="lblimage" Width="400" Visible="false" runat="server"></asp:Label>
                    
                <asp:Label ID="lblreservationdetail" runat="server"></asp:Label>
                </div>
                
                <div class="col-md-8">
                <div class="row">
                <div class="col-md-12">
                
                <asp:Panel ID="pnlPaymentSection" runat="server" Visible="false">
                        <div class="code" style="background: #fff; padding-top: 20px; padding-bottom: 20px;">
                            <ul>
								<li>Reservation</li>
                            <li style="width:100% !important;">
                                <h2>Your Reservaion is not Complete Until You've Submitted Payment Through Our Online Payment System,<br>
								<asp:HyperLink runat="server" ID="lnkpayment" class="bnt-continue" >Click Here to Continue to Pay U</asp:HyperLink>
								</h2>
                            </li>
                            
                            </ul>
                        </div>
                        
                     
                </asp:Panel>
                
                <asp:Panel ID="pnlPaymentSection2" runat="server" Visible="true">
				<div class="code">
					<ul>
						<li>Reservation</li>
							<li><asp:Label ID="lblconf" runat="server"></asp:Label>
                            <asp:Label id="lblconfTT" runat="server"></asp:Label> </li>
                            
							<li>
							<div class="desc">Check your email adress, we have sent you the reservation confirmation. If you did not receive our email, <a href="#" class="color" onClick="window.print();">click here</a> to download it.</div>
							</li>
						</ul>
				</div>
                </asp:Panel>
                
				</div>
                        
                 <div class="col-md-6" id="moved-rental-info">

				 </div>
                 
                 <div class="col-md-6">
                 
                 </div>
                 
                 <div class="col-md-12">
							<div class="info-important">
								<table>
									<thead>
										<tr>
											<th>IMPORTANT information for collecting the car</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td>
												<h4>When you collect the car, please make sure you hace the pollowing documents:</h4>
												<ul>
													<li>
														<span>Credit Card:</span>
														<figure>
															<img src="../images/visa-icon.png" alt="">
														</figure>
													</li>
													<li>
														<span>Driver License valid through the entire rental</span>
														<figure>
															<img src="../images/ser-img.png" alt="">
														</figure>
													</li>
													<li>
														<span>Personal ID or Passport</span>
														<figure>
															<img src="../images/ser-img2.png" alt="">
														</figure>
													</li>
												</ul>
												<p><span>Important information:</span>  Credit card: For collecting the car you will need to provide a valid VISA or MASTERCARD credit card. We will freeze AR$4.500 for the length of the rental.Driver licence: You will be required to present a valid driver license from your country of residence. International driver licence is not required.</p>
											</td>
										</tr>
									</tbody>
								</table>
							</div>
					</div>
                    
                    <div class="col-md-12">
							<div class="info">
								<div class="info-cancel col-xs-12">
									<h3>
										Free Amendments
									</h3>
									<span>
										If you change your mind you can modify or cancel your reservation for free before: <br>
                                         <asp:Label Text="" ID="lblfreecncl1" runat="server"></asp:Label>
									</span>
								</div>
							</div>
							<div class="infor hidden-md hidden-lg">
								<div class="row">
									<div class="col-sm-6">
											<div class="info-cancel">
										<figure>
											<img src="../images/icon-1.png" alt="">
										</figure>
										<h3>
											Free Cancellation
										</h3>
										<span>
											Modify or cancel your reservation at no cost before MONTH DAY YEAR
										</span>
									</div>
									</div>
									<div class="col-sm-6">
										<div class="info-i">
											<div class="info-item">
												<figure>
													<img src="../images/icon-5.png" alt="">
												</figure>
												<span>
													Minimum age of driver 21 years.
													<b>If the additional driver is between 18 and 21 years must pay an additional.</b>
												</span>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="info-attention">
								<div class="info-attention-contact">
									<ul>
										<li>
											<a href="#" class="fa fa-print"></a>
											<span>Print this reservation</span>
										</li>
										<li>
											<a href="#" class="fa fa-file-pdf-o"></a>
											<span>Share this offer</span>
										</li>
									</ul>
								</div>
							</div>
						</div>
                      </div>
                    </div>
                   
				  <script>
                  $(function(){
                  $(".rez-step3").addClass("active");	
				  
				  $("#moved-rental-info").html($(".rental-information"));
				  
				  $("#content-changer").removeClass("info-user")
				  $("#content-changer").addClass("ready")
				  
				  $("#Remove-side-bar").hide();

                        });
                    </script>
                    </asp:Panel>
                    
                </div>

    </div>
    </div>
   </asp:Panel>

	</section>
</asp:Content>