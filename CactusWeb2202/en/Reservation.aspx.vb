﻿Imports System.Xml.Linq
Imports System.Linq
Imports System.Net.Mail
Imports System.IO
Imports System.Net


Partial Class Reservation
Inherits System.Web.UI.Page

    Protected Sub btnReserve_Click3(ByVal sender As Object, ByVal e As System.EventArgs)

    End Sub
    Protected Sub btnReserve_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReserve.Click
        If Page.IsValid Then
            If chkpolicy.Checked Then
                lblchk.Text = ""

            ' If Not String.IsNullOrEmpty(Session("Extracode")) Then
            Dim pickupdate, dropoffdatedate As String
            Dim pickuptime, dropofftime As String
            Dim pr As New RequestMaker
            Dim cls As New RequestMaker
            Dim comments
            Dim classcode As String
            Dim ExtraCode As String

            Dim pickuplocation As String
            Dim dropofflocation As String

            ExtraCode = Session("ExtraCode")

            Dim disc = Request.QueryString("disc")
            Dim prcode
            Dim companyID = Request.QueryString("companyID")
            Dim htRentalParam As Hashtable = Nothing
            htRentalParam = TryCast(Session("_htRentalParam"), Hashtable)
            Dim val
            If htRentalParam Is Nothing Then
                classcode = Request.QueryString("classCode")

                comments = ""
                pickuptime = Trim(Request.QueryString("putime"))
                dropofftime = Trim(Request.QueryString("dotime"))

                pickuplocation = Trim(Request.QueryString("ploc"))
                dropofflocation = Trim(Request.QueryString("rloc"))

                pickupdate = Trim(Request.QueryString("pudt"))
                dropoffdatedate = Trim(Request.QueryString("dodt"))
                val = pickupdate.IndexOf("/")
                Trace.Write("val" & val)
                If val = -1 Then
                    pickupdate = Trim(Request.QueryString("pudt")) + pickuptime
                    dropoffdatedate = Trim(Request.QueryString("dodt")) + dropofftime
                Else
                    pickupdate = cls.processDateString(Request.QueryString("pudt")) + pickuptime
                    dropoffdatedate = cls.processDateString(Request.QueryString("dodt")) + dropofftime
                End If


                SaveReservation(pickuplocation, dropoffdatedate, pickupdate, dropoffdatedate, Request.QueryString("rateID"), "", classcode, ExtraCode, Request.QueryString("disc"), "")



            Else

                Dim name, lastname, address, city, state, zip, phone, email
                Dim cardname, cardtype, cardnumber, cardcode, cardexpiration, TotalCost
                Dim picklocation As String
                Dim droplocation As String
                Dim pudt As String
                Dim dodt As String
                Dim conf As String

                Dim classDescription As String

                pudt = htRentalParam("PickupDate")
                classcode = htRentalParam("ClassCode")
                classDescription = htRentalParam("ClassDescription")
                dodt = htRentalParam("DropoffDate")
                picklocation = htRentalParam("PickupLocation")
                droplocation = htRentalParam("DropoffLocation")
				
                TotalCost = htRentalParam("TotalCharge")

                name = htRentalParam("FirstName")
                lastname = htRentalParam("LastName")
                address = htRentalParam("Address")
                city = htRentalParam("City")
                state = htRentalParam("State")
                zip = htRentalParam("Zip")
                email = htRentalParam("Email")
                conf = htRentalParam("Conf")

                '  htRentalParam.Add("CardName", cardname)
                'cardtype = htRentalParam("CardType")
                'cardexpiration = htRentalParam("CardExpiration")
                'cardnumber = htRentalParam("CardNumber")
                'cardcode = htRentalParam("CardCode")


                Trace.Write("INSIDE")
                SaveReservation(picklocation, droplocation, pudt, dodt, Request.QueryString("rateID"), conf, classcode, ExtraCode, disc, "Modified Online")

                ' SendEmail(conf, cartype, "Modify Reservation")

                End If
            Else
                lblchk.Text = "Please accept terms and conditions"

            End If
            ' Response.Redirect("confirmation.aspx?conf=" & conf & "")
            '  Trace.Write("CONf", conf)
            ' SendEmail(conf, cartype)
            ' Else
            'Response.Redirect("sessionexpired.aspx")
            ' End If

            End If
    End Sub

    
    Public Sub SendEmail(ByVal conf As String, ByVal cartype As String)

        Dim body As String

        Dim fromaddress As String = System.Configuration.ConfigurationManager.AppSettings("fromaddress")
        Dim toaddress As String = System.Configuration.ConfigurationManager.AppSettings("fromaddress")
        Dim msg As String
        msg = ""


        Dim pickupdate, dropoffdatedate As String
        Dim pickuptime, dropofftime As String
        Dim pr As New RequestMaker
        Dim cls As New RequestMaker
        Dim pickuploc
        Dim dropoffloc
        'Dim age = tbAge.Text
        Dim promo = Request.QueryString("disc")
		Dim classCode = Request.QueryString("classCode")
		
        '  Dim del = drpdelivery.SelectedItem.Value
        Dim val
        ' age = tb

        pickuptime = Trim(Request.QueryString("putime"))
        dropofftime = Trim(Request.QueryString("dotime"))

        'dropoffdatedate = pr.processDateString(Trim(Request.QueryString("dodt"))) & " " & Me.Request.QueryString("dotime")
        pickupdate = Trim(Request.QueryString("pudt"))
        dropoffdatedate = Trim(Request.QueryString("dodt"))
        Val = pickupdate.IndexOf("/")
        'pickupdate = pr.processDateString(Trim(Request.QueryString("pudt"))) & " " & Me.Request.QueryString("putime")
        If val = -1 Then
            pickupdate = pickupdate.Insert(4, "/").Insert(2, "/")
            dropoffdatedate = dropoffdatedate.Insert(4, "/").Insert(2, "/")

        Else
            Trace.Write("I'm inside")
            pickupdate = Trim(Request.QueryString("pudt"))
            dropoffdatedate = Trim(Request.QueryString("dodt"))
            Trace.Write("Insert slash")

        End If



        pickupdate = Request.QueryString("pudt")
        dropoffdatedate = Request.QueryString("dodt")

        pickuploc = cls.GetLocation(Request.QueryString("ploc"))
        dropoffloc = cls.GetLocation(Request.QueryString("rloc"))
		
		Dim cxml As String
		cxml = RequestMaker.MakeRateRequest(RequestMaker.RateRequestClass(Request.QueryString("classCode")))
		Dim classdoc As XDocument = XDocument.Parse(cxml)
		Dim carModel As String() = classdoc...<SimilarText>.Value.Split(new String() { "or" }, StringSplitOptions.None)
		Dim carModelName as String = carModel(0).TrimEnd()

        Dim html As New StringBuilder


 html.AppendLine("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'><html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=utf-8' /><meta http-equiv='X-UA-Compatible' content='IE=edge' /><meta name='viewport' content='width=device-width, initial-scale=1.0'>")
    html.AppendLine("<title>RentMax - Confirmation</title>")
    html.AppendLine("<style type='text/css'>")
        html.AppendLine("body {Margin: 0;padding: 0;min-width: 100%;background-color: #ffffff;}")
        html.AppendLine("table {border-spacing: 0;font-family: sans-serif;color: #333333;}")
        html.AppendLine("td {padding: 0;}")
        html.AppendLine("img {border: 0;}")
        html.AppendLine(".wrapper {width: 100%;table-layout: fixed;-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;}")
        html.AppendLine(".webkit {max-width: 600px;}")
        html.AppendLine("table {border-collapse: collapse;}")
        html.AppendLine(".outer {Margin: 0 auto;width: 100%;max-width: 600px;}")
       	html.AppendLine(".full-width-image img {width: 100%;max-width: 600px;height: auto;}")
        html.AppendLine(".inner {padding: 10px;}")
        html.AppendLine(".policy {margin: 0px; width:100%;height:auto ; font-size:14px'}")
        html.AppendLine("p {Margin: 0;}")
        html.AppendLine("a {color: #ee6a56;text-decoration: underline;}")
        html.AppendLine(".h1 {font-size: 21px;font-weight: bold;margin: 18px 0;}")
        html.AppendLine(".h2 {font-size: 18px;font-weight: bold;margin: 12px 0;}")
        html.AppendLine(".h3 {font-size: 16px;margin: 10px 0;}")
        html.AppendLine(".one-column .contents {text-align: left;}")
        html.AppendLine(".one-column p {font-size: 14px;Margin-bottom: 10px;}")
		html.AppendLine(".two-column {text-align: center;font-size: 0;}")
        html.AppendLine(".two-column .column {width: 100%;max-width: 300px;display: inline-block;vertical-align: top;}")
    	html.AppendLine("</style>")
		html.AppendLine("</head>")

		html.AppendLine("<body style='Margin:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;min-width:100%;background-color:#ffffff;' >")
    	html.AppendLine("<center class='wrapper' style='width:100%;table-layout:fixed;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;' >")
        html.AppendLine("<div class='webkit' style='max-width:600px;' >")
        html.AppendLine("<!--[if (gte mso 9)|(IE)]>")
        html.AppendLine("<table width='600' align='center' style='border-spacing:0;font-family:sans-serif;color:#333333;border-collapse:collapse;' >")
        html.AppendLine("<tr>")
        html.AppendLine("<td style='padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;' >")
        html.AppendLine("<![endif]-->")
        html.AppendLine("<table class='outer' align='center' style='border-spacing:0;font-family:sans-serif;color:#333333;border-collapse:collapse;Margin:0 auto;width:100%;max-width:600px;' >")
        html.AppendLine("<tr>")
        html.AppendLine("<td class='full-width-image' style='padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;' >")
		
        html.AppendLine("<img src='http://www.datamine.net/development/cactus/images/mail-header.jpg' width='600' style='border-width:0;width:100%;max-width:600px;height:auto;' />")
			
        html.AppendLine("</td>")
        html.AppendLine("</tr>")
        html.AppendLine("<tr>")
        html.AppendLine("<td class='one-column' style='padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;' >")
        html.AppendLine("<table width='100%' style='border-spacing:0;font-family:sans-serif;color:#333333;border-collapse:collapse;' >")
        html.AppendLine("<tr>")
        html.AppendLine("<td class='inner contents' style='padding-top:10px;padding-bottom:10px;padding-right:10px;padding-left:10px;text-align:left;' >")
		
		
                html.AppendLine("<p class='h1' style='Margin:0;font-weight:bold;margin-top:18px;margin-bottom:18px;margin-right:0;margin-left:0;font-size:14px;Margin-bottom:10px;' >THIS IS AN AUTOMATED RESERVATION CONFIRMATION EMAIL</p>")
                html.AppendLine("<p class='h3' style='Margin:0;margin-top:10px;margin-bottom:10px;margin-right:0;margin-left:0;font-size:14px;Margin-bottom:10px; color:#24336e;' >Thank You for choosing Cactus Rent a Car</p>")
                html.AppendLine("<p class='h1' style='Margin:0;font-weight:bold;margin-top:18px;margin-bottom:18px;margin-right:0;margin-left:0;font-size:20px;Margin-bottom:10px; color:#0001ca;' >Your Confirmation No: " & conf & "</p>")
                
				
        html.AppendLine("<hr />")
        html.AppendLine("</td>")
        html.AppendLine("</tr>")
        html.AppendLine("</table>")
        html.AppendLine("</td>")
        html.AppendLine("</tr>")
        html.AppendLine("<tr>")
		html.AppendLine("<td class='two-column' style='padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;text-align:center;font-size:0;' >")
        html.AppendLine("<!--[if (gte mso 9)|(IE)]>")
        html.AppendLine("<table width='100%' style='border-spacing:0;font-family:sans-serif;color:#333333;border-collapse:collapse;' >")
        html.AppendLine("<tr>")
        html.AppendLine("<td width='50%' valign='top' style='padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;' >")
        html.AppendLine("<![endif]-->")
        html.AppendLine("<div class='column' style='width:100%;max-width:300px;display:inline-block;vertical-align:top;' >")
		html.AppendLine("<table width='100%' style='border-spacing:0;font-family:sans-serif;color:#333333;border-collapse:collapse;' >")
        html.AppendLine("<tr>")
        html.AppendLine("<td class='inner' style='padding-top:10px;padding-bottom:10px;padding-right:10px;padding-left:10px;' >")
		
				html.AppendLine("<p class='h1' style='Margin:0;font-weight:bold;margin-top:18px;margin-bottom:18px;margin-right:0;margin-left:0;font-size:20px;Margin-bottom:10px; color:#0001ca' > Rental Information </p> ")
                html.AppendLine("<p class='h1' style='Margin:0;font-weight:bold;margin-top:18px;margin-bottom:18px;margin-right:0;margin-left:0;font-size:16px;Margin-bottom:10px; color:#56649a;' >- Pick Up -</p>")
        html.AppendLine("<p style='margin-bottom:10px; font-Size:20px'>" & pickupdate & "<br>" & pickuptime & "</p>")
				'html.AppendLine("<p style='margin-bottom:10px;'>Ohana Rent A Car<br>")
				'html.AppendLine("411 Huku Li'i Place #104<br>")
				'html.AppendLine("Kihei, Hawaii 96753</p>")
				
				html.AppendLine("<p class='h1' style='Margin:0;font-weight:bold;margin-top:18px;margin-bottom:18px;margin-right:0;margin-left:0;font-size:16px;Margin-bottom:10px; color:#56649a;' >- Drop Off -</p>")
        html.AppendLine("<p style='margin-bottom:10px;font-Size:20px'>" & dropoffdatedate & "<br>" & dropofftime & "</p>")
				'html.AppendLine("<p style='margin-bottom:10px;'>Ohana Rent A Car<br>")
				'html.AppendLine("411 Huku Li'i Place #104<br>")
				'html.AppendLine("Kihei, Hawaii 96753</p>")
				
				html.AppendLine("<hr>")
				
				html.AppendLine("<p class='h1' style='Margin:0;font-weight:bold;margin-top:18px;margin-bottom:18px;margin-right:0;margin-left:0;font-size:20px;Margin-bottom:10px; color:#0001ca;' > Personal Information </p> ")
				
        html.AppendLine("<p style='margin-bottom:10px;font-Size:20px'>Name: " & tbFirstName.Text & " " & tbLastName.Text & "</p>")
        html.AppendLine("<p style='margin-bottom:10px;font-Size:20px'>Email: " & tbEmail.Text & "</p>")
        html.AppendLine("<p style='margin-bottom:10px;font-Size:20px'>Phone: " & tbCell.Text & "</p>")
        html.AppendLine("<p style='margin-bottom:10px;font-Size:20px'>Address: " & tbSA.Text & "<br>")
        html.AppendLine(tbCity.Text & ", " & tbState.Text & " " & tbZip.Text & "</p>")
        Dim HotelRequired As String
        If (radioHotelYes.Checked = True) Then
            HotelRequired = "Yes"
        Else
            HotelRequired = "No"
        End If

        html.AppendLine("<p style='margin-bottom:10px;font-Size:20px'>Arrival of Flight: " & arrivalflight.Text & "<br>")
        html.AppendLine("<p style='margin-bottom:10px;font-Size:20px'>Arrival of Flight: " & HotelRequired & "<br>")

        html.AppendLine("<p style='margin-bottom:10px;font-Size:20px'>Comments: " & Comments.Text & "<br>")

        If Not String.IsNullOrEmpty(txtCreditCard.Text) Then
            If txtCreditCard.Text.Length > 4 Then
                html.AppendLine("<p style='margin-bottom:10px;font-Size:20px'>Card Ending With: " & Microsoft.VisualBasic.Right(txtCreditCard.Text, 4) & "</p>")
            Else
                html.AppendLine("<p style='margin-bottom:10px;font-Size:20px'>Card Ending With: " & txtCreditCard.Text & "</p>")
            End If

        End If

        If Len(promo) > 2 Then
            html.AppendLine("<p style='margin-bottom:10px;font-Size:20px'>Promo Code: " & promo & "</p>")
        End If

        html.AppendLine("</td>")
        html.AppendLine("</tr>")
        html.AppendLine("</table>")
        html.AppendLine("</div>")
        html.AppendLine("<!--[if (gte mso 9)|(IE)]>")
        html.AppendLine("</td>")
        html.AppendLine("<td width='50%' valign='top' style='padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;' >")
        html.AppendLine("<![endif]-->")
        html.AppendLine("<div class='column' style='width:100%;max-width:300px;display:inline-block;vertical-align:top;' >")
        html.AppendLine("<table width='100%' style='border-spacing:0;font-family:sans-serif;color:#333333;border-collapse:collapse;' >")
        html.AppendLine("<tr>")
        html.AppendLine("<td class='inner' style='padding-top:10px;padding-bottom:10px;padding-right:10px;padding-left:10px;' >")

        html.AppendLine("<p class='h1' style='Margin:0;font-weight:bold;margin-top:18px;margin-bottom:18px;margin-right:0;margin-left:0;font-size:20px;Margin-bottom:10px;color:#0001ca;' >" & carModelName & "</p>")
        html.AppendLine("<img width='260' src='http://www.datamine.net/development/cactus/carimages/" & classCode & ".jpg'>")

        html.AppendLine("<p style='margin-bottom:10px;font-Size:20px'>" & lblcls.Text & "</p>")
        html.AppendLine("</td>")
        html.AppendLine("</tr>")
        html.AppendLine("</table>")
        html.AppendLine("</div>")

        html.AppendLine("</td>")
        html.AppendLine("</tr>")
		html.AppendLine("</td>")
        html.AppendLine("</tr>")
        html.AppendLine("</table>")
		
		html.AppendLine("<tr>")
        html.AppendLine("<td class='one-column' style='padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;' >")
        html.AppendLine("<table width='100%' style='border-spacing:0;font-family:sans-serif;color:#333333;border-collapse:collapse;' >")
        html.AppendLine("<tr>")
        html.AppendLine("<td class='inner contents' style='padding-top:10px;padding-bottom:10px;padding-right:10px;padding-left:10px;text-align:left;' >")
		
		html.AppendLine("<hr>")
		
		'html.AppendLine("<p class='h1' style='Margin:0;font-weight:bold;margin-top:18px;margin-bottom:18px;margin-right:0;margin-left:0;font-size:20px;Margin-bottom:10px; color:#0001ca;' > POLICIES </p>")
        'html.AppendLine("<div style='font-size:10px'>" + ltrlPolicy.Text + "</div>")
        html.AppendLine("</div>")
		

        html.AppendLine("</td>")
        html.AppendLine("</tr>")
		
   		html.AppendLine("</table>")
		html.AppendLine("</div>")
        html.AppendLine("<!--[if (gte mso 9)|(IE)]>")
        html.AppendLine("</td>")
        html.AppendLine("</tr>")
        html.AppendLine("</table>")
        html.AppendLine("<![endif]-->")
    	html.AppendLine("</td>")
		html.AppendLine("</tr>")
        html.AppendLine("</table>")
    	html.AppendLine("</td>")
		html.AppendLine("</tr>")
        html.AppendLine("</table>")
        html.AppendLine("<!--[if (gte mso 9)|(IE)]>")
        html.AppendLine("</td>")
        html.AppendLine("</tr>")
        html.AppendLine("</table>")
        html.AppendLine("<![endif]-->")
        html.AppendLine("</div>")
    	html.AppendLine("</center>")
		html.AppendLine("</body>")


        Dim address As New StringBuilder

        Dim locadd As String
        Dim locadd2
        Dim locname As String
        Dim locphone
        Dim locemail
        Dim loczip
        Dim locstate As String
        Dim loc As New RequestMaker
        Dim add1
        Dim city
        Dim state
        Dim zip
        Dim phone


        loc.GetAddress(Request.QueryString("ploc"), add1, city, state, zip, phone)

        locadd = add1
        locadd2 = city & "," & state & " " & zip
        locphone = phone




        address.AppendLine("<table width=400 cellspacing=0 cellpadding=3 border=0>")



        address.AppendLine("<tr>")
        address.AppendLine("<td>" & locadd & "</td>")
        address.AppendLine("</tr>")

        address.AppendLine("<tr>")
        address.AppendLine("<td>" & locadd2 & "</td>")
        address.AppendLine("</tr>")

        'address.AppendLine("<tr>")
        'address.AppendLine("<td>Telephone:" & locphone & "</td>")
        'address.AppendLine("</tr>")


        address.AppendLine("</table>")

        Dim rt As New StringBuilder
        rt.AppendLine("<table width=350 cellspacing=0 cellpadding=0 border=0>")
        rt.AppendLine("<tr>")
        '  rt.AppendLine("<td>" & lblemail.Text & "</td>")
        rt.AppendLine("</tr>")

        'rt.AppendLine("<tr>")
        'rt.AppendLine("<td><a href=""http://datamin/rental/rental-policies.htm"">Click here to view our Policies</a></td>")
        'rt.AppendLine("</tr>")
        rt.AppendLine("</table>")

        body = html.ToString & "<br><br>" & rt.ToString & "<br><br>" & address.ToString & "<hr align=left width=93%>"


        Dim ploc = Request.QueryString("ploc")


        Trace.Write("CONF", conf)

        Try

            Dim strFrom = fromaddress
            Dim strTo = tbEmail.Text
            Dim MailMsg As New MailMessage(New MailAddress(strFrom.Trim()), New MailAddress(strTo))
            MailMsg.BodyEncoding = Encoding.Default
            MailMsg.Subject = "Cactus Rent a Car - Resevation Confirmation"
            'MailMsg.CC.Add()
            MailMsg.Body = body
            MailMsg.Priority = MailPriority.High
            MailMsg.IsBodyHtml = True
            'Smtpclient to send the mail message 
			
			
			
			
			Dim SmtpMail As New SmtpClient
            Dim basicAuthenticationInfo As New System.Net.NetworkCredential("donotreply@datamine.net", "datamine2013")

            SmtpMail.Host = "mail.datamine.net"
            SmtpMail.EnableSsl = False
            SmtpMail.Port = 25
            SmtpMail.UseDefaultCredentials = False
            SmtpMail.Credentials = basicAuthenticationInfo
            SmtpMail.Send(MailMsg)
        Catch ex As Exception
            Trace.Write(ex.ToString)
        End Try

    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not Page.IsPostBack Then

            Dim CurYr As Int32 = DateAndTime.Now.Year
            For index = 1950 To CurYr
                selectYear.Items.Add(New ListItem(index.ToString(), index.ToString()))

            Next

            Dim CurMonth As Int32 = DateAndTime.Now.Month
            For index = 1 To 12
                selectMonth.Items.Add(New ListItem(index.ToString(), index.ToString()))

            Next

            Dim CurDay As Int32 = DateAndTime.Now.Day
            For index = 1 To 31
                selectDay.Items.Add(New ListItem(index.ToString(), index.ToString()))

            Next
			
			
			Dim DateOfBirth As String = selectDay.SelectedItem.Value + "/" + selectMonth.SelectedItem.Value + "/" + selectYear.SelectedItem.Value  





            Dim htRentalParam As Hashtable = Nothing

            htRentalParam = TryCast(Session("_htRentalParam"), Hashtable)
            BindButtonText()
            Dim classCode As String
            classCode = Request.QueryString("classCode")
            If htRentalParam Is Nothing Then
                Dim pickupdate, dropoffdatedate As String
                Dim pickuptime, dropofftime As String
                Dim discount As String
                Dim ploc As String
                Dim dloc As String
                Dim cl As New RequestMaker
                dropoffdatedate = Request.QueryString("dodt")
                pickupdate = Request.QueryString("pudt")

                discount = Trim(Request.QueryString("disc"))

                ploc = Trim(Request.QueryString("ploc"))

                dloc = Trim(Request.QueryString("rloc"))
                DisplayVehicleInfo(ploc, dloc, pickupdate, dropoffdatedate)
                GetClassDetail()
                If Request.QueryString("prepaid") = "y" Then
                    lblcost.Text = Extentions.ChangeCurrency(Request.QueryString("prepaidAmount"))
                    'lblcost2.Text = Extentions.ChangeCurrency(Request.QueryString("prepaidAmount"))
                Else
                    lblcost.Text = Extentions.ChangeCurrency(Request.QueryString("TotalCost"))
                    'lblcost2.Text = Extentions.ChangeCurrency(Request.QueryString("TotalCost"))

                End If
                lblreservationdetail.Text = Session("RentalDetail")
                lblimage.Text = "<img width=260  src=carimages/" + classCode + ".jpg>"
                '   HyperLink1.NavigateUrl = "terms.aspx?ploc=" & ploc
            Else


                Trace.Write("FALSE")
                '  pnlCard.Enabled = False

                If Request.QueryString("modify") = "true" Then
                Else
                    '  lnkStart.Visible = False
                    '  lnkChange.Visible = False
                    'lblvchange.Visible = False
                End If

                LoadInfo(htRentalParam)
                ' title.Text = "Modifying Reservation"


            End If
            Label1.Text = Session("Detail")
            Label2.Text = Session("ExtraChargeDetail")
            lblcost.Text = Extentions.ChangeCurrency(htRentalParam("TotalCharge"))
            LabelRPeriod.Text = Session("ReantalDays")
            'imgCar.ImageUrl = "carimages/" + classCode + ".jpg"
            'lb


            ltrlPolicy.Text = LoadDynamicPolicy()

            ''check Pyment Type and then Show credit Card Details
            If String.IsNullOrEmpty(Request.QueryString("paymentType")) = False And Request.QueryString("paymentType") = "pnow" Then
                cardDetail.Visible = False
            Else
                cardDetail.Visible = True
            End If
            'DisplayVehicleInfo()
        End If
    End Sub




    Public Sub SaveReservation(ploc, dloc, ptime, dtime, rateID, conf, classcode, Extracode, disc, subject)
        Dim ccNo As String = ""
        Dim IsExtra As Boolean
        IsExtra = True
        If Request.QueryString("modify") = "personal" Or Request.QueryString("modify") = "true" Then
            ' ccNo = Session("_CCNo")
        Else
            '  ccNo = txtCreditCard.Text
        End If

        If Request.QueryString("modify") = "personal" Then
            IsExtra = False
        End If
        Dim picloc As String = Request.QueryString("ploc")
        Dim drploc As String = Request.QueryString("rloc")
        Dim isPrepaidAmount = Request.QueryString("prepaid")
        Dim PrepaidAmount = Request.QueryString("TotalCost")
		Dim DateOfBirth As String = selectDay.SelectedItem.Value + "/" + selectMonth.SelectedItem.Value + "/" + selectYear.SelectedItem.Value
        ' Dim reqXml As String = RequestMaker.AddRezRequest(ptime, dtime, rateID, classcode, "", Me.Session.SessionID, Request.UserHostAddress, tbFirstName.Text, tbLastName.Text, tbEmail.Text, tbCell.Text, tbSA.Text, "", tbCity.Text, tbState.Text, tbZip.Text, conf, ploc, dloc, "", Extracode, disc, IsExtra)

        ' Dim reqXml As String = RequestMaker.AddRezRequest(ptime, dtime, Request.QueryString("rateID"), Request.QueryString("classCode"), "", Me.Session.SessionID, Request.UserHostAddress, tbFirstName.Text, tbLastName.Text, tbEmail.Text, tbCell.Text, tbSA.Text, "", tbCity.Text, tbState.Text, tbZip.Text, conf, "PAR", "PAR", "", Extracode, disc, drpcard.SelectedItem.Text, txtCreditCard.Text, txtcode.Text, txtExpMonth.Text, tbLicensenumber.Text, tbAge.Text, tbairlinenumber.Text, tbLicenseexpiration.Text, IsExtra)

        Dim HotelRequired As String
        If (radioHotelYes.Checked = True) Then
            HotelRequired = "Yes"
        Else
  HotelRequired = "No"
        End If
        Dim reqXml As String = RequestMaker.AddRezRequest(ptime, _
                                                   dtime, _
                                                   Request.QueryString("rateID"), Request.QueryString("classCode"), _
                                                   "", Me.Session.SessionID, _
                                                   Request.UserHostAddress, tbFirstName.Text, tbLastName.Text, tbEmail.Text, _
                                                   tbCell.Text, tbSA.Text, "", tbCity.Text, tbState.Text, _
                                                   tbZip.Text, conf, picloc, drploc, "", Extracode, disc, drpcard.SelectedItem.Value, Me.txtCreditCard.Text, Me.txtcode.Text, txtExpMonth.Text & "/" & Me.txtExpYear.Text, IsExtra, isPrepaidAmount, PrepaidAmount, Request.QueryString("iata"), arrivalflight.Text, radioHotelYes.Checked, DateOfBirth
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              )
        ' tbZip.Text, conf, picloc, drploc, "", Extracode, disc, drpcard.SelectedItem.Value, Me.txtCreditCard.Text, Me.txtcode.Text, txtExpMonth.Text & "/" & Me.txtExpYear.Text, tbLicensenumber.Text, tbAge.Text, tbairlinenumber.Text, tbairlinenumber.Text, tbLicenseexpiration.Text, IsExtra)

        Dim repXml As String = RequestMaker.MakeRateRequest(reqXml)
        'reserveLink.Text = repXml
        Dim xml As XDocument = XDocument.Parse(repXml)

        'SendEmail()
        Dim cartype, cxml
        cxml = RequestMaker.MakeRateRequest(RequestMaker.RateRequestClass(classcode))

        Dim classdoc As XDocument = XDocument.Parse(cxml)
        conf = xml...<ConfirmNum>.Value
        If Not String.IsNullOrEmpty(conf) Then

            lblconf.Text = "Revervation Code: <a href='' class='color'>" + conf + "</a>"
			lblconfTT.Text = "Total payment: <a href='' class='color'>" + Request.QueryString("TotalCost") + "</a>"
            cartype = lblcls.Text & "-" & classdoc...<ClassDescription>.Value
            SendEmail(conf, cartype)
            pnlsummary.Visible = True
            pnlReserve.Visible = False
            Session.Clear()
            pnlsummary.Visible = True
            pnlReserve.Visible = False
            ' lnkChange.Visible = False
            ' lnkStart.Visible = False
            'lblvchange.Visible = False
            ' Response.Write("CONF" & conf)
            pnlLeft.Visible = True
            '/// Show Payment link When Reservation Saved Successfully
            If String.IsNullOrEmpty(Request.QueryString("paymentType")) = False And Request.QueryString("paymentType") = "pnow" Then
                pnlPaymentSection.Visible = True
				pnlPaymentSection2.Visible = False
                lnkpayment.NavigateUrl = "~/en/payment.aspx?email=" + tbEmail.Text + "&referenceCode=" + conf + "&TotalCost=" + Request.QueryString("TotalCost") + ""
            End If
            Session.Abandon()
        Else

            lblMessage.Text = xml...<MessageDescription>.Value + " Please Search Again"

        End If

    End Sub

    Public Sub LoadInfo(htRentalParam)
        Dim name, lastname, address, city, state, zip, phone, email
        Dim cardname, cardtype, cardnumber, cardcode, cardexpiration, TotalCost
        Dim picklocation As String
        Dim droplocation As String
        Dim pudt As String
        Dim dodt As String
        Dim classCode As String
        Dim classDescription As String

        pudt = htRentalParam("PickupDate")
        Trace.Write("PUDT" & pudt)
        classCode = htRentalParam("ClassCode")
        classDescription = htRentalParam("ClassDescription")
        dodt = htRentalParam("DropoffDate")
        picklocation = htRentalParam("PickupLocation")
        droplocation = htRentalParam("DropoffLocation")
        TotalCost = htRentalParam("TotalCharge")

        name = htRentalParam("FirstName")
        lastname = htRentalParam("LastName")
        address = htRentalParam("Address")
        city = htRentalParam("City")
        state = htRentalParam("State")
        zip = htRentalParam("Zip")
        email = htRentalParam("Email")
        phone = htRentalParam("Phone")

        '  htRentalParam.Add("CardName", cardname)
        'cardtype = htRentalParam("CardType")
        'cardexpiration = htRentalParam("CardExpiration")
        'cardnumber = htRentalParam("CardNumber")
        'cardcode = htRentalParam("CardCode")
        tbFirstName.Text = name
        tbLastName.Text = lastname
        tbSA.Text = address
        tbCell.Text = phone
        tbState.Text = state
        tbEmail.Text = email
        txtConfemail.Text = email
        tbCity.Text = city
        tbState.Text = state
        tbZip.Text = zip
        lblcost.Text = Extentions.ChangeCurrency(TotalCost)
        'lblcost2.Text = Extentions.ChangeCurrency(TotalCost)
        lblcls.Text = classDescription

        '  HyperLink1.NavigateUrl = "terms.aspx?ploc=" & picklocation

        ' pnlCard.Enabled = True
        'txtCreditCard.Visible = False
        ' txtCreditCard.ReadOnly = True
        '  Session("_CCNo") = cardnumber.ToString()
      '

        ' If cardexpiration.ToString.Length > 2 Then
        '   txtExpMonth.Text = cardexpiration.ToString.Substring(0, 2)
        '  txtExpYear.Text = cardexpiration.ToString.Substring(cardexpiration.ToString.Length - 2)
        ' End If

        'If cardtype = "" Then
        'Else
        '    drpcard.SelectedValue = cardtype
        'End If


        Trace.Write("CALLING DISPLAY BEFORE")
        DisplayVehicleInfo(picklocation, droplocation, pudt, dodt)

        lblmconf.Text = htRentalParam("Conf")
        If (String.IsNullOrEmpty(lblmconf.Text)) Then
            lblconftext.Visible = False
        Else
            lblconftext.Visible = True
        End If

        lblreservationdetail.Text = Session("RentalDetail")
        lblimage.Text = "<img width=260  src=carimages/" + classCode + ".jpg>"
    End Sub
	
    Public Sub DisplayVehicleInfo(ploc, dloc, ptime, dtime)
        Trace.Write("INSIDE DISPLAY")
        Dim pickupdate, dropoffdatedate As String
        Dim pickuptime, dropofftime As String

        Dim loc As New RequestMaker

        Trace.Write("PTIME" & ptime)
        lblploc.Text = loc.GetLocation(ploc)
        lbldloc.Text = loc.GetLocation(dloc)
        
		pickuptime = Request.QueryString("putime")
        dropofftime = Request.QueryString("dotime")
		
        If Not String.IsNullOrEmpty(ptime) Then
			
            Dim freecncl As String = ptime.Insert(14, " ").Insert(4, " ").Insert(2, " ")
			dim freecnclDAr() As String = Split(freecncl)
			dim freecnclTAr() As String = Split(freecnclDAr(3), ":")
			Dim freecnclDate As New System.DateTime(freecnclDAr(2), freecnclDAr(0), freecnclDAr(1), freecnclTAr(0), freecnclTAr(1), 0) 
			
			lblfreecncl.Text = freecnclDate.AddDays(-1).ToString("g")
			lblfreecncl1.Text = freecnclDate.AddDays(-1).ToString("g")
			lblptime.Text = freecnclDate.ToString("g")
			
        Else
            Response.Redirect("sessionExpired.aspx")
        End If
		
        If Not String.IsNullOrEmpty(dtime) Then
			
			dim dDate As String = dtime.Insert(14," ").Insert(4, " ").Insert(2, " ")
			dim dDateDAr() As String = Split(dDate)
			dim dDateTAr() As String = Split(dDateDAr(3), ":")
			Dim dDateDate As New System.DateTime(dDateDAr(2), dDateDAr(0), dDateDAr(1), dDateTAr(0), dDateTAr(1), 0) 
			
			lbldtime.Text = dDateDate.ToString("g")
        End If

        '   lblcode1.Text = RequestMaker.Classes(Request.QueryString("classCode"))
        Dim address, city, state, zip, phone
        loc.GetAddress(ploc, address, city, state, zip, phone)

        'lbladdress.Text = address
        'lblcity.Text = city
        'lblstate.Text = state
        'lblzip.Text = zip
        'lblphone.Text = phone


        loc.GetAddress(dloc, address, city, state, zip, phone)

        'lbldaddress.Text = address
        'lbldcity.Text = city
        'lbldstate.Text = state
        'lbldzip.Text = zip
        'lbldphone.Text = phone


    End Sub

    Public Sub GetClassDetail()
        Dim xml2 As String
        xml2 = RequestMaker.MakeRateRequest(RequestMaker.RequestClass(Request.QueryString("classCode")))
        Dim doc As XDocument = XDocument.Parse(xml2)
        Dim luggage, capacity
        ' cls = doc...<ClassDescription>.Value
        luggage = doc...<LuggageCount>.Value
        capacity = doc...<PassengerCount>.Value
        Dim make = doc...<SimilarText>.Value

        lblcls.Text = make
        lblclass.Text = make
        ' lblluggage.Text = luggage
        'lblpassenger.Text = capacity
        ' lblmake.Text = make

    End Sub
    

    Protected Sub lnkChange_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkChange.Click
        Dim vh As New RequestMaker
        vh.RetainVehicleInfo()
    End Sub
    Public Sub BindButtonText()
        If Request.QueryString("prepaid") = "y" Then

            btnReserve.Text = "🔒 Book this car for AR " + Extentions.ChangeCurrency(Request.QueryString("TotalCost")) + " ❯"
        Else

            btnReserve.Text = "🔒 Book this car for AR " + Extentions.ChangeCurrency(Request.QueryString("TotalCost")) + " ❯"

        End If

    End Sub


    'Function Created By  pradeep 09-09-2015
    Public Function LoadDynamicPolicy() As String

        Dim _htRentalParam As Hashtable = Nothing
        _htRentalParam = TryCast(Session("_htRentalParam"), Hashtable)

        Dim pickupLocation As String = _htRentalParam("PickupLocation")
        Dim reqXML As String = RequestMaker.PolicyRequest(pickupLocation)
        Dim url As String = System.Configuration.ConfigurationManager.AppSettings("Url")

        Dim uri As String = url

        Dim req As WebRequest
        Dim rep As WebResponse


        req = WebRequest.Create(uri)
        req.Method = "POST"
        req.ContentType = "text/xml"

        Dim writer As New StreamWriter(req.GetRequestStream())
        writer.Write(reqXML)

        writer.Close()

        rep = req.GetResponse()

        Dim receivestream As Stream = rep.GetResponseStream()
        Dim encode As Encoding = Encoding.GetEncoding("utf-8")
        Dim readstream As New StreamReader(receivestream, encode)

        Dim read(256) As [Char]
        Dim result As String
        Dim text As String = ""
        Dim counter As Integer
        Dim Tcounter As Integer

        counter = 0
        Tcounter = 0
        result = readstream.ReadToEnd()
        Dim htmlContent As New StringBuilder()
        Dim doc As XDocument = XDocument.Parse(result)
        Dim p = From item In doc.Descendants("Policy")
        For Each item In p.ToList()
            text += item.Element("PolicyText").Value + "</br></br>"
        Next

        Return text
    End Function


    Public Function ConvertToDateTime(ByVal day As String, ByVal month As String, ByVal year As String) As DateTime
        Return Convert.ToDateTime(day & "/" & month & "/" & year).ToString()
    End Function

End Class
