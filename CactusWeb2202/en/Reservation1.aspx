<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Reservation1.aspx.vb" Trace="false" Inherits="Reservation1" MasterPageFile="MastarList.master" %>

<%@ Register Src="Controls/address.ascx" TagName="address" TagPrefix="ad" %>

<asp:Content ContentPlaceHolderID="head" runat="server" ID="header">
    <title>Reservation</title>


    <style>
        .error
        {
            color: red;
        }


        .plcy
        {
            height: 200px;
            overflow-x: auto;
            width: 100%;
            border: 1px solid #ccc;
            margin-top: 15px;
            margin-bottom: 15px;
            padding: 5px;
        }

        .please-wait
        {
            position: fixed;
            top: 0px;
            left: 0px;
            width: 100vw;
            height: 100vh;
            background: rgba(192,192,192,0.8);
            z-index: 99999999999;
        }

        .loading-msg
        {
            font-size: 3em;
            line-height: 1em;
            position: absolute;
            width: 50%;
            height: 50%;
            margin: auto;
            top: 0px;
            bottom: 0px;
            right: 0px;
            left: 0px;
            text-align: center;
        }

        .chbx
        {
            margin-right: 500px;
            margin-top: 10px;
            float: right;
            position: relative;
        }
    </style>
    <!--script>
        function ValidatePage() {
            try
			{
			 Page_ClientValidate();
            if (typeof (Page_ClientValidate) == 'function') {
                Page_ClientValidate();
				console.log("Inside");
            }

            if (Page_IsValid) {             
                $('#btnReserve').hide();
                $('#bWait').show();
                return true;
            }
            else {               
             return false;

            }
			}
			catch(ex)
			{
			console.log(ex);
			 return true;
			
			}
        }
		</script-->
      <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script type="text/javascript">
        function ShowAccountDetails() {
            alert("show");
            $("#cardDetail").show();
        }
        function ShowPaymentLink(obj, msg) {
            $("#lnkpayment").show();
            $(obj).hide();
            SetMessage(msg);
        }



        function SetMessage(msg) {
            $('#divmessage').html(msg);
        }



        $(document).ready(function () {
            (function preventMultipleSubmissions() {
                $(".please-wait").show();
                window.onbeforeunload = preventMultipleSubmissions
            });
            $(".please-wait").hide();
            $(".dateOfBirth").prop('required', true);
        });



    </script>
</asp:Content>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="ContentPlaceHolder1">

    <section class="info-user padding-section">

        <asp:Panel ID="pnlLeft" runat="server" Visible="true">
            <div id="divmessage" class='loading-msg'>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-md-4">
                        <div class="side-bar">
                            <div class="about-car">
                                <div class="title">
                                    <h2>Sedan 4 doors</h2>
                                    <span>Chevrolet Classic</span>

                                    <asp:Label ID="carModelName" runat="server"></asp:Label><asp:Label ID="lblcls" runat="server" Visible="false"></asp:Label>

                                </div>
                                <figure>
                                    <img src="/development/cactus/www/images/about-car.png" alt="">
                                </figure>
                                <ul class="hidden-sm hidden-xs">

                                    <li>
                                        <figure>
                                            <img src="/development/cactus/www/images/about-1.png" alt="">
                                        </figure>
                                        <span>6,5lts / 100km</span>
                                    </li>
                                </ul>
                                <ul class="hidden-sm hidden-xs">

                                    <li>
                                        <figure>
                                            <img src="/development/cactus/www/images/about-3.png" alt="">
                                        </figure>
                                        <span>A/C - Heater <span></li>
                                    <li>
                                        <figure>
                                            <img src="/development/cactus/www/images/about-4.png" alt="">
                                        </figure>
                                        <span>Manual Gear box</span>
                                    </li>
                                </ul>
                                <ul class="hidden-sm hidden-xs full">
                                    <li></li>
                                    <li>
                                        <figure>
                                            <img src="/development/cactus/www/images/about-6.png" alt="">
                                        </figure>
                                        <span>5 passengers </span>
                                    </li>
                                    <li>
                                        <figure>
                                            <img src="/development/cactus/www/images/about-7.png" alt="">
                                        </figure>
                                        <span>2 XL luggage and<br>
                                            2 M luggage</span>
                                    </li>
                                </ul>
                            </div>
                            <div class="rental-information">

                                <table>
                                    <thead>
                                        <tr>
                                            <th>Rental Details										</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>
                                                <span>Pick up Location:</span>
                                                <h3>
                                                    <asp:Label ID="lblploc" runat="server"></asp:Label></h3>
                                                <p>
                                                    <asp:Label ID="lblpickupdateText" runat="server" meta:resourcekey="lblpickupdateTextResource1"></asp:Label>
                                                    <asp:Label ID="lblptime" runat="server" meta:resourcekey="lblptimeResource1"></asp:Label>
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <span>Return Location:</span>
                                                <h3>
                                                    <asp:Label ID="lbldloc" runat="server"></asp:Label></h3>
                                                <p>
                                                    <asp:Label ID="lbldropffDatetext" runat="server" meta:resourcekey="lbldropffDatetextResource1"></asp:Label>
                                                    <asp:Label ID="lbldtime" runat="server" meta:resourcekey="lbldtimeResource1"></asp:Label>
                                                </p>
                                            </td>
                                        </tr>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th>Rental days:
                                                <asp:Label ID="LabelRPeriod" runat="server" meta:resourcekey="LabelRPeriod"></asp:Label>Days</th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                            <%--  <span style="color:#F00;">TIE IN EXTRAS SELECTION HERE</span>--%>
                            <table class="detail-user hidden-sm hidden-xs">
                                <thead>
                                    <tr>
                                        <th>DETAIL</th>
                                        <th>SUBTOTAL</th>
                                    </tr>
                                </thead>
                                <tbody>


                                    <asp:Label ID="Label1" runat="server" meta:resourcekey="Label1Resource1"></asp:Label>




                                </tbody>
                                <tfoot>
                                    <tr>
                                        <td>Total Payment</td>
                                        <td>
                                            <asp:Label ID="lblcost" CssClass="important-alert" runat="server"></asp:Label><br>
                                            Tax included
                                        </td>
                                    </tr>
                                </tfoot>
                            </table>
                            <asp:LinkButton CausesValidation="false" ID="lnkChange" Text="&laquo; Start Over" CssClass="grey-btn" runat="server" Visible="false"></asp:LinkButton>
                            <div class="info">
                                <div class="info-cancel">
                                    <figure>
                                        <img src="/development/cactus/www/images/icon-1.png" alt="">
                                    </figure>
                                    <h3>Free Cancellation
                                    </h3>
                                    <span>Modify or cancel your reservation at no cost before MONTH DAY YEAR
                                    </span>
                                </div>
                                <div class="info-attention">
                                    <figure>
                                        <img src="/development/cactus/www/images/icon-2.png" alt="">
                                    </figure>
                                    <h3>Minimum age of driver 21 years.
                                    </h3>
                                    <span>If the additional driver is between 18 and 21 years must pay an additional
                                    </span>
                                    <div class="info-attention-contact">
                                        <ul>
                                            <li>
                                                <i class="fa fa-print"></i>
                                                <span>Print this reservation</span>
                                            </li>
                                            <li>
                                                <i class="fa fa-envelope"></i>
                                                <span>Share this offer</span>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>


                    </div>

                    <asp:Panel runat="server" ID="pnlReserve">
                        <asp:Label ID="lblMessage" runat="server" Style="color: red; font-weight: bold; font-size: xx-large"></asp:Label>


                        <asp:Label ID="lblconftext" Text="Confirmation Number:" Visible="false" runat="server"></asp:Label>
                        <asp:Label ID="lblmconf" runat="server"></asp:Label>



                        <div class="col-md-8">
                            <div class="your-data">
                                <table class="data-user">
                                    <thead>
                                        <tr>
                                            <th>4. Driver Information</th>
                                        </tr>
                                    </thead>
                                    <tbody>




                                        <tr>
                                            <td><span>Name*:</span></td>
                                            <td>
                                                <asp:TextBox runat="server" placeholder="First Name" CssClass="txtinputfld " ID="tbFirstName" required="required"></asp:TextBox>

                                            </td>
                                        </tr>

                                        <tr>
                                            <td><span>Surname*:</span></td>
                                            <td>
                                                <asp:TextBox runat="server" placeholder="Surname" CssClass="txtinputfld" ID="tbLastName" required="required"></asp:TextBox>

                                            </td>
                                        </tr>
                                        <tr>
                                            <td><span>Date of birth</span></td>
                                            <td>
                                                <div class="data-item">
                                                    <label>Month</label>

                                                    <asp:DropDownList ID="selectMonth" runat="server">
                                                    </asp:DropDownList>
                                                </div>
                                                <div class="data-item">
                                                    <label>Date</label>
                                                    <asp:DropDownList ID="selectDay" runat="server">
                                                    </asp:DropDownList>
                                                </div>
                                                <div class="data-item">
                                                    <label>Year</label>

                                                    <asp:DropDownList ID="selectYear" runat="server">
                                                    </asp:DropDownList>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><span>Email</span></td>
                                            <td>
                                                <asp:TextBox runat="server" placeholder="Email" CssClass="txtinputfld" ID="tbEmail" required="required"></asp:TextBox></td>
                                            <td>
                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ControlToValidate="tbEmail" CssClass="error" ErrorMessage="Input valid email address!">
                                                </asp:RegularExpressionValidator></td>
                                        </tr>
                                        <tr>
                                        </tr>
                                        <tr>
                                            <td><span>Confirm Email</span></td>
                                            <td>
                                                <asp:TextBox runat="server" CssClass="txtinputfld" ID="txtConfemail" placeholder="Confirm Email" required="required"></asp:TextBox></td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td>
                                                <asp:CompareValidator ID="comparePasswords" CssClass="error" runat="server" ControlToCompare="tbEmail" ControlToValidate="txtConfemail" ErrorMessage="The email adddresses do not match up" Display="Dynamic" /></td>
                                        </tr>
                                        <tr>
                                            <td><span>Phone</span></td>
                                            <td>

                                                <asp:TextBox runat="server" CssClass="txtinputfld" type="tel" ID="tbCell" placeholder="+54"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <span>Over 25yrs of Age?:</span>
                                                <asp:CheckBox CssClass="chbx" ID="tbAge" runat="server" />


                                            </td>
                                        </tr>

                                        <tr>
                                            <td><span>Street Address:</span></td>
                                            <td>
                                                <asp:TextBox runat="server" ID="tbSA" placeholder="Street Address"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><span>City / Town:</span></td>
                                            <td>
                                                <asp:TextBox runat="server" ID="tbCity" placeholder="City"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><span>State:</span></td>
                                            <td>
                                                <asp:TextBox runat="server" ID="tbState" placeholder="State"></asp:TextBox>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td><span>Zip Code:</span></td>
                                            <td>
                                                <asp:TextBox runat="server" ID="tbZip" placeholder="Zip Code"></asp:TextBox>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td><span>Driver's License No:*</span></td>
                                            <td>
                                                <asp:TextBox runat="server" ID="tbLicensenumber" placeholder="License Number"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><span>Driver's License Expiration:*</span></td>
                                            <td>
                                                <asp:TextBox runat="server" ID="tbLicenseexpiration" Width="100%" placeholder="License Expiration Date"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                    </tbody>
                                </table>

                                <table class="code-user">
                                    <thead>
                                        <tr>
                                            <th>5. Additional Driver</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <!-- <tr>
									<td><span>Seleccione su Tarjeta:</span></td>
									<td>
										<select>
											<option>1</option>
											<option>2</option>
											<option>3</option>
											<option>4</option>
										</select>
									</td>
								</tr> -->
                                        <tr>
                                            <td>
                                                <span>Additional Driver N&deg;1:
                                                    <br>
                                                    Name and Surname*:
                                                </span>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="additionalFirstDriverName" placeholder="Name" runat="server"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><span>Date of birth</span></td>
                                            <td>
                                                <div class="data-item">
                                                    <label>Month</label>

                                                    <asp:DropDownList ID="selectMonth1" runat="server">
                                                    </asp:DropDownList>

                                                </div>
                                                <div class="data-item">
                                                    <label>Day</label>
                                                    <asp:DropDownList ID="selectDay1" runat="server">
                                                    </asp:DropDownList>

                                                </div>
                                                <div class="data-item">
                                                    <label>Year</label>

                                                    <asp:DropDownList ID="selectYear1" runat="server">
                                                    </asp:DropDownList>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <span>Additional Driver N&deg;2:
                                                    <br>
                                                    Name and Surname*:
                                                </span>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="additionalSecondDriverName" placeholder="Name" runat="server"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><span>Date of birth</span></td>
                                            <td>
                                                <div class="data-item">
                                                    <label>Month</label>

                                                    <asp:DropDownList ID="selectMonth2" runat="server">
                                                    </asp:DropDownList>
                                                </div>
                                                <div class="data-item">
                                                    <label>Day</label>

                                                    <asp:DropDownList ID="selectDay2" runat="server">
                                                    </asp:DropDownList>
                                                </div>
                                                <div class="data-item">
                                                    <label>Year</label>

                                                    <asp:DropDownList ID="selectYear2" runat="server">
                                                    </asp:DropDownList>


                                                </div>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                <table class="card-user" id="cardDetail" style="display: none">
                                    <thead>
                                        <tr>
                                            <th>6. GUARANTEE info</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>
                                                <span>Please select your card:</span>
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="drpcard" runat="server">

                                                    <asp:ListItem Value="#" Text="Please select your card"></asp:ListItem>
                                                    <asp:ListItem Value="VI" Text="VISA"></asp:ListItem>
                                                    <asp:ListItem Value="MC" Text="Master Card"></asp:ListItem>
                                                    <asp:ListItem Value="AX" Text="American Express"></asp:ListItem>
                                                    <asp:ListItem Value="DS" Text="Discover"></asp:ListItem>
                                                </asp:DropDownList>

                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <span>Credit Card Number:</span>
                                            </td>
                                            <td>
                                                <div class="secur">
                                                    <asp:TextBox runat="server" ID="txtCreditCard" Width="200" required="required"></asp:TextBox>
                                                </div>
                                                <div class="input-box">
                                                    <span class="hidden-sm hidden-xs hidden-md">No charges will be processed to this card! We only need this information to guarantee your reservation</span>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <span>Valid through:</span>
                                            </td>
                                            <td>
                                                <div class="data-item">
                                                    <label>Month</label>
                                                    <asp:DropDownList ID="txtExpMonth" runat="server">
                                                        <asp:ListItem Value="01" Text="01"></asp:ListItem>
                                                        <asp:ListItem Value="02" Text="02"></asp:ListItem>
                                                        <asp:ListItem Value="03" Text="03"></asp:ListItem>
                                                        <asp:ListItem Value="04" Text="04"></asp:ListItem>
                                                        <asp:ListItem Value="05" Text="05"></asp:ListItem>
                                                        <asp:ListItem Value="06" Text="06"></asp:ListItem>
                                                        <asp:ListItem Value="07" Text="07"></asp:ListItem>
                                                        <asp:ListItem Value="08" Text="08"></asp:ListItem>
                                                        <asp:ListItem Value="09" Text="09"></asp:ListItem>
                                                        <asp:ListItem Value="10" Text="10"></asp:ListItem>
                                                        <asp:ListItem Value="11" Text="11"></asp:ListItem>
                                                        <asp:ListItem Value="12" Text="12"></asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                                <div class="data-item">
                                                    <label>Year</label>
                                                    <asp:DropDownList ID="txtExpYear" runat="server">
                                                        <asp:ListItem Value="15" Text="15"></asp:ListItem>
                                                        <asp:ListItem Value="16" Text="16"></asp:ListItem>
                                                        <asp:ListItem Value="17" Text="17"></asp:ListItem>
                                                        <asp:ListItem Value="18" Text="18"></asp:ListItem>
                                                        <asp:ListItem Value="19" Text="19"></asp:ListItem>
                                                        <asp:ListItem Value="20" Text="20"></asp:ListItem>
                                                        <asp:ListItem Value="21" Text="21"></asp:ListItem>
                                                        <asp:ListItem Value="22" Text="22"></asp:ListItem>
                                                        <asp:ListItem Value="23" Text="23"></asp:ListItem>
                                                        <asp:ListItem Value="24" Text="24"></asp:ListItem>
                                                        <asp:ListItem Value="25" Text="25"></asp:ListItem>
                                                        <asp:ListItem Value="26" Text="26"></asp:ListItem>
                                                        <asp:ListItem Value="27" Text="27"></asp:ListItem>
                                                        <asp:ListItem Value="28" Text="28"></asp:ListItem>
                                                        <asp:ListItem Value="29" Text="29"></asp:ListItem>
                                                        <asp:ListItem Value="30" Text="30"></asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><span>CVV:</span></td>
                                            <td>
                                                <asp:TextBox runat="server" ID="txtcode"></asp:TextBox>


                                                <figure>
                                                    <a data-toggle="modal" data-target="#myModal2">
                                                        <img src="/development/cactus/www/images/icon-i.png" alt=""></a>
                                                </figure>
                                                <div class="modal fade" id="myModal2" role="dialog">
                                                    <div class="modal-dialog">
                                                        <!-- Modal content-->
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                                <figure>
                                                                    <img src="/development/cactus/www/images/icon-i-2.png" alt="">
                                                                </figure>
                                                                <h4 class="modal-title">Lorem Ipsum is</h4>
                                                            </div>
                                                            <div class="modal-body">
                                                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <figure>
                                                    <img src="/development/cactus/www/images/icon-3.png" alt="">
                                                </figure>
                                                <span>No charges will be processed to this card! We only need this information to guarantee your reservation</span>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                <table class="info-users">
                                    <thead>
                                        <tr>
                                            <th>7. Additional Info</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>
                                                <span>Do you know your arrival flight?:</span>
                                                <asp:TextBox ID="arrivalflight" runat="server"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <span>Need a Hotel?:</span>
                                                <asp:RadioButton ID="radioHotelYes" runat="server" GroupName="Hotel" Text="Yes" />

                                                <asp:RadioButton ID="radioHotelNo" runat="server" GroupName="Hotel" Text="No" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <span>Comments:</span>
                                                <asp:TextBox ID="Comments" runat="server" TextMode="MultiLine"></asp:TextBox>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                <table class="condition-user">
                                    <thead>
                                        <th>8. Terms and conditions</th>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>
                                                <asp:CheckBox ID="chkpolicy" runat="server" required="required" />


                                                <p>I have read and accepted the rental information and the terms and conditions.</p>


                                                <asp:Label ID="lblchk" ForeColor="red" runat="server" Font-Names="Arial" Text=""></asp:Label>

                                                <asp:Literal ID="ltrlPolicy" runat="server" Text=""></asp:Literal>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                <table class="detail-user hidden-md hidden-lg">
                                    <thead>
                                        <th>DETAIL</th>
                                        <th>SUBTOTAL</th>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>Rental day : 15</td>
                                            <td>11250$</td>
                                        </tr>
                                        <tr>
                                            <td>CDW PREMIUM</td>
                                            <td>12250$</td>
                                        </tr>
                                        <tr>
                                            <td>Baby seat</td>
                                            <td>21250$</td>
                                        </tr>
                                        <tr>
                                            <td>Local mobile phone</td>
                                            <td>1250$</td>
                                        </tr>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <td>Total Payment</td>
                                            <td>$xxxxxxx
										Taxes included
                                            </td>
                                        </tr>
                                    </tfoot>
                                </table>









                                <div class="info">
                                    <div class="info-cancel">
                                        <figure>
                                            <img src="/development/cactus/www/images/icon.png" alt="">
                                        </figure>
                                        <h3>Free Amendments
                                        </h3>
                                        <span>If you change your mind you can modify or cancel your reservation for free before: June 16th of 2015 a las 11:30.
                                        </span>
                                    </div>
                                </div>
                                <div class="continue">


                                    <asp:Button runat="server" CausesValidation="true" OnClientClick="return ValidatePage()" CssClass="" Style="float: left;" ID="btnReserve" ClientIDMode="Static"></asp:Button>
                                    <asp:HyperLink runat="server" ID="lnkpayment" Visible="false" ClientIDMode="Static">click here to continue to Pay U</asp:HyperLink>
                                    <button class="bnt-continue">
                                        <i class="fa fa-lock"></i>Book this car for AR
                                        <asp:Label ID="lblcost2" CssClass="important-alert" runat="server"></asp:Label><i class="fa fa-angle-right"></i></button>
                                </div>
                                <!-- <div class="total">
							<h4>Total:</h4>
							<span>
								ARS $1435,34 <br>
								<em>iva incluido</em>
							</span>
						</div> -->
                            </div>
                            <div class="col-md-12">
                                <div class="infor hidden-md hidden-lg">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="info-cancel">
                                                <figure>
                                                    <img src="/development/cactus/www/images/icon-1.png" alt="">
                                                </figure>
                                                <h3>Free Cancellation
                                                </h3>
                                                <span>Modify or cancel your reservation at no cost before MONTH DAY YEAR
                                                </span>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="info-i">
                                                <div class="info-item">
                                                    <figure>
                                                        <img src="/development/cactus/www/images/icon-5.png" alt="">
                                                    </figure>
                                                    <span>Minimum age of driver 21 years.
								
								<b>If the additional driver is between 18 and 21 years must pay an additional
                                </b>

                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="info-attention hidden-md hidden-lg">
                                    <div class="info-attention-contact">
                                        <ul>
                                            <li>
                                                <a href="#" class="fa fa-print"></a>
                                                <span>Print this reservation</span>
                                            </li>
                                            <li>
                                                <a href="#" class="fa fa-file-pdf-o"></a>
                                                <span>Share this offer</span>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <br style="clear: both;">








                        <!-- Right Bar-->
                        <div class="home-righ1t">

                            <div class="page">




                                <div class="col-sm-12" style="background: #fff; padding: 20px 0px;">




                                    <div class="col-sm-12">
                                        <div class="col-sm-12">
                                            <asp:TextBox runat="server" ID="txtconf" Width="195px" Visible="false"></asp:TextBox><br />
                                            <asp:Label ID="conNum" runat="server"></asp:Label>
                                        </div>
                                    </div>







                                    <div class="col-sm-12">



                                        <!-- TESTING ZONE -->







                                        <!-- END ZONE -->

                                    </div>

                                </div>
                            </div>
                    </asp:Panel>


                    <asp:Panel ID="pnlsummary" runat="server" Visible="false">

                        <div style="margin-bottom: 10px;">
                            <p>
                                <asp:Label ID="lblclass" Font-Bold="true" runat="server"></asp:Label>
                            </p>

                            <asp:Label ID="lblimage" Width="400" runat="server"></asp:Label>

                            <asp:Label ID="lblreservationdetail" runat="server"></asp:Label>
                        </div>

                        <div class="col-sm-12" style="background: #fff; padding-top: 20px; padding-bottom: 20px;">
                            <h2>Your Reservation Has Been Placed, Thank you for using Cactus Rent a Car!</h2>
                            <h2>Confirmation Number:
                  <span style="color: #F00;">
                      <asp:Label ID="lblconf" runat="server"></asp:Label></span>

                            </h2>

                            <p class="print-btn"><a href="javascript:window.print()"><i class="fa fa-print"></i>Print This Page</a> </p>
                            <p>An e-mail including your reservation details has been sent to you.</p>
                            <h2>Reservation Detail</h2>


                        </div>

                        <script>
                            $(function () {
                                $(".please-wait").hide();


                            });
                        </script>

                    </asp:Panel>
                </div>

            </div>
        </asp:Panel>

    </section>
    

</asp:Content>
