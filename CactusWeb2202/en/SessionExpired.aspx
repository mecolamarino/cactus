﻿<%@ Page Title="" Language="VB" MasterPageFile="MasterPage.master" AutoEventWireup="false" CodeFile="SessionExpired.aspx.vb" Inherits="SessionExpired" culture="auto" meta:resourcekey="PageResource1" uiculture="auto" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">

      .textArea {
            background-color: #fff;
            border: 1px solid #cacaca;
            border-radius: 8px;
            margin-bottom: 18px;
            margin-top: 10px;
            padding: 15px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div class="textArea" >
 <h2 style="color:#027cc4;">Are you still there?</h2>
 
 <h3> <asp:label runat="server" ID="lblsession" 
        Text="Your session has been expired. Please click on the link to reserve" 
        meta:resourcekey="lblsessionResource1" style="color:red" ></asp:label></h3>
        
        <p><a href="default.aspx" class="gen_button">
    <asp:label runat="server" ID="lblreserve" Text="Reserve" 
        meta:resourcekey="lblreserveResource1"></asp:label></a></p>
        </div>
</asp:Content>

