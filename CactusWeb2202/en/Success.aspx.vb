﻿Imports System.Security.Cryptography


Partial Class Success
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not Page.IsPostBack Then
            Dim transactionId = Request.QueryString("transactionId")

            If Not String.IsNullOrEmpty(transactionId) Then
                FillTransactionResponse()
            Else
                Response.Redirect("Default.aspx")
            End If
        End If
    End Sub
    Private Sub FillTransactionResponse()
        Dim ApiKey = ConfigurationManager.AppSettings("APIKey").ToString()
        Dim merchant_id = Request.QueryString("merchantId")
        Dim referenceCode = Request.QueryString("referenceCode")
        Dim TX_VALUE = Request.QueryString("TX_VALUE")
        Dim New_value = TX_VALUE
        Dim currency = Request.QueryString("currency")
        Dim transactionState = Request.QueryString("transactionState")
        '  hashstring = String.Format("{0}~{1}~{2}~{3}~{4}", apiKey, configmerchantId, referenceCode.Value, amount.Value, "USD")
        Dim firma_cadena = String.Format("{0}~{1}~{2}~{3}~{4}", ApiKey, merchant_id, referenceCode, New_value, currency)
        Dim firmacreada = GetMD5HashData(firma_cadena)
        Dim firma = Convert.ToString(Request.QueryString("signature"))
        Dim reference_pol = Request.QueryString("reference_pol")
        Dim cus = Request.QueryString("cus")
        Dim extra1 = Request.QueryString("description")
        Dim pseBank As String = Request.QueryString("pseBank")
        Dim lapPaymentMethod = Request.QueryString("lapPaymentMethod")
        Dim transactionId = Request.QueryString("transactionId")
        Dim estadoTx As String = ""

        If (Request.QueryString("transactionState") = 4) Then
            estadoTx = "Transaction approved"
        Else
            If (Request.QueryString("transactionState") = 6) Then
                estadoTx = "Transaction rejected"
            Else
                If (Request.QueryString("transactionState") = 104) Then
                    estadoTx = "Error"
                Else
                    If (Request.QueryString("transactionState") = 7) Then
                        estadoTx = "Pending payment"
                    Else
                        estadoTx = Request.QueryString("mensaje")
                    End If

                End If

            End If

        End If

        Dim SB As StringBuilder = New StringBuilder()
        If firma <> "" Then
            '  If ((firma <> "" And firmacreada <> "") And (firma.ToUpper() = firmacreada.ToUpper())) Then
            SB.AppendLine("<br/><h2>Transaction Summary</h2><br/>" & _
      "<div class='col-md-3'><div class='your-data'></div></div> <div class='col-md-6' style='text-align:center'><div class='your-data'><table class='data-user' align='center'>                                                 " & _
      " <tr>                                                    " & _
      " <td align='left'>Transaction status</td>                             " & _
      " <td align='left'>" + estadoTx + "</td>                       " & _
      " </tr>                                                   " & _
      " <tr>                                                    " & _
      " <tr>                                                    " & _
      " <td align='left'>Transaction ID</td>                                 " & _
      " <td align='left'>" + transactionId + "</td>                  " & _
      " </tr>                                                   " & _
      " <tr>                                                    " & _
      " <td align='left'>Reference sale</td>                                 " & _
      " <td align='left'>" + reference_pol + "</td>                  " & _
      " </tr>                                                   " & _
      " <tr>                                                    " & _
      " <td align='left'>Reference transaction</td>                          " & _
      " <td align='left'>" + referenceCode + "</td>                  " & _
      " </tr>  ")
            If (pseBank <> "") Then
                SB.AppendLine(" <tr>" & _
        "  <td align='left'>Cus </td>                                          " & _
        "  <td align='left'>" + cus + " </td>                          " & _
        "  </tr>                                                  " & _
        "  <tr>                                                   " & _
        "  <td align='left'>Bank </td>                                         " & _
        "  <td align='left'>" + pseBank + "</td>                      " & _
        "  </tr>       ")
            End If
            SB.AppendLine("<tr><td align='left'>Total Amount</td>    " & _
          " <td align='left'>" + TX_VALUE + "</td>" & _
             "</tr>                                                   " & _
            " <tr>                                                    " & _
            " <td align='left'>Currency</td>                                       " & _
            " <td align='left'>" + currency + "</td>                       " & _
            " </tr>                                                   " & _
            " <tr>                                                    " & _
            " <td align='left'>Description</td>                                    " & _
            " <td align='left'>" + extra1 + "</td>                       " & _
            " </tr>                                                   " & _
            " <tr>                                                    " & _
            " <td align='left'>Entity</td>                                        " & _
            " <td align='left'>" + lapPaymentMethod + "</td>             " & _
            " </tr>                                                   " & _
            " </table></div></div> ")
            'Else
            ' SB.AppendLine("<br/><h2>Error validating digital signature.</h2> ")

            'End If

            lbltransactionDetail.Text = SB.ToString()

        Else
            lbltransactionDetail.Text = "<br/><h2>Error in Transaction.</h2>"
        End If
        RequestMaker.WriteSchema("PaytamResponse.txt", SB.ToString())

    End Sub
    Public Function GetMD5HashData(hasstring As String) As String
        Dim encyroted As String = ""
        Dim mds As MD5 = MD5.Create()

        '  //convert the input text to array of bytes
        Dim hashData As Byte() = mds.ComputeHash(Encoding.Default.GetBytes(hasstring))

        '//create new instance of StringBuilder to save hashed data
        Dim returnValue As StringBuilder = New StringBuilder()
        '//loop for each byte and add it to StringBuilder
        For index = 0 To hashData.Length - 1
            returnValue.Append(hashData(index).ToString("X2"))
        Next
        '// return hexadecimal string
        Return returnValue.ToString()
        Return encyroted
    End Function
End Class
