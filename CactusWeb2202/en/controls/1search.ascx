﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="Search.ascx.vb" Inherits="controls_Search" %>

<style>
    .ui-datepicker {
        font-size: 85%;
    }

    .error {
        color: red;
    }

    .search_steps {
        background-color: #F0813B;
        color: #fff;
        font-size: 16pt;
        padding: 5px 10px;
    }
	
	input, select {
		height:35px;
	}
</style>
        
          <div id="freeshipping" class="hidden"><h3><asp:Label ID="lblCarName"  runat="server"></asp:Label></h3>
      
            	<asp:Image ID="imgCarPicture" runat="server" Width="260" /></div>
    
    			<div class="clearfix"></div>
                
                <asp:DropDownList ID="vechiletype" runat="server" Visible="false" ClientIDMode="Static">
                    <asp:ListItem Value="">ALL VEHICLES</asp:ListItem>
                    <asp:ListItem Value="15 PASSENGER VAN">15 PASSENGER VAN</asp:ListItem>
                    <asp:ListItem Value="SPRINTER PASSENGER VANS">SPRINTER PASSENGER VANS</asp:ListItem>
                    <asp:ListItem Value="SPRINTER CARGO VAN">SPRINTER CARGO VAN</asp:ListItem>
                    <asp:ListItem Value="CARGO VAN">CARGO VAN</asp:ListItem>
                    <asp:ListItem Value="MINI VAN">MINI VAN</asp:ListItem>
                    <asp:ListItem Value="LEXUS LX 570">LEXUS LX 570 - SUV</asp:ListItem>
                    <asp:ListItem Value="MERCEDES-BENZ M350">MERCEDES-BENZ M350 - SUV</asp:ListItem>
                    <asp:ListItem Value="CHEVY SUBURBAN">CHEVY SUBURBAN - SUV</asp:ListItem>
                    <asp:ListItem Value="CHEVY TAHOE">CHEVY TAHOE - SUV</asp:ListItem>
                    <asp:ListItem Value="CADILLAC ESCALADE">CADILLAC ESCALADE - SUV</asp:ListItem>
                    <asp:ListItem Value="FORD F150 PICKUP">FORD F150 PICKUP</asp:ListItem>
                    <asp:ListItem Value="HONDA PILOT">HONDA PILOT - SUV</asp:ListItem>
                    <asp:ListItem Value="HONDA CR-V">HONDA CR-V - SUV</asp:ListItem>
                    <asp:ListItem Value="NISSAN MURANO">NISSAN MURANO - SUV</asp:ListItem>
                    <asp:ListItem Value="NISSAN ROGUE">NISSAN ROGUE - SUV</asp:ListItem>
                    <asp:ListItem Value="JAGUAR XJ">JAGUAR XJ</asp:ListItem>
                    <asp:ListItem Value="SEDAN">STANDARD SEDAN</asp:ListItem>
                    <asp:ListItem Value="SEDAN">PREMIUM SEDAN</asp:ListItem>

                </asp:DropDownList>
                
                <div class="col-xs-12">
                <div><strong>Pick up</strong></div>
                <asp:Label ID="lblmsg" ForeColor="white" Font-Bold="true" runat="server"
                    meta:resourcekey="lblmsg"></asp:Label>

                <asp:DropDownList ID="drppickuploc" runat="server" Visible="true"  Width="100%" ClientIDMode="Static">
                	
                    <asp:ListItem Value="" Selected="True">- Select Pick up Location -</asp:ListItem>
					
                    <asp:ListItem Value="MIA">Miami International Airport</asp:ListItem>
                    <asp:ListItem Value="MIAC01">Miami Beach </asp:ListItem>
                    <asp:ListItem Value="TPA">Tampa International Airport</asp:ListItem>
                    <asp:ListItem Value="FLL">Fort Lauderdale </asp:ListItem>
                    <asp:ListItem Value="MCO">Orlando International Airport</asp:ListItem>
                 
                    

                    <%-- <asp:ListItem Value="15 PASSENGER VAN">15 PASSENGER VAN</asp:ListItem>
                    <asp:ListItem Value="SPRINTER PASSENGER VANS">SPRINTER PASSENGER VANS</asp:ListItem>
                    <asp:ListItem Value="SPRINTER CARGO VAN">SPRINTER CARGO VAN</asp:ListItem>
                    <asp:ListItem Value="CARGO VAN">CARGO VAN</asp:ListItem>
                    <asp:ListItem Value="MINI VAN">MINI VAN</asp:ListItem>
                    <asp:ListItem Value="LEXUS LX 570">LEXUS LX 570 - SUV</asp:ListItem>
                    <asp:ListItem Value="MERCEDES-BENZ M350">MERCEDES-BENZ M350 - SUV</asp:ListItem>
                    <asp:ListItem Value="CHEVY SUBURBAN">CHEVY SUBURBAN - SUV</asp:ListItem>
                    <asp:ListItem Value="CHEVY TAHOE">CHEVY TAHOE - SUV</asp:ListItem>
                    <asp:ListItem Value="CADILLAC ESCALADE">CADILLAC ESCALADE - SUV</asp:ListItem>
                    <asp:ListItem Value="FORD F150 PICKUP">FORD F150 PICKUP</asp:ListItem>
                    <asp:ListItem Value="HONDA PILOT">HONDA PILOT - SUV</asp:ListItem>
                    <asp:ListItem Value="HONDA CR-V">HONDA CR-V - SUV</asp:ListItem>
                    <asp:ListItem Value="NISSAN MURANO">NISSAN MURANO - SUV</asp:ListItem>
                    <asp:ListItem Value="NISSAN ROGUE">NISSAN ROGUE - SUV</asp:ListItem>
                    <asp:ListItem Value="JAGUAR XJ">JAGUAR XJ</asp:ListItem>
                    <asp:ListItem Value="SEDAN">STANDARD SEDAN</asp:ListItem>
                    <asp:ListItem Value="SEDAN">PREMIUM SEDAN</asp:ListItem>
                    <asp:ListItem Value="all">ALL VEHICLES</asp:ListItem>--%>
                    <%--<asp:ListItem Value="PAR" Text="PAR" Selected="True"></asp:ListItem>

                    <asp:ListItem Value="SDQ" meta:resourcekey="ListItemResourcesdq" Text="Las Americas Airport (SDQ)"></asp:ListItem>--%>
                </asp:DropDownList>
                </div>

                <%-- <input name="" type="text" value="12/2/2014" /> --%>
                	<div class="col-xs-7">
                    <asp:TextBox ID="txtpickupdate" runat="server"  Width="100%" class="txt-date" placeholder="Select Pickup" autocomplete="off" ClientIDMode="Static"></asp:TextBox>
                    </div>
                    <%--   <select name="asdf" size="dsf"><option value="#">8:00 am</option></select>--%>
                    
                    <div class="col-xs-5">
                    <asp:DropDownList ID="drpPickuptime"  class="txt-time" Width="100%" runat="server" ClientIDMode="Static">
                    
    
                        <asp:ListItem Value="09:00am" Text="09:00 am" ></asp:ListItem>
                        <asp:ListItem Value="09:30am" Text="09:30 am" ></asp:ListItem>
                        <asp:ListItem Value="10:00am" Text="10:00 am" ></asp:ListItem>
                        <asp:ListItem Value="10:30am" Text="10:30 am" ></asp:ListItem>
                        <asp:ListItem Value="11:00am" Text="11:00 am" ></asp:ListItem>
                        <asp:ListItem Value="11:30am" Text="11:30 am" ></asp:ListItem>
                        <asp:ListItem Value="12:00pm" Text="12:00 pm" ></asp:ListItem>
                        <asp:ListItem Value="12:30pm" Text="12:30 pm" ></asp:ListItem>
                        <asp:ListItem Value="01:00pm" Text="01:00 pm" ></asp:ListItem>
                        <asp:ListItem Value="01:30pm" Text="01:30 pm" ></asp:ListItem>
                        <asp:ListItem Value="02:00pm" Text="02:00 pm" ></asp:ListItem>
                        <asp:ListItem Value="02:30pm" Text="02:30 pm" ></asp:ListItem>
                        <asp:ListItem Value="03:00pm" Text="03:00 pm" ></asp:ListItem>
                        <asp:ListItem Value="03:30pm" Text="03:30 pm" ></asp:ListItem>
                        <asp:ListItem Value="04:00pm" Text="04:00 pm" ></asp:ListItem>
                        <asp:ListItem Value="04:30pm" Text="04:30 pm" ></asp:ListItem>
                        <asp:ListItem Value="05:00pm" Text="05:00 pm" ></asp:ListItem>
        

                    </asp:DropDownList>
                    
                    </div>
                    
                    <div class="col-xs-12">
                    <asp:RequiredFieldValidator CssClass="error" ID="RequiredFieldValidator1"
                        ControlToValidate="txtpickupdate" runat="server"
                        ErrorMessage="Pickup Date is Required"
                        meta:resourcekey="RequiredFieldValidator1Resource1"></asp:RequiredFieldValidator>
                    </div>
                
                <div class="col-xs-12" style="margin-bottom:15px;">
                <strong>Drop Off</strong><br />

                <asp:DropDownList ID="drpreturnloc" runat="server" Visible="true"  Width="100%" ClientIDMode="Static">
                    <asp:ListItem Value="Same" Selected="True" meta:resourcekey="ListItemResourceSame" Text="Same as Pickup" ></asp:ListItem>
					
                    <asp:ListItem Value="MIA">Miami International Airport</asp:ListItem>
                    <asp:ListItem Value="MIAC01">Miami Beach </asp:ListItem>
                    <asp:ListItem Value="TPA">Tampa International Airport</asp:ListItem>
                    <asp:ListItem Value="FLL">Fort Lauderdale </asp:ListItem>
                    <asp:ListItem Value="MCO">Orlando International Airport</asp:ListItem>

                    <%--<asp:ListItem Value="SDQ" meta:resourcekey="ListItemResourcesdq" Text="Las Americas Airport (SDQ)"></asp:ListItem>
                    <asp:ListItem Value="PAR" Text="PAR" Selected="True"></asp:ListItem>--%>
                </asp:DropDownList>
                </div>

                <div class="col-xs-7">
                    <asp:TextBox ID="txtdropoffdate" Width="100%" runat="server" placeholder="Select Dropoff" class="txt-date" autocomplete="off" ClientIDMode="Static"></asp:TextBox>
                    
                </div>

                <div class="col-xs-5">  
                    <asp:DropDownList ID="drpDropOfftime" Width="100%" class="txt-time" runat="server" ClientIDMode="Static"
                        meta:resourcekey="drpDropOfftimeResource1">

          
                        <asp:ListItem Value="09:00am" Text="09:00 am" ></asp:ListItem>
                        <asp:ListItem Value="09:30am" Text="09:30 am" ></asp:ListItem>
                        <asp:ListItem Value="10:00am" Text="10:00 am" ></asp:ListItem>
                        <asp:ListItem Value="10:30am" Text="10:30 am" ></asp:ListItem>
                        <asp:ListItem Value="11:00am" Text="11:00 am" ></asp:ListItem>
                        <asp:ListItem Value="11:30am" Text="11:30 am" ></asp:ListItem>
                        <asp:ListItem Value="12:00pm" Text="12:00 pm" ></asp:ListItem>
                        <asp:ListItem Value="12:30pm" Text="12:30 pm" ></asp:ListItem>
                        <asp:ListItem Value="01:00pm" Text="01:00 pm" ></asp:ListItem>
                        <asp:ListItem Value="01:30pm" Text="01:30 pm" ></asp:ListItem>
                        <asp:ListItem Value="02:00pm" Text="02:00 pm" ></asp:ListItem>
                        <asp:ListItem Value="02:30pm" Text="02:30 pm" ></asp:ListItem>
                        <asp:ListItem Value="03:00pm" Text="03:00 pm" ></asp:ListItem>
                        <asp:ListItem Value="03:30pm" Text="03:30 pm" ></asp:ListItem>
                        <asp:ListItem Value="04:00pm" Text="04:00 pm" ></asp:ListItem>
                        <asp:ListItem Value="04:30pm" Text="04:30 pm" ></asp:ListItem>
                        <asp:ListItem Value="05:00pm" Text="05:00 pm" ></asp:ListItem>                        

                    </asp:DropDownList>
                    </div>
                    
                    <div class="col-xs-12">
                    <asp:RequiredFieldValidator CssClass="error" ID="RequiredFieldValidator2"
                        ControlToValidate="txtdropoffdate" runat="server"
                        ErrorMessage="Drop off Date is Required"
                        meta:resourcekey="RequiredFieldValidator2Resource1"></asp:RequiredFieldValidator>
                        </div>
                
                <div class="col-xs-12">
                <strong>Coupon Code/Corporate ID</strong><br />

                <asp:TextBox ID="txtdiscount" Width="100%" class="txt-promo" runat="server" placeholder="Optional" ClientIDMode="Static"></asp:TextBox>
                </div>


                <div class="col-xs-12" style="margin-top: 15px;">
                    <asp:Button CssClass="btn-get" ID="BtnSubmit"
                        runat="server"  Text="Get Rates &#8250;"
                        meta:resourcekey="BtnSubmitResource1" OnClick="BtnSubmit_Click" />
                </div>
                
                <div class="clearfix"></div>
            
        
      

                
  