﻿Imports System.Net
Imports System.IO
Imports System.Xml
Imports System.Xml.Serialization
Imports System.Linq
Imports System.Xml.Linq
Imports System.Web.Script.Serialization

Partial Class controls_Search
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

       
        If Not IsPostBack Then



            txtpickupdate.Text = Request.QueryString("PickupDate")
            txtdropoffdate.Text = Request.QueryString("DropoffDate")
            drpPickuptime.SelectedValue = Request.QueryString("PickupTime")
            drpDropOfftime.SelectedValue = Request.QueryString("DropoffTime")



            If Not String.IsNullOrEmpty(Request.QueryString("PickupDate")) And Not String.IsNullOrEmpty(Request.QueryString("DropoffDate")) And Not String.IsNullOrEmpty(Request.QueryString("PickupTime")) And Not String.IsNullOrEmpty(Request.QueryString("DropoffTime")) Then
                Dim htRentalParam As New Hashtable
                htRentalParam.Add("PickupLocation", drppickuploc.SelectedItem.Value)
                htRentalParam.Add("DropoffLocation", drpreturnloc.SelectedItem.Value)
                'htRentalParam.Add("PickupLocation", "KNIS")
                'htRentalParam.Add("DropoffLocation", "KNIS")
                htRentalParam.Add("PickupDate", Request.QueryString("PickupDate"))
                htRentalParam.Add("PickupTime", Request.QueryString("PickupTime"))
                htRentalParam.Add("DropoffDate", Request.QueryString("DropoffDate"))
                htRentalParam.Add("DropoffTime", Request.QueryString("DropoffTime"))
                htRentalParam.Add("PromoCode", txtdiscount.Text)
                '  htRentalParam.Add("Age", rdoAge.SelectedItem.Value)
                Session.Add("_htRentalParam", htRentalParam)
                Trace.Write("INSIDE")
                Response.Redirect("list.aspx")
            Else
                'Display car name and image if user selects a car from the fleet (7/14/2015)


            End If
        End If
    End Sub


    Protected Sub BtnSubmit_Click(sender As Object, e As EventArgs)


        If drppickuploc.SelectedItem.Value = "Select" Then
            lblmsg.Text = "Please select Location"
        Else
            Dim str As String = ""
            Dim TurnDiscount = True

            'Dim nowdate As DateTime = Convert.ToDateTime(Now).ToShortDateString
            'Dim pickupdate As DateTime = Convert.ToDateTime(txtpickupdate.Text)
            'Dim totaldays As TimeSpan = pickupdate - nowdate
            '  Dim days As Integer = totaldays.Days
            'Dim discountcode As String
            Dim returnloc As String
            'Dim pickloc As String
            '  Dim ratecode As String = rdoRate.SelectedItem.Value.ToString
            Dim htRentalParam As New Hashtable

            If drpreturnloc.SelectedItem.Value = "Same" Then
                returnloc = drppickuploc.SelectedItem.Value
            Else
                returnloc = drpreturnloc.SelectedItem.Value
            End If

            Dim location As Locations = New Locations()

            Dim loca As String = "test" 'Locations.GetGioLocation()
            htRentalParam.Add("PickupLocation", drppickuploc.SelectedItem.Value)
            htRentalParam.Add("DropoffLocation", returnloc)
            'htRentalParam.Add("PickupLocation", "KNIS")
            htRentalParam.Add("CountryLoc", loca)
            htRentalParam.Add("PickupDate", txtpickupdate.Text)
            htRentalParam.Add("PickupTime", drpPickuptime.SelectedItem.Value)
            htRentalParam.Add("DropoffDate", txtdropoffdate.Text)
            htRentalParam.Add("DropoffTime", drpDropOfftime.SelectedItem.Value)
            htRentalParam.Add("PromoCode", txtdiscount.Text)
            'htRentalParam.Add("ClassCode", Request.QueryString("code"))
            htRentalParam.Add("vechileType", "CCAR")
			

            'Set the customer number based on the rental location

            Dim rentalTSDCode As String = ""


                rentalTSDCode = System.Configuration.ConfigurationManager.AppSettings("Passcode").ToString



            htRentalParam.Add("CustomerNumber", rentalTSDCode)



            '  htRentalParam.Add("Age", rdoAge.SelectedItem.Value)
            Session.Add("_htRentalParam", htRentalParam)
            Trace.Write("INSIDE")

            BindCarListWithOrder()
            getrates()
            'Response.Redirect("list.aspx")
        End If
    End Sub

    'Code moved to Search Page from the list page.

    Dim listOfCarCode As New List(Of String)
    Dim req As WebRequest
    Dim rep As WebResponse
    Protected Sub BindCarListWithOrder()
        listOfCarCode.Add("CCAR")
        listOfCarCode.Add("ICAR")
        listOfCarCode.Add("FCAR")
        listOfCarCode.Add("IXAR")
        listOfCarCode.Add("IFAR") ' New Car code is Added after Ariel Request showing in the last.
        listOfCarCode.Add("IPAR")
        listOfCarCode.Add("FPAR")
        listOfCarCode.Add("MVAR")
    End Sub

    Protected Sub BindSessionWithQueryString()
        Dim htRentalParam As Hashtable = Nothing
        htRentalParam = TryCast(Session("_htRentalParam"), Hashtable)

        If Not String.IsNullOrEmpty(Request.QueryString("PickupDate")) And
            Not String.IsNullOrEmpty(Request.QueryString("DropoffDate")) And
            Not String.IsNullOrEmpty(Request.QueryString("PickupTime")) And
            Not String.IsNullOrEmpty(Request.QueryString("DropoffTime")) Then

            htRentalParam = New Hashtable

            htRentalParam.Add("PickupLocation", Request.QueryString("PickupLocation"))

            Dim dropOffLoc As String
            If Not String.IsNullOrEmpty(Request.QueryString("DropoffLocation")) Then
                dropOffLoc = Request.QueryString("DropoffLocation")
            Else
                dropOffLoc = Request.QueryString("PickupLocation")
            End If

            htRentalParam.Add("DropoffLocation", dropOffLoc)

            htRentalParam.Add("PickupDate", Request.QueryString("PickupDate"))
            htRentalParam.Add("PickupTime", Request.QueryString("PickupTime"))
            htRentalParam.Add("DropoffDate", Request.QueryString("DropoffDate"))
            htRentalParam.Add("DropoffTime", Request.QueryString("DropoffTime"))
            htRentalParam.Add("PromoCode", Request.QueryString("txtdiscount"))
            htRentalParam.Add("ClassCode", Request.QueryString("code"))
            htRentalParam.Add("vechileType", Request.QueryString("vechileType"))

            Session.Add("_htRentalParam", htRentalParam)
            Trace.Write("INSIDE")
        End If


    End Sub



    Protected Sub getrates()
        Dim pickupdate, dropoffdatedate As String
        Dim pickuptime, dropofftime, vechileType As String
        Dim discount As String = ""
        Dim url As String = System.Configuration.ConfigurationManager.AppSettings("Url")
        Dim cl As New RequestMaker
        vechileType = ""
        Dim htRentalParam As Hashtable = Nothing
        htRentalParam = TryCast(Session("_htRentalParam"), Hashtable)
        ' Dim uri As String = url
        Dim val As Integer
        'If Request.QueryString("modify") = "true" Then
        '    If htRentalParam Is Nothing Then
        '        Response.Redirect("SessionExpired.aspx")
        '    Else
        '        txtpickupdate.Text = pickupdate.Substring(0, 8).Insert(4, "/").Insert(2, "/")
        '        txtdropoffdate.Text = dropoffdatedate.Substring(0, 8).Insert(4, "/").Insert(2, "/")
        '        lnkChange.Visible = False
        '    End If
        'Else


        Dim ploc As String
        Dim dloc As String
        Dim loc As New RequestMaker
        Dim cLoc As String

        If Request.QueryString("modify") = "true" Then
            If htRentalParam Is Nothing Then
                Response.Redirect("SessionExpired.aspx")
                Trace.Write("EXPIRED")
            Else
                dropoffdatedate = htRentalParam("DropoffDate")
                pickupdate = htRentalParam("PickupDate")
            End If
        Else
            If htRentalParam Is Nothing Then
                Response.Redirect("SessionExpired.aspx")
                Trace.Write("EXPIRED")
            End If

            vechileType = htRentalParam("vechileType")
            pickuptime = htRentalParam("PickupTime")
            dropofftime = htRentalParam("DropoffTime")
            pickupdate = htRentalParam("PickupDate")
            val = pickupdate.IndexOf("/")
            If val = -1 Then

                dropoffdatedate = htRentalParam("DropoffDate")

                pickupdate = htRentalParam("PickupDate")
                dropoffdatedate = htRentalParam("DropoffDate")

                Trace.Write("dont Insert slash")

            Else
                Trace.Write("VAL" & val)
                Trace.Write("INSERT SLACSH")

                'dropoffdatedate = htRentalParam("DropoffDate") & " " & dropofftime
                'pickupdate = htRentalParam("PickupDate") & " " & pickuptime

                dropoffdatedate = cl.processDateString(htRentalParam("DropoffDate")) & " " & dropofftime
                pickupdate = cl.processDateString(htRentalParam("PickupDate")) & " " & pickuptime
            End If

        End If

        '  discount = Trim(Request.QueryString("disc"))

        discount = htRentalParam("PromoCode")
        'Dim ploc As String
        'Dim dloc As String
        'Dim loc As New RequestMaker

        ploc = htRentalParam("PickupLocation")
        dloc = htRentalParam("DropoffLocation")
        cLoc = htRentalParam("CountryLoc")


        '   Dim companyID
        ' companyID = Request.QueryString("companyID")

        Dim classCode As String = htRentalParam("ClassCode")
        Try



            Dim uri As String = url


            req = WebRequest.Create(uri)
            req.Method = "POST"
            req.ContentType = "text/xml"

            Dim writer As New StreamWriter(req.GetRequestStream())
            Dim rateRequestString As String
            rateRequestString = RequestMaker.RateRequest(pickupdate, dropoffdatedate, classCode, Me.Session.SessionID, Me.Request.UserHostAddress, ploc, dloc, discount, cLoc)

            writer.Write(rateRequestString)
            RequestMaker.WriteSchema("listCarsRequest.txt", rateRequestString)
            writer.Close()

            rep = req.GetResponse()

            Dim receivestream As Stream = rep.GetResponseStream()
            Dim encode As Encoding = Encoding.GetEncoding("utf-8")
            Dim readstream As New StreamReader(receivestream, encode)
            Dim miles, rentaldays

            Dim read(256) As [Char]
            Dim result As String
            'Dim IsLocal As Boolean
            Dim counter As Integer
            Dim Tcounter As Integer
            counter = 0
            Tcounter = 0
            result = readstream.ReadToEnd()

            RequestMaker.WriteSchema("listCarsResponse.txt", result)


            Dim xml As XDocument = XDocument.Parse(result)

            Dim el2 = From ele In xml...<RateProduct> _
                      Order By CDbl(ele...<RateAmount>.Value) Ascending _
                 Select New Rate(ele.<RateID>.Value, ele.<ClassCode>.Value, ele.<ClassDesc>.Value, ele.<RatePlan>.Value, ele...<RateCode>.Value, ele...<RentalDays>.Value, ele...<TotalPricing>.<RateAmount>.Value, ele...<Prepaid>.<RateAmount>.Value, ele...<PrepaidBaseDiscount>.Value, ele...<FreeMiles>.Value, ele...<TotalFreeMiles>.Value, ele...<RateCharge>.Value, ele...<TotalTaxes>.Value, ele...<TotalPricing>.<TotalCharges>.Value, ele...<Prepaid>.<TotalPricing>.<TotalCharges>.Value, ele...<CH>.Value, ele...<ClassImageURL>.Value, ele...<RateDiscount>.Value, ele.<ModelDesc>.Value, ele...<PerMileAmount>.Value, ele...<Rate1PerDay>.Value)

            Dim el3 = From nor In xml...<NoRatesFound> _
                     Select New NoRate(nor.<Class>.Value, nor.<Description>.Value, nor.<ClassImageURL>.Value, nor.<ModelDesc>.Value, nor.<PickupPhone>.Value)


            If el2.Count = 0 Then
                ltrlNoVechile.Text = " <script type='text/javascript'> $(document).ready(function () { $('#noRecordPopup').modal('show');});</script>"

            Else
                Dim el = From e3 In el2

                Dim nolist = From e4 In el3

                Dim filtercarcodelist As New List(Of Rate)
                Dim filterbycode As Rate
                Dim CarCode As String


                Try
                    For Each item In listOfCarCode
                        For Each CAR In el.ToList()

                            If (CAR.ClassCode = item) Then
                                filtercarcodelist.Add(CAR)
                            End If


                        Next

                    Next



                    filtercarcodelist.AddRange(el.ToList().Where(Function(name) Not listOfCarCode.Contains(name.ClassCode)).ToList())

                Catch ex As Exception


                End Try
                Session("_vehicleList") = filtercarcodelist

                Dim minDays As Integer = 1 ' el.Min(Function(i) i.RentalDays)
                Dim sbd As New StringBuilder()
                Dim sbdL As New StringBuilder()
                Dim sbdT As New StringBuilder()
                ' sbd.AppendLine("<table width=570 border=0>")
                Dim RegRate As Double

                ' Dim el6 = filtercarcodelist
                Dim el6 = el

                If Not String.IsNullOrEmpty(vechileType) Then
                    el6 = el6.Where(Function(name) name.ClassCode.Contains(vechileType) Or name.ClassDescription.Contains(vechileType)).ToList()
                Else

                End If

                ' We need only the records that has days cost greater then zero.

                'Response.Write(el6.Where(Function(name) (name.PrepaidAmount / name.RentalDays) > 0).Count())
                For Each item As Rate In el6 '.Where(Function(name) (name.PrepaidAmount / name.RentalDays) > 0)

                    Dim carClassInfo As New CarClass(item.ClassCode)


                    ' miles = item.TotalFreeMiles
                    If counter = 0 Then
                        rentaldays = item.RentalDays
                    End If
                    counter += 1

                    pickupdate = htRentalParam("PickupDate")
                    dropoffdatedate = htRentalParam("DropoffDate")
                    Dim qsr As String = ("?pudt=" + pickupdate + "&putime=" + pickuptime + "&dodt=" + dropoffdatedate + "&dotime=" + dropofftime + "&classCode=" + item.ClassCode + "&rateCode=" + item.RateCode + "&rateID=" + item.RateID.ToString() & "&ploc=" + ploc + "&rloc=" + dloc + "&disc=" & discount & "&modify=" & Request.QueryString("modify"))

                    sbdL.AppendLine("<a href='RateDetails.aspx" + qsr + "&prepaid=y" + "' class='btn-next'>Pay Now</a>")
                    Response.Redirect("RateDetails.aspx" + qsr)
                Next
            End If

        Catch ex As Exception
            Trace.Write("EX" & ex.ToString)
        End Try
    End Sub

    Function SerializeRate(ByVal rate As Rate) As String
        Dim result As String = ""
        Dim xs As New XmlSerializer(GetType(Rate))
        Dim wrt As New StringWriter()
        xs.Serialize(wrt, rate)
        result = wrt.ToString()
        wrt.Close()
        result = result.Replace("<?xml version=""1.0"" encoding=""utf-16""?>", "")
        result = result.Replace("<Rate xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"">", "")
        Return result

    End Function

    Function processDateString(ByVal value As String) As String
        Dim re() As String
        Dim c() As Char = {"/"c}
        re = value.Split(c)
        If re(0).Length < 2 Then
            re(0) = "0" + re(0)
        End If
        If re(1).Length < 2 Then
            re(1) = "0" + re(1)
        End If
        Return re(0) + re(1) + re(2)
    End Function

End Class
