﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="address.ascx.vb" Inherits="Controls_address" %>

<div class="col-sm-3 top-itin">
    <strong>Pickup:</strong>
    <asp:Label ID="lblptime" runat="server"></asp:Label>
    <!-- Pick up Loc -->
    <asp:Label ID="lblploc" runat="server" meta:resourcekey="lblplocResource1"></asp:Label>

    <asp:Label ID="lbladdress" runat="server"></asp:Label>
    <asp:Label ID="lblcity" runat="server"></asp:Label>
    <asp:Label ID="lblstate" runat="server"></asp:Label>
    <asp:Label ID="lblzip" runat="server"></asp:Label>
    <asp:Label ID="lblphone" runat="server"></asp:Label>
</div>

<div class="col-sm-3 top-itin">
    <strong>Return:</strong>
    <asp:Label ID="lbldtime" runat="server"></asp:Label><br />
	<!-- Pick up Loc -->
    <asp:Label ID="lbldloc" runat="server" meta:resourcekey="lblplocResource1"></asp:Label>
    <asp:Label ID="lbldaddress" runat="server"></asp:Label>
    <asp:Label ID="lbldcity" runat="server"></asp:Label><asp:Label ID="lbldstate" runat="server"></asp:Label>
    <asp:Label ID="lbldzip" runat="server"></asp:Label>
    <asp:Label ID="lbldphone" runat="server"></asp:Label>
</div>