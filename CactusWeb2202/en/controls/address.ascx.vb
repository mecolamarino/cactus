﻿
Partial Class Controls_address
    Inherits System.Web.UI.UserControl


    Public Sub DisplayInfo()
        Dim ploc
        Dim dloc

        Dim pickupdate, dropoffdatedate As String
        Dim pickuptime, dropofftime As String
        Dim loc As New RequestMaker


        'lblploc.Text = loc.GetLocation(ploc)
        'lbldloc.Text = loc.GetLocation(dloc)

        Dim address, city, state, zip, phone

        Dim htRentalParam As Hashtable = Nothing

        htRentalParam = TryCast(Session("_htRentalParam"), Hashtable)
        Dim pdatetime As String
        Dim ddatetime As String
        Dim pdate As String
        Dim ptime As String
        Dim ddate As String
        Dim dtime As String
        Dim val
        If htRentalParam Is Nothing Then
            pickuptime = Trim(Request.QueryString("putime"))
            dropofftime = Trim(Request.QueryString("dotime"))
            ploc = Trim(Request.QueryString("ploc"))
            dloc = Trim(Request.QueryString("rloc"))
            'DisplayInfo(ploc, dloc)
            pickupdate = Trim(Request.QueryString("pudt"))
            dropoffdatedate = Trim(Request.QueryString("dodt"))
            val = pickupdate.IndexOf("/")
            Trace.Write("val" & val)
            If val > -1 Then
                pickupdate = Trim(Request.QueryString("pudt"))
                dropoffdatedate = Trim(Request.QueryString("dodt"))
                Trace.Write("Insert slash")

            Else
                Trace.Write("I'm inside")

                pickupdate = pickupdate.Insert(4, "/").Insert(2, "/")
                dropoffdatedate = dropoffdatedate.Insert(4, "/").Insert(2, "/")
            End If



            lblptime.Text = pickupdate
            lbldtime.Text = dropoffdatedate

            loc.GetAddress(ploc, address, city, state, zip, phone)

            lbladdress.Text = address
            lblcity.Text = city
            lblstate.Text = state
            lblzip.Text = zip
            lblphone.Text = phone


            loc.GetAddress(dloc, address, city, state, zip, phone)

            lbldaddress.Text = address
            lbldcity.Text = city
            lbldstate.Text = state
            lbldzip.Text = zip
            lbldphone.Text = phone
        Else
            pdatetime = htRentalParam("PickupDate")
            ddatetime = htRentalParam("DropoffDate")
            pdate = htRentalParam("PickupDate")
            ddate = htRentalParam("DropoffDate")
            ptime = htRentalParam("PickupTime")
            dtime = htRentalParam("DropoffTime")
            If pdate.Length > 10 Then
                'lblptime.Text = pdate
                'lbldtime.Text = ddate
                lblptime.Text = pdatetime.Substring(0, 8).Insert(4, "/").Insert(2, "/") & "  " & pdatetime.Substring(pdatetime.Length - 8).ToLower
                lbldtime.Text = ddatetime.Substring(0, 8).Insert(4, "/").Insert(2, "/") & "  " & ddatetime.Substring(ddatetime.Length - 8).ToLower
            Else
                lblptime.Text = pdate + " " + ptime
                lbldtime.Text = ddate + " " + dtime
            End If

            'lblptime.Text = pdate.Substring(0, 8).Insert(4, "/").Insert(2, "/") & "  " & pdate.Substring(pdate.Length - 8).ToLower
            'lbldtime.Text = ddatetime.Substring(0, 8).Insert(4, "/").Insert(2, "/") & "  " & ddatetime.Substring(ddatetime.Length - 8).ToLower
            lbladdress.Text = htRentalParam("PickupLocation")
            lbldaddress.Text = htRentalParam("DropoffLocation")
            If (lbladdress.Text = lbldaddress.Text) Then
                lbladdress.Visible = False
                lbldaddress.Visible = False
            End If



            End If

            ' ploc = Request.QueryString("


    End Sub

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        DisplayInfo()

    End Sub
End Class
