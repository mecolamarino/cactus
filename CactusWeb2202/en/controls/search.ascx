﻿<%@ Control Language="VB" AutoEventWireup="false" Inherits="controls_Search" CodeFile="Search.ascx.vb" %>

<style>
    .ui-datepicker {
        font-size: 85%;
    }

    .error {
        color: red;
    }

    .search_steps {
        background-color: #F0813B;
        color: #fff;
        font-size: 16pt;
        padding: 5px 10px;
    }

    input, select {
        height: 35px;
    }
</style>

<div class="clearfix"></div>

<h3>Reserve</h3>


<div class="col-xs-12 form-group">
    <div>
        <label>Pickup Location</label></div>

    <asp:Label ID="lblmsg" ForeColor="white" Font-Bold="true" runat="server"
        meta:resourcekey="lblmsg"></asp:Label>

    <asp:DropDownList ID="drppickuploc" runat="server" Visible="true" CssClass="form-control" Width="100%" ClientIDMode="Static">

        <asp:ListItem Value="" Selected="True">- Select Pick up Location -</asp:ListItem>

        <asp:ListItem Value="SLA">SALTA CENTRO - DOWNTOWN</asp:ListItem>
        <asp:ListItem Value="AEPSLA">AEROPUERTO SALTA</asp:ListItem>
        <asp:ListItem Value="MDZ">MENDOZA CENTRO - DOWNTOWN</asp:ListItem>
        <asp:ListItem Value="AEPMDZ">AEROPUERTO MENDOZA</asp:ListItem>



    </asp:DropDownList>
</div>

<div class="form-group-2 underline">

    <div class="col-xs-7 form-group">
        <label>Date</label>
        <asp:TextBox ID="txtpickupdate" runat="server" Width="100%" CssClass="txt-date form-control" placeholder="Select Pickup" autocomplete="off" ClientIDMode="Static"></asp:TextBox>
    </div>


    <div class="col-xs-5 form-group">
        <label>Time</label>
        <asp:DropDownList ID="drpPickuptime" CssClass="txt-time form-control" Width="100%" runat="server" ClientIDMode="Static">


            <asp:ListItem Value="09:00am" Text="09:00 am"></asp:ListItem>
            <asp:ListItem Value="09:30am" Text="09:30 am"></asp:ListItem>
            <asp:ListItem Value="10:00am" Text="10:00 am"></asp:ListItem>
            <asp:ListItem Value="10:30am" Text="10:30 am"></asp:ListItem>
            <asp:ListItem Value="11:00am" Text="11:00 am"></asp:ListItem>
            <asp:ListItem Value="11:30am" Text="11:30 am"></asp:ListItem>
            <asp:ListItem Value="12:00pm" Text="12:00 pm"></asp:ListItem>
            <asp:ListItem Value="12:30pm" Text="12:30 pm"></asp:ListItem>
            <asp:ListItem Value="13:00pm" Text="01:00 pm"></asp:ListItem>
            <asp:ListItem Value="13:30pm" Text="01:30 pm"></asp:ListItem>
            <asp:ListItem Value="14:00pm" Text="02:00 pm"></asp:ListItem>
            <asp:ListItem Value="14:30pm" Text="02:30 pm"></asp:ListItem>
            <asp:ListItem Value="15:00pm" Text="03:00 pm"></asp:ListItem>
            <asp:ListItem Value="15:30pm" Text="03:30 pm"></asp:ListItem>
            <asp:ListItem Value="16:00pm" Text="04:00 pm"></asp:ListItem>
            <asp:ListItem Value="16:30pm" Text="04:30 pm"></asp:ListItem>
            <asp:ListItem Value="17:00pm" Text="05:00 pm"></asp:ListItem>


        </asp:DropDownList>

    </div>

</div>

<script>
	$(function(){
		$("#check-box-hide").show();
		$("#check-box-snow").hide();
		$('#sec-loc').bind('change', function () {
		if($(this).is(':checked')) {
				$("#check-box-hide").slideUp();
				$("#check-box-snow").slideDown();
		}
		});
	});
</script>

<div class="col-xs-12 form-group" id="check-box-hide" style="display:none;">
	<label class="for-check"><input type="checkbox" id="sec-loc" /><span style="display: inline-block; margin-top: 10px;">I will return the car in a different location</span></label>
</div>

<div class="col-xs-12 form-group" id="check-box-snow">
    <label>Drop Off Location</label><br />

    <asp:DropDownList ID="drpreturnloc" runat="server" Visible="true" CssClass="form-control" Width="100%" ClientIDMode="Static">
        <asp:ListItem Value="Same" Selected="True" meta:resourcekey="ListItemResourceSame" Text="Same as Pickup"></asp:ListItem>

        <asp:ListItem Value="SLA">SALTA CENTRO - DOWNTOWN</asp:ListItem>
        <asp:ListItem Value="AEPSLA">AEROPUERTO SALTA</asp:ListItem>
        <asp:ListItem Value="MDZ">MENDOZA CENTRO - DOWNTOWN</asp:ListItem>
        <asp:ListItem Value="AEPMDZ">AEROPUERTO MENDOZA</asp:ListItem>

    </asp:DropDownList>
</div>

<div class="form-group-2">
    <div class="col-xs-7 form-group">
        <label>Date</label>
        <asp:TextBox ID="txtdropoffdate" Width="100%" runat="server" placeholder="Select Dropoff" CssClass="txt-date form-control" autocomplete="off" ClientIDMode="Static"></asp:TextBox>

    </div>

    <div class="col-xs-5 form-group">
        <label>Time</label>
        <asp:DropDownList ID="drpDropOfftime" Width="100%" CssClass="txt-time form-control" runat="server" ClientIDMode="Static"
            meta:resourcekey="drpDropOfftimeResource1">


            <asp:ListItem Value="09:00am" Text="09:00 am"></asp:ListItem>
            <asp:ListItem Value="09:30am" Text="09:30 am"></asp:ListItem>
            <asp:ListItem Value="10:00am" Text="10:00 am"></asp:ListItem>
            <asp:ListItem Value="10:30am" Text="10:30 am"></asp:ListItem>
            <asp:ListItem Value="11:00am" Text="11:00 am"></asp:ListItem>
            <asp:ListItem Value="11:30am" Text="11:30 am"></asp:ListItem>
            <asp:ListItem Value="12:00pm" Text="12:00 pm"></asp:ListItem>
            <asp:ListItem Value="12:30pm" Text="12:30 pm"></asp:ListItem>
            <asp:ListItem Value="13:00pm" Text="01:00 pm"></asp:ListItem>
            <asp:ListItem Value="13:30pm" Text="01:30 pm"></asp:ListItem>
            <asp:ListItem Value="14:00pm" Text="02:00 pm"></asp:ListItem>
            <asp:ListItem Value="14:30pm" Text="02:30 pm"></asp:ListItem>
            <asp:ListItem Value="15:00pm" Text="03:00 pm"></asp:ListItem>
            <asp:ListItem Value="15:30pm" Text="03:30 pm"></asp:ListItem>
            <asp:ListItem Value="16:00pm" Text="04:00 pm"></asp:ListItem>
            <asp:ListItem Value="16:30pm" Text="04:30 pm"></asp:ListItem>
            <asp:ListItem Value="17:00pm" Text="05:00 pm"></asp:ListItem>

        </asp:DropDownList>
    </div>
</div>

<script>
$(function(){
$("#searchDropdownBox").change(function(){
  var Search_Str = $(this).val();
  //replace search str in span value
  $("#nav-search-in-content").text(Search_Str);
});

$("#dicount-cd-btn-bx").show();
$("#discount-cd").hide();
$('#dicount-cd-btn').bind('change', function () {
if($(this).is(':checked')) {
		$("#dicount-cd-btn-bx").slideUp();
		$("#discount-cd").slideDown();
}
});
});
</script>

<div class="col-xs-12 form-group" id="dicount-cd-btn-bx" style="display:none;">
	<label class="for-check"><input type="checkbox" id="dicount-cd-btn" /><span style="display: inline-block; margin-top: 10px;">Have a Discount/Company Code?</span></label>
</div>

<div class="col-xs-12 form-group" id="discount-cd">

<div class="search_bar">
    		<span class=" nav-facade-active" id="nav-search-in">
              <span data-value="search-alias=aps" id="nav-search-in-content" style="width: 87px; overflow: visible;">
                Discount Code
              </span>
              <span class="nav-down-arrow nav-sprite"></span>
              <select title="Search in" class="searchSelect" id="searchDropdownBox" name="code_type"  style="top: -1px;">
				  <option value="Discount Code" title="Discount Code" selected="selected">Discount Code</option>
				  <option value="Company Code" title="Company Code">Company Code</option>
				  <option value="Rate Code" title="Rate Code">Rate Code</option>
			  </select>
            </span>
            <div class="nav-searchfield-outer nav-sprite">
              <asp:TextBox ID="txtdiscount" CssClass="twotabsearchtextbox" runat="server" placeholder="Optional" ClientIDMode="Static"></asp:TextBox>
            </div>
</div>

    
</div>


<div class="col-xs-12" style="margin-top: 10px;">
    <asp:Button CssClass="bnt-book-now" ID="BtnSubmit" runat="server" Text="Book Now" meta:resourcekey="BtnSubmitResource1" OnClick="BtnSubmit_Click" />
</div>

<div class="clearfix"></div>
  <div class="modal fade" tabindex="-1" role="dialog" id="noRecordPopup" data-backdrop="false">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">No Vehicles</h4>
                </div>
                <div class="modal-body">
                    <p>No Vehicles are available at this time, please adjust your search&hellip;</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default hide" data-dismiss="modal">Close</button>
                    <a  href="Default.aspx" class="btn btn-primary"  data-dismiss="modal">Modify</a>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->


  <asp:Literal ID="ltrlNoVechile" runat="server"></asp:Literal>    

