﻿<%@ Page Title="" Language="VB" MasterPageFile="MastarList.master" AutoEventWireup="false" CodeFile="modify-reservation.aspx.vb" Inherits="EditReservation" culture="auto" meta:resourcekey="PageResource1" uiculture="auto" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <title>Modify my Reservation | Mr Piper's Jeeps</title>
<meta name="keywords" content="Mr Piper's Jeeps | Rent a vehicle in St. John, Virgin Islands"/>
<meta name="description" content="View or Modify your reservation with Mr Piper's Jeeps."/>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<div class="col-sm-12" style="padding-bottom:20px; background:#fff;">
    <h1>Already have a Reservation?</h1>
	<h2>
	  <asp:Label ID="lblReservationTitleText" 
             runat="server" Text="View/Modify your Reservation" 
            meta:resourcekey="lblReservationTitleTextResource1" ></asp:Label>
	</h2>

	<p><asp:Label ID="lblReservationText" 
             runat="server" 
            Text="Please enter your confirmation number to view the reservation" 
            meta:resourcekey="lblReservationTextResource1" ></asp:Label></p>
    <div class="one_fourth first flex_column">
      <asp:Label ID="lblconfirmationtext" 
             runat="server" Text="Confirmation No" 
            meta:resourcekey="lblconfirmationtextResource1" ></asp:Label>
    </div>
    <div class="one_third flex_column">
        <asp:TextBox ID="txtcancel"  runat="server" 
            meta:resourcekey="txtcancelResource1"></asp:TextBox>
     </div>
     <div class="one_fourth first flex_column"  style="text-transform:capitalize;"><asp:Label ID="lbllastnametext" 
             runat="server" Text="Last Name" 
             meta:resourcekey="lbllastnametextResource1" ></asp:Label></div>
     <div class="one_third flex_column"><asp:TextBox ID="txtlastname" runat="server" 
             meta:resourcekey="txtlastnameResource1"></asp:TextBox></div>
    			
		 <asp:Panel ID="pnlshow" runat="server" Visible="False" 
        meta:resourcekey="pnlshowResource1">
        <div class="one_half first flex_column"><asp:Label ForeColor=Red ID="lblmessage" 
                runat="server" meta:resourcekey="lblmessageResource1"></asp:Label></div>
    </asp:Panel>   		
			
    	<div class="one_fourth first flex_column" style="height:100px;padding-top:10px;"> 
            <asp:Button runat="server"  
                CssClass="btn-next" Text="View Reservation" id="btnEdt" 
                meta:resourcekey="btnEdtResource1"></asp:Button></div>
       
      
</div>

  </asp:Content>

