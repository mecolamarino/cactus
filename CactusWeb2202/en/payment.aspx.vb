﻿Imports System.Security.Cryptography
Imports System.Configuration
Partial Class payment
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not Page.IsPostBack Then
            LoadRateDetails()
        End If
    End Sub

    Public Sub LoadRateDetails()

        If String.IsNullOrEmpty(Request.QueryString("TotalCost")) Or String.IsNullOrEmpty(Request.QueryString("email")) Or String.IsNullOrEmpty(Request.QueryString("referenceCode")) Then
            Response.Write("Invalid page, Please restart the process again.")
            responseUrl.Value = Request.Url.AbsoluteUri.Replace("/payment.aspx", "/Success.aspx")

        Else
            Dim apiKey As String = ConfigurationManager.AppSettings("APIKey").ToString()
            Dim configmerchantId As String = ConfigurationManager.AppSettings("merchantId").ToString()
            Dim configAccountId As String = ConfigurationManager.AppSettings("accountId").ToString()
            Dim testMode As String = ConfigurationManager.AppSettings("TestMode")
            Dim apiUrl = ConfigurationManager.AppSettings("APIUrl").ToString()
            Dim configResponseUrl = ConfigurationManager.AppSettings("responseUrl").ToString()
            paymentForm.Action = apiUrl
            merchantId.Value = configmerchantId
            accountId.Value = configAccountId
            test.Value = testMode
            amount.Value = Convert.ToDouble(Request.QueryString("TotalCost")).ToString("0.00")
            buyerEmail.Value = Request.QueryString("email")
            referenceCode.Value = Request.QueryString("referenceCode")

            Dim hashstring As String = ""
            hashstring = String.Format("{0}~{1}~{2}~{3}~{4}", apiKey, configmerchantId, referenceCode.Value, amount.Value, "ARS")
           Response.Write (hashstring)
	    signature.Value = GetMD5HashData(hashstring)
            responseUrl.Value = configResponseUrl
        End If


    End Sub

    Public Function GetMD5HashData(hasstring As String) As String
        Dim encyroted As String = ""
        Dim mds As MD5 = MD5.Create()

        '  //convert the input text to array of bytes
        Dim hashData As Byte() = mds.ComputeHash(Encoding.Default.GetBytes(hasstring))

        '//create new instance of StringBuilder to save hashed data
        Dim returnValue As StringBuilder = New StringBuilder()
        '//loop for each byte and add it to StringBuilder
        For index = 0 To hashData.Length - 1
            returnValue.Append(hashData(index).ToString("X2"))
        Next
        '// return hexadecimal string
        Return returnValue.ToString()
        Return encyroted
    End Function

End Class
