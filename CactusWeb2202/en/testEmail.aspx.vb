﻿Imports System.Xml.Linq
Imports System.Linq
Imports System.Net.Mail
Imports System.IO

Partial Class testEmail
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        SendEmail()
    End Sub
    Protected Sub SendEmail()
        Try
            Dim fromaddress As String = System.Configuration.ConfigurationManager.AppSettings("fromaddress")
            Dim toaddress As String = System.Configuration.ConfigurationManager.AppSettings("fromaddress")

            Dim strFrom = fromaddress
            Dim strTo = "test1groovy@gmail.com"
            Dim MailMsg As New MailMessage(New MailAddress(strFrom.Trim()), New MailAddress(strTo))
            MailMsg.BodyEncoding = Encoding.Default
            MailMsg.Subject = "Karmart Rental - Resevation Confirmation"
            'MailMsg.CC.Add()
            MailMsg.Body = "this is a test"
            MailMsg.Priority = MailPriority.High
            MailMsg.IsBodyHtml = True
            'Smtpclient to send the mail message 

            Dim SmtpMail As New SmtpClient
            Dim basicAuthenticationInfo As New System.Net.NetworkCredential("donotreply@datamine.net", "datamine2013")

            SmtpMail.Host = "mail.datamine.net"
            SmtpMail.EnableSsl = False
            SmtpMail.Port = 25
            SmtpMail.UseDefaultCredentials = False
            SmtpMail.Credentials = basicAuthenticationInfo
            SmtpMail.Send(MailMsg)

            Response.Write("Success")
        Catch ex As Exception
            Response.Write("Error")
            Response.Write(ex.ToString)
        End Try
    End Sub
End Class
