// JavaScript Document

    $(document).ready(function () {

        $('#txtpickupdate').datepicker({
            minDate: 0,
            dateFormat: 'mm/dd/yy',
            buttonImage: 'images/btn-calendar-sm.jpg',
            buttonImageOnly: true,
            beforeShowDay: function (date) {
                var day = date.getDay();
					if (day == 0) {
		 			return [false];
					} else if ($( "#drppickuploc" ).val() == "STATES" && day == 6) {
					return [false];
					} else {
					return [true];
					}
					
					
            },
			onSelect: function(selected,evnt) {
				changeDate()
			}
        });
        //  $('#txtpickupdate').datepicker({ beforeShowDay: $.datepicker.noSunday });

        $('#txtdropoffdate').datepicker({
            minDate: 0,
            dateFormat: 'mm/dd/yy',
            buttonImage: 'images/btn-calendar-sm.jpg',
            buttonImageOnly: true,
            beforeShowDay: function (date) {
			
			var drpLoc = $( "#drpreturnloc" ).val()
				if (drpLoc == "Same") {
					drpLoc = $( "#drppickuploc" ).val()
				}
				
                var day = date.getDay();
					if (day == 0) {
		 			return [false] ;
					} else if (drpLoc == "STATES" && day == 6) {
					return [false];
					} else {
					return [true] ;
					}
				},
			onSelect: function (selected,evnt) {
			}
        });

            $('.txtDateOfbirth').datepicker();
			
			
			
			$( "#drpPickuptime" ).change(function(){
				$(	"#drpDropOfftime" ).val($( "#drpPickuptime option:selected" ).val())
			})
        });
		
		// TIME FIELDS
		
		function changeDate() {
                var date2 = $('#txtpickupdate').datepicker('getDate', '+1d');
                var day = date2.getDay();

                date2.setDate(date2.getDate() + 1);
                $('#txtdropoffdate').datepicker('setDate', date2);

        }