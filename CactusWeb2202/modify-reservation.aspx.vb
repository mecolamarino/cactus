﻿Imports System.Xml
Imports System.Xml.Serialization
Imports System.Linq
Imports System.Xml.Linq
Imports System.Threading
Imports System.Globalization
Partial Class EditReservation
    Inherits System.Web.UI.Page
  
    Protected Overrides Sub InitializeCulture()
        Culture = Common.MyCulture

        'set culture to current thread
        Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(Common.MyCulture)
        Thread.CurrentThread.CurrentUICulture = New CultureInfo(Common.MyCulture)

        'call base class
        MyBase.InitializeCulture()
    End Sub
   
    Protected Sub btnEdt_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdt.Click
        Me.pnlshow.Visible = True
        If txtcancel.Text = "" Then
            lblmessage.Text = "Please enter your confirmation number"
            Return
        End If
        Response.Redirect("EditRes.aspx?con=" + txtcancel.Text + "&last=" & Me.txtlastname.Text)
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Request.QueryString("err") Is Nothing Then
        Else
            pnlshow.Visible = True
            If Request.QueryString("err") = "1" Then
                lblmessage.Text = "The confirmation number is invalid!"
            ElseIf Request.QueryString("err") = "2" Then
                lblmessage.Text = "This reservation was cancelled already!"
            ElseIf Request.QueryString("err") = "3" Then
                lblmessage.Text = "Your reservation has been modified. Please enter your confirmation number and click on modify button to view or modify your  reservation"
            ElseIf Request.QueryString("err") = "4" Then
                lblmessage.Text = "Invalid confirmation number/Last Name"
            End If
        End If
    End Sub




End Class