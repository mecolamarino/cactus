﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="payment.aspx.vb" Inherits="payment" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script src="http://www.datamine.net/development/cactus/js/jquery-2.1.4.min.js"></script>

</head>
<body>
    <form method="post" action="https://gateway.payulatam.com/ppp-web-gateway/" id="paymentForm" runat="server" style="display: none">
        <asp:HiddenField ID="merchantId" Value="532760" runat="server" />
        <asp:HiddenField ID="accountId" Value="534691" runat="server" />
        <asp:HiddenField ID="description" Value="Sedan 4 doors Chevrolet Classic" runat="server" />
        <asp:HiddenField ID="referenceCode" Value="" runat="server" />
        <asp:HiddenField ID="amount" Value="3" runat="server" />
        <asp:HiddenField ID="tax" Value="0" runat="server" />
        <asp:HiddenField ID="taxReturnBase" Value="0" runat="server" />
        <asp:HiddenField ID="currency" Value="ARS" runat="server" />
        <asp:HiddenField ID="signature" Value="" runat="server" />
        <asp:HiddenField ID="test" Value="0" runat="server" />
        <asp:HiddenField ID="buyerEmail" Value="" runat="server" />
        <asp:HiddenField ID="responseUrl" Value="" runat="server" />
        <asp:HiddenField ID="confirmationUrl" Value="" runat="server" />
        <input name="Submit" type="submit" value="Pay Now" />
    </form>
    <script type="text/javascript">


        $(document).ready(function () {
            $("#paymentForm").submit();
        });


    </script>
</body>

</html>
