<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Reserve your Car - Ohana Rent a Car</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
<link href="css/global.css" rel="stylesheet" type="text/css">
<link href="css/responsive.css" rel="stylesheet" type="text/css">
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
</head>
<body>

<!--#include file="include/header.asp"-->
<!--end of header-->


<section id="content">
<div class="container">


<section id="step">


<div class="col-xs-3 inactive"><span class="num">1</span> <span>Pick Date / time</span></div>
<div class="col-xs-3 active"><span class="num">2</span> <span>Select your vehicle</span></div>
<div class="col-xs-3 inactive"><span class="num">3</span> <span>Choose  options</span></div>
<div class="col-xs-3 inactive"><span class="num">4</span> <span>Place Reservation</span></div>

<div class="clearfix"></div>
</section>
<!--end of steps-->

<section id="body">
<div class="row">

<div class="col-md-8">
<article class="vehicle">

<div class="row">
<div class="col-md-5 col-sm-6">
<h2>Hyundai Accent or similar</h2>
<div class="type">Economy</div>
<img src="images/trace-vehicle.jpg" alt="">
</div>
<div class="col-md-4 col-sm-6 price">
<div class="daily">$73.63  Daily</div>
<div class="total">$301.81  Total</div> 
</div>
<div class="col-md-3">
<a href="#" class="link-book">Book NOW</a>
</div>
</div>



</article>
</div>
<!--end of all vehicle listing-->

<div class="col-md-4">
<article class="reserve">
<h2>Your itinerary</h2>
<h3>Pick-up</h3>
<div class="desc">Kihei, Hawaii</div>
<div class="desc">August 1, 2015 at 12:00 PM</div>
<br>
<br>
<h3>Drop-off</h3>
<div class="desc">Kihei, Hawaii</div>
<div class="desc">August 4, 2015 at 12:00 PM</div>

<div class="prev">
<div class="row">
<div class="col-sm-5">Already have a reservation?</div>
<div class="col-sm-7"><a href="#" class="link-cancel">cancel</a><a href="#" class="link-modify">Modify</a></div>
</div>
</div>
</article>
</div>
<!--end of reserve box-->

</div>
</section>
<!--end of body content-->


</div>
</section>
<!--end of main centered content-->


<!--#include file="include/footer.asp"-->
<!--end of footer area-->

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script> 
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
</body>
</html>