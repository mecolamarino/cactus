jQuery(document).ready(function($) {
  $(".main-slider").owlCarousel({
    autoPlay: 3000, //Set AutoPlay to 3 seconds
    pagination: true,
    singleItem : true,
    transitionStyle : "fade",
    });
  $(".testimonial-slide").owlCarousel({
    autoPlay: 3000, //Set AutoPlay to 3 seconds
    pagination: false,
    singleItem : true,
    transitionStyle : "fade",
    });
  var owl = $(".testimonial-slide");
  $(".bnt-next").click(function(){
    owl.trigger('owl.next');
  })
  $(".bnt-prev").click(function(){
    owl.trigger('owl.prev');
  })
  $(".logo-slider").owlCarousel({
    autoPlay: 3000, //Set AutoPlay to 3 seconds
    pagination: false,
    transitionStyle : "fade",
    itemsDesktop : [1920,6], //5 items between 1000px and 901px
    itemsDesktopSmall : [991,4], // betweem 900px and 601px
    itemsTablet: [767,4],
    itemsMobile: [570,2]
  });
});


jQuery(document).ready(function($) {
  var width = $(window).width();
  if (width < 992) {
    $(".feature").addClass("arrow_box_light");
  };
  if (width < 768) {
    $(".client-2").addClass("arrow_box_dark");
    $(".client-2").addClass("bg-dark");
  };
});

jQuery(document).ready(function($) {
  $("#location_map").gMap({
    maptype: google.maps.MapTypeId.ROADMAP,
    zoom: 14,
    markers:
      [
        {
          latitude: -33.905485,
          longitude: 151.169131,
          html: "<strong>Wedding Party</strong><br/>Forine Restaurant<br/>Rose Street ST08<br/><strong>GPS:</strong> -33.905485, 151.169131",
          popup: true,
        }
      ],
    panControl: true,
    zoomControl: true,
    mapTypeControl: true,
    scaleControl: true,
    streetViewControl: true,
    scrollwheel: false,
    styles: [ { "stylers": [ { "hue": "#bb5844" }, { "gamma": 1 }, { "saturation": -60 } ] } ],
    onComplete: function() {
    // Resize and re-center the map on window resize event
    var gmap = $("#location_map").data('gmap').gmap;
    window.onresize = function(){
      google.maps.event.trigger(gmap, 'resize');
        $("#location_map").gMap('fixAfterResize');
      };
    }
  });
});
